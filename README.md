# OVERVIEW

This bootstrapped WordPress installation is similar in concept and execution to how WP Roots/Soil works: a modified index.php `.app/assets/index.php` and wp-config.php `app/assets/wpxboostrap.php` files that configures WordPress as a sub-directory installation. This a composer-based project, which is largely used to manage dependencies (plugins, etc.) for the site. If there's a desire to switch over to a different deployment system like Deployer or Capistrano, this project can be easily re-configured to suite.

While the core theme uses conventional php in its templates, there are other components of the site that use Twig. This dependency is managed through composer, and is used in the custom plugins for the site.

# ESSENTIALS
Aside from the `theme` and `plugins` directories, There are only a couple directories in this repository that are fundamental to the operation of this site:

- `src` This directory contains several classes and libraries used in parts of the site like the weather widget;
- `build` This directory contains the core assets for the site, theme, mu-plugins, plugins;
- `app` This is where the bootstrapped application file resides.

As noted in earlier, these can be renamed/moved etc., if/when switching over to a different deployment system. 

Other directories in this project, e.g. `.app` contain bash scripts and assets used in composer's install/update process. Specifically the files, `.app/assets/index.php` and `.app/assets/wpxbootstrap.php`

**NOTE:** Some directories have been re-mapped (and redirected, see the .htaccess on production). Specifically, `wp-content` is re-mapped to `resources`. Other sub-directories (and some files) are syminks, e.g. `resources/uploads` exists outside of the public directory `DOCUMENT_ROOT/uploads` --> `public_html/resources/uploads`.

# CONFIGURATION
Have a look at the production version of the `.env` file for an overview of what's required in this installation.

By default, Plugin/Theme editing is disabled along with the ability to upload plugins through WP's manager. To toggle these features, update the following variables in .env file:

- `WP_DISALLOW_FILE_EDIT` The application bootstrap maps this to `DISALLOW_FILE_EDIT`
- `WP_DISALLOW_FILE_MODS` The application bootstrap maps this to `DISALLOW_FILE_MODS`

The `.env` file also contains the database connection credentials along with the All Weather API key and Location ID used for the weather widget, and some cache-related settings.

# PLUGINS
We inherited one component of this site, which is somewhat problematic:

The original site developers (not TWA) provided a highly customized version of WP Bakery that cannot be conventionally upgraded through WP plugin manager. These customizations are tightly coupled with the theme. This is why the plugin is bundled with this project, `build/plugins/js_composer`. Don't try to update this plugin. Bad things happen. You've been warned. 

**IMPORTANT:** If you accidentally update `WP Bakery` through WP's plugin manager, the process throws an unhandled exception and will most definitely prevent the site from loading correctly. This problem existed prior to TWA taking over the site. To rectify this error, run `composer install` to restore `WP Bakery`.

**Note:** This project has been updated to remove licensed dependencies (plugins) such as `Yoast Premium`, `Gravity Forms`, and `WP Bakery Ultimate Add-ons`. Re-add these dependencies with new licensed copies, or install those plugins manually *before attempting to update the production site*.

**OPTION TREE:** This is another plugin we inherited with the site, and is coupled with parts of the theme. It is further utilized with the site's global notification component. This is a banner that appears site-wide; its content is defined in the [Option Tree](https://tyax.com/wp/wp-admin/admin.php?page=ot-settings) plugin.

**SENDGRID:** For all of WP's main-related functions.

**TWA WEATHER FEED:** This is a plugin that consumes an AlLWeather.com feed. Currently uses the free plan, so caching is essential to keeping the number of requests low.
