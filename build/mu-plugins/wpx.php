<?php

/**
 * @package WPX
 */
/*
Plugin Name: WPX Bootstrap
Plugin URI: https://rasberryswwwirl.com/
Description: This a bootstrap plugin for the Raspberry WPX framework to override parts of the WordPress core relating to templating and data access.
Version: 0.0.1
Author: RASPBERRYSWWWIRL
Author URI: https://rasberryswwwirl.com
License: GPLv2 or later
Text Domain: rasberryswwwirl
*/

/*
Copyright 2018-2019 Chris Murphy.
*/

use \Exception as Exception;
use WPX\WPAPP;
use AUX\Utils\Log;
use AUX\Utils\Debug;


function wpx_admin_start_session() {
    $inst = WPAPP::inst();
    $session = $inst->initSessions();
    if( $session ) {
        $session->set('wp_admin_logged_in', is_user_logged_in());
    }
}

function wpx_admin_end_session() {
    $inst = WPAPP::inst();
    $session = $inst->session();
    $inst->SessionEnd();
}

add_action('init', 'wpx_admin_start_session',1);
add_action('wp_login', 'wpx_admin_end_session');
add_action('wp_logout', 'wpx_admin_end_session');

/**
 * Capture the WP_Query object for use for front-end features.
 */
function wpx_init() {

    try {
        global $wp_query;
        WPAPP::WPAPI()->WPXQuery($wp_query);
    } catch( \Exception $e ) {
        if( defined('WP_DEBUG') && WP_DEBUG ) {
            echo $e->getMessage();
        }
    }

}

// WordPress is Ready, capture the WP_Query
add_action( 'wp', 'wpx_init');

function wpadmin_filter($url, $path, $orig_scheme) {
    $old = array("/(wp-admin)/");
    $admin_dir = WP_ADMIN_DIR;
    $new = array($admin_dir);
    return preg_replace($old, $new, $url, 1);
}

// Disable update notifications on a per-plugin basis
function wpx_disable_plugin_update_notifications($value)
{
    $plugin_list = []; // Plugin name
    if (isset($value) && is_object($value)) {
        foreach ($plugin_list as $plugin) {
            if (isset($value->response[$plugin])) {
                unset($value->response[$plugin]);
            }
        }
    }
    return $value;
}

// add_filter('site_transient_update_plugins', 'wpx_disable_plugin_update_notifications');

// Disable auto-update email notifications for plugins & themes.
add_filter( 'auto_plugin_update_send_email', '__return_false' );
add_filter( 'auto_theme_update_send_email', '__return_false' );

// add_filter('site_url', 'wpadmin_filter', 10, 3);