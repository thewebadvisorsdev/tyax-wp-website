<?php

namespace AW;

use APP\App;
use AUX\Utils\Log;
use stdClass;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Cache\ItemInterface;
use WPX\Enums\EnumReportType;
use WPX\TWAWeather_Plugin;
use WPX\WPAPP;

class Report {
    protected $report_data = [];
    protected $data_sources = [];
    protected $location_id;
    protected $location_coords;
    protected $cache_path;
    protected $service_provider = null;

    public function __construct(array $endpoints, $location_id = null, array $lat_lon = null) {
        $this->data_sources      = $endpoints;
        $this->location_id       = $location_id;
        $this->location_coords   = $lat_lon;

        $this->service_provider = getenv('WEATHER_SERVICE');
    }

    public function render() {     
        foreach( $this->data_sources as $type => $endpoint ) {
            $this->fetch($type, $endpoint);
        }
        
        $output = null;
        $context = [
            EnumReportType::CURRENT   => $this->getWeatherDataByType( EnumReportType::CURRENT ),
            EnumReportType::FORECAST  => $this->getWeatherDataByType( EnumReportType::FORECAST ),
        ];

        $APP            = App::inst();
        $twig           = $APP->twig();
        $template       = 'WeatherDisplay.html.twig';
        $twig_template  = $twig->load($template);
        $c = $context[EnumReportType::CURRENT];

        // Update the L
        $userTimezone   = new \DateTimeZone('America/Vancouver');
        $gmtTimezone    = new \DateTimeZone('PDT');
        $myDateTime     = new \DateTime(date('Y-m-d H:i:s', $c->EpochTime), $gmtTimezone);        
        $offset         = $userTimezone->getOffset($myDateTime);
        $myInterval     = \DateInterval::createFromDateString((string)$offset . 'seconds');
        
        $myDateTime->add($myInterval);
        $result         = $myDateTime->format('Y-m-d H:i:s');

        $c->LocalObservationDateTime = $myDateTime;
        $c->LocalTime                = $result;
        $context[EnumReportType::CURRENT] = $c;

        $output         = $twig_template->render($context);

        return $output;
    }

    protected function getWeatherDataByType( $type ) {
        $data = (array_key_exists($type, $this->report_data)) ? $this->report_data[$type] ?? null : null;

        // CURRENT REPORT
        $data = $this->convertArrayToObject( $data );
        

        // FORECASTS
        if( $type == EnumReportType::FORECAST ) {
            $forecasts              = (array) $data->DailyForecasts;
            $forecast_current       = array_shift($forecasts);
            $data->CurrentForecast  = $forecast_current;
            $data->DailyForecasts   = $forecasts;
        }

        return $data;
    }

    protected function convertArrayToObject( $array ) {
        if ( !is_array( $array ) && !is_object( $array ) ) {
            return $array;
        }
        $obj = new stdClass();
        if( ( is_array( $array ) || is_object( $array ) ) ) {
            if( is_array( $array ) ) {
                if( count( $array ) > 0 ) {
                    foreach( $array as $k=>$v ) {
                        $obj->$k = $this->convertArrayToObject( $v );
                    }
                }
            }
            return $obj;
        } else {
            return false;
        }
    }

    /**
     * Fetch a weather report from accuweather.com
     * 
     * @example current conditions: http://dataservice.accuweather.com/currentconditions/v1/52807?apikey=HJGwjGNjKcsqWNqzFVxdWUc62PS9ISpa&details=true
     * @example 5-day forecast: http://dataservice.accuweather.com/currentconditions/v1/52807?apikey=HJGwjGNjKcsqWNqzFVxdWUc62PS9ISpa&details=true
     *
     * @param string $type
     * @param string $endpoint
     * @return Array
     */
    protected function fetch( $type, $endpoint ) {
        $api_key            = getenv('AW_APIKEY') ?? null;
        $endpoint           = sprintf("%s/%s", $endpoint, $this->location_id);
        
        $this->cache_path   = WP_CACHE_DIR;
        $client             = HttpClient::create();

        if( null !== $api_key && null != $this->location_id && ($this->cache_path && file_exists($this->cache_path)) ) {
            $location_id        = $this->location_id;
            $cache_key          = $location_id ? md5( sprintf("%s_%s", $location_id,$endpoint) ) : md5( sprintf("%s_%s", implode(",",$this->getLocationData()),$endpoint) );
            $cache_lifetime     = (int)getenv('WEATHER_CACHE_LIFETIME') ?? 0;
            $cache_namespace    = ($pos = strrpos(__CLASS__, '\\')) ? substr(__CLASS__, $pos + 1) : __CLASS__;
            $cache              = new FilesystemAdapter($cache_namespace,$cache_lifetime,rtrim(App::config('cache_location'), DIRECTORY_SEPARATOR));

            $query_string = [
                'apikey'    => $api_key,
                'details'   => 'true',
            ];

            // Specify Metric units
            if( $type == EnumReportType::FORECAST ) {
                $query_string = array_merge($query_string, [
                    'metric'   => 'true',
                ]);
            }

            // Log::trace(__METHOD__, $endpoint, $api_key, $query_string);

            // Cache the response
            $response   = $cache->get($cache_key,function( ItemInterface $item ) use($client, $endpoint, $query_string, $cache_lifetime){
                $item->expiresAfter($cache_lifetime);
                $r = null;
                try {
                    $r = $client->request('GET',$endpoint,[
                        'query' =>  $query_string
                    ]);
                }catch(\Exception $e){
                    Log::log( sprintf("%s \n %s",$e->getMessage(), $e->getTraceAsString()), Log::LOG_TYPE_ERROR );
                }
                return ( $r->getStatusCode() == Response::HTTP_OK  ) ? $r->getContent() : null;
            });
            
            $response_json = !!$response ? json_decode($response, true) : null;
            if ($type == EnumReportType::CURRENT) {
                $this->report_data[ $type ] = is_array($response_json) ? current($response_json) : $response_json;
            }else{
                $this->report_data[ $type ] = $response_json;
            }
            // Log::trace(__METHOD__ . ' CACHED REQUEST', $query_string, $endpoint, $this->report_data[ $type ]);

            return $this->report_data[ $type ];
        }else{
            // Fallback to XML
            Log::log("API key is invalid or weather data unavailable.", Log::LOG_TYPE_ERROR, true );
        }
        return null;
    }    
}