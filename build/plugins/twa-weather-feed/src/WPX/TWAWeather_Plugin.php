<?php

namespace WPX;

use APP\App;
use AUX\Utils\Log;
use AUX\Registry;
use AW\Report;
/**
 * @author The Web Advisors
 * @version 1.0.0
 * 
 * @see https://openweathermap.org/current
 * @example api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
 */
class TWAWeather_Plugin {

    const CACHE_LIFETIME        = 1;

    protected $WPX_PLUGIN_DIR;
    protected $WPX_PLUGIN_URL;
    protected $WPX_PLUGIN_TEMPLATE_DIR;
    protected static $REGISTRY;
    protected static $instance_twig;
    protected static $environment;
    protected static $endpoints = [
        'onecall'       => 'https://api.openweathermap.org/data/2.5/onecall',//?lat={lat}&lon={lon}&exclude={part}&appid={API key}',
        'owm_current'   => 'https://api.openweathermap.org/data/2.5/weather',
        'pwm_forecast'  => 'https://pro.openweathermap.org/data/2.5/forecast/hourly',
        'current'       => 'http://dataservice.accuweather.com/currentconditions/v1',
        'forecast'      => 'http://dataservice.accuweather.com/forecasts/v1/daily/5day',
    ];

    protected $resolver;
    protected $admin_pages = [];
    protected $parent_slug;

    protected $requirements = [];

    // ---------------------------------------------------------- //
    // REGISTRY
    // ---------------------------------------------------------- //
    /**
     * A scope-safe registry for common configuration options and variables
     */
    public static function Registry() {
        
        if( null == static::$REGISTRY ) {
            static::$REGISTRY = new Registry();
        }

        return static::$REGISTRY;
    }

    // ---------------------------------------------------------- //
    // CONSTRUCTION / INITIALIZATION
    // ---------------------------------------------------------- //
    public function __construct() {
        $this->init();
    }

    protected function init() {

        self::$environment  = getenv('WP_ENVIRONMENT') ?: 'production';

        // Enqueue and scripts and stylesheets
        add_action( 'admin_enqueue_scripts', array($this,'scripts') );
        add_action( 'admin_enqueue_scripts', array($this,'styles') );

        // Template paths
        $this->Twig();   
    }

    public function owm_get_weather_reports() {
        $weather_report = new Report([
            'current'   => self::$endpoints['current'],
            'forecast'  => self::$endpoints['forecast']
        ], ...func_get_args());
        return $weather_report->render();
    }

    // ---------------------------------------------------------- //
    // WORDPRESS HOOK CALLBACKS
    // ---------------------------------------------------------- //
    /**
     * Run the plugin. 
     * 
     * Currently Disabled: Pre-caches the parsed XML if things go wrong with
     * an on-demand parse via TWAWeather_Plugin::getSchedule
     * 
     * @see TWAWeather_Plugin::getSchedule
     */
    public function run() {
        try {
        }catch( \Exception $e ){
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }
    }

    /**
     * Enqueue Scripts
     */
    public function scripts() {
        if( empty( $this->requirements ) ) return false;
        array_walk($this->requirements, function($asset, $asset_key){
            $this->enqueue($asset_key, $asset);
        });
    }
    
    /**
     * Enqueue Stylesheets
     */
    public function styles() {
        if( !is_admin() ) {
            // $reg = self::Registry();
            // // $file       = $reg->WPX_PLUGIN_DIR . '/assets/css/twa_openweather_styles.css';
            // // $asset      = $reg->WPX_PLUGIN_URL . '/assets/css/twa_openweather_styles.css';
            // // $version    = filemtime( $file );
            // // wp_register_style( 'wpx-admin-styles', $asset, array(), "$version", 'all' );
            // // wp_enqueue_style( 'wpx-admin-styles' );
        }
    }

    /**
     * Twig instance
     */
    public function Twig() {

        if (null == self::$instance_twig) {

            try {
                $reg            = self::Registry();
                $twig           = App::inst()->Twig();
                $template_root  = $reg->WPX_PLUGIN_TEMPLATE_DIR;
                $loader         = $twig->getLoader();

                // Theme Templates
                $loader->addPath($template_root . DIRECTORY_SEPARATOR . 'Includes');
                $loader->addPath($template_root . DIRECTORY_SEPARATOR . 'Layout');
                
            } catch( \Exception $e ) {
                Log::log($e->getMessage(), Log::LOG_TYPE_ERROR, true);
            }

        }

        return self::$instance_twig;
    }    

    /**
     * Callback for WordPress' `register_activation_hook`. 
     * This method is responsible for performing any database table transformations
     * including adding new tables.
     */
    public function activate() {
        Log::log( __CLASS__ . " was activated." );
    }

    /**
     * Callback for WordPress' `register_deactivation_hook`
     */
    public function deactivate() {
        Log::log( __CLASS__ . " was deactivated." );
    }

    // ---------------------------------------------------------- //
    // WP ENQUEUE
    // ---------------------------------------------------------- //
    /**
     * Enqueue scripts for use in the admin
     * 
     * @param String $asset_key A unique key to identify the asset
     * @param String $asset     The asset to enqueue
     * @return ModelAdminBooks_Plugin  Returns the current instance for fluid method chaining.
     */
    protected function enqueue( $asset_key, $asset, $base_url = WP_CONTENT_URL, $base_path = WP_CONTENT_DIR ) {
        $file       = sprintf("%s%s", $base_path , $asset );
        $script     = sprintf("%s%s", $base_url , $asset );
        $version    = filemtime( $file );
        wp_enqueue_script( sprintf("wpxfids-%s",$asset_key) , $script, array('jquery'), "$version", true);
        return $this;
    }

    /**
     * Utility method to convert directory separator to the correct system separator
     * 
     * @return String The sparator defined in `DIRECTORY_SEPARATOR`
     */
    private static function DS($path) {
        $ds = DIRECTORY_SEPARATOR;
        return str_replace(array("/","\\"),$ds,$path);
    }
}