<?php

namespace WPX\Enums;

use ReflectionClass;

abstract class EnumReportType extends EnumBase {
    const CURRENT           = 'current';
    const FORECAST          = 'forecast';
}