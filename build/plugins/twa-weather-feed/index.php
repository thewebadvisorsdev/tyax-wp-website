<?php
/**
 * Model Admin: Weather
 *
 * A thin consumer for the openweather.com api
 *
 * @link              https://thewebadvisors.ca
 * @since             1.0.0
 * @package           TWA
 *
 * @wordpress-plugin
 * Plugin Name:       TWA - Weather Feed
 * Plugin URI:        https://thewebadvisors.ca
 * Description:       This is a slim implementation for reading and rendering weather information.
 * Version:           1.0.0
 * Author:            The Web Avisors
 * Author URI:        https://thewebadvisors.ca
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

use AUX\Utils\Log;
use AUX\Utils\Debug;
use AUX\Registry;
use WPX\TWAWeather_Plugin;

// ---------------------------------------------------------- //
// COMPOSER AUTOLOADER
// ---------------------------------------------------------- //
global $autoload;
if( $autoload ) {
    $autoload->addPsr4('AW\\', plugin_dir_path( __FILE__ ) . 'src/AW');
    $autoload->addPsr4('WPX\\', plugin_dir_path( __FILE__ ) . 'src/WPX');
}

if( !is_admin() ) {
    
    // ---------------------------------------------------------- //
    // REGISTRY
    // ---------------------------------------------------------- //
    $reg = TWAWeather_Plugin::Registry();
    
    $reg->WPX_PLUGIN_FILE           = __FILE__;
    $reg->WPX_ADMIN_URL             = get_admin_url();
    $reg->WPX_PLUGIN_DIR            = plugin_dir_path( __FILE__ );
    $reg->WPX_PLUGIN_URL            = plugin_dir_url( __FILE__ );
    $reg->WPX_PLUGIN_TEMPLATE_DIR   = $reg->get('WPX_PLUGIN_DIR') . 'assets/templates';
    
    // ---------------------------------------------------------- //
    // New plugin instance
    // ---------------------------------------------------------- //  
    $TWAWeather = new TWAWeather_Plugin();
    
    // ---------------------------------------------------------- //
    // template functions
    // ---------------------------------------------------------- //
    function owm_get_weather_reports() {
        global $TWAWeather;
        return call_user_func_array([$TWAWeather, __FUNCTION__], func_get_args());
    }
    
    // ---------------------------------------------------------- //
    // Activation/Deactivation        
    // ---------------------------------------------------------- //
    register_activation_hook( $reg->WPX_PLUGIN_FILE, array($TWAWeather,'activate') );
    register_deactivation_hook( $reg->WPX_PLUGIN_FILE, array($TWAWeather,'deactivate') );
    
    // ---------------------------------------------------------- //
    // RUN
    // ---------------------------------------------------------- //
    add_action( 'plugins_loaded', array($TWAWeather, 'run') );
}
