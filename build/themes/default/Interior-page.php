<?php
/**

Template Name: Interior Page Template

 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-banner">
<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
			} else { ?>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />
			<?php } ?>
	<div class="innerbanner-holder"><div class="container"><h1><?php the_title(); ?></h1></div></div>
</div>
<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();?>


        	<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>
        	
		
        	
        	

					<div class="container">
						
						

						<?php the_content(); ?>

					</div>

				</section>
			<?php endwhile;
		else :
			get_template_part( 'template-parts/post/content', 'none' );
		endif; ?>


<?php get_footer();
