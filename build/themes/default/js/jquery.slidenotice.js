(function($){

    var toggle = $('.component.ui-slidenotice__toggle');
    var component;

    $('.component.ui-slidenotice').livequery(function(){
        component   = $( this );
        toggle      = $('.component.ui-slidenotice__toggle');
        component.data('state', 1);
        
        toggle.on('click', function(){
            var state   = component.data('state');
            component.toggleClass('ui-slidenotice_active', ( state ));
            toggle.toggleClass('ui-slidenotice_active', ( state ));
            component.data('state', !state);
        });

        toggle.trigger('click');
    });

	/**
	 * Scroll handler to toggle the `Scroll to Top` button.
	 */
	if('debounce' in $ && typeof $.debounce === 'function') {        
        // Heights
        var obj             = $('.component.ui-slidenoticecontainer');
		var headers 		= $('.tybanner');
		var header 			= headers.eq(0);
		var headerh 		= null;
		var headeroffset	= 30;
        var debouncerate	= (4 * 2);
        var scrollTarget    = $('html,body');

		if( (null !== obj && obj.length) && ( null !== toggle && toggle.length ) ) {
            
            $(window).on('scroll', $.debounce(function(e){
                var win = $( this );
                headerh = headerh || header.height();
                
				var h 	= headerh || win.prop('innerHeight');
                var n 	= win.scrollTop();
                var evt = (n >= (h  - headeroffset) ) ? 'WindowScrollOut' : 'WindowScrollIn';

                if( n >= (h * .5) ) {
                    component.removeClass('ui-slidenotice_active');
                    toggle.removeClass('ui-slidenotice_active');
                    component.data('state', false);
                }else{
                    component.addClass('ui-slidenotice_active');
                    toggle.addClass('ui-slidenotice_active');
                    component.data('state', true);
                }

			},debouncerate)).trigger('scroll');
        }       
    }   

})(jQuery);