<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-banner">
<?php if ( have_posts() ) : ?>
<?php if ( has_post_thumbnail() ) {

			the_post_thumbnail();

			} else { ?>

			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />

			<?php } ?>

			<div class="innerbanner-holder"><div class="container"><?php
				the_archive_title( '<h1 class="">', '</h1>' );
				the_archive_description( '<p class="taxonomy-description">', '</p>' );
			?></div></div>

</div>
<?php endif; ?>





        	<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>

					<div class="container">
						<?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>
					</div>
				</section>

	

<?php get_footer();
