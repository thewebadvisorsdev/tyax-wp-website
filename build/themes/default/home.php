<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-banner">
    <?php if ( has_post_thumbnail() ) {
	    the_post_thumbnail();
    } else {
	    ?>
	    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />
	<?php } ?>
	<div class="innerbanner-holder">
		<div class="container">
			<h1><?php single_post_title(); ?></h1>
		</div>
	</div>
</div>

<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>
	<div class="container" style="padding:0;">
		<h2>
			Latest News From Tyax Lodge &amp; Heliskiing
		</h2>
	</div>
</section>

<?php if ( have_posts() ) :
    while ( have_posts() ) : the_post();?>
        <section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae archive-all ' ); ?>>
        	<div class="container archive-list">
				<div class="archive-thumb">
					<?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
					<?php endif; ?>
				</div>
				<div class="archive-content">
					<p class="archive-title"><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title();?></a></p>
					<div class="archive-excerpt"><p><?php the_excerpt();?></p></div>
					<p class="archive-cat" style="font-size: 0.7em !important; padding-top: 2em;">Category: <?php the_category(' / '); ?></p>
				</div>
			</div>
		</section>
	<?php endwhile;
	else :
	    get_template_part( 'template-parts/post/content', 'none' );
    endif;
?>
<section class="archive-page">
	<div class="container">
		<?php the_posts_pagination(); ?>
	</div>
</section>

<?php
get_footer();
