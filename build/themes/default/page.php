<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-banner">

<?php if ( has_post_thumbnail() ) {

			the_post_thumbnail();

			} else { ?>

			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />

			<?php } ?>

			<div class="innerbanner-holder"><div class="container"><h1><?php the_title(); ?></h1></div></div>

</div>


			<?php
			while ( have_posts() ) : the_post();?>

        	<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>

					<div class="container">
						<?php the_content(); ?>
					</div>
				</section>

			<?php endwhile; // End of the loop. ?>



<?php get_footer();
