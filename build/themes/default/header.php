<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php 
    $env = getenv('WP_ENVIRONMENT') ?: null;
    if( 'production' == $env ) {
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PDRH37J');</script>
<!-- End Google Tag Manager -->
<?php } ?>
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

<link rel="icon" href="data:;base64,iVBORw0KGgo=">
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php 
    $env = getenv('WP_ENVIRONMENT') ?: null;
    if( 'production' == $env ) {
?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDRH37J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php } ?>

<div id="wrapper">
	<header id="header">
		<div class="container">
			<nav id="nav" class="open-close">
				<a href="#" class="opener"><span></span></a>
				<div class="drop">
					
					<?php if ( is_active_sidebar( 'secondary-top-menu' ) ) : ?>
	
						<?php dynamic_sidebar( 'secondary-top-menu' ); ?>
	
					<?php endif; ?>
					
				</div>
			</nav>
			<div class="main-nav-area">
					<div class="logo-area">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php $optionsdata = get_option( 'option_tree' ); echo $optionsdata['upload_logo'];?>" alt="TYAX"></a>
					</div>
					<?php wp_nav_menu( array(
					'theme_location' => 'top',
					'menu_id'        => 'top-menu',
				) ); ?>
			</div>
			
			<div class="search-form-wrapper">
				<form class="search-form" id="" method="get" action="/">
					<div class="input-group">
						<input type="text" name="s" class="search form-control" placeholder="Search">
						<span class="input-group-addon search-close" id="basic-addon2"><i class="fa fa-window-close" aria-hidden="true"></i></span>
					</div>
				</form>
			</div>
			
		</div>
	</header><!-- #masthead -->

<main id="main" role="main">
