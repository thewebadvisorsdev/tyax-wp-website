<?php



/**



 * The template for displaying the footer



 *



 * Contains the closing of the #content div and all content after.



 *



 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials



 *



 * @package WordPress



 * @subpackage Twenty_Seventeen



 * @since 1.0



 * @version 1.2



 */

?>

<?php /*?><?php if (is_front_page()) : ?>

<?php else : ?>
<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>

			<div class="container">
				<?php echo do_shortcode( '[lc-list-slider-basic]' ); ?>
			</div>
</section>
<?php endif; ?><?php */?>	
		
		
		

		<section class="sect-newsletter-main">
			<div class="container">
				<?php
					if ( is_active_sidebar( 'bottom-footer-newsletter' ) ) { ?>
							<div class="sect-newsletter">
							  <?php dynamic_sidebar( 'bottom-footer-newsletter' ); ?>
							</div>
				<?php } ?>
			</div>
		</section>


		<section class="sect-cta-main">
			<div class="container">
				<?php
					if ( is_active_sidebar( 'bottom-footer-cta' ) ) { ?>
							<div class="sect-cta">
							  <?php dynamic_sidebar( 'bottom-footer-cta' ); ?>
							</div>
				<?php } ?>
			</div>
		</section>

		</main><!-- #main -->



		<footer id="colophon" class="site-footer mainfooter" role="contentinfo">



			<div class="wrap">

				

				<!--Start Footer Logo-->

				

				<div class="footer_logo_widget">

				 <?php

					if ( is_active_sidebar( 'bottom-footer-logo' ) ) { ?>

					<div class="widget-column footer-widget-footer-logo">

						<a href="/" title="Tyax Lodge and Heli-Skiing"><?php dynamic_sidebar( 'bottom-footer-logo' ); ?></a>

					</div>

				<?php } ?>



		     </div>



			

			<!--Start Widgets-->

			<div class="fcontact-holder">

				<?php

				get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>

				

			</div>

			
			         <?php if ( is_active_sidebar( 'bottom-footer-socialicons' ) ) { ?>
			
						<ul class="social-media">

							<?php dynamic_sidebar( 'bottom-footer-socialicons' ); ?>

						</ul><!-- .social-navigation -->

					<?php } ?>
				<!--Start Menu -->


			
				<div class="footer_navigation"> 

					<?php wp_nav_menu( array(

						'theme_location' => 'footer-menu',

						'menu_id'        => 'footer-menu',

					) ); ?> 

				</div>

				

				

                  <!--start site info-->



				<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>

				

				



			</div><!-- .wrap -->



		</footer><!-- #colophon -->



</div><!-- #wrraper -->


<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.livequery.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.main.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.debounce.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.slidenotice.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.js" type="text/javascript"></script>

<script type="text/javascript">

				jQuery(document).on('ready', function() {

					jQuery('.tybanner_slider').slick({

						dots: true,

						arrows: false,

						infinite: true,

						speed: 1000,

						fade: true,

						autoplay: true,

						slidesToShow: 1,

						cssEase: 'linear',

						adaptiveHeight: true

					});

					jQuery('.lodchalets-slider').slick({

						dots: false,

						arrows: false,

						infinite: true,

						speed: 300,

						fade:true,

						autoplay: false,

						slidesToShow: 1,

						asNavFor: '.lodchaletstext-slider',

						adaptiveHeight: true

					});

					jQuery('.lodchaletstext-slider').slick({

						slidesToShow: 1,

						asNavFor: '.lodchalets-slider',

						dots: false,

						arrows: true,

						fade:true,

						focusOnSelect: true

					});

					jQuery(".heliskiing-widgets").slick({

						dots: false,

						arrows: true,

						infinite: true,

						speed: 400,

						autoplay: false,

						slidesToShow: 3,

						slidesToScroll: 1,

						responsive: [

					{

						breakpoint: 991,

						settings: {

						slidesToShow: 2,
							arrows: true,
							}

					   },

					{
						 breakpoint: 639,

						 settings: {

						   slidesToShow: 1,
							 arrows: true,
							
							}

					   },

					]

					  });

				});

			</script>
			<script>
				$(window).scroll(function(){
				var sticky = $('#header'),
				 scroll = $(window).scrollTop();
				 animation: "slideup"
					
				if (scroll >= 50) sticky.addClass('fix-header');
				else sticky.removeClass('fix-header');
				});
			</script>

<script type="text/javascript">

	function closeNav() {
    //document.getElementsByClassName("mega-sub-menu")[0].style.display = "none";
    if ($('.mega-menu-item.mega-menu-item-type-custom').hasClass('mega-toggle-on'))
        jQuery('.mega-menu-item.mega-menu-item-type-custom').removeClass( 'mega-toggle-on' );
}
	
	
$( document ).ready(function() {
    $('[data-toggle=search-form]').click(function() {
        $('.search-form-wrapper').toggleClass('open');
        $('.search-form-wrapper .search').focus();
        $('html').toggleClass('search-form-open');
    });
    $('[data-toggle=search-form-close]').click(function() {
        $('.search-form-wrapper').removeClass('open');
        $('html').removeClass('search-form-open');
    });
    $('.search-form-wrapper .search').keypress(function( event ) {
    if($(this).val() == "Search") $(this).val("");
    });

    $('.search-close').click(function(event) {
    $('.search-form-wrapper').removeClass('open');
    $('html').removeClass('search-form-open');
    });
});

</script>


<?php wp_footer(); ?>







</body>



</html>



