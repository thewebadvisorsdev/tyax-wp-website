<?php

/**



Template Name: Home Template



 * The front page template file

 *

 * If the user has selected a static page for their homepage, this is what will

 * appear.

 * Learn more: https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header(); ?>

<section class="tybanner">

					<div class="tybanner_slider">

					
					   
       
					  <?php
				$query = new WP_Query( array( 'post_type' => 'sliders', 'posts_per_page' => -1 ) );
 
			if ( $query->have_posts() ) : ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?> 
        
						<div>
							
							<?php if ( has_post_thumbnail() ) : ?>
								
									<?php the_post_thumbnail(); ?>
								
							<?php endif; ?>

							

						</div>
						
					 <?php endwhile; wp_reset_postdata(); ?>
        <!-- show pagination here -->
        <?php else : ?>
        <!-- show 404 error here -->
        <?php endif; ?>	
						

					</div>

					<div class="tybanner-textarea">

						<div class="container">

							<div class="two-col">

								<?php $optionsdata = get_option( 'option_tree' ); echo $optionsdata['main_slider_content'];?>
								
							</div>

						</div>

					</div>

					<div class="bb-mask">

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bottommask.svg" alt="image">

					</div>

				</section>



<?php // Show the selected frontpage content.



		if ( have_posts() ) :



			while ( have_posts() ) : the_post();?>



				


				<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>
					<div class="container">
						<?php the_content(); ?>
					</div>
				</section>
			<?php endwhile;



		else :



			get_template_part( 'template-parts/post/content', 'none' );



		endif; ?>





<?php get_footer();

