<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-banner">

<img width="2560" height="1096" src="https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Tyax Lodge Heliskiing General Inquiries" srcset="https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries.jpg 2560w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-300x128.jpg 300w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-768x329.jpg 768w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-1024x438.jpg 1024w" sizes="100vw" />
<div class="innerbanner-holder"><div class="container"><?php if ( have_posts() ) : ?>
			<h1><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		<?php else : ?>
			<h1><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
		<?php endif; ?></div></div>
</div>




        	<section id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel tyax-lodge-arae ' ); ?>>

					<div class="container">
						<?php
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/post/content', 'excerpt' );

			endwhile; // End of the loop.

			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );

		else : ?>

			<p style="text-align:center;"><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
			<?php
				get_search_form();

		endif;
		?>
					</div>
				</section>

			




<?php get_footer();
