<?php

/*-----------------------------------------------------------------------------------*/
/*	Enables the Excerpt meta box in Page edit screen
/*-----------------------------------------------------------------------------------*/

use AUX\Utils\Log;

function wpcodex_add_excerpt_support_for_pages() {
	add_post_type_support( 'pkg', 'excerpt' );
	add_post_type_support( 'sliders', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );


/*-----------------------------------------------------------------------------------*/
/*	Add SVG to allowed file uploads
/*-----------------------------------------------------------------------------------*/

function add_file_types_to_uploads($file_types) {
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

/*-----------------------------------------------------------------------------------*/
/*	Add Categories and Sitemap Groups to Pages
/*-----------------------------------------------------------------------------------*/

function wpgo_reg_tax() {
	register_taxonomy_for_object_type('category', 'page');
	add_post_type_support('page', 'category');
}
add_action('init', 'wpgo_reg_tax');
function create_sitemap_groups() {
	$labels = array(
		'name'              => __( 'Sitemap Groups', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => __( 'Sitemap Group', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Groups', 'textdomain' ),
		'all_items'         => __( 'All Groups', 'textdomain' ),
		'parent_item'       => __( 'Parent Group', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Group:', 'textdomain' ),
		'edit_item'         => __( 'Edit Group', 'textdomain' ),
		'update_item'       => __( 'Update Group', 'textdomain' ),
		'add_new_item'      => __( 'Add New Group', 'textdomain' ),
		'new_item_name'     => __( 'New Group Name', 'textdomain' ),
		'menu_name'         => __( 'Sitemap Groups', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'sgroups' ),
	);
	register_taxonomy( 'sgroups', array( 'page' ), $args );
}
add_action( 'init', 'create_sitemap_groups', 0 );

/*-----------------------------------------------------------------------------------*/
/*	Queue/Dequeue CSS
/*-----------------------------------------------------------------------------------*/

if (!function_exists('inspiry_enqueue_child_styles')) {
	function inspiry_enqueue_child_styles(){
		if ( !is_admin() ) {

            // dequeue and deregister parent default css
            wp_dequeue_style( 'parent-default' );
            wp_deregister_style( 'parent-default' );
			wp_dequeue_style( 'twentyseventeen-style' );
            wp_deregister_style( 'twentyseventeen-style' );

            // dequeue parent custom css
            wp_dequeue_style( 'parent-custom' );

            // parent default css
            wp_enqueue_style( 'parent-default', get_template_directory_uri().'/style.css' );
            
            // parent custom css
            wp_enqueue_style( 'parent-custom' );

            // child default css
            wp_enqueue_style('child-default', get_stylesheet_uri(), array('parent-default'), '1.0', 'all' );

            // child custom css
            wp_enqueue_style('child-custom',  get_stylesheet_directory_uri() . '/child-custom.css', array('child-default'), '1.0', 'all' );

            // Modal offer
            wp_enqueue_style( 'child-modal', get_stylesheet_directory_uri().'/css/app.css' );

        }
    }
}
add_action( 'wp_enqueue_scripts', 'inspiry_enqueue_child_styles', PHP_INT_MAX );

/*-----------------------------------------------------------------------------------*/
/*	Register Footer Menu
/*-----------------------------------------------------------------------------------*/

register_nav_menus(array(
	'footer-menu'    => __( 'Footer Menu', 'twentyseventeen' ),
));

/*-----------------------------------------------------------------------------------*/
/*	Register Widgets
/*-----------------------------------------------------------------------------------*/

if (function_exists('register_sidebar')) {
	register_sidebar( array(
		'name'          => __( 'Secondary Top Menu', 'twentyseventeen' ),
		'id'            => 'secondary-top-menu',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<div id="%1$s" class=" %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Logo', 'twentyseventeen' ),
		'id'            => 'bottom-footer-logo',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Email', 'twentyseventeen' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Social Icons', 'twentyseventeen' ),
		'id'            => 'bottom-footer-socialicons',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class=" socialiconsse %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Newsletter', 'twentyseventeen' ),
		'id'            => 'bottom-footer-newsletter',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<div id="%1$s" class=" text-area %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="">',
		'after_title'   => '</h4>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer CTA', 'twentyseventeen' ),
		'id'            => 'bottom-footer-cta',
		'description'   => __( 'Add widgets here to appear in your above footer CTA.', 'twentyseventeen' ),
		'before_widget' => '<div id="%1$s" class=" text-area %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="">',
		'after_title'   => '</h4>',
	));
	register_sidebar( array(
		'name'          => __( 'Footer Copyright', 'twentyseventeen' ),
		'id'            => 'footer-copyrights-widget',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
};

/*-----------------------------------------------------------------------------------*/
/*	Register Post Types
/*-----------------------------------------------------------------------------------*/

function post_type_slidersection() {
	register_post_type('sliders', array(
		'labels' => array(
			'name' => __( 'Sliders' ),
			'singular_name' => __( 'Sliders' ),
			'add_new' => __( 'Add New' ),
			'add_new_item' => __( 'Add New Sliders' ),
			'edit' => __( 'Edit' ),
			'edit_item' => __( 'Edit Sliders' ),
			'new_item' => __( 'New Sliders' ),
			'view' => __( 'View Sliders' ),
			'view_item' => __( 'View Sliders' ),
			'search_items' => __( 'Search Sliders' ),
			'not_found' => __( 'No Sliders found' ),
			'not_found_in_trash' => __( 'No Sliders found in Trash' ),
			'parent' => __( 'Parent Sliders' ),
        ),
		'public' => true,
		'show_ui' => true,
		'exclude_from_search' => true,
		'hierarchical' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'query_var' => true
	));
}
add_action('init', 'post_type_slidersection');

function post_type_packages() {
	register_post_type('pkg', array(
		'labels' => array(
			'name' => __( 'Packages' ),
			'singular_name' => __( 'Packages' ),
			'add_new' => __( 'Add New' ),
			'add_new_item' => __( 'Add New Packages' ),
			'edit' => __( 'Edit' ),
			'edit_item' => __( 'Edit Packages' ),
			'new_item' => __( 'New Packages' ),
			'view' => __( 'View Packages' ),
			'view_item' => __( 'View Packages' ),
			'search_items' => __( 'Search Packages' ),
			'not_found' => __( 'No Packages found' ),
			'not_found_in_trash' => __( 'No Packages found in Trash' ),
			'parent' => __( 'Parent Packages' ),
		),
		'public' => true,
		'show_ui' => true,
		'exclude_from_search' => true,
		'hierarchical' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'query_var' => true
	));
}
add_action('init', 'post_type_packages');

function post_type_lodgeproperty () {
	register_post_type('propertyslider', array(
		'labels' => array(
			'name' => __( 'L&C Slider' ),
			'singular_name' => __( 'L&C Slider' ),
			'add_new' => __( 'Add New' ),
			'add_new_item' => __( 'Add New L&C Slider' ),
			'edit' => __( 'Edit' ),
			'edit_item' => __( 'Edit L&C Slider' ),
			'new_item' => __( 'New L&C Slider' ),
			'view' => __( 'View L&C Slider' ),
			'view_item' => __( 'View L&C Slider' ),
			'search_items' => __( 'Search L&C Slider' ),
			'not_found' => __( 'No L&C Slider found' ),
			'not_found_in_trash' => __( 'No L&C Slider found in Trash' ),
			'parent' => __( 'Parent L&C Slider' ),
		),
		'public' => true,
		'show_ui' => true,
		'exclude_from_search' => true,
		'hierarchical' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'query_var' => true
	));
}
add_action('init', 'post_type_lodgeproperty');

function create_lccategories_taxonomies() {
	$labels = array(
		'name' => _x( 'L & C Categories', 'taxonomy general name' ),
		'singular_name' => _x( 'L & C Categories', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search L & C Categories' ),
		'popular_items' => __( 'Popular L & C Categories' ),
		'all_items' => __( 'All L & C Categories' ),
		'parent_item' => __( 'Parent L & C Categories' ),
		'parent_item_colon' => __( 'Parent L & C Categories' ),
		'edit_item' => __( 'Edit L & C Categories' ),
		'update_item' => __( 'Update L & C Categories' ),
		'add_new_item' => __( 'Add New L & C Categories' ),
		'new_item_name' => __( 'New L & C Categories Name' ),
	);
	register_taxonomy('lccategories',array('propertyslider'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'lccategories' ),
	));
}
add_action('init', 'create_lccategories_taxonomies', 0);

/*-----------------------------------------------------------------------------------*/
/*	Packages Slider
/*-----------------------------------------------------------------------------------*/

function rmcc_post_listing_shortcode1( $atts ) {
	ob_start();
	$query = new WP_Query( array(
		'post_type' => 'pkg',
		'posts_per_page' => -1,
		'order' => 'DESC',
	));
	if ( $query->have_posts() ) { ?>
		<div class="heliskiing-widgets">
			<?php $count = 0; while ( $query->have_posts() ) : $query->the_post(); $count++; ?>
				<div>
					<div class="number">
						<span><?php echo $count; ?></span>
					</div>
					<h5><?php the_title(); ?></h5>
					<?php if ( has_excerpt() ) : ?>
						<p><span class="entry-subtitle"><?php echo get_the_excerpt(); ?></span></p>
					<?php endif; ?>
					<?php if( get_field('costume_link_packages') ): ?>
						<a href="<?php the_field('costume_link_packages'); ?>" class="readmore">Read more</a>
					<?php endif; ?>
				</div>
			<?php endwhile;
			wp_reset_postdata(); ?>
		</div>
		<?php $myvariable = ob_get_clean();
		return $myvariable;
	}
}
add_shortcode( 'list-posts-basic', 'rmcc_post_listing_shortcode1' );

/*-----------------------------------------------------------------------------------*/
/*	Page Slider
/*-----------------------------------------------------------------------------------*/

function lc_post_listing_shortcode1( $atts ) {
	ob_start();
	$cat_id = get_field('select_categories', get_the_ID());
	$query = new WP_Query( array(
		'post_type' => 'propertyslider',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'tax_query' => array( array(
			'taxonomy' => 'lccategories',
			'field' => 'term_id',
			'terms' => $cat_id
		))
	));
	if ( $query->have_posts() ) { ?>
		<div class="sect-lodgechalets">
			<div class="text-holder">
				<div class="text-area">
					<?php the_field('description_lc'); ?>
				</div>
				<div class="lodchaletstext-slider">
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div>
							<div class="lodge-taxtname">
								<a href="<?php the_field('custom_link_sliders'); ?>"><?php the_title(); ?></a>
							</div>
						</div>
					<?php endwhile;
					wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="lodchalets-slider">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php if ( has_post_thumbnail() ) : ?>
						<div>
							<a href="<?php the_field('custom_link_sliders'); ?>"><?php the_post_thumbnail(); ?></a>
						</div>
					<?php endif; ?>
				<?php endwhile;
				wp_reset_postdata(); ?>
			</div>
		</div>
		<?php $myvariable = ob_get_clean();
		return $myvariable;
	}
}
add_shortcode( 'lc-list-slider-basic', 'lc_post_listing_shortcode1' );

/*-----------------------------------------------------------------------------------*/
/*  Get Weather
/*-----------------------------------------------------------------------------------*/
/**
 * Generate a full weather schedule replacing __deprecated_display_weather
 * 
 * @see __deprecated_display_weather
 * @see WPX\TWAWeather_Plugin::owm_get_weather_reports
 * @see AW\Report;
 *
 * @return void
 */
function display_weather () {
    return owm_reports();
}

function __deprecated_display_weather () {
    // HTTP vs HTTPS
	$url       = "http://feeds.customfithosting.com/weather/accu-current-bralorne.xml";
    $url2      = "http://feeds.customfithosting.com/weather/accu-forecast-bralorne.xml";
	$response  = wp_remote_get($url);
	$response2 = wp_remote_get($url2);
	$body      = wp_remote_retrieve_body($response);
	$body2     = wp_remote_retrieve_body($response2);
	$xml       = simplexml_load_string($body);
    $xml2      = simplexml_load_string($body2);

    // Log::trace(__FUNCTION__, $xml, $xml2);

	?>
	<h2>Current Weather Conditions</h2>
	<div class="weather">
		<div class="weather-row">
			<div><img src="/wp-content/themes/tyax-child/weather/150/<?php echo $xml->current->weathericon; ?>.png" alt="<?php echo $xml->current->weathertext; ?>" /></div>
			<div class="weather-full">
				<strong><?php echo $xml->current->temperature; ?> °C</strong><br />
				<?php echo date("g:i A", strtotime($xml->current->observationtime)); ?><br />
				<?php echo $xml->current->weathertext; ?>
			</div>
		</div>
		<div class="weather-row">
			<div>Barometric Pressure:</div>
			<div><?php echo $xml->current->pressure; ?> kPa</div>
		</div>
		<div class="weather-row">
			<div>Pressure Tendency:</div>
			<div><?php echo $xml->current->pressure->attributes()->state; ?></div>
		</div>
		<div class="weather-row">
			<div>Feels Like:</div>
			<div><?php echo $xml->current->realfeel; ?> °C</div>
		</div>
		<div class="weather-row">
			<div>Dew Point:</div>
			<div><?php echo $xml->current->dewpoint; ?> °C</div>
		</div>
		<div class="weather-row">
			<div>Humidity:</div>
			<div><?php echo $xml->current->humidity; ?>%</div>
		</div>
		<div class="weather-row">
			<div>Winds:</div>
			<div><?php echo $xml->current->winddirection; ?> <?php echo $xml->current->windspeed; ?> km/h</div>
		</div>
		<div class="weather-row">
			<div>Visibility:</div>
			<div><?php echo $xml->current->visibility; ?> km</div>
		</div>
		<div class="weather-row">
			<div>Precipitation:</div>
			<div><?php echo $xml->current->precip; ?> mm</div>
		</div>
		<div class="weather-row">
			<div>UV Index:</div>
			<div><?php echo $xml->current->uvindex; ?></div>
		</div>
		<div class="weather-row">
			<div>Cloud Cover:</div>
			<div><?php echo $xml->current->cloudcover; ?></div>
		</div>
		<div class="weather-row">
			<div>Observed:</div>
			<div><?php echo date("g:i A", strtotime($xml->current->observationtime)); ?></div>
		</div>
	</div>
	<h2><?php echo $xml2->forecast->day->dayName; ?> Forecast</h2>
	<div class="weather">
		<div class="weather-row">
			<div><img src="/wp-content/themes/tyax-child/weather/150/<?php echo $xml2->forecast->day->daytime->weathericon; ?>.png" alt="<?php echo $xml2->forecast->day->daytime->longText; ?>" /></div>
			<div class="weather-full">
				<strong>Day Time</strong><br />
				High: <?php echo $xml2->forecast->day->daytime->hightemperature; ?> °C<br />
				Low: <?php echo $xml2->forecast->day->daytime->lowtemperature; ?> °C<br />
				<?php echo $xml2->forecast->day->daytime->longText; ?>
			</div>
		</div>
		<div class="weather-row">
			<div><img src="/wp-content/themes/tyax-child/weather/150/<?php echo $xml2->forecast->day->nighttime->weathericon; ?>.png" alt="<?php echo $xml2->forecast->day->nighttime->longText; ?>" /></div>
			<div class="weather-full">
				<strong>Night Time</strong><br />
				Rain: <?php echo $xml2->forecast->day->nighttime->rain; ?> cm<br />
				Snow: <?php echo $xml2->forecast->day->nighttime->snow; ?> cm<br />
				<?php echo $xml2->forecast->day->nighttime->longText; ?>
			</div>
		</div>
	</div>
	<h2>Extended Weather Forecast</h2>
	<div class="weather">
	<?php
	$i = 0;
	foreach ($xml2->forecast->day as $node) {
		$i++;
		if($i==1) continue;
		$arr = $node->attributes();
		?>
		<div class="weather-row">
			<div><img src="/wp-content/themes/tyax-child/weather/150/<?php echo $node->daytime->weathericon; ?>.png" alt="<?php echo $node->daytime->shortText; ?>" /></div>
			<div class="weather-full">
				<strong><?php echo $node->dayName; ?></strong><br />
				High: <?php echo $node->daytime->hightemperature; ?> °C<br />
				Low: <?php echo $node->daytime->lowtemperature; ?> °C<br />
				<?php echo $node->daytime->shortText; ?>
			</div>
		</div>
		<?php
		if($i==6) break;
	}
	?>
	</div>	
	<?php
}
add_shortcode( 'display-tyax-weather', 'display_weather' );

/*-----------------------------------------------------------------------------------*/
/*  Guides Report
/*-----------------------------------------------------------------------------------*/

function display_report () {
	?>
	<div class="daily-report">
		<!--<span><?php /*$report_date = get_option( 'option_tree' ); echo date("l, F d, Y @ g:i A", strtotime($report_date['report_date'])); */ ?></span>
		<?php /*$report_content = get_option( 'option_tree' ); echo $report_content['report_content']; */ ?>-->

<span>
<?php 	
$post_id = "user_2";
$value = get_field( 'report_date', $post_id );
echo 'Report Date: '.$value
?>	
</span>
<?php 	
$post_id = "user_2";
$value = get_field( 'guides_content', $post_id );
echo $value
?>
<?php 	
$post_id = "user_2";
$value = get_field( 'report_image', $post_id );
if ($value != "") {
echo '<p><img style="width:100%;display:block;" src="'.$value.'"/></p>';
}
?>	
		
	</div>
	<?php
}
add_shortcode( 'display-tyax-report', 'display_report' );

/*-----------------------------------------------------------------------------------*/
/*  Contact Form 7 Redirects
/*-----------------------------------------------------------------------------------*/
 
function redirect_cf7() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
   if ( '34' == event.detail.contactFormId ) {
	   location = 'https://www.tyax.com/general-inquiry/confirmed/';
   } else if ( '543' == event.detail.contactFormId ) {
	   location = 'https://www.tyax.com/heli-ski-reservation-requests/confirmed/';
   } else if ( '954' == event.detail.contactFormId ) {
	   location = 'https://www.tyax.com/heli-skiing-packages/standby-program/confirmed/';
   } else if ( '1542' == event.detail.contactFormId ) {
	   location = 'https://www.tyax.com/employment/application/confirmed/';
    } else {
		location = 'https://www.tyax.com/general-inquiry/confirmed/';
    }
}, false );
</script>
<?php
}

add_action( 'wp_footer', 'redirect_cf7' );

/*-----------------------------------------------------------------------------------*/
/*  Search Exclude
/*-----------------------------------------------------------------------------------*/

add_action('pre_get_posts','wpse67626_exclude_posts_from_search');
function wpse67626_exclude_posts_from_search( $query ){

    if( $query->is_main_query() && is_search() ){
         //Exclude posts by ID
         $post_ids = array(326,334,336,372,958,1812);
         $query->set('post__not_in', $post_ids);
    }

}


if( current_user_can( 'subscriber' ) ) {
    add_filter( 'show_password_fields', '__return_false' );
	remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
}


if ( ! function_exists( 'cor_remove_personal_options' ) ) {
	function cor_remove_personal_options( $subject ) {
		$subject = preg_replace('#<h2>'.__("Personal Options").'</h2>#s', '', $subject, 1); // Remove the "Personal Options" title
		$subject = preg_replace('#<tr class="user-rich-editing-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Visual Editor" field
		$subject = preg_replace('#<tr class="user-comment-shortcuts-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Keyboard Shortcuts" field
		$subject = preg_replace('#<tr class="show-admin-bar(.*?)</tr>#s', '', $subject, 1); // Remove the "Toolbar" field
		$subject = preg_replace('#<h2>'.__("Name").'</h2>#s', '', $subject, 1); // Remove the "Name" title
		$subject = preg_replace('#<tr class="user-display-name-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Display name publicly as" field
		$subject = preg_replace('#<h2>'.__("Contact Info").'</h2>#s', '', $subject, 1); // Remove the "Contact Info" title
		$subject = preg_replace('#<tr class="user-url-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Website" field
		$subject = preg_replace('#<h2>'.__("About Yourself").'</h2>#s', '', $subject, 1); // Remove the "About Yourself" title
		$subject = preg_replace('#<tr class="user-description-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Biographical Info" field
		$subject = preg_replace('#<tr class="user-profile-picture(.*?)</tr>#s', '', $subject, 1); // Remove the "Profile Picture" field
		return $subject;
	}
	function cor_profile_subject_start() {
		if ( ! current_user_can('manage_options') ) {
			ob_start( 'cor_remove_personal_options' );
		}
	}
	function cor_profile_subject_end() {
		if ( ! current_user_can('manage_options') ) {
			ob_end_flush();
		}
	}
}
add_action( 'admin_head', 'cor_profile_subject_start' );
add_action( 'admin_footer', 'cor_profile_subject_end' );

// SHORTCODES IN WIDGETS
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode', 11);

# +------------------------------------------------------------------------+
# UI NOTICE
# +------------------------------------------------------------------------+
/**
 * Basic static class to encapsulate the notice functionality and OptionTree API
 */
class UINotice {
    public static $slider_notice_template;
    public static $slider_notice_active = false;

    public static function init() {
        $slider_notice = null;
        $prefix = 'notice';
        $template = file_get_contents( dirname( __FILE__ )  . '/template-parts/footer/slide-notice.php' );

        // Support Page or Post types
        $url = ot_get_option( 'notice_url', null );
        $url = $url ? get_permalink( $url ) : get_permalink( ot_get_option('notice_post_url'), null ) ?: 'javascript:void(0);';

        if ( function_exists( 'ot_get_option' ) ) {
            $slider_notice  = [
                'title'             => ot_get_option( 'notice_title', 'Untitled' ),
                'description'       => ot_get_option( 'notice_description', 'Lorem ipsum dolor sit amet' ),
                'url'               => $url,
                'call_to_action'    => ot_get_option( 'notice_call_to_action', 'LEARN MORE' )
            ];
    
            foreach( $slider_notice as $key => $value ) {
                $replacement = sprintf("{{%s_%s}}", $prefix, $key);
                $template = str_replace($replacement, $value, $template);
            }
    
            self::$slider_notice_template = $template;
            $activate = ot_get_option( 'notice_active', false );
            $activate = is_array( $activate ) ? current($activate) : false;
            self::$slider_notice_active = $activate;
        }
    }

    public static function render() {
        if( null != self::$slider_notice_template && self::$slider_notice_active ) {
            echo self::$slider_notice_template;
        }
    }
}

/**
 * Only enable this featre if WP_UINOTICE is set in .env
 */
if( (getenv('WP_UINOTICE') == 1) ) {
    add_action( 'after_setup_theme', [UINotice::class, 'init'], PHP_INT_MAX );
    add_action( 'wp_footer', [UINotice::class, 'render'], PHP_INT_MAX);
}

# +------------------------------------------------------------------------+
# WEATHER
# +------------------------------------------------------------------------+
function owm_reports() {
    $geo_coords = [ 'bralorne' => ['lat' => 50.775, 'lon' => -122.8138] ];
    $locations  = [ 'bralorne' => [ getenv('AW_LOCATION_ID') ] ];
    $report = function_exists('owm_get_weather_reports') ? call_user_func_array('owm_get_weather_reports', $locations['bralorne']) : null;

    return $report;
}