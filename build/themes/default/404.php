<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>




<div class="inner-banner">

<img width="2560" height="1096" src="https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Tyax Lodge Heliskiing General Inquiries" srcset="https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries.jpg 2560w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-300x128.jpg 300w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-768x329.jpg 768w, https://www.tyax.com/wp-content/uploads/2018/05/tyax-lodge-heli-skiing-general-inquiries-1024x438.jpg 1024w" sizes="100vw" />
<div class="innerbanner-holder"><div class="container"><h1>Page Not Found</h1></div></div>

</div>

<div class="container">
<p style="text-align:center;"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?></p>
			
			<?php get_search_form(); ?>

</div><br/><br/><br/><br/><br/><br/><br/>


<?php get_footer();
