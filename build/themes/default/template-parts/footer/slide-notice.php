<div class="component ui-slidenoticecontainer">
    <a href="javascript:void(0);" class="component ui-slidenotice__toggle"></a>
    <aside class="component ui-slidenotice">
        <div class="ui-slidenotice__imageoverlay">      
            <div class="ui-slidenotice__coloroverlay"></div>
        </div>
        <div class="ui-slidenotice__content">
            <h3 class="ui-slidenotice__heading">{{notice_title}}</h3>
            <p class="ui-slidenotice__description">{{notice_description}}</p>
            <a href="{{notice_url}}" class="ui-slidenotice__cta">{{notice_call_to_action}}</a> 
        </div>
    </aside>
</div>