<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php if ( is_active_sidebar( 'footer-copyrights-widget' ) ) { ?>
<div class="site-info">
	<div class="copyright"><?php dynamic_sidebar( 'footer-copyrights-widget' ); ?></div>
</div><!-- .site-info -->
<?php } ?>