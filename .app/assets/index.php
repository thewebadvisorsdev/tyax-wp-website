<?php
/**
 * This file bootstraps wordpress with a `font-controller`
 * designed to bypass WordPress' Rewrite API for use outside of
 * a plugin context. This file also encapsulates and defines
 * many of the config settings traditionally found in wp-config.php
 * through a singleton instance of `APP\APP`.
 * 
 * In addition to bootstrapping WP, this file provides a configurable 
 * caching mechansim based upon Symfony's Component\Cache classes.
 * Enable or disable caching through this application's .env file.
 *  
 * Composer autoloaders are included for required vendor libraries.
 *
 * @package WPX
 */

use APP\App;
use AUX\Utils\Log;

if( !defined('APP_PATH') ) {
    define('APP_PATH', dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR);
}

try {
    $autoload = require_once APP_PATH . 'vendor/autoload.php';

    if( class_exists('APP\\App') ) {
        $APP            = App::inst()->init( APP_PATH );
        $APP->autoloader($autoload);
        $table_prefix   = App::db('db_prefix');

        if( App::config('subdirectory') ?? false ) {
            $runtime_path = sprintf("%s/%s/wp-blog-header.php", App::config('document_root'), App::config('subdirectory') );
        }else{
            $runtime_path = sprintf("%s/wp-blog-header.php", App::config('document_root') );
        }
    
        require($runtime_path);
    }
    

} catch( \Exception $e ) {
    Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
    die();
}
