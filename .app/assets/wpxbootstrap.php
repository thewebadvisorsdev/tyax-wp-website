<?php
/**
 * Bootstrap file for setting the APP_PATH and initializing
 * WPAPP, which defines much of what wp-config.php defined,
 * but is now encapsulated into WPAPP.php
 *  
 * This file also initializes Composer autoloaders for use
 * with all required vendor libraries.
 *
 * @package WPX
 */

use APP\App;
use AUX\Utils\Log;
use \Exception as Exception;

define('WPX_SUPERCACHE_START',1);

if( !defined('APP_PATH') ) {
    define('APP_PATH', dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR);
   
    try {
        global $autoload;
        $autoload = $autoload ?: require_once APP_PATH . 'vendor/autoload.php';
    
        if( class_exists('APP\\App') ) {
            global $table_prefix;
            $APP            = App::inst()->init( APP_PATH );
            $APP->autoloader($autoload);
            $table_prefix   = App::db('db_prefix');
        }
    
    } catch( \Exception $e ) {
        Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        die();
    }
    
}

/* WORDPRESS */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

require_once( ABSPATH . 'wp-settings.php' );
