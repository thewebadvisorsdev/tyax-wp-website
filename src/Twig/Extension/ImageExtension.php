<?php

namespace Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use AUX\Utils\Log;
use AUX\Utils\Image;
use Twig\TwigFilter;

/**
 * Foundation class for AUX Twig extensions
 * 
 * @link https://twig.symfony.com/doc/2.x/advanced.html#creating-an-extension
 */

class ImageExtension extends AbstractExtension {

    public function getFunctions() {
        return array(
            new TwigFunction('orientation', [$this,'orientation']),
            new TwigFunction('uri_resource_path', [$this,'uri_resource_path']),
        );
    }

    public function getFilters() {
        return array(
            new TwigFilter('resize_width', [$this,'resize_width']),
            new TwigFilter('resize_height', [$this,'resize_height']),
            new TwigFilter('square', [$this,'square']),
            new TwigFilter('scale', [$this,'scale']),
            new TwigFilter('resize', [$this,'resize']),
            new TwigFilter('crop', [$this,'crop']),
            new TwigFilter('crop_from_center', [$this,'crop_from_center']),
            new TwigFilter('fit_image_max', [$this,'fit_image_max']),
            new TwigFilter('fit_image_min', [$this,'fit_image_min']),
            new TwigFilter('max_area_fill', [$this,'max_area_fill']),
        );
    }

    public function getName() {
        return 'image';
    }

    # +------------------------------------------------------------------------+
    # FUNCTIONS
    # +------------------------------------------------------------------------+
    public function orientation(string $filename, $as_string = false) {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = Image::getInstance();
            $result = $img->load($filename)->Orientation($as_string);
            return $result;
        }

        return false;
    }

    /**
     * Transforms a URI into a valid file resource path.
     * If the file does not exist on disk, and a default image path
     * is supplied, this method will return the default.
     * 
     * @param String    $filename The absolute URI to the image
     * @return String   The file resource path to the desired image or a default image if one is supplied.
     */
    public function uri_resource_path(string $filename, $default_filepath = null) {
        $file_path = null;

        try {

            if ( preg_match('#((\d{4})\/(\d{2})\/.*?$)#', $filename, $matches) ){
                $filename  = WP_UPLOADS_URL_ABSOLUTE . DIRECTORY_SEPARATOR . trim($matches[0], DIRECTORY_SEPARATOR);
            }
    
            $path_info  = pathinfo($filename);
            $dir        = str_replace(WP_UPLOADS_URL_ABSOLUTE, WP_UPLOADS_DIR, $path_info['dirname']);
            $file_path  = sprintf('%s/%s', $dir, $path_info['basename']);
            $file_path  = !file_exists($file_path) ? $default_filepath ?? '' : $file_path;

        }catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString()(), Log::LOG_TYPE_ERROR);
        }

        return $file_path;
    }

    # +------------------------------------------------------------------------+
    # FILTERS
    # +------------------------------------------------------------------------+
    public function resize_width() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('ResizeWidth', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function resize_height() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('ResizeHeight', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function square() {
        $args       = func_get_args();
        
        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('Square', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }
    
    public function scale() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('Scale', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }
    
    public function resize() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('Resize', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }
    
    public function crop() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('Crop', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function crop_from_center() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('CropFromCenter', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function fit_image_max() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('FitImageMax', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function fit_image_min() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('FitImageMin', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }

    public function max_area_fill() {
        $args       = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $img = $this->process('MaxAreaFill', $args);
            if( is_a( $img, Image::class ) ) {
                $file_resource = $img->Save();
                $asset_uri = $this->getAssetURI($file_resource);
            }else{
                $asset_uri = $this->getAssetURI($img);
            }
            return $asset_uri;
        }

        return false;    
    }    

    # +------------------------------------------------------------------------+
    # HELPERS
    # +------------------------------------------------------------------------+
    private function process( $method, $method_arguments ) {
        $filename   = array_shift($method_arguments);

        try {
            if( null != trim($filename) ) {
                $img = Image::getInstance();
                
                if( $img_resource = $img->isCachedImage($method, $method_arguments, $filename) )  {
                    return $img_resource;
                }

                // Only load the image if it's not cached to avoid disk thrashing
                $img->Load($filename);
                
                switch( $method ) {
                    case 'ResizeHeight' :
                        $height = array_shift($method_arguments);
                        $img->ResizeHeight($height);
                        break;
                    case 'ResizeWidth' :
                        $width = array_shift($method_arguments);
                        $img->ResizeWidth($width);
                        break;
                    case 'Square' :
                        $size = array_shift($method_arguments);
                        $img->Square($size);
                        break;
                    case 'Scale' :
                        $scale = (float) array_shift($method_arguments);
                        $img->Scale($scale);
                        break;
                    case 'Resize' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $img->Resize($width, $height);
                        break;
                    case 'Crop' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $x      = array_shift($method_arguments);
                        $y      = array_shift($method_arguments);
                        $img->Crop($width, $height, $x, $y);
                        break;
                    case 'CropFromCenter' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $img->CropFromCenter($width, $height);
                        break;
                    case 'FitImageMax' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $img->FitImageMax($width, $height);
                        break;
                    case 'FitImageMin' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $img->FitImageMin($width, $height);
                        break;
                    case 'MaxAreaFill' :
                        $width  = array_shift($method_arguments);
                        $height = array_shift($method_arguments);
                        $hex    = array_shift($method_arguments) ?: '#000000';
                        $img->MaxAreaFill($width, $height, $hex);
                        break;
                }
    
                return $img;
            }
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }

        return false;
    }

    private function getAssetURI( $filename ) {
        $path_info  = pathinfo($filename);
        $file_uri   = null;

        if( ($path_info['dirname'] ?? null) && ($path_info['basename'] ?? null) ) {            
            $dir_uri    = str_replace(WP_UPLOADS_DIR, WP_UPLOADS_URL_RELATIVE, $path_info['dirname']);
            $file_uri   = sprintf('/%s/%s', $dir_uri, $path_info['basename']);
        }

        return $file_uri;
    }

}