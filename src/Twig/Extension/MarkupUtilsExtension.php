<?php

namespace Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use AUX\Utils\Log;
use Twig\TwigFilter;
use Twig\Markup;
use WPX\WPAPP;
use AUX\Utils\Debug;
use AUX\Utils\TruncateHTML;

/**
 * Foundation class for AUX Twig extensions
 * 
 * @link https://twig.symfony.com/doc/2.x/advanced.html#creating-an-extension
 */

class MarkupUtilsExtension extends AbstractExtension {

    public function getFunctions() {
        return array(
            new TwigFunction('site_tree', [$this,'site_tree']),
            new TwigFunction('debug', [$this,'debug']),
            new TwigFunction('log', [$this,'log']),
            new TwigFunction('picsum', [$this,'picsum']),
            new TwigFunction('human_readable_filesize', [$this,'human_readable_filesize']),            
            new TwigFunction('array_shuffle', [$this,'array_shuffle']),            
            new TwigFunction('proxy_cache', [$this,'proxy_cache']),
            new TwigFunction('slashify', [$this,'slashify']),
            new TwigFunction('getenv', 'getenv')
        );
    }
    
    public function getFilters() {
        return array(
            new TwigFilter('content_html', [$this,'content_html']),
            new TwigFilter('canonical_tag', [$this,'canonical_tag']),
            new TwigFilter('seo_title', [$this,'seo_title']),
            new TwigFilter('meta_tag', [$this,'meta_tag']),
            new TwigFilter('json_decode', [$this,'json_decode']),
            new TwigFilter('lowercase', [$this,'lowercase']),
            new TwigFilter('uppercase', [$this,'uppercase']),
            new TwigFilter('titlecase', [$this,'titlecase']),
            new TwigFilter('sentencecase', [$this,'sentencecase']),
            new TwigFilter('split_words', [$this,'split_words']),
            new TwigFilter('split_on_char', [$this,'split_on_char']),
            new TwigFilter('to_slug', [$this,'to_slug']),
            new TwigFilter('truncate_words', [$this,'truncate_words']),
            new TwigFilter('truncate_chars', [$this,'truncate_chars']),
            new TwigFilter('truncate_words_html', [$this,'truncate_words_html']),
            new TwigFilter('truncate_chars_html', [$this,'truncate_chars_html']),
            new TwigFilter('wp_texturize', [$this,'wp_texturize']),
            new TwigFilter('encode_to_utf8', [$this,'encode_to_utf8']),
            new TwigFilter('highlight_term', [$this,'highlight_term']),
            new TwigFilter('get_surrounding_text', [$this,'get_surrounding_text']),
            new TwigFilter('root_relative_url', [$this,'root_relative_url']),
            new TwigFilter('array_column', [$this,'array_column']),
            new TwigFilter('unserialize', [$this,'unserialize']),
            new TwigFilter('slashify', [$this,'slashify']),
            new TwigFilter('no_breaking_phrase', [$this,'no_breaking_phrase']),
            new TwigFilter('fahrenheit_to_celsius', [$this,'fahrenheit_to_celsius']),
            new TwigFilter('kelvin_to_celsius', [$this,'kelvin_to_celsius']),
            new TwigFilter('wind_degree_to_cardinal', [$this,'wind_degree_to_cardinal']),
            new TwigFilter('uvindex_to_words', [$this,'uvindex_to_words']),
            new TwigFilter('str_pad', 'str_pad')
        );
    }

    public function getName() {
        return 'markuputils';
    }

    # +------------------------------------------------------------------------+
    # FUNCTIONS
    # +------------------------------------------------------------------------+
    public function site_tree() {
        $args       = func_get_args();
        $root_only  = array_shift($args);
        $refresh    = array_shift($args);
        $id         = array_shift($args);

        $html = WPAPP::WPAPI()->SiteTree(!!$root_only, !!$refresh, is_int($id) ? $id : false);

        return new Markup($html, 'UTF-8');
    }

    public function debug() {
        $args = func_get_args();
        $html = [];
        $tag    = Debug::DEBUG_TYPE_DEBUG;
        $bt     = array_slice(debug_backtrace(), 1);
        $info   = array_shift($bt);

        $timestamp = @date('d-M-Y H:i:s e O');
        $line_trace = sprintf("Logged from file %s on line %s", $info['file'], $info['line']);
        $msg_format = sprintf("[%s] (%s) %s:\n %s\n", $timestamp, $tag, $line_trace,  print_r( $args, 1 ));

        array_push($html, '<pre class="wpx-debug">');
        array_push($html, $msg_format);
        array_push($html, '</pre>');
        return $this->Output(implode("\n",$html));
    }

    public function log() {
        call_user_func_array([Log::class,'trace'], func_get_args());
    }

    public function picsum() {
        $args = func_get_args();
        $url = 'https://picsum.photos/%s/%s?ipsum='. rand(0,1000);
        $w = array_shift($args) ?? 640;
        $h = array_shift($args) ?? 480;
        $as_img = array_shift($args) ?? false;
        return $as_img ? $this->Output(sprintf('<img src="%s" alt="Placeholder Image">', sprintf($url, $w, $h))) : sprintf($url, $w, $h);
    }

    public function proxy_cache() {
        $args       = func_get_args();
        $asset      = array_shift($args) ?: null;
        try {
            $WPAPI  = WPAPP::inst()->WPAPI();
            $asset  = call_user_func_array([$WPAPI, 'getProxiedAsset' ], func_get_args());
            return $asset;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return $asset;
    }

    # +------------------------------------------------------------------------+
    # FILTERS
    # +------------------------------------------------------------------------+
    public function content_html() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            // return $this->Output( stripslashes( iconv('iso-8859-1', 'utf-8', $content) ) );
            return $this->Output( stripslashes( $content ) );
        }

        return false;   
    }
    
    public function json_decode() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            return json_decode($content);
        }

        return false;   
    }

    public function lowercase() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            return mb_strtolower($content);
        }

        return false;   
    }

    public function uppercase() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            return mb_strtoupper($content);
        }

        return false;   
    }

    public function titlecase() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            return ucwords($content);
        }

        return false;   
    }
    
    public function sentencecase() {
        $args = func_get_args();

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args);
            return ucfirst($content);
        }

        return false;   
    }

    public function canonical_tag() {
        $args = func_get_args();
        $output = false;

        if( !empty($args) && count($args) > 0 ) {
            $url = array_shift($args);

            if( !!$url || null != $url ) {
                $output = $this->Output( sprintf('<link rel="canonical" href="%s" />', $url) );
            }

            return $output;
        }

        return false;
    } 
    
    public function meta_tag() {
        $args = func_get_args();
        $output = false;

        if( !empty($args) && count($args) > 0 ) {
            $content = array_shift($args) ?? null;
            $name    = array_shift($args) ?? null;
            
            if( null != $name && null != $content ) {
                $content = strip_tags($this->wp_texturize($content, true));
                $content = strlen($content) > 60 ? $this->truncate_chars($content, 155, '') : $content;
                $output = $this->Output( sprintf('<meta name="%s" content="%s" />', $name, $content) );
            }

            return $output;
        }

        return false;
    } 
    
    public function seo_title() {
        $args = func_get_args();
        $output = false;

        if( !empty($args) && count($args) > 0 ) {
            $title = array_shift($args) ?? null;            
            if( null != $title ) {
                $title = strip_tags($this->wp_texturize($title, true));
                $output = $this->Output( sprintf('<title>%s</title>', $title) );
            }

            return $output;
        }

        return false;
    }  
    
    public function split_words() {
        $args   = func_get_args() ?: null;
        $output = array_shift($args);

        $targeted_words = ['About Us' => "About&nbsp;Us",'Images & Video' => "Images &amp;&nbsp;Video", 'to go' => "To&nbsp;Go", 'to do' => "To&nbsp;Do", 'choose your' => "Choose&nbsp;Your"];

        if( $output ) {
            array_filter(array_keys($targeted_words), function($v,$l) use (&$output, $targeted_words) {
                if( preg_match("#(\b". $v ."\b)#i", $output, $matches) === 1 ) { 
                    $replacement = $targeted_words[ $v ];
                    $output = str_replace($matches[1], $replacement, $output);
                }                                
                return $output;
            },ARRAY_FILTER_USE_BOTH);
            $words = explode(" ", $output);
            $words = array_map(function($word){
                return sprintf('<span class="word">%s</span>', $word);
            }, $words);
        }

        if( !empty($words) ) {
            $output = implode("\n",$words);
        }

        return $this->Output($output);
    }

    public function split_on_char() {
        $args       = func_get_args() ?: null;
        $output     = array_shift($args);
        $delimiter  = array_shift($args);
        $glue       = array_shift($args);

        if( $output ) {
            $words = explode($delimiter ?: ' ', $output);
            $words = array_map(function($word){
                return sprintf('<span>%s</span>', trim($word));
            }, $words);
        }
        if( !empty($words) ) {
            $output = implode($glue ?: "\n",$words);
        }

        return $this->Output($output);
    }

    public function to_slug() {
        $args = func_get_args() ?: null;
        $output = array_shift($args);
        $delimiter = array_shift($args) ?: '-';

        $replace = [
            '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya', '&' => 'and', ' ' => $delimiter
        ];

        $subject = $output;
        $subject = str_replace(array_keys($replace), $replace, $subject);       // Replace special characters
        $subject = preg_replace('/[^\w\-]+/', '', $subject, 1 );                // Remove all non-word characters
        $subject = preg_replace('/^-+/', $delimiter, $subject );                        // Trim - from start of text
        $subject = preg_replace('/-+$/', $delimiter, $subject );                        // Trim - from end of text
        $subject = preg_replace('/(-){2,}/', $delimiter, $subject, 1 );         // Replace multiple - with single -

        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-\s]+/', $delimiter, $subject), $delimiter));

        return $slug;
    }

    public function human_readable_filesize(){
        $args       = func_get_args();
        $bytes      = array_shift($args); 
        $decimals   = array_shift($args) ?? 2;

        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    public function truncate_chars() {
        $args       = func_get_args();
        $text       = array_shift($args);
        $limit      = array_shift($args) ?? 60;
        $terminator = array_shift($args) ?? '...';

        if( strlen($text) > $limit ) {
            $endpos = strpos(str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $text), ' ', $limit);
            if($endpos !== FALSE)
                $text = trim(substr($text, 0, $endpos)) . $terminator;
        }
        return $text;
    }

    public function truncate_words() {
        $args       = func_get_args();
        $text       = array_shift($args);
        $limit      = array_shift($args) ?? 60;
        $terminator = array_shift($args) ?? '...';

        $words = preg_split("/[\n\r\t ]+/", $text, $limit + 1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_OFFSET_CAPTURE);
        if (count($words) > $limit) {
            end($words);
            $last_word = prev($words);
               
            $text =  substr($text, 0, $last_word[1] + strlen($last_word[0])) . $terminator;
        }
        return $text;
    }

    public function truncate_words_html() {
        try {
            $args       = func_get_args() ?: null;
            $html       = array_shift($args);
            $limit      = array_shift($args) ?: 60;
            $terminator = array_shift($args) ?: '…';
            
            $output     = TruncateHTML::truncateWords($html, $limit, $terminator);
            $output     = $this->Output( $this->wp_texturize($output, true) );

        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return $output ?: null;
    }

    public function truncate_chars_html() {
        try {
            $args       = func_get_args();
            $html       = array_shift($args);
            $limit      = array_shift($args) ?? 140;
            $terminator = array_shift($args) ?? '…';

            $output     = TruncateHTML::truncateChars($html, $limit, $terminator);
            $output     = $this->Output( stripslashes( $output ) );

            // Log::trace(__METHOD__, $output);

        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $output ?: null;
    }

    public function wp_texturize() {
        try {
            $args           = func_get_args();
            $content        = array_shift($args);
            $simple_filters = array_shift($args) ?: false;
            $content    = WPAPP::WPAPI()->applyWPContentFilters( $content, $simple_filters );
            return $content;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    public function slashify() {
        try {
            $args           = func_get_args();
            $href           = array_shift($args);
            
            if( strpos($href, 'javascript:void(0)') ) return $href;
            if( null !== parse_url($href,PHP_URL_SCHEME) ) return $href;
            if( !!pathinfo($href, PATHINFO_EXTENSION) ) return $href;
            
            $normalize_href = $href;
            $normalize_href = ltrim($normalize_href,"/");
            $normalize_href = rtrim($normalize_href,"/");
            $normalize_href = sprintf("/%s/", $normalize_href);

            return $normalize_href;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $href ?: null;
    }

    public function encode_to_utf8() {
        try {
            $args       = func_get_args();
            $content    = array_shift($args);
            // $content    = Encoding::toUTF8($content);
            return $content;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    public function no_breaking_phrase() {
        try {
            $args       = func_get_args();
            $content    = array_shift($args);
            $phrase     = array_shift($args);
            $spans      = array_shift($args);

            $replacement_string = $spans ? '<span class="nbsp">&nbsp;</span>' : "&nbsp";

            // Find the phrase in string
            $text_fragment = html_entity_decode($content);
            $text_fragment = str_replace($phrase, preg_replace("/\s/",$replacement_string,$phrase), $content );

            $content = $text_fragment;
            return $content;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    /**
     * Get the surrounding context
     * 
     * $content, $keyword, $padding
     * 
     * @link https://stackoverflow.com/a/5188641
     */
    public function get_surrounding_text() {
        $content = null;
        try {
            $args       = func_get_args();
            $content    = array_shift($args);
            $keyword    = array_shift($args);
            $padding    = array_shift($args) ?: 15;

            $position   = strpos($content, $keyword);
            // starting at (where keyword was found - padding), retrieve
            // (padding + keyword length + padding) characters from the content
            $snippet = substr($content, $position - $padding, (strlen($keyword) + $padding * 2));

            return '…' . $snippet . '…';
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;        
    }

    public function highlight_term() {
        $content = null;

        try {
            $args       = func_get_args();
            $content    = array_shift($args);
            $term       = array_shift($args);

            $content    = preg_replace("/\p{L}*?".preg_quote($term)."\p{L}*/ui", "<mark>$0</mark>", $content);
            return $content;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    public function root_relative_url() {
        $content = null;

        try {
            $args       = func_get_args();
            $content    = array_shift($args) ?: '';
            $content    = str_replace(WP_HOME,'',$content);
            return $content;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    public function array_column() {
        $content = null;

        try {
            $args           = func_get_args();
            $array_subject  = array_shift($args) ?: [];
            $array_column   = array_shift($args) ?: null;
            if( null !== $array_column && !empty($array_subject) ) {                
                $array_subject  = array_column($array_subject, $array_column);
            }
            return $array_subject;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $content ?: null;
    }

    public function array_shuffle() {
        $array_subject = null;

        try {
            $args           = func_get_args();
            $array_subject  = array_shift($args) ?: [];
            shuffle($array_subject);
            return $array_subject;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $array_subject ?: null;
    }
    
    public function unserialize() {
        $array_subject = null;

        try {
            $args           = func_get_args();
            $array_subject  = array_shift($args) ?: null;
            return (null !== $array_subject) ? @unserialize($array_subject) : null;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return $array_subject ?: null;
    }

    # +------------------------------------------------------------------------+
    # TEMPERATURE CONVERSION
    # +------------------------------------------------------------------------+
    /*
     * @link https://gist.github.com/ugraphix/396166118e472a1385368e7f97f801ae
     * Celsius to Fahrenheit ° F = 9/5 ( ° C) + 32
     * Fahrenheit to Celsius ° C = 5/9 (° F - 32)
     * Celsius to Kelvin K = ° C + 273.15
     * Kelvin to Celsius ° C = K - 273.15
     * Fahrenheit to Kelvin K = 5/9 (° F - 32) + 273.15
     * Kelvin to Fahrenheit 	° F = 9/5 (K - 273.15) + 32
     */


    //Fahrenheit to celsius
    public function fahrenheit_to_celsius()
    {
        $args               = func_get_args();
        $given_value        = array_shift($args);
        $celsius            = 5/9*($given_value-32);
        return $celsius;
    }
    
    // //Fahrenheit to kelvin
    // public function fahrenheit_to_kelvin($given_value)
    // {
    //     $kelvin=$this->fahrenheit_to_celsius($given_value) + 273.15;
    //     return $kelvin ;
    // }

    // //Celsius to fahrenheit
    // public function celsius_to_fahrenheit($given_value)
    // {
    //     $fahrenheit=$given_value*9/5+32;
    //     return $fahrenheit ;
    // }

    // //Celsius to kelvin 
    // public function celsius_to_kelvin($given_value)
    // {
    //     $kelvin=$given_value+273.15;
    //     return $kelvin ;
    // }

    // //Kelvin to fahrenheit equation
    // public function kelvin_to_fahrenheit($given_value)
    // {
    //     $fahrenheit=9/5*($given_value-273.15)+32;
    //     return $fahrenheit ;
    // }

    //Kelvin to celsius equation
    public function kelvin_to_celsius()
    {
        $args               = func_get_args();
        $given_value        = array_shift($args);
        $celsius            = $given_value-273.15;
        return $celsius;
    }

    public function wind_degree_to_cardinal($degrees) {
        $directions = array('N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N');
        return $directions[round($degrees / 22.5)];
    }

    public function uvindex_to_words($index = 0) {
        $i = floor($index);        
        $index_as_words = null;
        
        if( in_array($i, range(0,2)) ) {
            return 'Low';
        }

        if( in_array($i, range(3,5)) ) {
            return 'Moderate';
        }

        if( in_array($i, range(6,7)) ) {
            return 'High';
        }

        if( in_array($i, range(8,10)) ) {
            return 'Very high';
        }

        if( $i > 11 ) {
            return 'Extreme';
        }

        return $index_as_words;
    }

    # +------------------------------------------------------------------------+
    # HELPERS
    # +------------------------------------------------------------------------+
    private function Output( $content ) {
        if( !is_string($content) || null == $content ) return false;
        return new Markup($content, 'UTF-8');
    }

}