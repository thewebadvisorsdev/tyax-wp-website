<?php

namespace WPX;

use AUX\Utils\Debug;
use AUX\Utils\Log;

/**
 * Encapsulation for WordPress' WP_Query and Conditional Tags
 * 
 * @see https://codex.wordpress.org/Conditional_Tags
 */
class WPQuery {

    const ENUM_TYPE_SINGLE              = 'single';
    const ENUM_TYPE_PREVIEW             = 'preview';
    const ENUM_TYPE_PAGE                = 'page';
    const ENUM_TYPE_POST                = 'post';
    const ENUM_TYPE_ARCHIVE             = 'archive';
    const ENUM_TYPE_DATE                = 'date';
    const ENUM_TYPE_YEAR                = 'year';
    const ENUM_TYPE_MONTH               = 'month';
    const ENUM_TYPE_DAY                 = 'day';
    const ENUM_TYPE_TIME                = 'time';
    const ENUM_TYPE_AUTHOR              = 'author';
    const ENUM_TYPE_CATEGORY            = 'category';
    const ENUM_TYPE_TAG                 = 'tag';
    const ENUM_TYPE_TAX                 = 'tax';
    const ENUM_TYPE_SEARCH              = 'search';
    const ENUM_TYPE_FEED                = 'feed';
    const ENUM_TYPE_COMMENT_FEED        = 'comment_feed';
    const ENUM_TYPE_TRACKBACK           = 'trackback';
    const ENUM_TYPE_HOME                = 'home';
    const ENUM_TYPE_404                 = '404';
    const ENUM_TYPE_EMBED               = 'embed';
    const ENUM_TYPE_PAGED               = 'paged';
    const ENUM_TYPE_ADMIN               = 'admin';
    const ENUM_TYPE_ATTACHMENT          = 'attachment';
    const ENUM_TYPE_SINGULAR            = 'singular';
    const ENUM_TYPE_ROBOTS              = 'robots';
    const ENUM_TYPE_POSTS_PAGE          = 'posts_page';
    const ENUM_TYPE_POST_TYPE_ARCHIVE   = 'post_type_archive';

    protected $page_post_type = self::ENUM_TYPE_PAGE;
    protected static $constants = [];

    public function WPQuery($global_wp_query) {

        if( NULL !== $global_wp_query ) {
            $this->parseWPConditionalTags($global_wp_query);
        }

        return $this;
    }    

    public function Type($page_post_type = null) {
        if( NULL !== $page_post_type ) {
            $this->page_post_type = trim($page_post_type);
        }

        return $this->page_post_type;
    }

    public function __construct( $global_wp_query = null ) {
        if( NULL !== $global_wp_query && !$global_wp_query->is_admin ) {
            $this->parseWPConditionalTags($global_wp_query);
        }
    }

    # +------------------------------------------------------------------------+
    # HELPERS
    # +------------------------------------------------------------------------+
    /**
     * Search through WP_Query Conditional Tags and note the type
     */
    private function parseWPConditionalTags($global_wp_query) {
        if( !$global_wp_query->post ) return false;
        
        $query          = get_object_vars( $global_wp_query );
        $page_post_type = $global_wp_query->post->post_type;

        $keys           = array_keys( $query );
        $matches        = [];

        // Extract Class Constants - one-time operation
        if( empty(self::$constants) ) {
            $ref = new \ReflectionClass($this);
            self::$constants = $ref->getConstants();
        }

        // Find all Conditional Tags
        foreach($keys as $key) {
            if( strpos($key, 'is_') !== false ) {
                array_push($matches, $key);
            }
        }

        // Search for any Conditional Tags that are set
        foreach( $matches as $key ) {
            $value =  $query[ $key ];
            
            if( $value != null ) {
                $enum_key   = strtoupper( "ENUM_TYPE_" .  str_replace('is_', '', $key) );

                if( array_key_exists($enum_key, self::$constants ) && ($value == 1) ) {
                    $const = constant('self::'. $enum_key);
                    switch( $const ) {
                        case self::ENUM_TYPE_ARCHIVE:
                        case self::ENUM_TYPE_CATEGORY:
                        case self::ENUM_TYPE_DATE:
                        case self::ENUM_TYPE_YEAR:
                        case self::ENUM_TYPE_MONTH:
                            $this->Type( self::ENUM_TYPE_ARCHIVE );
                            break;
                        case self::ENUM_TYPE_HOME:
                            $this->Type( self::ENUM_TYPE_HOME );
                            break;
                        case self::ENUM_TYPE_SINGULAR:
                        case self::ENUM_TYPE_SINGLE:
                        case self::ENUM_TYPE_PAGE:
                            if( $page_post_type == self::ENUM_TYPE_PAGE ) {
                                $this->Type( self::ENUM_TYPE_PAGE );
                            }else{
                                $this->Type( self::ENUM_TYPE_SINGULAR );
                            }
                            break;
                        default :
                            $this->Type($const);
                            break;
                    }
                }
            }
        }

        return $this->Type();
    }


}