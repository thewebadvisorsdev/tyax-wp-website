<?php

namespace WPX\HTML;

use AUX\Utils\Log;
use AUX\Utils\Debug;
use AUX\Utils\Text;

class Element {
    protected static $template;
    protected static $tag;
    protected $render_engine;
    protected $element_attributes_string;
    protected $element_attributes   = [];
    protected $element_classes      = [];

    public function __get($key) {
        return array_key_exists($key, $this->element_attributes) ? $this->element_attributes[$key] : null;
    }

    public function __set($key, $value = null){
        $this->element_attributes[$key] = $value;
    }

    public function __construct( array $attributes = null ) {
        if( null != $attributes && !empty($attributes) ) {
            $this->element_attributes = array_merge( [], $this->element_attributes, $attributes );
        }
    }

    public function __isset($key) {
        return property_exists($this,$key) ? $this->{$key} : null;
    }

    // ---------------------------------------------------------- //
    // SUGAR
    // ---------------------------------------------------------- //
    public function attributes( $attributes = [], $overwrite_all_attributes = true ) {
        if( !empty( $attributes ) ) {
            $this->element_attributes =  $overwrite_all_attributes ? $attributes : array_merge([], $this->element_attributes, $attributes);
            $this->init();
        }
        return $this->element_attributes;
    }

    public function template( $template ) {
        static::$template = $template;
    }

    /**
     * Shorthand for getting or setting attributes and values
     * 
     * @param   String    $key    The attribute name
     * @param   String    $value  The attribute value
     * @return  String|Mixed      Returns the attribute value if not value is specified.
     */
    public function attr( $key, $value = null ) {
        if( null != $value ) {
            $this->element_attributes[$key] = $value;
            return $this;
        }
        return array_key_exists($key, $this->element_attributes) ? $this->element_attributes[$key] : null;
    }

    /**
     * Add a css class
     * 
     * @param   String    $class    The class name to add
     * @return  Element Returns the current instance for fluent method chaining.
     */
    public function addClass($class) {

        if ( preg_match('/\s/',$class) ) {
            $classes = explode(' ', trim($class) );
            $this->element_classes = array_merge([], $this->element_classes, $classes);
        }else{
            array_push($this->element_classes, $class);
        }
        $this->element_classes = array_unique($this->element_classes);

        
        return $this;
    }

    public function addClasses() {
        $this->element_classes = array_merge([], $this->element_classes, func_get_args());
        $this->element_classes = array_unique($this->element_classes);        
        return $this;
    }

    /**
     * Remove a css class
     * 
     * @param String $class The class to remove.
     * @return  Element Returns the current instance for fluent method chaining.
     */
    public function removeClass($class) {

        if( in_array($class,$this->element_classes) ) {
            unset( $this->element_classes[$class] );
            $this->element_classes = array_values($this->element_classes);
        }

        return $this;
    }

    public function data() {
        try {
            $data = [
                'tag'               => static::$tag,
                'id'                => $this->attr('id'),
                'url'                => $this->attr('url'),
                'class'             => empty($this->element_classes) ? null : implode(' ', $this->element_classes),
                'attributes'        => $this->arrayToAttributes($this->attributes())
            ];
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }
        return $data;
    }

    public function render( $render_engine ) {
        $twig       = $render_engine;
        $template   = static::$template;
        if( $template ) {
            try {
                $element    = $twig->load($template);
                $output     = $element->render($this->data());
                return $output;
            } catch( \Exception $e ) {
                if( defined('WP_DEBUG') && WP_DEBUG ) {
                    Debug::Dump($e->getMessage());
                    Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
                }else{
                    Log::trace($e->getMessage(), $this->debug_string_backtrace());
                }
            }

        }else{
            throw new \Exception("Unable to render control without a template.");
        }

        return null;
    }

    // ---------------------------------------------------------- //
    // HELPER METHODS
    // ---------------------------------------------------------- //
    protected function arrayToAttributes( $attributes ) {
        $compiled = [];
        foreach( $attributes as $key => $value ) {
            $val = is_object( $value ) ? get_object_vars($value) : $value;
            if( false !== strpos($key, 'data-') ) {
                $val = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
            }else{
                $val = is_array($value) ? implode(' ', $val) : $val;
            }
            $attr = sprintf('%s="%s"', trim($key), trim($val) );
            array_push($compiled, $attr);
        }

        return join(' ', $compiled);
    }

    protected function slugify($string){
        return Text::slugify($string);
    }

    protected function debug_string_backtrace() { 
        ob_start(); 
        debug_print_backtrace(); 
        $trace = ob_get_contents(); 
        ob_end_clean(); 

        // Remove first item from backtrace as it's this function which is redundant. 
        $trace = preg_replace ('/^#0\s+' . __FUNCTION__ . "[^\n]*\n/", '', $trace, 1); 

        // Renumber backtrace items. 
        $trace = preg_replace ('/^#(\d+)/me', '\'#\' . ($1 - 1)', $trace); 

        return $trace; 
    }
}