<?php

namespace WPX\Decorator;

use AUX\Utils\Log;
use AUX\Utils\Debug;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WPX\ModelAdminCCC_Plugin;
use WPX\WPAPP;

/**
 * A thin wrapper (Decorator) for any Spot\Entity class or subclass that provides
 * helper methods not natively available on the Entity.
 */
class EntityDecorator extends EntityDecoratorBase {
    protected static $id_not_required    = ['index','add'];

    // ---------------------------------------------------------- //
    // CONFIG OPTIONS FOR AJAX POSTS
    // ---------------------------------------------------------- //
    /**
     * A configuration array used for AJAX posts to WordPress' API.
     * Since all config options are required, the array is validated against
     * a known list of fields and their expected/allowed types.
     * 
     * @return String|Array Returns and JSON encoded string, or an array if $as_array is defined
     */
    public function getAJAXRequestConfig($action = 'submit', $user_public_key = null, $as_array = false) {
        try {
            $reg                = ModelAdminCCC_Plugin::Registry();
            $private_key        = file_get_contents(APP_PATH . '.wpxapitoken');
            $public_key         = null == $user_public_key ? ModelAdminCCC_Plugin::getAPIKey() : $user_public_key;
            $nonce              = hash_hmac('sha256', $private_key . $public_key . getenv('NONCE_SALT') , $private_key);

            if( !preg_match('/\S/', $private_key) ) {
                Log::log('Unable to generate JSON config data. Missing private key.', Log::LOG_TYPE_ERROR);
                throw new \Exception('Unable to generate JSON config data.', 0);
            }
        
            if( !preg_match('/\S/', $public_key) ) {
                Log::log('Unable to generate JSON config data. Missing public key.', Log::LOG_TYPE_ERROR);
                throw new \Exception('Unable to generate JSON config data.', 1);
            }

            $entity_pk      = $this->entity->primaryKeyField();
            $entity_class   = $this->getEntityClass();
            $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );
            $entity         = $this->entity;
            $relations      = $entity->relations($entity_mapper, $entity);

            $entity_relations = [];
            foreach( $relations as $key => $relation ) {
                $rel_class  = $relation->entityName();
                $rel_ent    = (new $rel_class());
                $entity_relations[] = [
                    'relation'              => $key,
                    'relation_type'         => (new \ReflectionClass($relation))->getShortName(),
                    'relation_entity'       => get_class($entity),
                    'relation_id'           => $entity->{$entity_pk},
                    'through_entity'        => method_exists($relation, 'throughEntityName') ? $relation->throughEntityName() : null,
                    'entity'                => $relation->entityName(),
                    'entity_primarykey'     => $rel_ent->primaryKeyField(),
                    'foreign_key'           => $relation->foreignKey(),
                    'local_key'             => $relation->localKey(),
                    'identity_value'        => $relation->identityValue()
                ];
            }

            $config = [
                'data'          => json_encode([$entity_pk => $this->entity->{$entity_pk}]), 
                '_wpxnonce_'    => $nonce,
                '_wpxtoken_'    => $public_key,
                '_class_'       => $this->getEntityClass(false),
                '_entity_'      => $this->getEntityClass(),
                '_primarykey_'  => $entity_pk,
                '_action_'      => $action,
                '_relations_'   => $entity_relations ?? null
            ];

            $resolver = $this->setAJAXRequestConfigDefaults();
            $options = $resolver->resolve($config);

            return $as_array ? $options : json_encode($options);
        }catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                // Debug::Dump( sprintf("%s (%s)", $e->getMessage(), $e->getCode()) );
            }
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString() , Log::LOG_TYPE_ERROR);
        }

    }

    protected function get_calling_class() {

        //get the trace
        $trace = debug_backtrace();
    
        // Get the class that is asking for who awoke it
        $class = $trace[1]['class'];
    
        // +1 to i cos we have to account for calling this function
        for ( $i=1; $i<count( $trace ); $i++ ) {
            if ( isset( $trace[$i] ) ) // is it set?
                 if ( $class != $trace[$i]['class'] ) // is it a different class
                     return $trace[$i]['class'];
        }
    }

    /**
     * Creates an new OptionsResolver object, and validates its values.
     * 
     * @see EntityDecorator::getAJAXRequestConfig
     * @return OptionsResolver Returns and OptionsResolver instance
     */
    protected function setAJAXRequestConfigDefaults() {
        $resolver = new OptionsResolver();
        $defaults = [
            'data'          => null, 
            '_wpxnonce_'    => null,
            '_wpxtoken_'    => null,
            '_class_'       => null,
            '_entity_'      => null,
            '_primarykey_'  => null,
            '_action_'      => null,
            '_relations_'   => null
        ];
        
        $resolver->setDefaults($defaults);
        $resolver->setRequired(array_keys($defaults));

        $resolver->setAllowedTypes('data', ['string','array','null']);
        $resolver->setAllowedTypes('_wpxnonce_', ['string','null']);
        $resolver->setAllowedTypes('_wpxtoken_', ['string','null']);
        $resolver->setAllowedTypes('_class_', ['string','null']);
        $resolver->setAllowedTypes('_entity_', ['string','null']);
        $resolver->setAllowedTypes('_primarykey_', ['string','null']);
        $resolver->setAllowedTypes('_action_', ['string','null']);
        $resolver->setAllowedTypes('_relations_', ['array','null']);

        return $resolver;
    }    

}