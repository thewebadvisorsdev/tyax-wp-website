<?php

namespace WPX\Decorator;

use anytizer\pluralizer;
use AUX\Utils\Text;
use WPX\WPAPP;
use Spot\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AUX\HTTP\RestUtils;
use AUX\Utils\Log;
use AUX\Utils\Debug;

/**
 * A thin wrapper (Decorator) for any Spot\Entity class or subclass that provides
 * helper methods not natively available on the Entity.
 */
abstract class EntityDecoratorBase {
    protected $entity;
    protected $uri_base;
    protected static $id_not_required= [];
    protected $__config_resolver;

    public function __get($index) {
        return $this->entity->{$index};
    }

    public function __set($key, $value) {
        $this->entity->{$key} = $value;
    }    

    public function __construct( $entity, string $uri_base = null) {

        if( !(is_subclass_of($entity, 'Spot\Entity') ) ) {
            throw new \Exception( 'Must implement EntityInterface');
        }
        
        $this->entity = $entity;
        $this->uri_base = $uri_base;

    }

    public function __isset($key) {
        return isset($this->entity->{$key});
    }

    public function __call($prop_or_method,$params){
        try {
            if( method_exists($prop_or_method, $this->entity) ) {
                return call_user_func_array([$this->entity,$prop_or_method],$params);
            }else if( property_exists($this, $prop_or_method) ) {
                return $this->{$prop_or_method};
            }
        }catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString() , Log::LOG_TYPE_ERROR);
        }
        return null;
    }

    // ---------------------------------------------------------- //
    // HELPER METHODS (PUBLIC)
    // ---------------------------------------------------------- //
    /**
     * Export the encapsulated Entity
     * 
     * @return Entity Returns the encapsulated Entity
     */
    public function export() {
        return $this->entity;
    }

    public function getPrimaryKeyField() {
        return $this->entity->primaryKeyField();
    }

    /**
     * Get the primary field to be used to output content and UI messages.
     * 
     * @return String   A display friendly string.
     */
    public function getPrimaryDisplayField() {
        $field = ( !defined( get_class($this->entity).'::DISPLAY_FIELD' ) ) ? ucwords($this->getControllerClass()) : ($this->entity)::DISPLAY_FIELD;
        return $field;
    }

    /**
     * Gets a heading defined in the Entity for use in relations views.
     * 
     * @return String   A display friendly heading.
     */
    public function getRelationHeading() {
        $heading = ( !defined( get_class($this->entity).'::RELATION_HEADING' ) ) ? 'Manage Related Entity' : ($this->entity)::RELATION_HEADING;
        return $heading;
    }

    /**
     * Get the plural or singular form of the Entity class name.
     * 
     * @param String    $type       Accepts: `plural` or `singular`
     * @param Bool      $title_case Return the entity class name in title case (capitalized)
     * @return String   Returns the plural or singular form of the Entity class name.
     */
    public function getEntityTag( $type, $title_case = false ) {
        $entity_tags = $this->entity->entity_tag ?: null;
        if( null == $entity_tags ) {
            $entity_class = $this->getEntityClass(false);
            $pluralizer = new pluralizer();
            $pluralized_entity = $pluralizer->pluralize($entity_class);
            $feaux_tags = ['singular' => $entity_class, 'plural' => $pluralized_entity ];
            return $title_case ? ucwords( $feaux_tags[ $type ] ): strtolower( $feaux_tags[ $type ] );
        }

        return $title_case ? ucwords(strtolower($entity_tags[ $type ])) : strtolower($entity_tags[ $type ]);
    }    

    /**
     * Alias for EntityDecorator::LinkByPage(). Requires only the $action, assuming that
     * the current page controller is the target controller for the action.
     * 
     * @param String    $action     A valid action found on the page controller
     */
    public function Link($action = null) {
        $request = Request::createFromGlobals();
        return $this->LinkByPage( $action, $request->query->get('page') );
    }

    /**
     * Get a link for this data model by page controller.
     * 
     * @param String    $page_name  The slug for a page controller, e.g. `model-admin-books`
     * @param String    $action     A valid action found on the page controller
     */
    public function LinkByPage($action = 'index', $page_name = null) {
        $query_string     = [
            'page'      => $page_name ?: sprintf("model-admin-%s", $this->getEntityTag('plural')),
            'action'    => $action
        ];

        if( !in_array($action, self::$id_not_required) ) {
            $query_string['id'] = $this->id;
        }

        $q = [];
        foreach( $query_string as $key => $value ) {
            array_push($q,strtolower( sprintf("%s=%s", $key, $value) ) );
        }
        $uri = sprintf("admin.php?%s",implode('&', $q));

        return $uri;
    }

    public function getAPIEndpoint($endpoint, array  $params = null) {
        $result = RestUtils::getAPIEndpoint($endpoint, $params);//call_user_func_array(['RestUtils','getAPIEndpoint'], func_get_args());
        return $result;
    }

    /**
     * Overloads the Entity::data() method to merge field definitions
     * with values in the data model. Applies some basic sanitization
     * and normalization for NULL values before injecting values
     * back into the encapsulated Entity.
     * 
     * @param Array $data   An associative array containing values mapped to this entity
     * @return EntityManager|Array Returns the current instance if $data is supplied, or a populated array 
     */
    public function data(array $data = []) {

        static $__data = [];

        // Input filtering
        if( !empty( $data ) ) {            
            $data = $this->sanitize($data);
            
            $fields = array_keys($this->entity->fields());
            foreach( $data as $key => $value ) {
                if( in_array( $key, $fields) ) {
                    $this->entity->set($key, $value, true);
                }
            }
            
            return $this;
        }

        // Output
        $__data = $this->entity->data();

        // Ensure utf-8 encoding for transport
        array_walk_recursive($__data, function (&$value) {
            if( !is_object( $value ) ) {
                $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
            }
        });   

        return $__data;
    }

    /**
     * Basic string sanitization.
     * 
     * @link https://stackoverflow.com/a/45045823
     * 
     * @param Array $array Data contained in this Entity.
     * @param Boolean $filter Optional filtering to be applied beyond enforcing NULL
     */
    protected function sanitize(array &$array, $filter_var = false) {
        array_walk_recursive($array, function (&$value) use ($filter_var) {
            if( !is_object( $value ) ) {
                if ($filter_var) {
                    $value = filter_var($value, FILTER_SANITIZE_STRING|FILTER_FLAG_NO_ENCODE_QUOTES);                    
                }
                // $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
                // Log::trace(__METHOD__, $value);
                $value = (trim($value) === '') ? NULL : $value;
                $value = $this->verifyDateString( $value ) ? new \DateTime($value) : $value;
            }
        });
        return $array;
    }

    /**
     * Validate date format Y-m-d (no timezeone)
     * @link https://stackoverflow.com/a/13194442
     * 
     * @param String $date The date value given as a string.
     * @return Boolean Returns false if checkdate() fails, true if otherwise
     */
    protected function verifyDateString($date) {
        $split  = [];
        $result = false;
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $split)) {
            $result = checkdate($split[2], $split[3], $split[1]);
        }
        return $result;
    }

    /**
     * Merge Entity fields and values together as defintions
     * for use with WPX\Form and WPX\FormField.
     * 
     * @return Array    Returns this Entity's fields hydrated with corresponding values.
     */
    public function fields() {
        $fields = $this->entity->fields();
        $data   = $this->data();

        foreach( $data as $key => $value ) {
            if( !!$value ) {
                $fields[$key]['value'] = $value;
            }
        }

        return $fields;
    }
    
    // ---------------------------------------------------------- //
    // HELPER METHODS (PROTECTED)
    // ---------------------------------------------------------- //
    /**
     * Checks for the presence of `sort_order`
     * as a defined field to determine if an Entity is sortable.
     * 
     * @return Bool Returns true if the Entity supports sorting, false if otherwise
     */
    public function supportsSortable() {
        $sortable = in_array('sort_order', array_keys($this->entity->fields()));
        return $sortable;
    }

    /**
     * Checks for the presence of `datedeleted` 
     * as a defined field to determine if an entity can be marked as deleted
     * without actually deleting the record. This is considered a "soft delete".
     * 
     * * @return Bool Returns true if the Entity supports soft deletion, false if otherwise
     */
    public function supportsSoftDelete() {
        $sortable = in_array('deleted_on', array_keys($this->entity->fields()));
        return $sortable;
    }

    /**
     * Extracts the Entity class name from a fully namespaced string.
     */
    protected function getControllerClass() {
        $class_name = explode('\\', get_class($this->entity));
        $class_name = array_pop($class_name);
        return $class_name;
    }

    /**
     * Alias for EntityDecorator::getControllerClass()
     * 
     * @see EntityDecorator::getControllerClass
     */
    public function getEntityClass($include_namespace = true, $pluralize = false) {
        $class_name = $include_namespace ? get_class($this->entity) : $this->getControllerClass();
        if( $pluralize ) {
            $pluralizer = new pluralizer();
            $class_name = $pluralizer->pluralize($class_name);
        }
        return $class_name;
    }

    public function getOwnerID() {
        $entity = $this->entity;
        return array_key_exists('owner_id', $entity->fields()) ? $entity->owner_id : null;
    }

    public function getFieldValueByName( $field_name ) {
        $entity = $this->entity;
        return array_key_exists($field_name, $entity->fields()) ? $entity->{$field_name} : null;
    }

    /**
     * Get an array of fields suitable for display purposes
     * @return Array An array of Entity fields 
     */
    public function getDisplayFields() {
        $display_fields = call_user_func_array([$this->entity, 'getDisplayFields'], func_get_args());
        $entity_fields = $this->fields();
        $typed_display_fields = [];
        foreach( $display_fields as $key ) {
            if( array_key_exists($key, $entity_fields) ) {
                $typed_display_fields[ $key ] = $entity_fields[ $key ]['type'] ?: 'text';
            }
        }

        return $typed_display_fields;
    }

    /**
     * Gets a collection of common class variables and constants that are used in templates.
     * 
     * @return Array A collection of class properties and constants
     */
    public function getEntityClassInfo() {

        $class_info = null;
        
        try {
            $mapper         = WPAPP::WPAPI()->Mapper( $this->getEntityClass() );
            $relations      = $this->entity->relations($mapper, $this->entity);
    
            $class_info = [
                'class_name'            => $this->getEntityClass(false),
                'class_fullname'        => $this->getEntityClass(),
                'display_fields'        => $this->getDisplayFields(),
                'display_field'         => $this->entity::DISPLAY_FIELD,
                'relation_heading'      => $this->entity::RELATION_HEADING,
                'relations'             => array_keys($relations),
                'json'                  => $this->getAJAXRequestConfig()
            ];
        }catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump($e->getMessage());
            }
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }
        

        return $class_info;
    }

    /**
     * Get an array of defined Entity constants
     * 
     * @return Array    An array of defined constants.
     */
    public function getENUMS() {
        static $class_constants = [];
        
        if( empty($class_constants) ) {
            $ref = new \ReflectionClass( get_class($this->entity) );
            $class_constants = $ref->getConstants();
        }
 
        return $class_constants ;
    }    

    // ---------------------------------------------------------- //
    // CONFIG OPTIONS FOR AJAX POSTS
    // ---------------------------------------------------------- //
    /**
     * A configuration array used for AJAX posts to WordPress' API.
     * Since all config options are required, the array is validated against
     * a known list of fields and their expected/allowed types.
     * 
     * @return String|Array Returns and JSON encoded string, or an array if $as_array is defined
     */
    abstract public function getAJAXRequestConfig($action = 'submit', $as_array = false);

    /**
     * Creates an new OptionsResolver object, and validates its values.
     * 
     * @see EntityDecorator::getAJAXRequestConfig
     * @return OptionsResolver Returns and OptionsResolver instance
     */
    abstract protected function setAJAXRequestConfigDefaults();

}