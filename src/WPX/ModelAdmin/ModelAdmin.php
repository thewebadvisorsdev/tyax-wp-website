<?php

namespace WPX\ModelAdmin;

use anytizer\pluralizer;
use WPX\WPAPP;
use WPX\ModelAdminCCC_Plugin;
use WPX\View\View;
use WPX\Controller\WPXAdminPageController;
use WPX\Decorator\EntityDecorator;
use WPX\Controls\TabControl;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\HTTP\RestUtils;
use AUX\Utils\Text;
use Symfony\Component\Inflector\Inflector;

/**
 * @author Chris Murphy
 * @version 0.0.1
 */

class ModelAdmin extends WPXAdminPageController {

    /**
     * A generic table listing of all records for an Entity 
     */
    public function index() {
        // Tabbed Navigation control
        $tabs = $this->buildTabNavigation([
            'index' => [ 'label' => sprintf('All %s', ucwords(static::$entity_tag['plural'])), 'url' => "%s", 'view' => "%s_%s" ],
            'add' => [ 'label' => sprintf('Add %s',ucwords(static::$entity_tag['singular'])), 'url' => "%s", 'view' => "%s_%s" ]
        ]);

        $endpoint           = ModelAdminCCC_Plugin::getRESTEndpoint();
        $api_key            = ModelAdminCCC_Plugin::getAPIKey();
        $entity_instance    = new EntityDecorator( new static::$entity );
        $class_info         = $entity_instance->getEntityClassInfo();
        $rest               = [
            'endpoint'          => $endpoint,
            'api_key'           => $api_key,
            'request_format'    => $entity_instance->getAJAXRequestConfig('submit',$api_key, true),
        ];
        
        if ( $entity_instance->supportsSortable() ) {
            $sortable = [
                'sortable'              => true,
                'sortable_posturl'      => RestUtils::getAPIEndpoint($endpoint, [$this->getControllerClass(),'sort']),
                'sortable_ajaxconfig'   => $entity_instance->getAJAXRequestConfig('sort'),
            ];

            $class_info = array_merge([], $class_info, $sortable);
        }        

        $context    = [
            'request'       => $this->request,
            'messages'      => $this->getSystemMessages(),
            'collection'    => [
                'collection_name' => $entity_instance->getEntityTag('plural'),
                'collection' => $this->entity_collection,
            ],
            'add_link'      => $this->buildNavLink('add'),
            'nonce'         => wp_create_nonce(),
            'navigation'    => $tabs->render(WPAPP::inst()->Twig()),
            'entity'        => $class_info,
            'options'       => [
                'entity_count' => $this->entity_collection ? count($this->entity_collection) : 0,
                'rest'  => $rest
            ],
            'search_options' => [
                'mode'          => static::SEACH_MODE_FILTER,
                'entity_tag'    => $entity_instance->getEntityTag('plural')
            ]
        ];        
        
        $this->view_data = array_merge($this->view_data, $context);

        $this->render();
    }
    
    /**
     * Add or Update a given Entity.
     * 
     * This method must be overloaded in a child class
     */
    public function edit() {
        // Overload this method in child class
    }

    /**
     * Forward any `add` actions on this controller to `edit`
     */
    public function add() {
        return $this->edit();
    }  
    // ---------------------------------------------------------- //
    // RELATIONS
    // ---------------------------------------------------------- //
    /**
     * Generate a Tab Control instance for each relation found.
     */
    public function getRelationsNavigation( array $relations, $entity, array $label_overrides = null ) {
        
        $endpoint   = ModelAdminCCC_Plugin::getRESTEndpoint();
        $tabs       = new TabControl();
        
        if( !!$entity->id ) {

            $tabs->addClasses('wpx-navtabs_panels');
            $tabs->addTab( sprintf("Edit %s", ucwords(static::$entity_tag['singular'])), $entity->Link('edit') , ['id' => 'tab_default', 'data-tabindex' => 'default' ]);
            
            if( !empty( $relations ) ) {
                foreach( $relations as $relation ) {
                    $url                            = RestUtils::getAPIEndpoint($endpoint,['relations','id',$entity->id]);
                    $attributes                     = [];
                    $attributes['data-tabindex']    = $relation;
                    $attributes['data-relation']    = $relation;
                    $attributes['data-posturl']     = $url;
                    $attributes['data-postdata']    = $entity->getAJAXRequestConfig('relations');

                    if( is_array($label_overrides) ) {
                        if( array_key_exists($relation,  $label_overrides) ) {
                            $pluralized_entity = $label_overrides[ $relation ];
                        }else{
                            $pluralized_entity = Inflector::pluralize($relation);
                        }
                    }else{
                        $pluralized_entity = Inflector::pluralize($relation);
                    }
                    
                    $tabs->addTab(ucwords($pluralized_entity), $url , $attributes);
                    
                }
            }

        }else{
            $label  = sprintf('All %s', ucwords(static::$entity_tag['plural']));
            $url    = $this->buildNavLink('index');
            $tabs->addTab($label, $url);
        }

        return $tabs;
    }    

    # +------------------------------------------------------------------------+
    # RENDER
    # +------------------------------------------------------------------------+
    protected function render() {
        // View Handling
        $class_name     = $this->getControllerClass( get_called_class() );
        $action         = $this->view_data['action'];

        // Template Override: Make Edit and Add the same
        switch( $action ) {
            case 'add' :
            case 'edit' :
                $action = 'edit';
                break;
            default :
                $action = $action;
        }

        $view_name      = sprintf("%s_%s", 'ModelAdmin', $action );
        $template       = sprintf("%s.html.twig", $view_name );

        $this->view_data = array_merge($this->view_data, ['view_layout' => $template]);
        $this->view_data = array_merge($this->view_data, ['view_name' => $view_name]);        
        $this->view_data = array_merge($this->view_data, ['class_name' => $class_name]);

        $this->view = new View(WPAPP::inst()->Twig(), 'page-admin', [ 'page-admin' => 'AdminPage.html.twig' ], 'AdminPage.html.twig');

        parent::render();
    }
}