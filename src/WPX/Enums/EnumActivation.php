<?php

namespace WPX\Enums;

use ReflectionClass;

abstract class EnumActivation extends EnumBase {
    const DEACTIVATE           = 0;
    const ACTIVATE             = 1;
}