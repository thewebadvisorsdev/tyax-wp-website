<?php

namespace WPX\Enums;

use ReflectionClass;

abstract class EnumStatus extends EnumBase {
    const PENDING           = 'pending';
    const DRAFT             = 'draft';
    const PUBLISH           = 'publish';
    const ARCHIVE           = 'archive'; 
}