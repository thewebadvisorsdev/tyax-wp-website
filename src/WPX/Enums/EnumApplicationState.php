<?php

namespace WPX\Enums;

use AUX\Utils\Log;
use AUX\Utils\Text;
use ReflectionClass;

abstract class EnumApplicationState extends EnumBase {
    const INCOMPLETE    = 0;
    const COMPLETE      = 1;
}