<?php

namespace WPX\Enums;

use ReflectionClass;

abstract class EnumEnvironment extends EnumBase {
    const DEVELOPMENT   = 'development';
    const STAGING       = 'staging';
    const PRODUCTION    = 'production';
}