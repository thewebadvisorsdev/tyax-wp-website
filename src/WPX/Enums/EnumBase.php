<?php

namespace WPX\Enums;

use AUX\Utils\Log;
use AUX\Utils\Text;
use ReflectionClass;

abstract class EnumBase {

    protected static $const_cache = null;

    public static function getConstants() {
        if (self::$const_cache == null) {
            self::$const_cache = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$const_cache ?? [])) {
            $reflect = new ReflectionClass($calledClass);
            self::$const_cache[$calledClass] = $reflect->getConstants();
        }
        return self::$const_cache[$calledClass];
    }

    public static function getEnums() {
        $constants = self::getConstants();
        $constants = array_flip($constants);
        array_walk($constants, function($v, $k) use(&$constants){
            $constants[ $k ] = Text::camelCaseToSentenceCase(strtolower($v));
            return true;
        },ARRAY_FILTER_USE_BOTH);
        return $constants;
    }    

    public static function getEnumsDatalist() {
        $constants = self::getConstants();
        $constants = array_flip($constants);
        $enums = [];
        foreach( $constants as $k => $v ) {
            array_push($enums,[
                'value' => $k,
                'label' => Text::camelCaseToSentenceCase(strtolower($v))
            ]);
        }
        return $enums;
    }   

    public static function isValidName($name, $strict = false) {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true) {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }    
}