<?php

namespace WPX;

use AUX\Utils\Debug;
use AUX\Exception;
use AUX\Utils\Log;
use WPX\Entity\WPEntity;


class WPOptions {
    private static $instance;
    protected static $options = [];

    public static function getOption( $option ) {
        $value = self::$options[$option];
        return ( NULL != $value ) ? $value : null;
    }

    public static function setOption( $option, $value) {
        
        if( function_exists('sanitize_option') ) {
            $value = sanitize_option( $option, $value );
        }

        self::$options[$option] = $value;
        return self::getOption($option);
    }

    public static function Options() {
        return self::$options;
    }

    public static function getInstance() {
        if (self::$instance == null) {
            $class = __CLASS__;
            self::$instance = new $class();     
        }
        
        return self::$instance;
    }
    
    private function __construct() {
        $this->init();
    }

    private function init() {
        try {
            self::$options = [];
            
            // Load WP Options
            $wp_options_mapper = WPAPP::WPAPI()->content( WPEntity::ENUM_TYPE_OPTION );
            if( $options = $wp_options_mapper->all()->where(['autoload' => 'yes']) ) {
                
                foreach($options->toArray() as $key => $option) {
                    $v      = (object) $option;
                    $name   = $v->option_name;
                    $value  = $v->option_value;
    
                    self::setOption($name, $value);
                }
            }

            // Load Application options
            if( class_exists('WPX\\Entity\\Setting') ) {
                if( null != $app_options_mapper = WPAPP::WPAPI()->Mapper( 'WPX\\Entity\\Setting' ) ) {
    
                    if( $app_options = $app_options_mapper->all() ) {
        
                        foreach($app_options->toArray() as $key => $option) {
                            $v      = (object) $option;
                            $name   = $v->key;
                            $value  = $v->value;
            
                            self::setOption($name, $value);
                        }
                    }

                }
            }

        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }
        }
    }
}