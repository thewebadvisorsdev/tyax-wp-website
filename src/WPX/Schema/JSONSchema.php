<?php

namespace WPX\Schema;

use \Exception;
use AUX\Utils\Log;
use \Error;

/**
 * Generate a JSON LD reprensetation of a data array.
 * 
 * @example
 *  { 
 *      "@context": "http://schema.org", 
 *      "@type": "BlogPosting",
 *      "headline": "14 Ways Json Can Improve Your SEO",
 *      "alternativeHeadline": "and the women who love them",
 *      "image": "http://example.com/image.jpg",
 *      "award": "Best article ever written",
 *      "editor": "John Doe", 
 *      "genre": "search engine optimization", 
 *      "keywords": "seo sales b2b", 
 *      "wordcount": "1120",
 *      "publisher": "Book Publisher Inc",
 *      "url": "http://www.example.com",
 *      "datePublished": "2015-09-20",
 *      "dateCreated": "2015-09-20",
 *      "dateModified": "2015-09-20",
 *      "description": "We love to do stuff to help people and stuff",
 *      "articleBody": "You can paste your entire post in here, and yes it can get really really long.",
 *        "author": {
 *         "@type": "Person",
 *         "name": "Steve"
 *      }
 *  }
 * 
 *  { 
 *      "@context": "http://schema.org", 
 *      "@type": "WebSite", 
 *      "url": "http://www.example.com/", 
 *      "name": "Generally the title",
 *      "author": {
 *          "@type": "Person",
 *          "name": "Jane Doe"
 *      },
 *      "description": "Any sort of description, I'd keep it short",
 *      "publisher": "publisher name",
 *      "potentialAction": { 
 *          "@type": "SearchAction", 
 *          "target": "http://www.example.com/?s={search_term}", 
 *          "query-input": "required name=search_term" 
 *      } 
 *  } 
 * 
 * Note: Output must be enclosed in <script type="application/ld+json"></script> if generating a json string
 * 
 * @return  string|array  A JSON string representing this model.
 */
class JSONSchema {

    const ENUM_TYPE_WEBSITE     = 'WebSite';
    const ENUM_TYPE_BLOGPOSTING = 'BlogPosting';
    const ENUM_TYPE_OHER        = 'Other';

    protected $schema_markup = '<script type="application/ld+json">%s</script>';
    protected $data;
    protected $schema_collection = [
        'BlogPosting' => [
            "@context"              => "http://schema.org", 
            "@type"                 => "BlogPosting",
            'headline'              => null,
            'alternativeHeadline'   => null,
            'image'                 => null,
            'award'                 => null,
            'editor'                => null,
            'genre'                 => null,
            'keywords'              => null,
            'publisher'             => null,
            'url'                   => null,
            'dateCreated'           => null,
            'datePublished'         => null,
            'dateModified'          => null,
            'description'           => null,
            'articleBody'           => null,
            'author'                => [
                "@type" => "Person",
                "name" => null
            ]
        ],
        'WebSite' => [
            "@context"              => "http://schema.org", 
            "@type"                 => "WebSite",
            'name'                  => null,
            'author'                => [
                "@type" => "Person",
                "name" => null
            ],
            'description'           => null,
            'publisher'             => null,
            "potentialAction" => [ 
                    "@type"         => "SearchAction", 
                    "target"        => "http://www.example.com/?s={search_term}", 
                    "query-input"   => "required name=search_term" 
            ]
        ]
    ];

    public function __construct( array $data ) {
        $this->data = $data;
    }

    public function addSchema( array $data, $type = self::ENUM_TYPE_OHER ) {
        if( !array_key_exists($type, $this->schema_collection) ) {
            $this->schema_collection[ $type ] = $data;
        }else{
            throw new \Error("Cannot redefine schema for ${type}.");
        }
    }

    public function render( string $type = self::ENUM_TYPE_WEBSITE, bool $as_json = true ) {

        if( empty($this->data) ) {
            throw new Exception("Unable to render JSON-LD schema without data.");
        }

        if( !array_key_exists($type, $this->schema_collection) && !isset($this->schema_collection[$type ]) ) {
            $s = htmlentities2($type);
            throw new Exception("Schema, '$s', does not exist.");
        }

        $schema         = $this->schema_collection[$type];
        $schema_keys    = array_keys( $schema );

        // Remove potentialAction for website schemas
        if( $type == self::ENUM_TYPE_WEBSITE ) {
            if( !array_key_exists('potentialAction', $this->data) ) {
                unset($schema['potentialAction']);
            }
        }

        
        foreach( $this->data as $key => $value ) {
            if( in_array( $key, $schema_keys ) ) {
                if( $key == 'author' ) {
                    $schema[ $key ]['name'] = $value;
                }else{
                    $schema[ $key ] = $value;
                }
            }
        }

        $schema = array_filter($schema);

        return $as_json ? sprintf($this->schema_markup,json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE)) : $schema;
    }
}