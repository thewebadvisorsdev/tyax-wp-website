<?php

namespace WPX\Controller;

use AUX\Utils\Log;
use \Exception;
use AUX\Utils\Debug;
use WPX\WPAPP;
use WPX\View\View;

/**
 * A basic controller class where business logic can be processed,
 * and views are handed through the Twig templating engine.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 * 
 * @see https://developer.wordpress.org/files/2014/10/Screenshot-2019-01-23-00.20.04.png
 */
class Controller implements IController {

    public static $allowed_actions = [];

    protected $properties = [];

    protected $view_data = [];

    protected $view;

    /**
     * Set view data by key
     * 
     * @param key string
     * @param value mixed
     */
    public function setViewData( $key, $value ) {
        $this->view_data[$key] = $value;
    }

    /**
     * Get view data by key
     * 
     * @param key string
     * @return mixed
     */
    public function getViewData( $key ) {
        return $this->view_data[$key];
    }    

    /**
     * Gets or sets the view data
     * 
     * @param array $data An array containing all of the data needed to render the view
     * @return array
     */
    public function ViewData( array $data = null ) {
        if( NULL != $data ) {
            $this->view_data = $data;
        }

        return $this->view_data;
    }

    /**
     * Gets or sets the View instance
     * 
     * @param View $view
     * @return View
     */
    public function View( View $view = null ) {
        if( NULL != $view ) {
            $this->view = $view;
        }

        return $this->view;
    }

    public function __get($property) {
        return $this->properties[$property];
    }

    public function __set($property,$value) {
        $this->properties[$property] = $value;
        return $this->properties[$property];
    }

    # +------------------------------------------------------------------------+
    # CONTROLLER ACTIONS
    # +------------------------------------------------------------------------+
    /**
     * The default action for this Controller
     */
    public function index() {

    }

    # +------------------------------------------------------------------------+
    # HTTP RESPONSES
    # +------------------------------------------------------------------------+
    /**
     * Overrides the HTTP Response headers.
     */
    protected function setHTTPResponseHeaders() {

    }
    
    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     * 
     * @param Exception $e
     */
    protected function Exception( \Exception $e ) {
        
    }
}