<?php

namespace WPX\Controller;

use WPX\WPAPP;
use WPX\Entity\Image;
use WPX\Decorator\EntityDecorator;
use WPX\Controller\WPXAdminPageController;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\HTTP\HttpQueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AUX\Structs\Queue;
use AUX\HTTP\RestUtils;
use Firebase\JWT\JWT;
use Spot\Relation\HasManyThrough;
use WPX\Entity\WPEntity;
use WPX\Entity\WPPage;
use WPX\Entity\WPPost;
use WPX\Enums\EnumStatus;

class RESTController implements IController {

    const MESSAGE_TYPE_ERROR    = 'error';
    const MESSAGE_TYPE_WARNING  = 'warning';
    const MESSAGE_TYPE_NOTICE   = 'notice';
    const MESSAGE_TYPE_SUCCESS  = 'success';

    public static $model_admin  = 'WPX\ModelAdmin';
    public static $allowed_actions = [];
    protected $request;

    public function __construct() {
        $this->request = Request::createFromGlobals();
    }

    // ---------------------------------------------------------- //
    // CONTROLLER ACTIONS
    // ---------------------------------------------------------- //    
    /**
     * The default action for this Controller
     */
    public function index() {
        $this->validateAPIToken();
        $this->sendJSONResponse(['success' => true],Response::HTTP_OK);
    }

    // ---------------------------------------------------------- //
    // CRUD METHOD: POST HANDLER
    // ---------------------------------------------------------- //
    public function save($post_data) {
        
        $relation_info  = $post_data['_relation_'] ?? null;

        if( !!$relation_info ) { // Save Related Entities
            
            // Log::trace(__METHOD__, $relation_info['relation_type']);
            switch($relation_info['relation_type']) {
                case 'HasOne':
                    $this->saveHasOne( $post_data, $relation_info );
                    break;
                case 'BelongsTo':
                    $this->saveBelongsTo( $post_data, $relation_info );
                    break;
                case 'HasMany':
                    $this->saveHasMany( $post_data, $relation_info );
                    break;
                case 'HasManyThrough':
                    $this->saveHasManyThrough( $post_data, $relation_info );
                    break;
            }


        }else{ // Save non-relation Entities
            $data           = json_decode($post_data['data'], true);
            $entity_class   = $post_data['_entity_'];
            $entity_temp    = (new $entity_class());
            $entity_pk      = $entity_temp->primaryKeyField();
            $id             = $data[$entity_pk];
            $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );
            $entity         = new EntityDecorator( $id ? $entity_mapper->get($id) : new $entity_class() );

            $result         = false;
            $error_message  = null;
            $error_number   = 0;
            $display_field  = $entity->getPrimaryDisplayField();
    
            // Handle Media
            try {
    
                if( $post_data['_media_metadata_'] ?? null ) {
                    $mm = current($post_data['_media_metadata_']);
    
                    foreach( $mm as $k => $v ) {
                        $media = $this->saveMediaAsset($v, $entity);
                        if( $media ) {
                            $data[ $k ] = $media;
                        }
                    }
                    
                }
    
            } catch( \Exception $e ) {
                Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
            }
    
            try {
    
                $entity->data($data);
                $result = $entity_mapper->save($entity->export());
    
            } catch( \Exception $e ) {
                Log::log($e->getMessage() . "\n" . $e->getTraceAsString(),Log::LOG_TYPE_ERROR);
    
                $error_message  = !!$id ? sprintf("An error occurred while updating this %s.", $entity->getEntityTag('singular')) : sprintf("An error occurred while adding this %s.", $entity->getEntityTag('singular'));
                $error_message  = $error_message . "\n" . $e->getMessage();
                $error_number   = !!$id ? 4 : 3;
    
                if( strpos($e->getMessage(),'SQLSTATE[23000]') ) {
                    $error_message = sprintf("Unable to update this %s. The %s you're attempting to link is associated with another record.", $entity->getEntityTag('singular'), $entity->getEntityTag('singular'));
                }
    
                $this->sendJSONErrorResponse(['success' => 0, 'errors' => $entity->errors], new \Error($error_message, $error_number));
            }  
    
            if( !!$result || (null != $entity->id) ) {
                // SUCCESS
                $location           = null;
                $success_message    = ['success' => true];
    
                if( !$id ) {
                    $q = [
                        'page'      => $post_data['_controllerslug_'],
                        'action'    => 'index'
                    ];
                    $location = sprintf( "%s?%s", $this->getReferer(), $this->buildQueryString($q) );
                    $success_message['location'] = $location;
                    $success_message['message'] = sprintf("You added a new %s: \"%s\"",$entity->getEntityTag('singular'), stripslashes($entity->{$display_field}));
                }else{
                    $success_message['message'] = sprintf("You updated: \"%s\"", stripslashes($entity->{$display_field}));
                }      
                
                $this->sendJSONResponse($success_message);
    
            }else{
                // FAIL
                $this->sendJSONErrorResponse(['success' => 0, 'errors' => $entity->errors], new \Error($error_message, $error_number), Response::HTTP_UNPROCESSABLE_ENTITY);
            } 
    
            exit(0);
        }

    }
    
    /**
     * Save a has-one relation. This deals with saving the other side of a belongs-to relation.
     * 
     * @param Array $post_data An array containing the owner_id and primary_key of the related entities
     * @param Array $relation_info An array containing the table relationships between entities
     */
    public function saveHasOne(array $post_data, array $relation_info) {
        $data           = json_decode($post_data['data'], true);
        $save_result    = false;
        $entity_class   = $relation_info['entity'];
        $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );

        $lk             = $relation_info['local_key'];
        $fk             = $relation_info['foreign_key'];
        $id             = $data[$lk] ?: null;
        $entity         = new EntityDecorator( $id ? $entity_mapper->get($id) : new $entity_class() );

        if( array_key_exists( $lk, $entity->data() ) ) {
            $entity->{$fk} = $data[$fk];
            try {
                $save_result = $entity_mapper->save( $entity->export() );
            }catch( \Exception $e ) {
                Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            }
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Log::trace(__METHOD__, 'BelongsTo - Updates HasOne', $post_data, $relation_info, $entity->data());

        $this->sendJSONSaveResponse($relation_info['relation_type'], $save_result);

        die();        
        
    }

    /**
     * Save a belongs-to relation. This deals with saving the other side of a has-one relation.
     * 
     * @param Array $post_data An array containing the owner_id and primary_key of the related entities
     * @param Array $relation_info An array containing the table relationships between entities
     */
    public function saveBelongsTo(array $post_data, array $relation_info) {
        $data           = json_decode($post_data['data'], true);
        $save_result    = false;
        $entity_class   = $relation_info['relation_entity'];
        $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );

        $pk             = $relation_info['entity_primarykey'];
        $lk             = $relation_info['local_key'];
        $fk             = $relation_info['foreign_key'];
        $id             = $data[$pk] ?: null;
        $entity         = new EntityDecorator( $id ? $entity_mapper->get($id) : new $entity_class() );

        if( array_key_exists( $lk, $entity->data() ) ) {
            $entity->{$lk} = $data[$lk];
            try {
                $save_result = $entity_mapper->save( $entity->export() );
            }catch( \Exception $e ) {
                Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            }
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        // Log::trace(__METHOD__, 'HasOne - Updates BelongsTo', $post_data, $relation_info, $entity->{$fk}, $data[$lk], $entity->data());

        $this->sendJSONSaveResponse($relation_info['relation_type'], $save_result);

        die();        
        
    }

    /**
     * Save as has-many relation
     * 
     * @param Array $post_data An array containing the primary key and the foreign_key for the related entities
     * @param Array $relation_info An array containing the table relationships between entities
     */
    public function saveHasMany(array $post_data, array $relation_info) {
        $data           = json_decode($post_data['data'], true);
        $save_result    = false;
        $entity_class   = $relation_info['entity'];
        $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );

        $lk             = $relation_info['local_key'];
        $fk             = $relation_info['foreign_key'];
        $id             = $data[$lk] ?: null;
        $entity         = new EntityDecorator( $id ? $entity_mapper->get($id) : new $entity_class() );

        if( array_key_exists( $lk, $entity->data() ) ) {
            $entity->{$fk} = $relation_info['identity_value'];
            try {
                $save_result = $entity_mapper->save( $entity->export() );
            }catch( \Exception $e ) {
                Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            }
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Log::trace(__METHOD__, 'HasMany - Updates HasMany', $post_data, $data, $relation_info, $entity->data(), $save_result);

        $this->sendJSONSaveResponse($relation_info['relation_type'], $save_result);

        die();        
        
    }    

    /**
     * Save has-many-through relation
     * 
     * Note: Find a matching relation; if found update or use that existing entity,
     * otherwise create a new entity and save that instead. This prevents duplicating
     * records for the same relation since there's no other way to determine if a relation
     * exists outside of the `object_id` and `relation_id` or (`term_taxonomy_id`) fields.
     * 
     * @param Array $post_data An array containing the object_id and term_taxonomy_id of the related entities
     * @param Array $relation_info An array containing the table relationships between entities
     */
    public function saveHasManyThrough(array $post_data, array $relation_info) {
        $data           = json_decode($post_data['data'], true);
        $save_result    = false;

        // Log::trace(__METHOD__, $post_data, $data, is_array($data), count($data));
        if( array_key_exists('collection', $data) ) {
            $count = count($data['collection']);
            foreach( $data['collection'] as $obj_data ) {

                if( $obj_data && array_key_exists('object_id', $obj_data) && (array_key_exists('term_taxonomy_id', $obj_data) || array_key_exists('relation_id', $obj_data)) ) {
                    $entity_mapper   = WPAPP::WPAPI()->Mapper( $relation_info['through_entity'] );
                    $entity = $entity_mapper->first($obj_data);
    
                    try {
                        if( $entity && $entity->id ) {
                            array_walk($data, function($v,$k) use (&$entity) {
                                if( property_exists( $k, $entity ) ) {
                                    $entity->{$k} = $v;
                                }
                            });
                        }else{
                            $entity = $entity_mapper->build($data);
                        }
                        $save_result = $entity_mapper->save( $entity );
                    }catch( \Exception $e ) {
                        Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                    }                    

                    $count--;        

                    if( $count == 0 ) {
                        $this->sendJSONSaveResponse($relation_info['relation_type'], $save_result);
                        die();             
                    }  
                } 

            }

        }else{

            if( $data && array_key_exists('object_id', $data) && (array_key_exists('term_taxonomy_id', $data) || array_key_exists('relation_id', $data)) ) {
                
                $entity_mapper   = WPAPP::WPAPI()->Mapper( $relation_info['through_entity'] );
                $entity = $entity_mapper->first($data);

                try {
                    if( $entity && $entity->id ) {
                        array_walk($data, function($v,$k) use (&$entity) {
                            if( property_exists( $k, $entity ) ) {
                                $entity->{$k} = $v;
                            }
                        });
                    }else{
                        $entity = $entity_mapper->build($data);
                    }
                    $save_result = $entity_mapper->save( $entity );
                }catch( \Exception $e ) {
                    Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                }
    
                $this->sendJSONSaveResponse($relation_info['relation_type'], $save_result);
    
                die();                    
            } 
        }
        
    }  
    
    /**
     * Output a response.
     */
    protected function sendJSONSaveResponse( string $relation_type, bool $success ) {
        if( $success ) {
            $this->sendJSONResponse( [ 'success' => $success ] ) ;
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error("Unable to save this related (${relation_type}) entity.",1),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }


    // ---------------------------------------------------------- //
    // CRUD METHOD: DELETE
    // ---------------------------------------------------------- //    
    /**
     * Delete a given entity.
     */
    public function delete( $post_data ) {
        $data   = json_decode($post_data['data'], true);
        $pk     =  $post_data['_primarykey_'] ?: 'id';

        if( !!$data && isset($data[$pk]) ) {
            try {
                $entity_class   = $post_data['_entity_'];
                $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );
                $entity         = new EntityDecorator( $entity_mapper->get($data[$pk]) );

                if( $entity->supportsSoftDelete() ) {
                    $entity->deleted_on = new \DateTime('now');
                    $this->logDeletion($entity, true);
                    $this->sendJSONResponse( [ 'success' => $entity_mapper->save( $entity->export() ) ] ) ;
                }else{
                    $this->logDeletion($entity);
                    $this->sendJSONResponse( [ 'success' => $entity_mapper->delete([$pk => [$data[$pk]]]) ] ) ;
                }
            }catch( \Exception $e ) {
                Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false],new \Error($e->getMessage(), $e->getCode()),Response::HTTP_UNPROCESSABLE_ENTITY);
            }            
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error('Missing required parameters.'),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    protected function logDeletion( $deleted_entity, $soft_delete = false ) {
        $json = is_a($deleted_entity->toString(), EntityDecorator::class) ? $deleted_entity->entity->data() : $deleted_entity->data();
        $deletion_message = sprintf("%s was %s. \n %s", $deleted_entity->getEntityClass(), $soft_delete ? 'soft deleted' : 'permanently deleted', json_encode($json));
        Log::log($deletion_message, Log::LOG_TYPE_DELETION);
    }

    // ---------------------------------------------------------- //
    // CRUD METHOD: UNLINK RELATION
    // ---------------------------------------------------------- //    
    /**
     * Delete a given entity.
     */
    public function unlink( $post_data ) {
        $data       = json_decode($post_data['data'], true);
        $pk         =  $data['relation_details']['entity_primarykey'] ?: 'id';
        
        if( !!$data && isset($data[$pk]) ) {
            $relation_info   = array_key_exists('relation_details', $data ) ? $data['relation_details'] : null;
            $entity_class       = $post_data['_entity_'];
            $entity_mapper      = WPAPP::WPAPI()->Mapper( $entity_class );
            $entity             = new EntityDecorator( $entity_mapper->get($data[$pk]) );
            // Log::trace(__METHOD__, $entity_class, $relation_info);
            try {
                // HasManyThrough
                if( $relation_info['relation_type'] == 'HasManyThrough' ) {
                    $through_class  = $relation_info['through_entity'];
                    $through_mapper = WPAPP::WPAPI()->Mapper($through_class);
                    $through_fk     = $relation_info['foreign_key'];
                    $through_lk     = $relation_info['local_key'];
                    $through_entity = $through_mapper->first([$through_lk => $relation_info['identity_value'], $through_fk => $entity->{$pk}]);

                    if( $through_entity ) {
                        $sql    = sprintf('DELETE FROM %s WHERE %s = %s AND %s = %s', $through_mapper->table(), $through_lk, $relation_info['identity_value'], $through_fk, $entity->{$pk});
                        $result = $through_mapper->exec($sql);

                        // Log::trace(__METHOD__, [$through_lk => $relation_info['identity_value'], $through_fk => $entity->id], $through_entity, $sql, $result);

                        $this->sendJSONResponse( [ 'success' => $result ] ) ;
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unable to delete has-many-through relation.',1),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    die();                    
                }
            }catch( \Exception $e ) {
                Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false],new \Error($e->getMessage(), $e->getCode()),Response::HTTP_UNPROCESSABLE_ENTITY);
            }  
            
            try {
                // BelongsTo -> Remove Has One
                if( in_array( $relation_info['relation_type'], ['HasOne'] ) ) {
                    $save_result = false;
                    $foreign_key = $relation_info['foreign_key'];

                    // Log::trace('BelongsTo -> Remove Has One', $relation_info['relation_type'],$relation_info['entity'],$foreign_key, $entity->{$foreign_key},$entity->data());
                    // Log::trace('$entity->owner_id', $entity->owner_id, $entity->data());

                    if( array_key_exists( $foreign_key, $entity->data() ) ) {
                        $entity->{$foreign_key} = null;
                        try {
                            $save_result = $entity_mapper->save( $entity->export() );
                        }catch( \Exception $e ) {
                            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                        }
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    if( $save_result ) {
                        $this->sendJSONResponse( [ 'success' => $save_result ] ) ;
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unable to unlink this related entity.',1),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    die();
                }
                
                // Has One -> Remove BelongsTo
                if( in_array( $relation_info['relation_type'], ['BelongsTo'] ) ) {
                    $save_result            = false;
                    $foreign_entity_mapper  = WPAPP::WPAPI()->Mapper( $relation_info['relation_entity'] );
                    $foreign_entity         = $foreign_entity_mapper->get( $relation_info['relation_id'] );
                    $foreign_entity_lk      = $relation_info['local_key'];

                    // Log::trace('Has One -> Remove BelongsTo', $relation_info['relation_type'],$relation_info['relation_entity'],$foreign_entity->{$foreign_entity_lk},$foreign_entity->data());

                    if( $foreign_entity  && array_key_exists( $foreign_entity_lk, $foreign_entity->data() ) ) {
                        $foreign_entity->{$foreign_entity_lk} = null;    

                        // Log::trace('$foreign_entity->' . $foreign_entity_lk, $foreign_entity->{$foreign_entity_lk}, $foreign_entity);

                        try {
                            $save_result = $foreign_entity_mapper->save( $foreign_entity );
                        }catch( \Exception $e ) {
                            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                        }
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    if( $save_result ) {
                        $this->sendJSONResponse( [ 'success' => $save_result ] ) ;
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unable to unlink this related entity.',1),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    die();
                }

                // HasMany
                if( in_array( $relation_info['relation_type'], ['HasMany'] ) ) {
                    $save_result = false;
                    $foreign_key = $relation_info['foreign_key'];

                    if( array_key_exists( $foreign_key, $entity->data() ) ) {
                        $entity->{$foreign_key} = null;
                        try {
                            $save_result = $entity_mapper->save( $entity->export() );
                        }catch( \Exception $e ) {
                            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                        }
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    // Log::trace('HasMany -> Remove HasMany', $relation_info['relation_type'],$relation_info['entity'],$foreign_key, $entity->{$foreign_key},$entity->data());

                    if( $save_result ) {
                        $this->sendJSONResponse( [ 'success' => $save_result ] ) ;
                    }else{
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Unable to unlink this related entity.',1),Response::HTTP_UNPROCESSABLE_ENTITY);
                    }

                    die();
                }                
            }catch( \Exception $e ) {
                Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false],new \Error($e->getMessage(), $e->getCode()),Response::HTTP_UNPROCESSABLE_ENTITY);
            }  

            try {
                // HasMany
                if( $entity->owner_id ?: null ) {
                    $entity->owner_id = null;
                    $this->sendJSONResponse( [ 'success' => $entity_mapper->save( $entity->export() ) ] ) ;
                }else{
                    $this->sendJSONErrorResponse(['success' => false],new \Error('Unrecognized foreign key.',0),Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                die();
            }catch( \Exception $e ) {
                Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false],new \Error($e->getMessage(), $e->getCode()),Response::HTTP_UNPROCESSABLE_ENTITY);
            }
          
        }else{
            $this->sendJSONErrorResponse(['success' => false],new \Error('Missing required parameters.'),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    } 

    // ---------------------------------------------------------- //
    // SORTING
    // ---------------------------------------------------------- //
    /**
     * Update the sort_order for any entities that support sorting.
     */
    public function sort($post_data) {
        $data       = json_decode($post_data['data'], true);
        $item_ids   = [];
        $item_order = [];
        $sort_order = [];
        $result     = false;

        if( $data ) {

            // Split the collection into two separate arrays for simplicity
            foreach( $data as $item_element ) {
                array_push($item_ids, $item_element['id']);
                array_push($item_order, $item_element['order']);
            }
            // Recombine the arrays so the IDs become the index, and sort order becomes the value
            $sorting    = array_combine(array_keys(array_flip($item_ids)), $item_order);
            
            try {
                $entity         = $post_data['_entity_'];
                $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity );
                $entities       = $entity_mapper->all()->where(['id' => $item_ids]);

                // Enqueue all items
                $queue = new Queue();
                foreach( $entities as $item ) {
                    $valid = $sorting[ $item->id ];
                    if( !!$valid ) {
                        $item->sort_order = $sorting[ $item->id ] ?: 0;
                        $queue->enqueue($item);
                    }
                }

                // Iterate over the queue;
                // Only items that were modified will be updated
                $q = null;
                while( null !== ($q = !!$queue->peek() ? $queue->dequeue() : null ) ) {
                    $updated = $entity_mapper->save( $q );
                    array_push($sort_order, ['id' => $q->id, 'order' => $q->sort_order]);
                }

                $result = (false == $queue->peek());

            }catch( \Exception $e ) {
                Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false], new \Error($e->getMessage(),$e->getCode()), Response::HTTP_UNPROCESSABLE_ENTITY );
            }
        }

        if( $result ) {
            $this->sendJSONResponse([
                'success' => true,
                'order' => $sort_order
            ]);
        }else{
            $this->sendJSONErrorResponse(['success' => false], new \Error('Unable to update sort order at this time.',0), Response::HTTP_UNPROCESSABLE_ENTITY );
        } 
    }  

    // ---------------------------------------------------------- //
    // SEARCH
    // ---------------------------------------------------------- //   
    /**
     * Full text seach of all DISPLAY fields for a given entity.
     */
    public function search( $post_data ) {
        $data = json_decode($post_data['data'], true);

        try {
            if( $data['terms'] ?? null ) {
                try {
                    $entity_class   = $post_data['_entity_'];                
                    $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );
                    $entity         = new EntityDecorator( new $entity_class() );
                    $display_field  = $entity->getPrimaryDisplayField();
                    $entity_fields  = array_keys($entity->getDisplayFields());
                    $entity_fields  = implode(",",$entity_fields);
                    $terms          = filter_var($data['terms'],FILTER_SANITIZE_STRING|FILTER_SANITIZE_SPECIAL_CHARS);
                    
                    if( !trim( $terms ) ) {
                        $this->sendJSONErrorResponse(['success' => false],new \Error('Invalid search terms.',0), Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
    
                    $query          = sprintf("SELECT * FROM %s WHERE LOWER(CONCAT_ws(' ',%s)) LIKE LOWER('%%%s%%')", call_user_func([$entity->export(), 'table']), $entity_fields, $terms);
                    $entities       = $entity_mapper->query( $query );
                    
                    if( count( $entities ) ) {
                        $collection = [];
                        foreach( $entities as $entity ) { 
                            if( in_array($entity_class,[WPPage::class, WPPost::class]) ) {
                                $allowed_types = [ WPEntity::ENUM_TYPE_POST, WPEntity::ENUM_TYPE_PAGE ];
                                if( in_array($entity->post_type, $allowed_types) && $entity->post_status == EnumStatus::PUBLISH ) {
                                    $e = new EntityDecorator($entity);
                                    array_push($collection, ['label' => $entity->{$display_field}, 'entity' => $e->data()] );
                                }
                            }else{
                                $e = new EntityDecorator($entity);
                                array_push($collection, ['label' => $entity->{$display_field}, 'entity' => $e->data()] );
                            }
                        }
                        $this->sendJSONResponse( [ 'success' => true, 'data' => $collection ]) ;
                    }else{
                        $this->sendJSONErrorResponse(['success' => false, 'data' => null],new \Error('No results found.',1),Response::HTTP_NO_CONTENT);
                    }
    
                }catch( \Exception $e ) {
                    Log::trace($e->getMessage() . "\n" . $e->getTraceAsString(), json_last_error());
                    $this->sendJSONErrorResponse(['success' => false, 'data' => null],new \Error($e->getMessage(), $e->getCode()),Response::HTTP_UNPROCESSABLE_ENTITY);
                }            
            }else{
                $this->sendJSONErrorResponse(['success' => false, 'data' => null],new \Error('Missing required parameters.'),Response::HTTP_UNPROCESSABLE_ENTITY);
            }        
    
            $results = "";
    
            $this->sendJSONResponse([
                'success' => true,
                'html' => $results
            ]);       
            
            return true;
        }catch( \Exception $e ) {
            Log::log($e->getMessage . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        $this->sendJSONResponse([
            'success' => false,
            'html' => null
        ]);

        return false;

    }

    // ---------------------------------------------------------- //
    // MEDIA ASSETS (RELATION)
    // ---------------------------------------------------------- //    
    protected function saveMediaAsset( $media_metadata ) {
        $request = $this->request;
        $media_metadata = json_decode(stripslashes($media_metadata));
        
        if( !$media_metadata || null == $media_metadata ) return false;
        
        try {
            $mapper         = WPAPP::WPAPI()->Mapper( Image::class );
            $media          = $mapper->first( ['id' => $media_metadata->id] );
            $media = (null != $media) ? $media : $mapper->build( [
                'id'        => $media_metadata->id,
                'name'      => $media_metadata->name,
                'type'      => $media_metadata->subtype,
                'asset'     => $media_metadata->url,
                'hash'      => md5($media_metadata->name),
                'metadata'  => json_encode([
                    'width' => $media_metadata->width, 
                    'height' => $media_metadata->height,
                    'orientation' => $media_metadata->orientation
                ]),
                'othermetadata' => json_encode($media_metadata),
                'created'  => new \DateTime('now'),
                'modified'  => new \DateTime('now'),
            ]);
            
            $mapper->save( $media );
            // Log::trace(__METHOD__, 'Success?', $media, $media->id);

            return $media->id ?? false;
        }catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump($e->getMessage());
            }
            Log::log( $e->getMessage(), Log::LOG_TYPE_ERROR);
            return new \Error("An error occurred while adding or updating this media asset: " . $e->getMessage(),0);
        }
        return false;
    }

    /**
     * Manage Entity relations
     */
    public function relations() {
        $request        = Request::createFromGlobals();
        $post_data      = $request->request->all();
        $data           = json_decode($request->request->get('data'), true);
        $id             = $data['id'];

        if( !!$id ) {

            try {
                $related = $this->findEntityRelation();
            }catch( \Error $e ) {
                if( defined('WP_DEBUG') && WP_DEBUG ) {
                    Debug::Dump( $e->getMessage());
                }
                Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
                $this->sendJSONErrorResponse(['success' => false, 'code' => 0, 'error' => $e->getMessage()], new \Error($e->getMessage(), $e->getCode()), Response::HTTP_BAD_REQUEST);
                die();
            }
    
            $entity     = $related['entity'];
            $count      = count($related['collection']);
            
            try {
                $current_entity_mapper  = WPAPP::WPAPI()->Mapper( $post_data['_entity_'] );
                $current_entity         = $current_entity_mapper->get($id);
                $relations              = $current_entity->relations($current_entity_mapper, $current_entity);
                $relation               = $relations[ $data['relation'] ];

                $endpoint           = call_user_func([static::$model_admin, 'getRESTEndpoint' ]);
                $api_key            = call_user_func([static::$model_admin, 'getAPIKey' ]);
                $entity_instance    = new EntityDecorator( new $entity );
                $class_info         = $entity_instance->getEntityClassInfo();
                $heading            = $entity_instance->getRelationHeading();
                $request_format     = $entity_instance->getAJAXRequestConfig('submit',$api_key, true);

                $rel_class          = $relation->entityName();
                $rel_ent            = (new $rel_class());

                $rest               = [
                    'endpoint'          => $endpoint,
                    'api_key'           => $api_key,
                    'request_format'    => $request_format,
                ];

                $context    = [
                    'request'               => $this->request,
                    'is_relation'           => null !== $related,                    
                    'collection'            => [
                        'collection_name'   => $entity_instance->getEntityTag('plural'),
                        'collection'        => $related['collection'],
                    ],
                    'entity'                => $class_info,
                    'options'               => [
                        'link_relations'    => true,
                        'unlink_relations'  => true,
                        'relation_heading'  => $heading,
                        'entity_count'      => $count,
                        'reload_endpoint'   => sprintf("%s/relations/id/%s", $endpoint, $id),
                        'rest'              => $rest,//json_encode($rest),
                        'relation_details'  => $related['relation_details'] ?? [],
                    ],
                    'search_options'        => [
                        'mode'              => WPXAdminPageController::SEACH_MODE_SEARCH,
                        'owner_id'          => $id,
                        'entity_tag'        => $entity_instance->getEntityTag('plural'),
                        'endpoint'          => sprintf("%s/%s", $endpoint, $entity_instance->getEntityTag('plural')),
                        'form_action'       => sprintf("%s/%s/%s", $endpoint, $entity_instance->getEntityTag('plural'), 'search'),
                        'request_format'    => $entity_instance->getAJAXRequestConfig('search',$api_key, true),
                        'current_relation'  => [
                            'relation_type'         => (new \ReflectionClass($relation))->getShortName(),
                            'relation_entity'       => $post_data['_entity_'],
                            'relation_id'           => $id,
                            'through_entity'        => method_exists($relation, 'throughEntityName') ? $relation->throughEntityName() : null,
                            'entity'                => $relation->entityName(),
                            'entity_primarykey'     => $rel_ent->primaryKeyField(),
                            'foreign_key'           => $relation->foreignKey(),
                            'identity_value'        => $relation->identityValue(),
                            'local_key'             => $relation->localKey()                            
                        ]
                    ]    
                ];

                $template   = 'ModelAdmin_relation.html.twig';
                $twig       = WPAPP::inst()->Twig();
                $element    = $twig->load($template);
                $output     = $element->render($context);    
                
                $this->sendJSONResponse([
                    'success' => true,
                    'html' => $output
                ]);

                die();
            }catch( \Exception $e ) {
                Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
            }
        }

        $this->sendJSONErrorResponse(['success' => false ], new \Error("Unable to load relations",0), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Find a specific Entity Relation and create a collection of
     * found results of the given Entity.
     */
    public function findEntityRelation() {
        $request        = Request::createFromGlobals();
        $post_data      = $request->request->all();
        $data           = json_decode($request->request->get('data'), true);
        $id             = $data['id'] ?? null;
        $relation       = $data['relation'] ?? null;

        if( !$id ) {
            throw new \Error('id not supplied.', 2);
        }
        
        if( !$relation ) {
            throw new \Error('NULL is not a valid relation for this entity.', 3);
        }

        try {
            $entity_class   = $post_data['_entity_'];
            $entity_mapper  = WPAPP::WPAPI()->Mapper( $entity_class );
            $entity         = new EntityDecorator( $entity_mapper->get($id) );
            $relations      = $entity->export()->relations($entity_mapper, $entity->export());
            // Log::trace(__METHOD__, $data, $relation, $entity_class, $entity);
            // Log::trace(__METHOD__, $entity_mapper->where(['id' => $id])->with( array_keys( $relations ))->toArray() );
        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(),Log::LOG_TYPE_ERROR);
        }
        

        if( !in_array($relation, array_keys( $relations ) ) ) {
            throw new \Error('No such relation exists for this entity.', 4);
        }

        $relations      = ($relations[$relation]);
        $related_entity = $relations->entityName();
        $related_entity = (new $related_entity());
        
        try {
            $collection         = [];
            $entity_pk          = $entity->export()->primaryKeyField();
            $entity_relation    = $entity->{$relation};
            $related            = $entity_relation->execute();

            // The mapper returns either an instance of `Collection` if multiple results found,
            // or an `Entity` instance if only a single record is found.
            if( is_a($related,'Spot\Entity\Collection') ) {
                foreach( $related as $ent ) {
                    array_push($collection, new EntityDecorator($ent));
                }
            }else{
                if( !!$related && class_implements($related, 'Spot\Entity\EntityInterface') ) {
                    array_push($collection, new EntityDecorator($related) );
                }
            }

            $relation_info = [
                'relation'              => $relation,
                'relation_type'         => (new \ReflectionClass($relations))->getShortName(),
                'relation_entity'       => get_class($entity->export()),
                'relation_id'           => $entity->{$entity_pk},
                'through_entity'        => method_exists($entity_relation, 'throughEntityName') ? $entity_relation->throughEntityName() : null,
                'entity'                => $entity_relation->entityName(),
                'entity_primarykey'     => $related_entity->primaryKeyField(),
                'foreign_key'           => $entity_relation->foreignKey(),
                'identity_value'        => $entity_relation->identityValue(),
                'local_key'             => $entity_relation->localKey()
            ];

            return [ 'entity' => get_class($related_entity), 'collection' => $collection, 'relation_details' => $relation_info ];
        }catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            return [ 'entity' => get_class($related_entity), 'collection' => [] ];
        }

    }    

    // ---------------------------------------------------------- //
    // ERROR HANDLING
    // ---------------------------------------------------------- //
    /**
     * Generic error handler
     * 
     * @param Exception $e
     */
    protected function Exception( \Exception $e ) {
        Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
    }

    /**
     * Validates an API token in the application root.
     * Only tokens passed through a POST request will be considered.
     * 
     * @return Bool Returns true if the tokens match, redirects and terminates any request otherwise.
     */
    protected function validateAPIToken() {
        $request    = $this->request;
        $file       = APP_PATH . '.wpxapitoken';

        // Log::trace(__METHOD__, $request->isMethod('POST'), $request, $request->request->get('_wpxtoken_', null), $request->request->get('_wpxnonce_', null));

        if( !file_exists($file) ) {
            Log::log('API Token ' . $file . ' could not be found.', Log::LOG_TYPE_ERROR);
            $this->invalidateRequest(Response::HTTP_UNAUTHORIZED, RestUtils::ERROR_UNAUTHORIZED_ACCESS);
            die();            
        }
        
        if( !$request->isMethod('POST') ) {
            $this->invalidateRequest(Response::HTTP_UNAUTHORIZED, RestUtils::ERROR_METHOD_NOT_ALLOWED);
            die();
        }
        
        if( null === $request->request->get('_wpxtoken_', null ) ) {
            $this->invalidateRequest(Response::HTTP_UNAUTHORIZED, RestUtils::ERROR_INVALID_APITOKEN);
            die();
        }

        if( null === $request->request->get('_wpxnonce_', null ) ) {
            $this->invalidateRequest(Response::HTTP_UNAUTHORIZED, RestUtils::ERROR_INVALID_NONCE);
            die();
        }

        $nonce              = $request->request->get('_wpxnonce_');
        $public_key         = $request->request->get('_wpxtoken_');
        $private_key        = file_get_contents(APP_PATH . '.wpxapitoken');
        $hash_message       = hash_hmac('sha256', $private_key . $public_key . getenv('NONCE_SALT') , $private_key);

        // Log::trace(__METHOD__, $nonce, $public_key, $private_key, $hash_message);
        if ( hash_equals($hash_message, $nonce) ) {
            return true;
        }else{
            $this->invalidateRequest(Response::HTTP_UNAUTHORIZED, RestUtils::ERROR_UNAUTHORIZED_ACCESS);
        }

    }

    // ---------------------------------------------------------- //
    // HTTP UTILITIES
    // ---------------------------------------------------------- //
    protected function buildQueryString( array $params = null ) {
        $qb = new HttpQueryBuilder( $params );
        return $qb->__toString();
    }

    // ---------------------------------------------------------- //
    // JWT
    // ---------------------------------------------------------- //
    /**
     * Generate a URL to load the Content Builder editor template.
     */
    public static function getTokenizedURL( $endpoint, $action = 'load', $jwt_token = null ) {
        $url = sprintf('%s%s/%s',getenv('WP_HOMEURL'),$endpoint, $action);
        if( null != $jwt_token ) {
            $url = sprintf('%s/jwt/%s',$url, $jwt_token);
        }
        return $url;        
    }

    public static function getJWToken( $api_key, $as_json = false ) {
        try {
            $tokenId    = base64_encode( getenv('SECURE_AUTH_KEY') );
            $issuedAt   = time();
            $notBefore  = $issuedAt + 1;            // Adding 2 seconds
            $expire     = $notBefore + (60 * 60);   // Adding 60 seconds * 60 = 1 hr
            $serverName = getenv('WP_SITEURL');
    
            $data = [
                'iat'  => $issuedAt,                        // Issued at: time when the token was generated
                'jti'  => $tokenId,                         // Json Token Id: an unique identifier for the token
                'iss'  => $serverName,                      // Issuer
                'nbf'  => $notBefore,                       // Not before
                'exp'  => $expire,                          // Expire
            ];

            $data       = array_merge([], $data, [
                'data' => [                                     // Data
                    'api_key'       => $api_key,                // API Key
                ]
            ]);             
            
            $secret_key = getenv('AUTH_KEY');
            $jwt = JWT::encode($data, $secret_key, 'HS512');

            // Log::trace(__METHOD__, $data, null == $api_key ? null : $as_json ? json_encode(['jwt' => $jwt]) : $jwt);
    
            return null == $api_key ? null : $as_json ? json_encode(['jwt' => $jwt]) : $jwt;
        }catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return null;
    }    

    public static function validateJWToken( $jwt_token ) {
        try {
            $secret_key = getenv('AUTH_KEY');
            if( $token = JWT::decode($jwt_token, $secret_key, array('HS512')) ) {
                return true;
            } else {
                return false;
            }
        } catch( \Exception $e ) {
            Log::log($e->getMessage()() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR );
        }

        return false;
    }    

    // ---------------------------------------------------------- //
    // SESSION UTILITIES
    // ---------------------------------------------------------- //
    /**
     * Add a system message.
     * 
     * @param String $type      The type of message to display.
     * @param String $message   The message to display.
     */
    protected function addSystemMessage( $type = self::MESSAGE_TYPE_NOTICE, $message ) {
        $session = WPAPP::inst()->session();
        if( null !== $session ) {
            $session->getFlashBag()->add( $type, $message );
        }
    }    

    // ---------------------------------------------------------- //
    // RESPONSE METHODS
    // ---------------------------------------------------------- //    
    /**
     * Unauthorized.
     * @link https://github.com/symfony/symfony/blob/master/src/Symfony/Component/HttpFoundation/Response.php#L23
     */
    protected function invalidateRequest( $status_code = Response::HTTP_UNAUTHORIZED, $error_code = RestUtils::ERROR_UNKNOWN ) {
        $error = RestUtils::getResponseMessage($error_code);
        $this->sendJSONErrorResponse(['success' => false], $error, $status_code);
        die();
    }

    /**
     * Convenience. A simple method to get the referer.
     * 
     * @return String Returns the referer
     */
    protected function getReferer() {
        $request = $this->request;
        return $request->headers->get('referer');
    }

    /**
     * Send a JSON response
     */
    protected function sendJSONResponse( array $content, $http_code = Response::HTTP_OK) {
        // $encoded = $force_json_encoding ? mb_convert_encoding( json_encode($content), 'UTF-8', 'UTF-8') : $content;
        // unset($content['data'][0]['entity']['description']);
        // Log::trace(__METHOD__, $content, $http_code);
        $response = JsonResponse::create( $content, $http_code, array('Content-Type'=>'application/json; charset=utf-8' ));
        $response->send();
        die();
    }

    protected function sendJSONErrorResponse( array $content, \Error $error, $http_code = Response::HTTP_UNAUTHORIZED ) {
        $response_message = array_merge([], $content, ['error' => $error->getMessage(), 'code' => $error->getCode()]);
        $response = JsonResponse::create($response_message, $http_code, array('Content-Type'=>'application/json; charset=utf-8' ));
        $response->send();
        die();
    }

    protected function get_calling_class() {

        //get the trace
        $trace = debug_backtrace();
    
        // Get the class that is asking for who awoke it
        $class = $trace[1]['class'];
    
        // +1 to i cos we have to account for calling this function
        for ( $i=1; $i<count( $trace ); $i++ ) {
            if ( isset( $trace[$i] ) ) // is it set?
                 if ( $class != $trace[$i]['class'] ) // is it a different class
                     return $trace[$i]['class'];
        }
    }

}