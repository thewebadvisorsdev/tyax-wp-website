<?php 

namespace WPX\Controller;

use AUX\Exception;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use WPX\WPAPP;
use WPX\View\WPView;

/**
 * A generic Plugin controller
 */
class WPXAdminController extends Controller {

    public static $allowed_actions = ['index'];


    public function __construct() {
        // Debug::Dump(__METHOD__);
    }

    /**
     * Handles rendering a single post or page
     */
    public function index() {
        // Debug::Dump(__METHOD__);
        // require APP_PATH . 'public_html/wp/wp-admin/index.php';
        // die();
    }
    
    public function foo() {
        Debug::Dump(__METHOD__);
    }
    
    public function bar() {
        Debug::Dump(__METHOD__);
    }
    
    public function error() {
        Debug::Dump(__METHOD__);
    }
    
    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     */
    protected function Exception(\Exception $e) {
        if( defined('WP_DEBUG') && WP_DEBUG ) {
            echo $e->getMessage();
        }
        Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
    }  

}