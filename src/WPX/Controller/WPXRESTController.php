<?php

namespace WPX\Controller;

use AUX\HTTP\HttpStatus;
use AUX\Utils\Log;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use WPX\Entity\Post;
use WPX\Entity\Term;
use WPX\Enums\EnumStatus;
use WPX\WPAPP;

/**
 * This class extends the base RESTController class, and provides
 * the basic CRUD functionality for the plugin in which it resides.
 * The functionality of this REST controller is not intended to be shared
 * between controllers, so some amount of DRY principle is sacrificed 
 * in order to isolate as much of the functionality where possible.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 * 
 */
class WPXRESTController extends RESTController {
    const PAGINATION_LOAD_LIMIT = 9;

    public static $allowed_actions  = [
        'index', 
        'load'
    ];

    // ---------------------------------------------------------- //
    // CONTROLLER ACTIONS: MAIN
    // ---------------------------------------------------------- //
    /**
     * Front controller
     */
    public function index() {
        if( $this->request->isMethod('POST') ) {

            $params     = func_get_arg(0);
            $payload    = $this->request->request->all();
            $action     = $params['action'];
            $is_valid   = self::validateJWToken( array_shift($payload) );
    
            if( $is_valid ) {
                switch( $action ) {
                    case 'load':
                        call_user_func_array([$this,'load'], [$payload]);
                        break;
                } 
            }else{
                $response = new Response(HttpStatus::getMessageForCode(HttpStatus::HTTP_FORBIDDEN),HttpStatus::HTTP_FORBIDDEN);
                $response->send();
            }

        }else{
            $response = new Response(HttpStatus::getMessageForCode(HttpStatus::HTTP_BAD_REQUEST),HttpStatus::HTTP_BAD_REQUEST);
            $response->send();
        }

        die();
    }

    // ---------------------------------------------------------- //
    // ACTIONS
    // ---------------------------------------------------------- //
    public function load() {

        $args = current(func_get_args());
        $WPAPP              = WPAPP::inst();
        
        $twig               = $WPAPP->Twig();
        $template           = $args['term'] == 'Featured' ? 'FeaturedStory_Block.html.twig' : 'Newsletter_Block.html.twig';
        $twig_template      = $twig->load($template);

        $post_mapper        = WPAPP::WPAPI()->Mapper( Post::class );
        $term_mapper        = WPAPP::WPAPI()->Mapper( Term::class );
        
        $entity             = $args['entity'] ?: null;
        $total              = 0;
        $limit              = self::PAGINATION_LOAD_LIMIT;
        $pages              = 0;
        $page               = 1;
        $offset             = 0;

        // JSONResponse
        $response           = null;
        $jwt = [
            'endpoint' => WPXRESTController::getTokenizedURL('wpxrest/v1','load'),
            'token' => WPXRESTController::getJWToken( getenv('AUTH_KEY') )
        ];

        if( $entity ){
            $collection             = [];
            
            try {
                $page       = (int) $args['page'] ?: $page;
                $limit      = (int) $args['limit'] ?: 9;
                $format     = $args['format'] ?: 'post';
                $term_name  = $args['term'] ?: 'Stories';
                
                $term       = $term_mapper->first(['name' => $term_name]);
                $tag        = $term_mapper->first(['name' => ucwords($format)]);

                if( $term ) {
        
                    $term_id    = ($format == WPStoriesController::STORY_FORMAT_POST)? $term->term_id : $tag->term_id;
                    
                    if( null == $term_id ) return null;
                    
                    $tax_type   = ($format == WPStoriesController::STORY_FORMAT_POST) ? 'category' : 'post_tag';
                    $total      = count($post_mapper->byTermID($term_id, null, true, $tax_type)) ?: 0;

                    $pages      = ceil($total / $limit);
                    $page       = min($pages,$page);
                    $next_page  = min(($page + 1),$pages);
                    $offset     = ($page - 1) * $limit;

                    
                    
                    if( $next_page <= $pages ) {
                        $post_ids   = $post_mapper->byTermID($term_id, null, true, $tax_type);
                        // Log::trace(sprintf("[%s]", implode(',',$post_ids)));
                        $entities   = $post_mapper->all()->where(['ID' => $post_ids])->with(['meta','terms'])->order(['post_date' => 'DESC'])->limit($limit, $offset);
                        // Log::trace('TOTAL', $total, count($entities->toArray()));                        
                    }else{
                        $entities = null;
                    }
                }
            }catch( \Exception $e ){
                Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
            }

            if( !empty( $entities ) && null != $entities ) {
                try {
    
                    // Get all attachments
                    $attachments = $post_mapper->attachments($post_ids);
    
                    // Stories
                    foreach( $entities as $entity ) {
                        $e          = $entity->data();
                        $e['wpapi'] = WPAPP::WPAPI();
                        $e['media'] = $attachments[ $entity->ID ] ?? null;

                        // Filter the title and content
                        $e['post_title']    = WPAPP::inst()->WPAPI()->filterCurlyQuotes( $e['post_title'] );
                        $e['post_content']  = WPAPP::inst()->WPAPI()->applyWPContentFilters( $e['post_content'] );

                        array_push($collection, $e);
                    }
    
                }catch( \Exception $e ){
                    Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
                }
    
                // Render
                $context        = ['stories' => $collection];
                $html           = $twig_template->render($context);

                $payload        = array_merge([],$jwt,[
                    'success'   => true,
                    'html'      => $html,
                    'pages'     => $pages,
                    'page'      => $next_page,
                    'limit'     => (int) $limit,
                    'total'     => $total
                ]);

                $response = new JsonResponse(json_encode($payload), Response::HTTP_OK, ['content-type' => 'application/json'], true);
            }else{
                $payload        = [
                    'success'   => false,
                    'html'      => null,
                    'pages'     => $pages,
                    'page'      => $next_page,
                    'limit'     => (int) $limit,
                    'total'     => $total
                ];
                $response = new JsonResponse(json_encode($payload), Response::HTTP_OK, ['content-type' => 'application/json'], true);
            }

            // Respond
            $response->setCharset('UTF-8');
            $response->send();

        }else{
            $response = new Response(HttpStatus::getMessageForCode(HttpStatus::HTTP_BAD_REQUEST),HttpStatus::HTTP_BAD_REQUEST);
            $response->send();
        }

        die();
    }
}