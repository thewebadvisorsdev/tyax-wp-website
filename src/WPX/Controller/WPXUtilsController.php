<?php 

namespace WPX\Controller;

use \Exception;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use WPX\HTTP\HttpStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * A generic Plugin controller
 */
class WPXUtilsController extends Controller {

    public static $allowed_actions = ['index','export'];


    public function __construct() {
        // Debug::Dump(__METHOD__);
    }

    /**
     * Handles rendering a single post or page
     */
    public function index() {
        // Debug::Dump(__METHOD__);
        // require APP_PATH . 'public_html/wp/wp-Utils/index.php';
        // die();
    }
    
    public function export() {
        $request = Request::createFromGlobals();
        $args = func_get_arg(0);
        $args = $args['params'];
        // Log::trace(__METHOD__, $request->request->all(), get_defined_vars());
        if( !empty( $args ) ) {
            $action  = array_shift($args);
            $apikey  = $request->request->get('wpxapikey', null);
            switch( $action ) {
                case 'db' :
                    $this->exportDatabase($apikey);
                    break;
                default: 
                    break;
            }
        }
    }
    
    public function error() {
        Debug::Dump(__METHOD__);
    }

    // ---------------------------------------------------------- //
    // METHODS: PRIVATE
    // ---------------------------------------------------------- //
    private function exportDatabase($security_token) {
        $key = getenv('WP_EXPORT_TOKEN');
        if($key === $security_token) {

            $user       = getenv('DB_USER');
            $pass       = getenv('DB_PASSWORD');
            $db         = getenv('DB_NAME');
            $host       = getenv('DB_HOST');
            
            $cmd        = "mysqldump -u${user} -p${pass} -h ${host} ${db} --ignore-table=${db}.wp_users";
            header('Content-Type: application/sql');
            passthru( $cmd );
            exit(0);
        }else{
            $response = new Response("Unauthorized", Response::HTTP_UNAUTHORIZED);
            $response->send();
            exit(0);     
        }
    }
    
    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     */
    protected function Exception(\Exception $e) {
        if( defined('WP_DEBUG') && WP_DEBUG ) {
            echo $e->getMessage();
        }
        Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
    }  

}