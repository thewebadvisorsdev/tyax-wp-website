<?php 

namespace WPX\Controller;

use AUX\Exception;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\HTTP\HttpQueryBuilder;
use AUX\HTTP\RestUtils;
use WPX\WPAPP;
use WPX\View\View;
use WPX\Form\Form;
use WPX\Controls\TabControl;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * A generic Plugin controller
 */
class WPXAdminPageController extends Controller {

    const MESSAGE_TYPE_ERROR    = 'error';
    const MESSAGE_TYPE_WARNING  = 'warning';
    const MESSAGE_TYPE_NOTICE   = 'notice';
    const MESSAGE_TYPE_SUCCESS  = 'success';

    const SEACH_MODE_FILTER     = 'filter';
    const SEACH_MODE_SEARCH     = 'search';

    public static $allowed_actions = ['index', 'add', 'edit', 'delete', 'submit', 'relations', 'sort'];
    protected static $controller_slug;
    protected static $entity;
    protected static $entity_tag;
    protected static $navigation;
    protected $entity_collection;
    protected $request;

    public function __construct( array $view_data = null, Request $request = null ) {
        $this->view_data = $view_data;
        $this->request = $request;
    }

    /**
     * Default action
     */
    public function index() {
        $this->view = new View( WPAPP::Twig(), 'admin-page', ['admin-page' => 'AdminPage.html.twig'] );
        $this->render();
    }

    # +------------------------------------------------------------------------+
    # RENDER
    # +------------------------------------------------------------------------+
    protected function render() {
        if( null != $this->view ) {
            $this->view->ViewData( $this->ViewData() );
            $this->view->ViewName( __CLASS__, 'index' );

            try {
                $this->view->render();
            } catch(Exception $e) {
                $this->Exception( $e );
            }
            
        }else{
            $this->Exception( new Exception("Unable to render view.") );
        }
    }
    
    
    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     */
    protected function Exception(\Exception $e) {
        Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        if( defined('WP_DEBUG') && WP_DEBUG ) {
            Debug::Dump( $e->getMessage(), debug::DEBUG_TYPE_ERROR );
        }
    }

    // ---------------------------------------------------------- //
    // SESSION MANAGEMENT (SYMFONY SESSIONS)
    // ---------------------------------------------------------- //
    /**
     * Check to see if there are any session messages.
     * 
     * @param String    $type   The type of message to check for.
     */
    protected function hasFlashMessages( $type = self::MESSAGE_TYPE_NOTICE ) {
        return WPAPP::session()->getFlashBag()->has( $type );
    }

    /**
     * Retrive any sessions messages of a given type.
     * 
     * @param String    $type   The type of message to check and retrieve.
     * @return Array    Returns a collection of flash messages, null of none available.
     */
    protected function getFlashMessages( $type = self::MESSAGE_TYPE_NOTICE ) {
        if( $this->hasFlashMessages( $type ) ) {
            return WPAPP::session()->getFlashBag()->get( $type );
        }
        
        return null;
    }

    /**
     * Add a system message.
     * 
     * @param String $type      The type of message to display.
     * @param String $message   The message to display.
     */
    protected function addSystemMessage( $type = self::MESSAGE_TYPE_NOTICE, $message ) {
        WPAPP::session()->getFlashBag()->add( $type, $message );
    }

    /**
     * Retrieves the complete collection of session messages.
     * 
     * @return Array    An array of session messages sorted by severity
     */
    protected function getSystemMessages() {
        $messages = [];

        if( $this->hasFlashMessages(self::MESSAGE_TYPE_ERROR) ) {
            $messages[self::MESSAGE_TYPE_ERROR] = $this->getFlashMessages(self::MESSAGE_TYPE_ERROR);
        }

        if( $this->hasFlashMessages(self::MESSAGE_TYPE_WARNING) ) {
            $messages[self::MESSAGE_TYPE_WARNING] = $this->getFlashMessages(self::MESSAGE_TYPE_WARNING);
        }

        if( $this->hasFlashMessages(self::MESSAGE_TYPE_NOTICE) ) {
            $messages[self::MESSAGE_TYPE_NOTICE] = $this->getFlashMessages(self::MESSAGE_TYPE_NOTICE);
        }

        if( $this->hasFlashMessages(self::MESSAGE_TYPE_SUCCESS) ) {
            $messages[self::MESSAGE_TYPE_SUCCESS] = $this->getFlashMessages(self::MESSAGE_TYPE_SUCCESS);
        }

        return $messages;
    }

    // ---------------------------------------------------------- //
    // HELPER METHODS
    // ---------------------------------------------------------- //
    protected function getControllerClass($entity = null) {
        $class_name = explode('\\', $entity ?: get_called_class());
        $class_name = array_pop($class_name);
        return $class_name;
    }
    
    protected function getEntityClass($entity) {
        return $this->getControllerClass($entity);
    }

    // ---------------------------------------------------------- //
    // NAVIGATION
    // ---------------------------------------------------------- //
    /**
     * Builds a query string based on the current page, with a valid controller action
     * 
     * @param String $action            The action for the given controller
     * @param String $additional_params An array of additional parameters to append as part of the query string
     */
    protected function buildNavLink($action = null, array $additional_params = null) {        
        $args = array_merge([], [$this->request->get('page')], func_get_args());
        return call_user_func_array([$this, 'buildNavLinkByPage'], $args);
    }    

    /**
     * Builds a query string for a desired page page and a valid controller action
     * 
     * @param String $page_name         The slug of the WP menu page controller, e.g. model-admin-{class}
     * @param String $action            The action for the given controller
     * @param String $additional_params An array of additional parameters to append as part of the query string
     */
    protected function buildNavLinkByPage($page_name, $action = null, array $additional_params = []) {
        $query_string     = [
            'page'      => $page_name,
            'action'    => $action ? $action : 'index'
        ];

        if( !empty($additional_params) ) {
            $query_string = array_merge([], $query_string, $additional_params);
        }

        $uri = sprintf("admin.php?%s", build_query($query_string));
        return $uri;
    }

    /**
     * Builds a navigation list based on a given class name.
     * 
     * @param Array     [$navigation_items]   Optional. An associative array matching the structure of WPXAdminPageController::$navigation
     * @param String    [$additional_params]  Optional. An array of additional parameters to append as part of the query string
     */
    protected function buildTabNavigation( array $navigation_items = [], array $additional_params = []) {

        $tabs = null;
        $navigation = count($navigation_items) ? $navigation_items : static::$navigation;        

        if( !empty($navigation) ) {
            $tabs       = new TabControl();
            foreach( $navigation as $action => $item ) {
                $name       = array_key_exists('class_name', $item) ? $item['class_name'] : $this->getControllerClass( get_called_class() );
                $tab_url    = call_user_func_array( [$this,'buildNavLink'], [$action, $additional_params] );
                $tab_view   = sprintf( $item['view'], $name, $action );
                $tab_label  = $item['label'];
                $tabs->addTab($tab_label, $tab_url, ['data-tabindex' => $tab_view ]);
            }
        }

        return $tabs;
    }

    /**
     * Convenience. A simple method to get the referer.
     * 
     * @return String Returns the referer
     */
    protected function getReferer() {
        $request = Request::createFromGlobals();
        return $request->headers->get('referer');
    }

    /**
     * Redirect to a different view.
     * 
     * @param String $location      A URL to redirect to
     * @param Integer   $http_code  An http response code
     */
    protected function redirectToReferrer($location = null, $http_code = Response::HTTP_FOUND) {
        $location = (null != $location) ? $location : $this->getReferer();
        $response = new RedirectResponse($location, $http_code);
        $response->send();
        die();
    }

    protected function sendJSONResponse( array $content, $http_code = Response::HTTP_OK ) {
        $response = new JsonResponse($content, $http_code);
        $response->send();
        die();
    }

    protected function buildQueryString( array $params = null ) {
        $qb = new HttpQueryBuilder( $params );
        return $qb->__toString();
    }

    // ---------------------------------------------------------- //
    // FORMS
    // ---------------------------------------------------------- //
    protected function buildCRUDForm( $endpoint, $entity, $api_key ) {

        $collection_class   = $entity->getEntityTag('plural');
        $method             = 'POST';
        $action             = $entity->id ? 'update' : 'add';
        $params             = $entity->id ? [ $collection_class,'id',$entity->id] : [$collection_class];
        $action_url         = RestUtils::getAPIEndpoint($endpoint, $params);
        $private_key        = file_get_contents(APP_PATH . '.wpxapitoken');
        $public_key         = $api_key;
        $nonce              = hash_hmac('sha256', $private_key . $public_key . getenv('NONCE_SALT') , $private_key);

        $form = new Form(WPAPP::inst()->Twig(),[
            'action'       => $action_url,
            'method'       => $method,
            'enctype'      => 'application/x-www-form-urlencoded',
            'classes'      => ['component', 'ui-editform'],
            'id'           => $collection_class,
            'novalidate'   => false,
            'autocomplete' => false,
            'honeypot'     => false,
            'add_submit'   => true,
            'form_tags'    => true
        ]);

        // Additional fields
        $form->add([ 'type' => 'hidden', 'name' => '_wpxnonce_',        'value' => $nonce,                      'data-noserialize' => null]);
        $form->add([ 'type' => 'hidden', 'name' => '_wpxtoken_',        'value' => $public_key,                 'data-noserialize' => null]);
        $form->add([ 'type' => 'hidden', 'name' => '_wpnonce_',         'value' => wp_create_nonce(),           'data-noserialize' => null]);        
        $form->add([ 'type' => 'hidden', 'name' => '_action_',          'value' => $action,                     'data-noserialize' => null]);
        $form->add([ 'type' => 'hidden', 'name' => '_class_',           'value' => $collection_class,           'data-noserialize' => null]);
        $form->add([ 'type' => 'hidden', 'name' => '_entity_',          'value' => static::$entity,             'data-noserialize' => null]);
        $form->add([ 'type' => 'hidden', 'name' => '_controllerslug_',  'value' => static::$controller_slug,    'data-noserialize' => null]);

        $form->add([ 'type' => 'submit', 'name' => 'submit', 'value' => !!$entity->id ? sprintf('Apply Changes to %s', static::$entity_tag['singular']) : sprintf('Create New %s', static::$entity_tag['singular']) ]);

        return $form;
    }
}