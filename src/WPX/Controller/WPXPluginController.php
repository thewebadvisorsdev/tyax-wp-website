<?php 

namespace WPX\Controller;

use AUX\Exception;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use WPX\WPAPP;
use WPX\View\WPView;

/**
 * A generic Plugin controller
 */
class WPXPluginController extends Controller {

    public static $allowed_actions = ['index', 'foo', 'bar'];


    public function __construct() {
    }

    public function index() {
        Debug::Dump(__METHOD__);
    }
    
    public function foo() {
        Debug::Dump(__METHOD__);
    }
    
    public function bar() {
        Debug::Dump(__METHOD__);
    }
    
    public function error() {
        Debug::Dump(__METHOD__);
    }
    
    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     */
    protected function Exception(\Exception $e) {
        if( defined('WP_DEBUG') && WP_DEBUG ) {
            echo $e->getMessage();
        }
        Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
    }  

}