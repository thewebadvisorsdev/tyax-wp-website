<?php 

namespace WPX\Controller;

use AUX\Exception;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\Utils\Text;
use WPX\WPAPP;
use WPX\WPAPI;
use WPX\Entity\WPEntity;
use WPX\Entity\Post;
use WPX\Entity\Page;
use WPX\View\WPView;
use WPX\View\View;
use WPX\WPQuery;
use AUX\OGTag;
use WPX\Schema\JSONSchema;
use AUX\HTTP\HttpStatus;
use Symfony\Component\HttpFoundation\Request;

/**
 * A generic page/post controller
 */
class WPController extends Controller {

    public static $allowed_actions = ['index', 'home', 'archive'];

    protected $boilerplate_data = [];

    protected $wp_query;
    protected $root_id          = null;
    protected $uri_data         = null;

    public function WPQuery($global_wp_query) {

        if( NULL !== $global_wp_query ) {
            $this->wp_query = $global_wp_query;
        }

        return $this->wp_query;
    }

    public function __construct($global_wp_query = null) {

        if( NULL !== $global_wp_query ) {
            $this->wp_query = $global_wp_query;
        }

        // View
        try {
            if( NULL !== $global_wp_query ) {
                $this->view = new WPView( WPAPP::inst()->Twig(), WPAPP::inst()->WPAPI()->WPXQuery()->Type() );
            }else{
                $this->view = new WPView( WPAPP::inst()->Twig(), WPQuery::ENUM_TYPE_PAGE );
            }
        } catch( \Exception $e ) {
            $this->Exception( $e );
        }

        $this->initBoilerplateViewData();
        $this->initializeController();

    }

    /**
     * Handles rendering a single post or page
     */
    public function index() {

        if( null != $this->view ) {
            $args               = func_get_args() ?? null;
            $template_override  = $args ? func_get_arg(0) : null;
            $viewname_override  = $args ? func_get_arg(1) : null;

            $this->view->ViewData( $this->ViewData() );
            $this->view->ViewName( __CLASS__, 'index' );

            // Override the view name from the template
            if( null != $viewname_override ) {
                $this->view->setViewData('view_name', $viewname_override);
            }

            try {
                $this->view->render($template_override);
            } catch(Exception $e) {
                $this->Exception( $e );
            }
        }else{
            $this->Exception( new Exception("Unable to render view.") );
        }

    }
    
    public function error($http_code = 404) {
        
        // Response Headers
        $http_header = HttpStatus::getInstance()->getHttpHeader($http_code);
        $http_message = HttpStatus::getInstance()->getMessageForCode($http_code);

        // Clear the page title
        WPAPP::inst()->Permalink(WPAPP::BaseURL());

        // Initialize view data
        $view_data = $this->boilerplate_data;
        $view_data = array_merge($view_data,[
            'title' => $http_message,
            'permalink' => WPAPP::inst()->Permalink(),
            'wp_postpage_thumbnail' => null
        ]);        
        $this->ViewData( $view_data );

        if( null != $this->view ) {
            $this->view->ViewData( $this->ViewData() );
            $this->view->ViewName( __CLASS__, $http_code );
            
            try {
                header($http_header);
                $this->view->render( sprintf('%s.html.twig',$http_code), 'view-error');
                exit;
            } catch(Exception $e) {
                $this->Exception( $e );
            }
        }else{
            $this->Exception( new Exception("Unable to render view.") );
        }

    }

    /**
     * Handles running an archive
     */
    public function archive() {
        $limit      = WPAPP::Options()->getOption('posts_per_page');
        $mapper     = WPAPP::WPAPI()->Mapper(Post::class);
        $query_data = $mapper->latest($limit);

        // Initialize view data
        $view_data = $this->boilerplate_data;
        if( $query_data !== false ) {
            $normalized_posts = $this->normalizePosts($query_data->toArray());
            $view_data = array_merge($view_data, ['posts' => $normalized_posts]);
        }

        // OG Tags
        $view_data = $this->initOpenGraphTags($view_data);

        // WP Head
        $view_data = $this->initNormalizeWPHeadAndFooter($view_data);

        $this->ViewData( array_merge($this->view_data ?? [], $view_data) );

        if( null != $this->view ) {
            $this->view->ViewData( $this->ViewData() );
            $this->view->ViewName( __CLASS__, 'archive' );

            try {
                $this->view->render();
            } catch(Exception $e) {
                $this->Exception( $e );
            }
        }else{
            $this->Exception( new Exception("Unable to render view.") );
        }        
    }

    # +------------------------------------------------------------------------+
    # CONTENT
    # +------------------------------------------------------------------------+
    protected function initializeController() {
        $details    = $this->getWPQueryDetails();
        $query_data = null;

        $entity     = ($details->type == WPEntity::ENUM_TYPE_PAGE) ? WPEntity::ENUM_TYPE_PAGE : WPEntity::ENUM_TYPE_POST;
        $id         = $details->ID;
        
        if( $id && in_array(WPAPP::inst()->WPAPI()->WPXQuery()->Type(),['page','singular'])) {
            $mapper     = WPAPP::WPAPI()->content( $entity );
            $query_data = $mapper->where(['ID' => $id])->with(['meta','author', 'media']);
        }

        // Initialize view data
        $view_data = array_merge([],$this->view_data, $this->boilerplate_data);
        $view_data = array_merge([], $view_data, (array) $details);
        
        if( null !== $query_data && !!$query_data->first() ) {
            $view_data = array_merge($view_data, $query_data->first()->data());
            $view_data = $this->normalize( $view_data );  

            // Thumbnails
            try {
                if( array_key_exists('media',$view_data) ) {
                    $thumbnail = WPAPP::WPAPI()->MediaThumbnail( $view_data['media'] );
                    if( !empty($thumbnail) && null != $thumbnail ) {
                        $view_data = array_merge($view_data, ['thumbnail' => $thumbnail]);
                    }
                }
            } catch(Exception $e) {
                $this->Exception( $e );
            }

            // Terms (allowed for both posts and pages)
            $terms = WPAPP::WPAPI()->Terms($id);
            $view_data = array_merge($view_data, ['terms' => $terms]);

            // OG Tags
            $view_data = $this->initOpenGraphTags($view_data);
            
            // WP Head
            $view_data = $this->initNormalizeWPHeadAndFooter($view_data);

            // JSON-LD Schema
            $view_data = $this->initJSONLDSchema($view_data);

            // Content Builder
            $view_data = array_merge([], $view_data, ['content_builder_page' => WPAPP::WPAPI()->getMetadataValueByKey($view_data['meta'], 'content_builder_page')]);
            
            // Disable image defer
            $view_data = array_merge([], $view_data, ['disable_image_defer' => WPAPP::WPAPI()->getMetadataValueByKey($view_data['meta'], 'disable_image_defer')]);

            // ACF Blocks
            if( $cta_blocks = WPAPP::inst()->WPAPI()->getCTABlocks() ) {
                $view_data['cta_blocks'] = $cta_blocks;
            }
        }

        // App settings
        $options = WPAPP::Options()->Options();
        $view_data['options'] = $options;

        $this->ViewData( array_merge($this->view_data ?? [], $view_data) );
        
        return $this;
    }
    /**
     * Initialise boilerplate data
     */
    protected function initBoilerplateViewData() {
        
        // Initialize boilerplate data
        try {
            $inst = WPAPP::inst();
            $post_details = $this->getWPQueryDetails();

            $data = $this->view_data ?: [];
            $data = array_merge($data, ['template_path' => $inst->TemplateURL()]);
            $data = array_merge($data, ['base_url' => $inst->BaseURL()]);
            $data = array_merge($data, ['site_name' => $inst->Title()]);
            $data = array_merge($data, ['view_generator' => View::GENERATOR]);
            $data = array_merge($data, ['environment' => $inst->environment()]);

            $title = ['title' => (array_key_exists('title', $data) &&  $data['title']) ? $data['title'] : $inst->Title()];
            if( function_exists('get_the_title') ) {
                $title = ['title' => get_the_title($post_details->ID)];
            }

            $data = array_merge($data, $title);
            $data = array_merge($data, ['permalink' => $inst->Permalink()]);

            // Media
            $data = array_merge($data, ['wp_postpage_thumbnail' => ($post_details && $post_details->ID) ? $inst->WPAPI()->getPostPageThumbnail($post_details->ID) : null]);

            // Navigation
            $data = array_merge($data, ['navigation_primary' => $inst->WPAPI()->Nav(2)]);
            try {
                $data = array_merge($data, ['navigation_secondary' => $post_details->type == 'page' ? $inst->WPAPI()->SubNav( $post_details->ID ?: 0, 2) : null]);
    
            } catch( \Exception $e ) {
                $this->Exception($e);
            }

            // API
            $data = array_merge($data, ['wpapi' => $inst->WPAPI()]);

            // SITE TREE
            // $data = array_merge($data, ['site_tree' => $inst->WPAPI()->SiteTree()]);

            $this->boilerplate_data = $data;
            $this->view_data = array_merge([], $this->view_data, $this->boilerplate_data);

        } catch( \Exception $e ) {
            $this->Exception($e);
        }

        return $this->boilerplate_data;
    }

    private function initNormalizeWPHeadAndFooter( array $view_data ) {

        if( function_exists('wp_head') ) {
            $WPAPP = WPAPP::inst();
            $WPAPI = $WPAPP->WPAPI();
            
            $header_content = $WPAPI->filterWPHead();
            $footer_content = $WPAPI->filterWPFooter();

            $view_data = array_merge($view_data, [
                'view_wp_head' => $header_content,
                'view_wp_footer' => $footer_content
            ]);
            
            // Log::trace(__METHOD__, $header_content, $footer_content);
        }

        return $view_data;

    }

    public function updateWPHeaderAndFooter( array $view_data ) {
        return $this->initNormalizeWPHeadAndFooter( $view_data );
    }

    /**
     * Iitialise default open graph tags.
     */
    private function initOpenGraphTags( array $view_data ) {
        $view_type = ($this->view->ViewType() == 'singular') ? 'article' : 'website';
        $view_permalink = WPAPP::Permalink();

        $data = [
            'og:type'           => $view_type,
            'og:title'          => $view_data['title'],
            'og:url'            => null != $view_permalink ? $view_permalink : $view_data['base_url'],
            'og:image'          => sprintf("%s%simages/logos/logo.png", WPAPP::BaseURL(), WPAPP::TemplateURL()),
            'og:site_name'      => array_key_exists('site_name', $view_data) ? $view_data['site_name'] : null
        ];

        // Page
        if( $view_type == 'page' ) {
            $data = array_merge( $data, [
                'og:description'    => array_key_exists('og_description', $view_data) ? $view_data['og_description'] : WPAPP::Social('ogdescription')
            ]);
        }

        // Post
        if( $view_type == 'post' ) {
            $data = array_merge( $data, [
                'og:description'    => array_key_exists('excerpt', $view_data) ? $view_data['excerpt'] : null
            ]);
        }

        // OG Tags
        $ogtags = new OGTag(OGTag::ENUM_TAGTYPE_COLLECTION, array_filter($data));

        try {
            $view_data = array_merge($view_data, ['og_tags' => [
                'type'      => sprintf("og:%s",$view_type),
                'metatags'  => $ogtags->render(OGTag::ENUM_COLLECTION_DEFAULT)]
            ]);
        } catch( \Exception $e ) {
            $this->Exception( $e );
        }

        return $view_data;
    }

    public function initJSONLDSchema( array $view_data ) {
        $view_type = ($this->view->ViewType() == 'singular') ? 'article' : 'website';        
        $data       = [];
        $type       = JSONSchema::ENUM_TYPE_WEBSITE;

        $image      = isset($view_data['thumbnail']) ? $view_data['thumbnail']['uri'] : null;
        $author     = isset($view_data['author']) ? $view_data['author']['display_name'] : null;
        $categories = null;// @todo Make a new Category model, A relation in Post and Pages
        $tags       = null;// @todo Make a new Tags model, A relation in Post and Pages

        if( $view_type == 'article' ) {
            $type       = JSONSchema::ENUM_TYPE_BLOGPOSTING;
            $content    = str_replace("\n",'',strip_tags($view_data['post_excerpt']));

            $data = [
                'headline'              => $view_data['title'],
                'alternativeHeadline'   => isset( $view_data['title_alternate'] ) ? $view_data['title_alternate'] : null,
                'image'                 => sprintf("%s%s",WPAPP::BaseURL(), $image),
                'award'                 => null,
                'editor'                => $author,
                'genre'                 => $categories,
                'keywords'              => $tags,
                'publisher'             => $view_data['site_name'],
                'url'                   => $view_data['base_url'],
                'dateCreated'           => date_format($view_data['post_date'], 'Y-m-d'),
                'datePublished'         => date_format($view_data['post_date'], 'Y-m-d'),
                'dateModified'          => date_format($view_data['post_modified'], 'Y-m-d'),
                'description'           => $content,
                'articleBody'           => strip_tags($view_data['content']),
                'author'                => $author
            ];
        }else{
            $data = [
                'name'                  => $view_data['title'],
                'author'                => $author,
                'description'           => WPAPP::Social('ogdescription'), // @todo Get the user-generated version from an options page
                'publisher'             => $view_data['site_name']                
            ];
        }

        $schema = new JSONSchema($data);
        try {
            $result = $schema->render($type);

            if( null != $result ) {
                $view_data = array_merge($view_data, ['jsonld' => $result]);
            }
        } catch( \Exception $e ) {
            $this->Exception( $e );
        }

        return $view_data;
    }

    # +------------------------------------------------------------------------+
    # HELPERS (PUBLIC)
    # +------------------------------------------------------------------------+
    /**
     * Retrieves the essential PAGE/POST properties from the global $wp_query
     * 
     * @return object
     */
    public function getWPQueryDetails($as_array = false) {

        if( NULL === $this->wp_query ) {
            throw new Exception("Unable to render view.");
        }

        $details = [];
        $details['ID'] = null;
        $details['type'] = null;
        $details['status'] = null;

        try {
            $wp_query = $this->wp_query;

            if( $wp_query->queried_object ) {
                $details['ID']      = $wp_query->queried_object->ID;
                $details['type']    = $wp_query->queried_object->post_type;
                $details['status']  = $wp_query->queried_object->post_status;
            }

        } catch( \Exception $e ) {
            $this->Exception( $e );
        }

        return $as_array ? $details : (object) $details;
    }

    # +------------------------------------------------------------------------+
    # HELPERS (PRIVATE)
    # +------------------------------------------------------------------------+
    /**
     * Get the PAGE/POST ID from the global $wp_query.
     * 
     * @return string
     */
    private function getWPQueryID() {
        $id = 0;

        if( NULL === $this->wp_query ) {
            throw new Exception("Unable to render view.");
        }

        try {
            $wp_query = $this->wp_query;
            $id = ( NULL !== $wp_query ) ? $wp_query->queried_object->ID : $id;
        } catch( \Exception $e ) {
            $this->Exception( $e );
        }

        return $id;
    }

    /**
     * Find the root page ID
     * 
     * @return int|boolean Returns the ID of the current page's root (ancestor), false if none exists.
     */
    protected function findRootPage() {
        $request        = Request::createFromGlobals();
        $uri            = parse_url($request->getRequestUri(), PHP_URL_PATH);

        $view_data      = $this->ViewData();
        $root_id        = WPAPP::WPAPI()->isDescendentOf( $uri, $this->view_data );
        
        $this->root_id  = $root_id;
        $this->uri_data = [
            'uri'       => $uri,
            'segments'  => array_filter(explode("/", $uri)),
            'segment'   => basename($uri)
        ];

        $this->view_data = array_merge([], $view_data, [
            'root_page_id' => $root_id
        ]);

        return $root_id;
    }

    /**
     * Applies a series of normalization routines to the post/page content.
     * 
     * @see WPController::normalizeTitle
     * @see WPController::normalizeContent
     * @see WPController::normalizeExcerpt
     * 
     * @param array $content An associative array that represents the view data
     */
    private function normalize( array $content ) {
        $content = $this->normalizeMedia( $content );
        $content = $this->normalizeTitle( $content );
        $content = $this->normalizeContent( $content );
        $content = $this->normalizeExcerpt( $content );
        
        return $content;
    }

    /**
     * Normalize a series of posts.
     * 
     * @param array $normalized_posts A collection of post objects
     */
    private function normalizePosts( array $normalized_posts ) {
        foreach( $normalized_posts as $key => $post ) {
            $normalized_posts[ $key ] = $this->normalize( $post );
        }

        return $normalized_posts;
    }

    /**
     * Unserialize any metadata fields that might be serialized.
     * 
     * @todo Refactor this
     */
    private function normalizeMedia( array $content ) {
        if( (NULL != $content) && (array_key_exists('media', $content)) ) {
            foreach( $content['media'] as $key => $value ) {
                $media = $value;
                // Unserialize data
                if( array_key_exists('metadata', $media) ) {
                    if( Text::serialized($media['metadata']) ) {
                        $content['media'][$key]['metadata'] = @unserialize($media['metadata']);
                    }
                }
                if( array_key_exists('metadata', $media) ) {
                    if( Text::serialized($media['othermetadata']) ) {
                        $content['media'][$key]['othermetadata'] = @unserialize($media['othermetadata']);
                    }
                }
            }
        }

        return $content;
    }
    
    /**
     * Normalizes the post title
     * 
     * @param array $content An associative array that represents the view data
     */
    private function normalizeTitle( array $content ) {
        
        if( (NULL != $content) && (array_key_exists('post_title', $content)) ) {
            $title = $content['post_title'];
        }else if( (NULL != $content) && (array_key_exists('title', $content)) ) {
            $title = $content['title'];
        }else{
            $title = WPAPP::Title();
        }

        $title = WPAPP::inst()->WPAPI()->filterCurlyQuotes( $title );        
        $title = str_replace('&nbsp;',' ', $title);
        $content = array_merge($content, ['title' => $title]);

        return $content;
    }

    /**
     * Normalize the content, 
     * 
     * - Removes WP Blocks comment tags
     * - Converts periods to ellipsis entities,
     * - Removes the <!--more--> tags
     * - Removes comment tags
     * - etc.
     * 
     * @param array $content An associative array that represents the view data
     */
    private function normalizeContent( array $content ) {
        $WPAPP = WPAPP::inst();
        $WPAPI = $WPAPP->WPAPI();
        

        $normalized_content = '';
        if( (NULL != $content) && (array_key_exists('post_content', $content)) ) {
            $normalized_content = $content['post_content'];
            // Allow WordPress to apply additional content filters including utf-8 encoding
            $normalized_content = $WPAPI->applyWPContentFilters( $normalized_content );
            $normalized_content = str_replace(']]>', ']]&gt;', $normalized_content);
            $normalized_content = preg_replace('/<!--(.|\s)*?-->/', '', $normalized_content);
            $normalized_content = preg_replace('/(\.{3})/', '&hellip;', $normalized_content);
            $normalized_content = preg_replace('/<!--more(.*?)?-->/', '', $normalized_content);

            // http:// -> https: //
            $normalized_content = preg_replace('#(http:\/\/)#', 'https://', $normalized_content);

            if( (null != $normalized_content) && !ctype_space($normalized_content) ) {
                $normalized_content = $WPAPI->filterWPContentLinks( $normalized_content, true );
                $normalized_content = $WPAPI->filterWPContentIframes( $normalized_content );

                // Image defferal
                $disable_defer = $WPAPI->getMetadataValueByKey($content['meta'], 'disable_image_defer') ?: false;
                $normalized_content = $WPAPI->filterWPContentImageDefer( $normalized_content, false, $disable_defer );
            }
        }
        $content = array_merge($content, ['content' => $normalized_content]);
        return $content;
    }


    private function normalizeExcerpt( $content ) {

        if( (NULL != $content) && (array_key_exists('post_content', $content) || array_key_exists('content', $content)) ) {
            $excerpt = Text::truncateWords( array_key_exists('content', $content) ? $content['content'] : $content['post_content'], 0, 140 );
        }
        $content = array_merge($content, ['excerpt' => $excerpt]);

        return $content;
    }

    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    /**
     * Generic error handler
     */
    protected function Exception(\Exception $e) {
        Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
    }  

}