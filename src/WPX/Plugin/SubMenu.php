<?php

namespace WPX\Plugin;

use AUX\Utils\Log;
use AUX\Utils\Debug;

/**
 * Class representing a submenu in the WordPress admin.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 * 
 */
class SubMenu extends Menu {

    protected $parent_slug;
    // protected $position = 2;
    
    /**
     * Wrapper method for adding a admin options page
     * @link https://codex.wordpress.org/Function_Reference/add_options_page
     */
    public function addMenuPage() {
        add_submenu_page(
            $this->parent_slug,
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->menu_slug,
            $this->callback_function
        );

        return $this;
    }

}