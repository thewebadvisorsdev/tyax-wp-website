<?php

namespace WPX\Plugin;

use AUX\Utils\Debug;
use WPX\Controller\WPXAdminPageController;

class SubMenuPage extends WPXAdminPageController {

    public function __construct() {
    }

    public function index() {
        Debug::Dump(__METHOD__);
    }
}