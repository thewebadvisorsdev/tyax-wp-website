<?php

namespace WPX\Plugin;

use AUX\Utils\Log;
use AUX\Utils\Debug;
use WPX\Controller\IController;

/**
 * Class representing a submenu in the WordPress admin.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 * 
 */
class Menu {

    
    protected $menu_page_controller;
    protected $page_title;
    protected $menu_title;
    protected $capability;
    protected $menu_slug;
    protected $icon_url;
    protected $position = 99;
    protected $callback_function;
    protected $controller_action = 'index';

    public function __set($key, $value) {
        if( property_exists($this, $key) ) {
            $this->{$key} = $value;    
        }
    }

    public function __get($key) {
        return property_exists($this, $key) ? $this->{$key} : null;
    }

    public function controller() {
        return $this->menu_page_controller;
    }

    /**
     * Submenu constructor.
     * 
     * @param {string} $menu_page_controller    An instance of \WPX\Controller\WPXAdminPageController (or implements \WPX\Controller\IController)
     * @param {array} $options                  An array of options reflecting WordPress' add_options_page function
     * @link https://developer.wordpress.org/reference/functions/add_menu_page/
     */
    public function __construct( $menu_page_controller, array $options) {

        if( ! $menu_page_controller instanceof IController ) {
            throw new \Exception("\$menu_page_controller must implement IController.");
        }

        $this->menu_page_controller = $menu_page_controller;
        $this->controller_action = isset( $options['action'] ) ? $options['action'] : $this->controller_action;

        if( !in_array(  $this->controller_action, $this->menu_page_controller::$allowed_actions ) ) {
            $this->controller_action == 'index';
        }

        $this->callback_function = array( $this->menu_page_controller, $this->controller_action );

        foreach($options as $option => $value) {
            if( property_exists($this, $option) ) {
                $this->{$option} = esc_attr($value);
            }
        }
    }

    /**
     * Initialize the submenu
     */
    public function init() {
        if( !function_exists('add_action') ) return false;
        add_action('admin_menu', array($this,'addMenuPage'));
    }

    /**
     * Wrapper method for adding a admin options page
     * @link https://developer.wordpress.org/reference/functions/add_menu_page/
     */
    public function addMenuPage() {
        add_menu_page(
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->menu_slug,
            $this->callback_function,
            $this->icon_url,
            $this->position
        );

        return $this;
    }

}