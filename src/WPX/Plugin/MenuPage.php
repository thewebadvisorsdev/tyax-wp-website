<?php

namespace WPX\Plugin;

use AUX\Utils\Debug;
use AUX\Utils\Log;
use WPX\WPAPP;
use WPX\Controller\WPXAdminPageController;
use WPX\View\View;

class MenuPage extends WPXAdminPageController {

    public function index() {
        
        $type = $this->view_data['menu_slug'];
        $app = WPAPP::inst();

        try {
            $this->view = new View($app->Twig(), $type, [ $type => 'AdminPage.html.twig' ]);
            // Debug::Dump($this->view);
        }catch( \Exception $e ) {
            Debug::Dump($e->getMessage());
        }
        
        if( null != $this->view ) {
            $this->view->ViewData( $this->ViewData() );
            $this->view->ViewName( __CLASS__, 'index' );
            
            try {
                // Debug::Dump($this->view);
                $this->view->render();
            } catch(\Exception $e) {
                $this->Exception( $e );
            }
            
        }else{
            $this->Exception( new \Exception("Unable to render view.") );
        }
    }
    
}