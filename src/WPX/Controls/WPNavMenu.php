<?php

namespace WPX\Controls;

use AUX\Utils\Log;
use AUX\Utils\Text;

/**
 * Builds a navigational markup structure
 * 
 * @example
 * <ul class="ui-navigation__list ui-navigation__list_unstyled">
 *     <li class="ui-navigation__item">
 *         <a href="javascript:void(0);" class="ui-navigation__link">Some Link</a>
 *     </li>
 * </ul>
 */
class WPNavMenu {
    protected $current_depth = 0;
    protected $output;
    protected $nodes;
    protected $max_depth;
    protected $root_only;
    protected $slug;
    protected $label;
    protected $trailing_slash = false;

    public function __construct( $nodes, bool $root_only = false, $max_depth = -1, $slug = null, int $current_page_id = null, $trailing_slash = false ) {
        $this->nodes            = $nodes;
        $this->root_only        = $root_only;
        $this->max_depth        = $max_depth;
        $this->current_depth    = $max_depth;
        $this->slug             = $slug;
        $this->current_page_id  = $current_page_id;
        $this->trailing_slash   = $trailing_slash;

        if( $this->nodes ) {
            $this->output = $this->build($this->nodes, $this->slug);
        }
        return $this;
    }

    protected function build(array $nodes, $slug = null) {
        if (!$this->nodes || empty($this->nodes)) return null;
        $output = '';

        try {

            // Iterator
            $nodes_it = new \RecursiveIteratorIterator( new \RecursiveArrayIterator($nodes), \RecursiveIteratorIterator::SELF_FIRST );
            $nodes_it->setMaxDepth($this->max_depth ?: -1);
            
            $output = sprintf("%s\n", '<ul class="ui-navigation__list ui-navigation__list_unstyled">');
            
            foreach( $nodes_it as $node ) {

                $id     = $node->ID;
                $link   = (null != $slug) ? sprintf("%s/%s", $slug, $node->post_name) : $node->post_name;
                if( $this->trailing_slash ) {
                    $link = sprintf("/%s/", trim($link, '/'));
                }else{
                    $link = sprintf("/%s", trim($link, '/'));
                }

                // Clenaup
                $link = str_replace('//','/',$link);
                $has_children = ( $node->countChildren() > 0 );

                $slug_class = $this->getClassFromName( $node->post_title );

                $output .= sprintf("\t<li data-id=\"%s\" class=\"ui-navigation__item%s %s %s\">\n",$node->ID, ($has_children && !$this->root_only) ? ' ui-navigation__item_container' : '', $slug_class, $id == $this->current_page_id ? 'current' : '');
                $output .= sprintf("\t\t<a href=\"%s\" class=\"ui-navigation__link\"><span>%s</span></a>", $link, $node->post_title);

                // Nested navligation links
                if( !$this->root_only ) {
                    if( $has_children ) {
                        if( $this->current_depth === -1 ) {
                            // NO DEPTH LIMIT
                            $output .= "\n\t\t\t";
                            $output .= $this->build($node->getChildren(), $link);
                        }else{
                            // LIMIT DEPTH
                            --$this->current_depth;
                            if( $this->current_depth > 0 ) {
                                $output .= "\n\t\t\t";
                                $output .= $this->build($node->getChildren(), $link);
                            }    
                        }
                    }
                }

                $output .= "\t</li>\n";
            }

            $output .= "</ul>\n";


        }catch(\Exception $e) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(),Log::LOG_TYPE_ERROR);
        }

        return $output ?: null;
    }

    protected function getClassFromName( $name ) {
        $slug_class = html_entity_decode( $name );
        $slug_class = strip_tags( $slug_class );
        $slug_class = filter_var($slug_class, FILTER_SANITIZE_STRING );
        $slug_class = Text::slugify($slug_class);

        return $slug_class;
    }

    public function render() {
        return $this->output ?: $this->build($this->nodes, $this->max_depth);
    }
    

}