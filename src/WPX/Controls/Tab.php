<?php
namespace WPX\Controls;

use WPX\HTML\Element;
use AUX\Utils\Log;
use AUX\Utils\Debug;

class Tab extends Element {
    protected static $template      = 'Tab.html.twig';
    protected static $tag           = 'li';
    protected $tab_label;
    protected $tab_url;
    protected $element_classes      = ['wpx-navtabs__item'];

    public function label( $label = null ) {
        if( null != $label ) {
            $this->tab_label = $label;
            return $this;
        }

        return $this->tab_label;
    }
    
    public function url( $url = null ) {
        if( null != $url ) {
            $this->tab_url = $url;
            return $this;
        }

        return $this->tab_url;
    }

    // ---------------------------------------------------------- //
    // HELPERS
    // ---------------------------------------------------------- //
    public function data() {
        try {
            $data = [
                'tag'               => static::$tag,
                'url'               => $this->tab_url,
                'label'             => $this->tab_label,
                'class'             => empty($this->element_classes) ? null : implode(' ', $this->element_classes),
                'attributes'        => $this->arrayToAttributes($this->attributes())
            ];
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }
        return $data;
    }

}