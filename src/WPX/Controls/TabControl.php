<?php
namespace WPX\Controls;

use WPX\HTML\Element;
use WPX\Controls\Tab;
use AUX\Utils\Log;
use AUX\Utils\Debug;
use AUX\Utils\text;

class TabControl extends Element {
    protected static $template      = 'TabControl.html.twig';
    protected static $tag           = 'ul';
    protected $element_classes      = ['wpx-navtabs'];
    protected $tabs = [];

    // ---------------------------------------------------------- //
    // API
    // ---------------------------------------------------------- //
    public function addTab( $label, $url = 'javascript:void(0);', array $attributes = [] ) {
        $id         = sprintf("tab_%s", $this->slugify($label));
        $tab        = new Tab( array_merge([], ['id' => $id], $attributes) );
        
        // Filter the relation tab label
        if( Text::startsWith(strtolower($label), 'wp') ) {
            $replacement = '${1} ';
            $label = preg_replace('/(wpx|wp)/i',$replacement, $label);
            $label_parts = explode(' ', $label);
            foreach( $label_parts as $k => $v ) {
                $label_parts[ $k ] = ucwords($v);
            }
            $label_parts[0] = mb_convert_case($label_parts[0], MB_CASE_UPPER, "UTF-8");
            $label = implode(' ', $label_parts);
        }

        $tab->label($label);
        $tab->url($url);
        $this->tabs[ $label ] = $tab;

        return $this;
    }

    public function removeTab( $label ) {
        if( array_key_exists($label, $this->tabs) ) {
            unset( $this->tabs[$label] );
            $this->tabs = array_values($this->tabs);
        }
        return $this;
    }

    public function replaceTab( $label, $url = 'javascript:void(0);', array $attributes = null ) {
        if( array_key_exists($label, $this->tabs) ) {
            $attr       = array_merge([], ['url' => $url], $attributes);
            $tab        = new Tab(self::$tab_template, $attr);
            $this->tabs[$label] = $tab;
        }
        return $this;
    }

    public function render( $render_engine , array $tab_order = null ) {

        try {
            $twig           = $render_engine;
            $template       = self::$template;
            $tabs_html      = $this->__render($render_engine, $tab_order);
            $element        = $twig->load($template);
            $context        = array_merge([], $this->data(), ['tabs' => $tabs_html]);
            $tab_control    = $element->render( $context );
            return $tab_control;
        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump($e->getMessage());
                Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
            }else{
                Log::trace($e->getMessage(), $this->debug_string_backtrace());
            }
        }

    }
    
    public function __render( $render_engine, array $tab_order = null ) {

        try {
            // Sort the tabs as needed
            $tabs_sorted = [];
            if( null != $tab_order && !empty($tab_order) ) {
                $render_first = [];
                $render_last = [];

                foreach( $tab_order as $key ) {
                    if( in_array($key, $this->tabs ) ) {
                        array_push($render_first, $this->tabs[ $key ]);
                    }else{
                        array_push($render_last, $this->tabs[ $key ]);
                    }
                }

                $tabs_sorted = array_merge( $tabs_sorted, $render_first, $render_last );
            }else{
                $tabs_sorted = $this->tabs;
            }
    
            // Render all tabs
            $tabs_html = [];
            foreach( $tabs_sorted as $tab ) {
                array_push( $tabs_html, $tab->render($render_engine) );
            }

            return implode("\n", $tabs_html);

        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump($e->getMessage());
                Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
            }else{
                Log::trace($e->getMessage(), $this->debug_string_backtrace());
            }
        }

    }
    
    // ---------------------------------------------------------- //
    // HELPERS
    // ---------------------------------------------------------- //

    
}