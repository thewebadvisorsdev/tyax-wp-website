<?php

namespace WPX\Mapper;

use WPX\Entity\Post;
use AUX\Utils\Log;

class WPPageMapper extends PostMapper {

    public static $type = Post::ENUM_TYPE_PAGE;
}