<?php 

namespace WPX\Mapper;

use WPX\Entity\WPEntity;

class PageMapper extends PostMapper {
    public static $type = WPEntity::ENUM_TYPE_PAGE;
}