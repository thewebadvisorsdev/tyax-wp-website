<?php

namespace WPX\Mapper;

use Spot\Mapper;
use WPX\Entity\Post;
use AUX\Utils\Log;
use AUX\Utils\Text;
use WPX\Entity\WPEntity;
use WPX\Enums\EnumStatus;
use WPX\WPAPP;

class PostMapper extends Mapper {

    public static $type = Post::ENUM_TYPE_POST;

    public function latest($limit = 10) {
        $resultset = $this->where(['post_status' => Post::ENUM_STATUS_PUBLISH])
        ->where(['post_type' => self::$type])
        ->having(['post_password IS NULL'])
        ->order(['post_date' => 'DESC'])
        ->with(['author','media'])
        ->limit($limit);

        return $resultset;
    }

    /**
     * Retrieve the current Post
     * 
     * @param   int The `ID` of the desired post
     * @return  Spot\Mapper
     */
    public function byID(int $id) {
        $resultset = $this->where(['ID' => $id])
        ->andWhere(['post_type' => self::$type])
        ->andWhere(['post_status' => WPEntity::ENUM_STATUS_PUBLISH])
        ->first();
        return $resultset;
    }

    public function byCategoryAndTags($category,$tags, $limit = -1, $shuffle = false, array $exclude = []) {
        $posts = null;
        $sql = [];
        array_push($sql,"SELECT");
        array_push($sql,"  DISTINCT p.ID");
        array_push($sql,"FROM");
        array_push($sql,"  wp_posts p,");
        array_push($sql,"  wp_postmeta pm,");
        array_push($sql,"  wp_terms t,");
        array_push($sql,"  wp_term_taxonomy tt,");
        array_push($sql,"  wp_term_relationships tr,");
        array_push($sql,"  wp_terms t2,");
        array_push($sql,"  wp_term_taxonomy tt2,");
        array_push($sql,"  wp_term_relationships tr2");
        array_push($sql,"WHERE");
        array_push($sql,"  p.id = tr.object_id");
        array_push($sql,"  AND t.term_id = tt.term_id");
        array_push($sql,"  AND p.post_type = 'post'");
        array_push($sql,"  AND p.post_status = 'publish'");
        array_push($sql,"  AND tr.term_taxonomy_id = tt.term_taxonomy_id");
        array_push($sql,"  AND p.id = tr2.object_id");
        array_push($sql,"  AND t2.term_id = tt2.term_id");
        array_push($sql,"  AND tr2.term_taxonomy_id = tt2.term_taxonomy_id");
        array_push($sql,"  AND (");
        array_push($sql,"    tt.taxonomy = 'category'");
        array_push($sql,"    AND tt.term_id = t.term_id");
        array_push($sql,"    AND t.name = '%s'");
        array_push($sql,"  )");
        array_push($sql,"  AND (");
        array_push($sql,"    tt2.taxonomy = 'post_tag'");
        array_push($sql,"    AND tt2.term_id = t2.term_id");
        array_push($sql,"    AND t2.name IN (\"%s\")");
        array_push($sql,"    OR t2.slug IN (\"%s\")");
        array_push($sql,"  )");
        array_push($sql,"GROUP BY p.id");
        array_push($sql,"ORDER BY p.post_title, p.post_date ASC");

        // Convert tag names to slugs
        $slugs = [];
        foreach( $tags as $tag ) {
            $s = Text::slugify($tag);
            array_push($slugs, $s);
            if( strpos($s,'-and-') !== false ) {
                $s = str_replace('-and-','-', $s);
                array_push($slugs, $s);
            }
        }

        $sql_statement = sprintf(implode("\n",$sql), $category, implode("\",\"",$tags),  implode("\",\"",$slugs));

        $entities = $this->query( $sql_statement );
        $ids = $entities->resultsIdentities();

        if( !empty( $exclude ) ) {
            $ids = array_diff($ids, $exclude);
        }
        
        // Log::trace('BEFORE', $sql_statement,  $ids);
        
        if( $ids ) {
            if( $shuffle && count($ids) >= $limit ) {
                $ids = array_rand(array_flip($ids), ($limit > 0) ? min($limit, abs(count($ids) - 1)) : (count($ids) - 1) );
                // Log::trace('AFTER', min($limit, abs(count($ids) - 1)) );
                $result_ids = $ids;
            }else{
                $result_ids = ( $limit > 0 ) ? array_slice($ids,0,$limit) : $ids;
            }

            // Log::trace($shuffle ? 'DID SHUFFLE':'NO SHUFFLE', $entities->resultsIdentities(), $result_ids);
            $posts = $this->all()->where(['ID' => $result_ids])->with(['meta']);
        }

        return $posts;
    }

    /**
     * Retieve the previous and next Posts based on the current Post `ID`
     * 
     * @param   int The `ID` of the current post
     * @return  Spot\Mapper
     */
    public function adjacent(int $id) {
        $objPost    = new Post();
        $tablename  = Post::table();
        $pk         = $objPost->primaryKeyField();
        $status     = Post::ENUM_STATUS_PUBLISH;
        $otype      = $objPost->type();

        $sql         = []; 
        $sql[]       = "SELECT DISTINCT * FROM {$tablename} WHERE"; 
        $sql[]       = "(";
        $sql[]       = "{$pk} = IFNULL((SELECT MIN({$pk}) FROM {$tablename} WHERE post_type LIKE '%{$otype}%' AND post_status LIKE '%{$status}%' AND {$pk} > {$id}), 0)";
        $sql[]       = "OR";
        $sql[]       = "{$pk} = IFNULL((SELECT MAX({$pk}) FROM {$tablename} WHERE post_type LIKE '%{$otype}%' AND post_status LIKE '%{$status}%' AND {$pk} < {$id}), 0)";
        $sql[]       = ")";
        $sql[]       = "GROUP BY {$pk}";

        $resultset = $this->query(implode(" ",$sql));  

        return $resultset;
    }

    /**
     * Retieve the previous Post based on the current Post `ID`
     * 
     * @param   int The `ID` of the current post
     * @return  Spot\Mapper
     */
    public function prev(int $id) {
        $tablename  = Post::table();
        $status     = Post::ENUM_STATUS_PUBLISH;
        $pk         = Post::primaryKeyField();
        $otype      = self::$type;

        $sql         = []; 
        $sql[]       = "SELECT DISTINCT * FROM {$tablename} WHERE"; 
        $sql[]       = "{$pk} = IFNULL((SELECT MAX({$pk}) FROM {$tablename} WHERE post_type LIKE '%{$otype}%' AND post_status LIKE '%{$status}%' AND {$pk} < {$id}), 0) ";

        $resultset = $this->query(implode(" ",$sql));
        return $resultset;
    }

    /**
     * Retieve the next Post based on the current Post `ID`
     * 
     * @param   int The `ID` of the current post
     * @return  Spot\Mapper
     */
    public function next(int $id) {
        $tablename  = Post::table();
        $status     = Post::ENUM_STATUS_PUBLISH;
        $pk         = Post::primaryKeyField();
        $otype      = self::$type;
        
        $sql         = []; 
        $sql[]       = "SELECT DISTINCT * FROM {$tablename} WHERE"; 
        $sql[]       = "{$pk} = IFNULL((SELECT MIN({$pk}) FROM {$tablename} WHERE post_type LIKE '%{$otype}%' AND post_status LIKE '%{$status}%' AND {$pk} > {$id}), 0) ";

        $resultset = $this->query(implode(" ",$sql));
        return $resultset;
    }

    public function attachments() {
        $args           = func_get_args();
        $attachments    = [];
        if( count($args) == 0 ) return $attachments;

        $post_parent    = array_shift( $args ) ?: 0;        
        $query = ['post_type' => Post::ENUM_TYPE_ATTACHMENT];

        if( !!$post_parent && is_int($post_parent)) {
            $query = array_merge($query, ['post_parent' => $post_parent]);
        }

        if( !!$post_parent && is_array( $post_parent ) ) {
            $query = array_merge($query, ['post_parent' => $post_parent]);
        }

        try {
            $entities = $this->where($query)->with(['meta'])->order(['post_date' => 'ASC']);

            
            if( $entities ) {
                foreach( $entities->toArray() as $post) {
                    $id                             = $post['post_parent'];
                    $meta                           = $post['meta'] ?? null;
                    $unserialized_meta              = (null != $meta) && (count($meta)  >= 2) ? @unserialize($meta[1]['meta_value']) : null;
                    $unserialized_meta['file_path'] = (null != $unserialized_meta && array_key_exists('file', $unserialized_meta ?: []) )? WP_UPLOADS_DIR . DIRECTORY_SEPARATOR . $unserialized_meta['file'] : null;
                    $attachments[ $id ] = $unserialized_meta;
                }
            }
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }

        return $attachments;
    }    

    public function byTermID( $term_id, $limit  = -1, $as_ids = false, $taxonomy_type = 'category' ) {
        $tablename   = Post::table();
        $status      = Post::ENUM_STATUS_PUBLISH;
        $otype       = self::$type;

        $sql         = [];
        $sql[]       = "SELECT DISTINCT * FROM {$tablename}";
        $sql[]       = "LEFT JOIN wp_term_relationships ON";
        $sql[]       = "(wp_posts.ID = wp_term_relationships.object_id)";
        $sql[]       = "LEFT JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)";
        $sql[]       = "WHERE wp_posts.post_type LIKE '%{$otype}%'";
        $sql[]       = "AND wp_posts.post_status LIKE '%{$status}%'";
        $sql[]       = sprintf("AND wp_term_taxonomy.taxonomy = '%s'", $taxonomy_type);
        $sql[]       = "AND wp_term_taxonomy.term_id = {$term_id}";
        $sql[]       = "ORDER BY wp_posts.post_date DESC";
        if( $limit > 0 ) {
            $sql[]       = "LIMIT {$limit}";
        }

        try {
            $resultset = $this->query(implode(" ",$sql));
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }

        return $as_ids ? $resultset->resultsIdentities() : $resultset;
    }

    public function search($term, $limit = -1, $offset = 0) {
        $sql   = [];
        $sql[] = "(SELECT wp_posts.ID ID, wp_posts.post_title post_title, wp_posts.post_name, wp_posts.post_content, wp_posts.post_status, 'page' as post_type";
        $sql[] = "FROM `wp_posts`";
        $sql[] = "WHERE 1 = 1";
        $sql[] = sprintf("AND (( ( wp_posts.post_title LIKE '%%%s%%' )", $term);
        $sql[] = sprintf("OR ( wp_posts.post_excerpt LIKE '%%%s%%' )", $term);
        $sql[] = sprintf("OR ( wp_posts.post_content LIKE '%%%s%%' ) ))", $term);
        $sql[] = "AND ( wp_posts.post_password = '' )";
        $sql[] = "AND wp_posts.post_type IN ( 'post', 'page' )";
        $sql[] = "AND ( wp_posts.post_status = 'publish' ))";
        $sql[] = sprintf("ORDER BY post_title LIKE '%%%s%%' DESC", $term);
        

        if( $limit > 0 && !$offset ) {
            $sql[] = sprintf("LIMIT  0, %s",$limit);
        }

        if( $limit && $offset ) {
            $sql[] = sprintf("LIMIT %s, %s",$limit, $offset);
        }

        
        $result = $this->query( implode(" ", $sql) );
        
        $result = $result ? $result->toArray(): [];

        return $result;
    }
}