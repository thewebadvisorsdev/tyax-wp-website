<?php

namespace WPX\Mapper;

use WPX\Entity\Post;
use AUX\Utils\Log;

class WPPostMapper extends PostMapper {

    public static $type = Post::ENUM_TYPE_POST;
}