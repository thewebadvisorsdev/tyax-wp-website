<?php
namespace WPX;

use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\DBSettings;
use AUX\Exception;
use AUX\Cache\VarCache;
use AUX\HTTP\HttpStatus;
use AUX\HTTP\Router;
use WPX\Controller\Controller;
use WPX\Utils\URLHandler;
use Twig\SwitchTokenParser;
use Twig\Extension\StringLoaderExtension;
use Twig\Extension\ImageExtension;
use Twig\Extension\MarkupUtilsExtension;
use Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use PageCache\PageCache;
use Sabre\Event\EventEmitter;
use Symfony\Contracts\Cache\ItemInterface;

final class WPAPP {
    # +------------------------------------------------------------------------+
    # CONSTANTS
    # +------------------------------------------------------------------------+
    const WP_DEVELOPMENT    = 'development';
    const WP_STAGING        = 'staging';
    const WP_PRODUCTION     = 'production';
    const WP_MEMORYLIMIT    = '512M';
    const WP_SESSION_NAME   = 'WPXAPP_SESSION';

    const WP_CACHE_LIFETIME = 60; // Seconds

    # +------------------------------------------------------------------------+
    # MEMBERS
    # +------------------------------------------------------------------------+
    private static $instance;
    private static $instance_wpapi;
    private static $instance_autoloader;
    private static $instance_twig;
    private static $instance_options;
    private static $instance_controller;
    private static $instance_session;
    private static $instance_router;
    private static $instance_emitter;
    
    private static $db;
    private static $rawconfig;
    private static $environment;
    private static $environment_settings;
    
    private static $multisite = false;
    private static $document_root;
    private static $public_html;

    private static $page_cache;
    private static $cache_location;
    
    private static $default_title = 'WPX Bootstrap';
    private static $base_url;
    private static $template_path;
    private static $subdirectory = 'wp';
    private static $contentdirectory = 'resources';
    
    private static $prefix = 'wp_';

    private static $theme = 'default';
    private static $theme_default = 'twentynineteen';
    private static $theme_settings;
    private static $theme_templates = [];
    private static $social_settings;

    private static $wp_permalink;
    private static $wp_memorylimit;
    private static $tmp_dir;

    # +------------------------------------------------------------------------+
    # ACCESSORS
    # +------------------------------------------------------------------------+
    /**
     * Gets the environment path
     * 
     * @return string
     */
    public static function BasePath() {
        return self::$document_root;
    }

    /**
     * Gets the public_html path
     * 
     * @return string
     */
    public static function PublicPath() {
        return rtrim(self::$public_html, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    /**
     * Gets the directory path to the current theme dirctory
     * 
     * @return string
     */
    public static function TemplatePath() {
        return self::$template_path ?: $template_path = self::DS(sprintf("%s/public_html/%s/themes/%s/", rtrim(self::$document_root, DIRECTORY_SEPARATOR),self::$contentdirectory, self::$theme));
    }

    /**
     * Gets the url to the current theme directory
     * 
     * @return string
     */
    public static function TemplateURL() {
        return self::DS(sprintf("%s/themes/%s/", self::$contentdirectory, self::$theme));
    }

    /**
     * Get's the base url
     * 
     * @return string Returns the base URL of the site
     */
    public static function BaseURL() {
        return self::$base_url . '/';
    }

    /**
     * Get the system's tmp directory
     */
    public static function tmp() {
        return self::$tmp_dir;
    }

    /**
     * Gets or sets the permalink generated by WordPress
     * 
     * @param   string  $permalink  The WordPress generated permalink
     * @return  string  Returns WordPress' permalink
     */
    public static function Permalink( string $permalink = null ) {
        if( isset($permalink) ) {
            self::$wp_permalink = $permalink;
        }

        return self::$wp_permalink;
    }

    /**
     * Get's the url to the content directory
     * 
     * @return string
     */
    public static function PublicURL() {
        return self::BaseURL() . self::$contentdirectory . '/';
    }

    /**
     * Gets or sets a default title
     * 
     * @param string $title
     * @return string
     */
    public static function Title($title = null) {
        if( null != $title ) {
            self::$default_title = $title;
        }

        return getenv('WP_SITETITLE') ?: self::$default_title;
    }

    /**
     * Gets or sets a default title
     * 
     * @param   string  $key    The index of a social settings element
     * @return  string  The value of $key
     */
    public static function Social(string $key = null) {
        if( isset($key) ) {
            return array_key_exists($key, self::$social_settings) ? self::$social_settings[ $key ] : null;
        }

        return self::$social_settings;
    }

    /**
     * Gets the current environment.
     * 
     * @see WPEnvironment::WP_DEVELOPMENT, WPEnvironment::WP_STAGING, WPEnvironment::WP_PRODUCTION
     * 
     * @return int
     */
    public static function environment() {
        return self::$environment;
    }

    /**
     * Sets whether or not to configure WP with multisite.
     * 
     * @param type $use_multisite
     */
    public static function multisite($use_multisite) {
        self::$multisite = $use_multisite;
    }

    /**
     * Gets the current database details based on the environment
     * 
     * @return DBSettings
     */
    public static function db() {
        return self::$db;
    }

    public static function session() {
        return self::$instance_session;
    }

    public static function router( Router $router = null ) {
        if( null != $router ) {
            self::$instance_router = $router;
        }

        return self::$instance_router;
    }

    public static function emitter( EventEmitter $emitter = null ) {
        if( null != $emitter ) {
            self::$instance_emitter = $emitter;
        }

        return self::$instance_emitter;
    }

    /**
     * Gets the current database prefix
     * 
     * @return string
     */
    public static function prefix() {
        return self::$prefix;
    }

    /**
     * Retrieve an environment variable
     * 
     * @return mixed
     */
    public static function env($key) {
        return array_key_exists($key, self::$rawconfig) ? self::$rawconfig[ $key ] : null;
    }
    
    /**
     * Retrieve an theme variable
     * 
     * @return mixed
     */
    public static function ThemeSettings($key) {
        return array_key_exists($key, self::$theme_settings) ? self::$theme_settings[ $key ] : null;
    }

    /**
     * Retrieve the raw configuration data
     * 
     * @return array
     */
    public static function raw() {
        return self::$rawconfig;
    }

    /**
     * Override the memory limit
     * 
     * @return string
     */
    public static function MemoryLimit($limit_mb) {
        self::$wp_memorylimit = $limit_mb;
    }

    /**
     * Set or get an instance of either Composer's autoloader or the built-in Autoload class.
     * 
     * If Composer's autoloader is used, add additional namespaces with WPAPP::autoloader()->add('NameSpace', 'path/to/source/');
     * 
     * @param   object  $autoloader   A reference to composer's autoloader instance.
     * @return  mixed   Returns Composer's autoloader instance.
     */
    public function autoloader( $autoloader = null ) {
        if( null != $autoloader ) {
            self::$instance_autoloader = $autoloader;
            
            // Register additional WP Plugin Classes
            $this->autoloadPluginClasses();
        }

        return self::$instance_autoloader;
    }

    public static function cachelocation() {
        return self::$cache_location;
    }

    public static function cachelifetime() {
        $cache_lifetime = !self::bypassCache() ? getenv('WP_CACHE_LIFETIME') ?: self::WP_CACHE_LIFETIME : 0;
        return (int) $cache_lifetime;
    }

    /**
     * Gets or sets an instance of WPX\Controller
     * 
     * @param Controller $instance
     * @return Controller
     */
    public static function Controller( Controller $instance = null ) {
        if( isset($instance) ) {
            self::$instance_controller = $instance;
        }

        return self::$instance_controller;
    }    

    # +------------------------------------------------------------------------+
    # ROUTING
    # +------------------------------------------------------------------------+
    public static function Render( $template_override = null, $viewname_override = null, array $options = [] ) {
        $controller = self::$instance_controller;

        if( null != $controller ) {
            $ctype = self::WPAPI()->Type();
            $method = 'index';

            switch( $ctype ) {
                case WPQuery::ENUM_TYPE_ARCHIVE :
                case WPQuery::ENUM_TYPE_YEAR :
                case WPQuery::ENUM_TYPE_MONTH :
                case WPQuery::ENUM_TYPE_DATE :
                    $method = 'archive';
                    break;
                case WPQuery::ENUM_TYPE_SEARCH :
                    $method = 'search';
                    break;
                case WPQuery::ENUM_TYPE_HOME :
                    $method = 'home';
                    break;
                case WPQuery::ENUM_TYPE_404 :
                    $method = 'error';
                    break;
                case WPQuery::ENUM_TYPE_PAGE :
                case WPQuery::ENUM_TYPE_SINGLE :
                case WPQuery::ENUM_TYPE_SINGULAR :
                default :
                    $method = 'index';
                    break;
    
            }

            // Fallback to index
            if( (property_exists($controller, 'allowed_actions') && in_array($method,$controller::$allowed_actions)) ) {
                $method = 'index';
            }

            if( method_exists($controller, $method) ) {
                call_user_func_array( array($controller, $method), func_get_args() ?? [] );
            }else{
                throw new Exception("Missing controller method, `{$method}`. Unable to route request.");
            }
        }
    }

    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    private function __construct() {}

    /**
     * Static constructor for WPAPP
     */
    public static function inst() {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function init($environtment_root, $environment_settings) {

        // Register a shutdown handler
        register_shutdown_function([self::inst(), 'shutdown']);

        // Path to the environment settings
        self::$document_root        = self::DS(rtrim($environtment_root, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
        self::$public_html          = self::DS(rtrim($environtment_root . 'public_html' . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
        self::$cache_location       = self::DS($environtment_root . 'cache');
        self::$environment_settings = $environment_settings;

        // Load the .evn config file
        if ($this->config()) {
            $this->initSessions();
            $this->initEventEmitter();
            $this->initAdditionalSettings();
            $this->initWPConfig();
            $this->initErrorLogging();
            $this->initThemeSettings();
            $this->initSocialSettings();
        }
    }

    public function shutdown() {
        $error = error_get_last();
        if ($error['type'] === E_ERROR) { 
            Log::trace(__METHOD__, $error, E_ERROR, Log::LOG_TYPE_ERROR);
        } 
    }

    /**
     * Enable sessions in WordPress.
     * - Add hook for logout
     * - Add hook for login
     */
    public function initSessions() {
        $instance = self::inst();
        return $instance->SessionStart();
    }

    public function initEventEmitter() {
        $instance = self::inst();
        return $instance->emitter( new EventEmitter() );
    }

    public function SessionStart() {
        if(!session_id()) {
            self::$instance_session = new Session();
            self::$instance_session->setName(self::WP_SESSION_NAME);
            self::$instance_session->start();
        }

        return self::$instance_session ?: null;
    }

    public function SessionEnd() {
        if( self::$instance_session ) {
            self::$instance_session->invalidate();
        }
    }

    /**
     * Static Constructor for WPAPI
     */
    public static function WPAPI() {
        if (self::$instance_wpapi == null) {
            self::$instance_wpapi = new WPAPI();

            self::Options();
        }

        return self::$instance_wpapi;
    }
    
    public static function Options() {
        if (self::$instance_options == null) {
            self::$instance_options = WPOptions::getInstance();
        }
    
        return self::$instance_options;
    }

    /**
     * @todo Initialize any datamodels. Always runs in a development environment; on production environments, 
     * it runs only when `WP_MIGRATE=1` is configured in the `.env` file.
     */
    public function initDataModels() {
        $instance = self::inst();

        if( self::$environment == self::WP_DEVELOPMENT || (getenv('WP_MIGRATE') == 1) ) {
            try {
                $entities = [
                    'WPX\Entity\Media'
                ];
    
                while( null !== ($entity = array_shift($entities)) ) {
                    $mapper     = $instance::WPAPI()->Mapper($entity);
                    $mapper->migrate();
                }
    
            }catch( \Exception $e ) {
                Log::log("Failed to initialize data models.\n\n" . $e->getMessage() . "\n\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            }
        }
    }

    /**
     * Twig instance
     */
    public function Twig() {

        if (self::$instance_twig == null) {

            try {
                $template_root = self::TemplatePath() . 'templates';
                $autoload_twig = new \Twig_Loader_Filesystem( $template_root );

                // Theme Templates
                $autoload_twig->addPath($template_root . DIRECTORY_SEPARATOR . 'Includes');
                $autoload_twig->addPath($template_root . DIRECTORY_SEPARATOR . 'Components');
                $autoload_twig->addPath($template_root . DIRECTORY_SEPARATOR . 'Layout');
                
                // Model Admin Templates
                $autoload_twig->addPath(APP_PATH . self::DS('src/resources/assets/templates') );
                $autoload_twig->addPath(APP_PATH . self::DS('src/resources/assets/templates/Includes') );
                $autoload_twig->addPath(APP_PATH . self::DS('src/resources/assets/templates/Layout') );
                
                // Twig Extensions
                self::$instance_twig = new \Twig_Environment($autoload_twig);//, ['strict_variables' => false]
                self::$instance_twig->addTokenParser( new SwitchTokenParser() );
                self::$instance_twig->addExtension( new StringLoaderExtension() );
                self::$instance_twig->addExtension( new \Twig_Extensions_Extension_Text() );
                self::$instance_twig->addExtension( new \Twig_Extension_Debug() );

                // Custom Extensions
                self::$instance_twig->addExtension( new ImageExtension() );
                self::$instance_twig->addExtension( new MarkupUtilsExtension() );

            } catch( \Exception $e ) {
                echo $e->getMessage();
            }

        }

        return self::$instance_twig;
    }

    # +------------------------------------------------------------------------+
    # METHODS (PRIVATE)
    # +------------------------------------------------------------------------+
    /**
     * Parse the environment settings
     * 
     * @throws Exception 
     */
    private function initAdditionalSettings() {
        try {
            $settings_yaml = self::$environment_settings;

            if (!file_exists($settings_yaml) && !is_readable($settings_yaml)) {
                throw new \Exception("{$settings_yaml} does not exist. Please ensure the path to this file is correct.");
            }

            // Load additional configuration files *.yaml
            $cache = new VarCache($settings_yaml, self::$cache_location);
            $config = self::$rawconfig = $cache->deserialize();
            
            if( !is_array($config) || null == $config || !$config ) {
                $config = self::$rawconfig = Yaml::parseFile($settings_yaml);
                $cache->serialize($config);
            }

            // Theme
            self::$theme            = (array_key_exists('theme', $config) && isset($config['theme'])) ? $config['theme'] : 'default';

            // Check if multisite is enabled
            if ($config['network'] != null && array_key_exists('multisite', $config['network'])) {
                self::$multisite = (bool)$config['network']['multisite'];
            }

            if (self::$environment == null || self::$document_root == null  || self::$base_url == null) {
                Log::Log('500 Internal Server Error','There was a problem reading the environment configuration settings. Please check your settings file.');
                header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
                die();
            } else {
                return true;
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Settings found in .env
     */
    private function config() {

        $env = Dotenv::create(APP_PATH);
        $env->load();
        
        if( $env->required(['WP_ENVIRONMENT', 'WP_HOME', 'WP_SUBDIRECTORY', 'WP_RESOURCES']) ) {
            self::$environment      = getenv('WP_ENVIRONMENT');
            self::$base_url         = getenv('WP_HOME');
            self::$subdirectory     = getenv('WP_SUBDIRECTORY');
            self::$contentdirectory = getenv('WP_RESOURCES');            
            self::Title(getenv('WP_SITETITLE') ?: self::$default_title);
        }else{
            throw new \Exception("Missing critical settings. Please check your .env file.");
        }        

        if( $env->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_HOST', 'DB_PREFIX']) ) {
            $env_keys = array_keys($_ENV);
            $db_settings = [];
            while( null !== ($key = array_shift($env_keys) ) ) {
                $val = getenv($key);
                define($key, $val);
                if (strpos($key, 'DB_') !== false) {
                    $db_settings[$key] = $val;
                }                
            }
            self::$db = new DBSettings($db_settings);
        }else{
            throw new \Exception("Missing critical settings. Please check your .env file.");
        }

        define('DB_CHARSET', 'utf8');
        define('DB_COLLATE', 'utf8mb4_unicode_ci');
        
        // Add the table prefix to $GLOBALS
        // $GLOBALS['table_prefix'] = getenv('DB_PREFIX');

        return true;
    }    

    private function initWPConfig() {
        try {

            // SSL SUPPORT
            define('FORCE_SSL_LOGIN',true);
            define('FORCE_SSL_ADMIN', FORCE_SSL_LOGIN);

            // Memory limit
            $memory = isset(self::$wp_memorylimit) ? self::$wp_memorylimit : self::WP_MEMORYLIMIT;
            define( 'WP_MEMORY_LIMIT', $memory );
            @ini_set( 'upload_max_size' , $memory );

            // Theme
            define('WP_USE_THEMES', true);
            define('WP_DEFAULT_THEME', 'default');

            // Absolute path to the WordPress directory.
            define('ABSPATH', self::DS(sprintf("%s%s/%s/", self::$document_root, 'public_html', self::$subdirectory)));
            
            // WP CONFIG PATH
            define('WP_CONFIG_BASE_PATH', self::DS(self::$document_root));

            // The current site URL - PUBLIC
            if( !defined('WP_HOME') ) {
                define('WP_HOME', self::$base_url );
            }

            // The current site URL - Where WordPress is installed
            // @link https://wordpress.stackexchange.com/a/123854
            if( !defined('WP_SITEURL') ) {
                define('WP_SITEURL', self::$base_url  . "/" . self::$subdirectory);
            }

            // The new content path
            define('WP_CONTENT_DIR', self::DS(self::$document_root . 'public_html/' . self::$contentdirectory));
            define('WP_CONTENT_URL', self::$base_url . '/' . self::$contentdirectory);

            // Uploads directory
            define('WP_UPLOADS_DIR', self::DS(WP_CONTENT_DIR . '/uploads'));
            define('WP_UPLOADS_URL_ABSOLUTE', WP_CONTENT_URL . '/uploads');
            define('WP_UPLOADS_URL_RELATIVE', self::$contentdirectory . '/uploads');

            // Common Plugin directories
            define('WP_PLUGIN_DIR', self::DS(self::$document_root . 'public_html/' . self::$contentdirectory . '/plugins'));
            define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');

            // MU-Plugins
            define('WPMU_PLUGIN_DIR', self::DS(self::$document_root . 'public_html/' . self::$contentdirectory . '/mu-plugins'));
            define('WPMU_PLUGIN_URL', WP_CONTENT_URL . '/mu-plugins');

            // Theme
            if( !defined('WP_DEFAULT_THEME') ) {
                define( 'WP_DEFAULT_THEME', self::$theme ? self::$theme : self::$theme_default );
            }

            // Language
            define ('WPLANG', '');
            
            // REVISIONS
            define ('WP_POST_REVISIONS', 3);
            define( 'AUTOSAVE_INTERVAL', 600 );

            // define('COOKIE_DOMAIN', WP_SITEURL); // don't omit the leading '.'
            // define('COOKIEPATH', preg_replace('|https?://[^/]+|i', '', WP_HOME));
            // define('SITECOOKIEPATH', preg_replace('|https?://[^/]+|i', '', WP_SITEURL));
            // define('PLUGINS_COOKIE_PATH', preg_replace('|https?://[^/]+|i', '', WP_PLUGIN_URL));

            // // ADMIN DIRECTORY
            // define('WP_ADMIN_DIR', 'admin');
            // define('ADMIN_COOKIE_PATH', SITECOOKIEPATH . WP_ADMIN_DIR);

            // CACHING SUPPORT
            define('WP_CACHE_DIR', self::DS(self::$cache_location . '/') );

            // Multisite configuration
            if (self::$multisite) {
                define('WP_ALLOW_MULTISITE', true);
            }

            if (self::$multisite && WP_ALLOW_MULTISITE) {
                define('MULTISITE', true);
                define('SUBDOMAIN_INSTALL', false);
                define('DOMAIN_CURRENT_SITE', parse_url(self::$base_url, PHP_URL_HOST));
                define('PATH_CURRENT_SITE', self::DS('/'));
                define('SITE_ID_CURRENT_SITE', 1);
                define('BLOG_ID_CURRENT_SITE', 1);
            }
            
            define('DISALLOW_FILE_EDIT', true);
            define('DISALLOW_FILE_MODS', self::$environment !== WPAPP::WP_PRODUCTION);

            // Store the tmp directory
            self::$tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
            
            if ( in_array('key',$_REQUEST) &&  $_REQUEST['key'] == getenv('WP_HARDENKEY') ) {
                setcookie( 'updatebypass', 1 );
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    private function initThemeSettings() {
        try {
            $config = self::$rawconfig['themesettings'];
            self::$theme_settings = $config;

            $wpx_template_path = self::TemplatePath();
            $theme_yaml = $wpx_template_path . "theme.yaml";

            if (!file_exists($theme_yaml) && !is_readable($theme_yaml)) {
                if( defined('WP_DEBUG') && WP_DEBUG ) {
                    // Log::log("{$theme_yaml} does not exist. Please ensure the path to this file is correct.", Log::LOG_TYPE_ERROR);
                }
            }else{
                // Load additional configuration theme files: theme.yaml
                $theme_cache = new VarCache($theme_yaml, self::$cache_location);
                $theme_config = $theme_cache->deserialize();
                
                if( !is_array($config) || null == $config || !$config ) {
                    $theme_config = self::$rawconfig = Yaml::parseFile($theme_yaml);
                    $theme_cache->serialize($theme_config);
                    self::$theme_templates = $theme_config;
                }
            }
        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump('Unable to initialize theme settings.');
            }else{
                Log::log('Unable to initialize theme settings.', Log::LOG_TYPE_ERROR);
            }
        }
    }

    private function initSocialSettings() {
        try {
            $config = self::$rawconfig['social'];
            self::$social_settings = $config;
        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                Debug::Dump('Unable to initialize social settings.');
            }else{
                Log::log('Unable to initialize social settings.', Log::LOG_TYPE_ERROR);
            }
        }
    }

    // ---------------------------------------------------------- //
    // CACHING
    // ---------------------------------------------------------- //
    /**
     * Callback function for ob_start() calls to capture and cache the rendered output.-acf
     * @param string $buffer_output The buffer output to cache.
     * @return string The buffered output.
     */
    public static function obPageCache( $buffer_output ) {
        try {
            
            $request            = Request::createFromGlobals();
            $path               = $request->getPathInfo();
            $url_handler        = new URLHandler($path);

            $cache_lifetime     = WPAPP::cachelifetime();
            $cache_namespace    = str_replace('\\','_', WPAPP::class);
            $cache_index        = sprintf("%s.cache", md5( rtrim($url_handler->path(),'/') ));
            $cache_fsa          = new FilesystemAdapter($cache_namespace,$cache_lifetime,rtrim(WPAPP::cachelocation(), DIRECTORY_SEPARATOR));
            $cache              = new TagAwareAdapter($cache_fsa);

            $cached_output      = $cache->get( $cache_index, function (ItemInterface $item) use($cache_index) {
                $item->tag('wpx_view');
                $item->tag($cache_index);
                return $item;
            });

            if( is_object($cached_output) && ( is_subclass_of($cached_output, ItemInterface::class) ) ) {
                $cache->save( $cached_output->set($buffer_output) );
            }

            return $buffer_output;
            
        } catch( \Throwable $t ) {
            Log::log($t->getMessage() . "\n" . $t->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }  catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }  
        
        return $buffer_output;
    }

    public static function flushCache( $force_flush=0 ) {
        $flushed            = false;
        
        $request            = Request::createFromGlobals();
        $ip                 = $request->getClientIp();
        $path               = $request->getPathInfo();
        $url_handler        = new URLHandler($path);
        $cache_buster       = $force_flush ?: $request->query->get('wpxcache', null);    
        
        $cache_lifetime     = self::cachelifetime();
        $cache_namespace    = str_replace('\\','_', WPAPP::class);
        $cache_index        = sprintf("%s.cache", md5( rtrim($url_handler->path(),'/') ));
        $cache_fsa          = new FilesystemAdapter($cache_namespace,$cache_lifetime,rtrim(self::cachelocation(), DIRECTORY_SEPARATOR));
        $cache              = new TagAwareAdapter($cache_fsa);

        // Cache Invalidation
        if( null !== $cache_buster ) {
            try {
                switch( $cache_buster )  {
                    case 1 : // Invalidate cache by $cache_index (path)
                        $flushed = $cache_fsa->delete($cache_index) && $cache->delete($cache_index) && $cache->invalidateTags(['stakeholder_map_data',$cache_index]);
                        Log::log("Invalidating page: ${path} from IP (${ip}).", Log::LOG_TYPE_NOTICE);
                        break;
                    case 'all' :
                    case 2 : // Invalidate cache by tag
                        $cache->invalidateTags(['stakeholder_map_data','wpx_view']);
                        $flushed = self::flush();
                        
                        $ip_whitelist = array_filter(explode(",",getenv('WP_IP_WHITELIST') ?: ''), 'trim');
                        if( in_array($ip, $ip_whitelist) ) {
                            Log::log("Invalidating all cache items from approved IP (${ip})", Log::LOG_TYPE_NOTICE);
                        }else{
                            Log::log("Cache invalidation attempted from non-approved IP (${ip})", Log::LOG_TYPE_NOTICE);
                            // $response =  new Response(HttpStatus::getMessageForCode(HttpStatus::HTTP_FORBIDDEN), HttpStatus::HTTP_FORBIDDEN);
                            // $response->send();
                            // die();
                        }
                        break;
                }    
            }catch( \Exception $ex ) {
                Log::log( $ex->getMessage() . "\n" . $ex->getTraceAsString(), Log::LOG_TYPE_ERROR);
            }
        }
        
        return $flushed;

    }

    private static function flush() {
        $flushed = false;
        $dir = self::cachelocation(); 

        $dir            = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS); 
        $dir            = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::CHILD_FIRST); 
        $cache_log      = [];
        $cache_log_dev  = [];

        try {
            foreach ( $dir as $file ) {  
                array_push($cache_log, $file->isDir() ? "[d]\t${file}" : "[f]\t${file}");
                if( self::environment() != self::WP_PRODUCTION ) {
                    array_push($cache_log_dev, $file->isDir() ? "[d]\t${file}": "[f]\t${file}");
                }
                $file->isDir() ?  rmdir($file) : unlink($file); 
            }

            if( self::environment() != self::WP_PRODUCTION && !empty($cache_log_dev) ) {
                Log::log("CACHE CLEARED (DEV):\n" . implode("\n", $cache_log_dev), Log::LOG_TYPE_NOTICE);
            }else{
                
            }
    
            if( !empty($cache_log) ) {
                $cache_log[] = "\n";
            }
            $cache_log = array_merge([], $cache_log, scandir(self::cachelocation()));

            $flushed = true;
            Log::log("CACHE CLEARED:\n" . implode("\n", $cache_log), Log::LOG_TYPE_NOTICE);
        }catch( \Exception $ex ) {
            Log::log( $ex->getMessage() . "\n" . $ex->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return $flushed;
    }

    public static function isCacheEnabled() {
        $request            = Request::createFromGlobals();
        if( $request->query->has('p') ) return false;        
        $cache_enabled = (true == (bool) getenv('WP_CACHE_ENABLED')) || (true == (bool) getenv('WP_DEBUG_CACHE'));
        return $cache_enabled;
    }

    /**
     * Do not serve a cached page if:
     * Case 1. It's a stakeholder Map API request, e.g. /partners/load/*
     * Case 2. It's a stakeholder application form, e.g. /partners-form
     * Case 3. It's a page request (non-permalink), e.g. ?p=4832
     * Case 4. It's a search request, e.g. ?s=foo
     * Case 5. Admins are logged in (session check)
     * 
     * @return Bool Returns true if one of the 4 use cases are met, false if otherwise
     */
    public static function bypassCache() {
        $session            = self::session();
        $request            = Request::createFromGlobals();
        $path               = $request->getPathInfo();
        $url_handler        = new URLHandler($path);

        // Case: 3
        if( $request->query->has('p') ) return false;

        // Test
        // Case: 4
        $bypass_cache       = $request->query->has('s');
        // Case: 1
        $bypass_cache       = (true == strpos($url_handler->path(), '/partners/load')) || $bypass_cache;
        // Case: 2
        $bypass_cache       = (true == strpos($url_handler->path(), '/partners-form')) || $bypass_cache;
        // Case: 5
        $bypass_cache       = (true == (bool) $session->get('wp_admin_logged_in', false)) || $bypass_cache;

        return $bypass_cache;
    }

    // ---------------------------------------------------------- //
    // STATIC HELPERS
    // ---------------------------------------------------------- //
    
    /**
     * Sets the errorlogging based on the environment
     * 
     * This will log all errors notices and warnings to a file called debug.log in
     * wp-content only when WP_DEBUG is true. if Apache does not have write permission, 
     * you may need to create the file first and set the appropriate permissions (i.e. use 666).
     * 
     * @throws Exception
     */
    private function initErrorLogging() {
        try {
            // Errors visible on staging and development only
            $IsSiteDev = (self::$environment == self::WP_DEVELOPMENT);
            define('WP_DEBUG', $IsSiteDev); 
            define('WP_DEBUG_DISPLAY', false);
            @ini_set('display_errors', 0);

            if( defined('WP_DEBUG') && WP_DEBUG ) {
                define('WP_DEBUG_LOG', $IsSiteDev);
                error_reporting(E_ALL ^ E_WARNING);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function DS($path) {
        $ds = DIRECTORY_SEPARATOR;
        return str_replace(array("/","\\"),$ds,$path);
    }

    /**
     * Check if SSL is enabled for the current environment
     * @link https://stackoverflow.com/a/41591066
     * @return {boolean} Returns true if SSL is enabled, false if otherwise
     */
    private static function isSSLEnabled() {
        if (
            ( ! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || ( ! empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
            || ( ! empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
            || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
            || (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == 443)
            || (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https')
        ) {
            return true;
        } else {
            return false;
        }
    }

    // ---------------------------------------------------------- //
    // AUTOLOAD PLUGIN CLASSES
    // ---------------------------------------------------------- //
    /**
     * Include plugin classes for autoloading
     */
    private function autoloadPluginClasses() {
        if( null != self::$instance_autoloader ) {
            $wpx_plugins_dir    = APP_PATH . 'build/plugins';
            $wpx_plugins_fsi    = new \FilesystemIterator($wpx_plugins_dir, \FilesystemIterator::SKIP_DOTS);
            foreach($wpx_plugins_fsi as $entry) {
                if( $entry->isDir() ) {
                    $path = sprintf("%s%s", $entry->getPathname(), self::DS('/src/WPX'));
                    self::$instance_autoloader->addPsr4('WPX\\', $path);
                }
            }     
            
        }
    }
    

    # +------------------------------------------------------------------------+
    # METHODS (CLONE)
    # +------------------------------------------------------------------------+
    private final function __clone() { }
    private final function __wakeup() { }
}