<?php

namespace WPX\View;

use \Exception;
use AUX\Utils\Log;
use AUX\Utils\Debug;
use WPX\WPAPP;


class WPView extends View {

    protected $template_defaults = [
        'page'      => 'Page.html.twig',
        'singular'  => 'Singular.html.twig',
        'archive'   => 'Archive.html.twig',
        'year'      => 'Archive.html.twig',
        'month'     => 'Archive.html.twig',
        'day'       => 'Archive.html.twig',
        'search'    => 'Search.html.twig',
        '404'       => '404.html.twig',
        'home'      => 'Homepage.html.twig',
    ];

    /**
     * Initialise additional templates that this View will likely need.
     * 
     * @param array $templates Overrides the default templates in WPX\View\View.
     * @return View Returns the current view instance for fluid method chaining
     */
    protected function initTemplates(array $templates) {

        if( empty($templates)) {
            $vtype = $this->view_type;

            switch( $vtype ) {
                case 'archive' :
                case 'year' :
                case 'month' :
                case 'day' :
                    $this->template = $this->template_defaults['archive'];
                    break;
                case 'search' :
                    $this->template = $this->template_defaults['search'];
                    break;
                case '404' :
                    $this->template = $this->template_defaults['404'];
                    break;
                case 'home' :
                    $this->template = $this->template_defaults['home'];
                    break;
                case 'singular' :
                    $this->template = $this->template_defaults['singular'];
                    break;
                default :
                    $this->template = $this->template_defaults['page'];
                    break;
            }            

        }else{
            $new_templates = array_replace($this->templates,$templates);
            $this->templates = $new_templates;
            $layout_key        = array_keys($this->templates)[0];
            $this->template = $this->templates[ $layout_key ];
        }

        return $this;
    }

    # +------------------------------------------------------------------------+
    # VIEW RENDERING
    # +------------------------------------------------------------------------+
    /**
     * Handles any business logic before rendering the final output.
     * 
     * @see View::renderWith()
     * @return void
     */
    public function render() {
        $template_override = (func_num_args() > 0) ? func_get_arg(0) : null; 

        $data = [
            'view_name'     => $this->view_name,
            'view_layout'   => $template_override ? $template_override: $this->template,
            'view_type'     => $this->view_type
        ];
        
        if( !empty($this->view_data) ) {
            
            $data       = array_merge($data, $this->view_data);
            $template   = $template_override ? $template_override: $this->template;
            if ( NULL == $template ) {
                $this->initTemplates($this->template_defaults);
            }

            $this->renderWith($template, $data);
            
        } else {
            // @todo render nice ERROR page
            // $this->renderWith('Page.html.twig', $data);
        }
    }

}