<?php

namespace WPX\View;

use \Exception;
use AUX\Utils\Log;
use AUX\Utils\Debug;
use WPX\WPAPP;
use Symfony\Component\HttpFoundation\Request;

class View implements IView {

    const GENERATOR = '<meta name="generator" content="WPX" />';
    
    protected $template_parser;

    protected $view_data = [];

    protected $view_type;

    protected $view_name = "View";

    protected $template;

    protected $templates = ['page' => 'Page.html.twig'];

    protected $template_defaults = [ 'page' => 'Page.html.twig' ];

    protected $layout = 'Page.html.twig';

    /**
     * Gets or sets the $view_name with an optional value from the index.
     * 
     * @param string The action to render. Defaults to 'index' if none supplied.
     * @return string Returns "ClassNameFoo" if no $action is supplied, "ClassNameFoo_action" otherwise
     */
    public function ViewName( $class, $action = "index" ) {
        $ViewName = substr($class, strrpos($class, '\\') + 1);
        $ViewName = ( null != $action ) ? "{$ViewName}_{$action}" : $ViewName;

        $this->view_name = $ViewName;
        return $this->view_name;
    }

    /**
     * Gets or sets the $view_type
     * 
     * @param string $view_type e.g. `home`, `archive`, `page`, etc.
     */
    public function ViewType( $view_type = null ) {
        if( null != $view_type ) {
            $this->view_type = $view_type;
        }

        return $this->view_type;
    }

    /**
     * Set view data by key
     * 
     * @param key string
     * @param value mixed
     */
    public function setViewData( $key, $value ) {
        $this->view_data[$key] = $value;
    }

    /**
     * Get view data by key
     * 
     * @param key string
     * @return mixed
     */
    public function getViewData( $key ) {
        return $this->view_data[$key];
    }  

    /**
     * Gets or sets the view data
     * 
     * @param array $data An array containing all of the data needed to render the view
     * @return array
     */
    public function ViewData( array $data = null ) {
        if( NULL != $data ) {
            $this->view_data = $data;
        }

        return $this->view_data;
    }    

    /**
     * Gets or sets the layout that the View::renderWith() needs
     * 
     * @param string $layout overrides the default layout
     * @see $layout
     */
    public function Layout( $layout = null ) {
        if( null != $layout ) {
            $this->layout = $layout;
        }

        return $this->layout;
    }

    /**
     * The template parser used to render the view.
     * 
     * @param mixed $parser
     * @return mixed
     */
    public function TemplateParser( $parser = null ) {
        if( NULL != $parser ) {
            $this->template_parser = $parser;
        }

        return $this->template_parser;
    }

    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    /**
     * Instantiate a new View
     * 
     * @param Twig      $parser         Twig instance (dependency)
     * @param String    $template_key   The template key to use for content layouts. Found in: /templates/Layouts/<template_name>.html.twig
     * @param Array     $templates      A collection of content layouts
     * @param String    $layout         The containing layout Found in: /templates/<template_name>.html.twig
     * @return View Returns the current View instance to use for fluid method chaining.
     */
    public function __construct( $parser = null , $template_key = null, array $templates = null, $layout = 'Page.html.twig' ) {

        // The template parsing engine
        if( NULL != $parser ) {
            $this->template_parser = $parser;
        }

        // The template key to use for content layouts (/Layouts/Custom.html.twig)
        if( NULL != $template_key ) {
            $this->view_type = $template_key;
        }

        // The containing layout (Page.html.twig)
        if( NULL != $layout ) {
            $this->layout = $layout;
        }
        $this->initTemplates( (null == $templates) ? [] : $templates );

        return $this;
    }

    /**
     * Initialise the templates that this View will likely need.
     * 
     * @param array $templates Overrides the default templates.
     * @return View Returns the current view instance for fluid method chaining
     */
    protected function initTemplates(array $templates) {
        if( empty($templates)) {
            $this->template = $this->template_defaults['page'];
        }else{
            $new_templates = array_replace($this->templates,$templates);
            $this->templates = array_reverse($new_templates);

            $layout_key        = array_keys($this->templates)[0];
            $this->template = $this->templates[ $layout_key ];
        }

        return $this;

    }

    # +------------------------------------------------------------------------+
    # VIEW RENDERING
    # +------------------------------------------------------------------------+
    /**
     * Handles any business logic before rendering the final output.
     * 
     * @see View::renderWith()
     * @return void
     */
    public function render() {
        $data = [];
        $data = array_merge($data, ['view_name' => $this->view_name]);
        $data = array_merge($data, ['view_layout' => $this->template]);
        
        if( !empty($this->view_data) ) {
            
            $data = array_merge($data, $this->view_data);

            $title = WPAPP::Title();
            $data = array_merge($data, ['title' => $title]);
            
            $content = '';
            $data = array_merge($data, ['content' => $content]);
            
            $template_override = (func_num_args() > 0) ? func_get_arg(0) : null; 
            $template = $template_override ? $template_override: $this->template;
            
            if ( NULL == $template ) {
                $this->initTemplates($this->template_defaults);
            }
            $this->renderWith( $template, $data );
            
        } else {
            // @todo render nice ERROR page
            // $this->renderWith('Page.html.twig', $data);
        }
    }

    /**
     * Handles the final output/response to the browser.
     * 
     * @param template string The name of a Twig template
     * @param view_data array An associative array reprensenting view data, which also contains a reference to the content template to include in the layout.
     * @return void
     */
    public function renderWith($template, array $view_data = null) {
        
        if( NULL == $this->template_parser ) {
            $this->template_parser = WPAPP::Twig();
        }
        
        try {
            $layout = $this->template_parser->load($this->layout);
            $output = $layout->render($view_data);
            echo $output;
            // if( (WPAPP::environment() == WPAPP::WP_DEVELOPMENT) || defined('WP_DEBUG') && WP_DEBUG  ) {
            //     Log::log( get_class($this) . " ({$this->view_type}) rendered with ".  $this->template . " using " . get_num_queries().' queries in '.timer_stop(0).' seconds.' );
            // }
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR );
        }
    }

    # +------------------------------------------------------------------------+
    # ERROR HANDLING
    # +------------------------------------------------------------------------+
    public function Exception( \Exception $e ) {

    }
}