<?php

namespace WPX\View;

use \Exception;

interface IView {
    
    public function render();
    
    public function renderWith($template, array $view_data = null);

    public function Exception( \Exception $e );
    
}