<?php

namespace WPX;

use AUX\Cache\VarCache;
use AUX\HTTP\HttpStatus;
use \Exception;
use AUX\Registry;
use AUX\Utils\Debug;
use AUX\Utils\Log;
use AUX\Utils\Text;
use Spot\Locator;
use Spot\Config;
use WPX\Entity\WPEntity;
use WPX\Entity\Term;
use BlueM\Tree;
use ForceUTF8\Encoding;
use GK\JavascriptPacker;
use Rct567\DomQuery\DomQuery;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Sabre\Event\EventEmitter;
use SSD\SmartQuotes\Utf8CharacterSet;
use SSD\SmartQuotes\Factory as SmartQuotesFactory;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\Cache\ItemInterface;
use WPX\Controls\WPNavMenu;
use WPX\Entity\Post;
use WPX\Enums\EnumStatus;

/**
 * Intended as a thin API to bypass some of WordPress' heavier API, this class provides access to the
 * core database content, and other essential features necessary to render a view.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 */
final class WPAPI {

    const ENUM_MEDIA_CREATE = 'create';
    const ENUM_MEDIA_UPDATE = 'update';

    private $properties;
    private $config;
    private $locator;
    private $entity_types = [];
    private $wp_query;
    private $wpx_query;
    private $allowed_extensions = '/\.(jpe?g|png|gif|webp)$/g';
    private $site_tree = [];
    private $trailing_slash = true;

    private $emitter;
    // private static $nav_currentdepth = 0;

    public function WPXQuery($global_wp_query = null) {
        
        if( null != $global_wp_query ) {
            $wpx_query          = new WPQuery( $global_wp_query );
            $this->wpx_query    = $wpx_query;
        }
        
        return $this->wpx_query;
    }

    public function Locator() {
        return $this->locator;
    }

    public function Mapper($entity_name) {
        return $this->locator->mapper($entity_name);
    }

    public function Type() {
        return $this->wpx_query->Type();
    }

    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    public function __construct() {
        // Log::log(__CLASS__ . " is ALIVE!");
        $this->properties = new Registry();
        $this->init();
    }

    private function init() {
        $cfg = new Config();
        $this->config = $cfg;
        if( class_exists('APP\\App') ) {
            $this->config->addConnection('mysql', \APP\App::db()->asConnectorObject());
        }else{
            $this->config->addConnection('mysql', WPAPP::db()->asConnectorObject());
        }

        $locator = new Locator( $this->config );
        $this->locator = $locator;

        // Content Types
        $this->entity_types[WPEntity::ENUM_TYPE_PAGE]           = 'WPX\Entity\Page';
        $this->entity_types[WPEntity::ENUM_TYPE_POST]           = 'WPX\Entity\Post';
        $this->entity_types[WPEntity::ENUM_TYPE_OPTION]         = 'WPX\Entity\Option';
        $this->entity_types[WPEntity::ENUM_TYPE_META]           = 'WPX\Entity\PostMeta';
        $this->entity_types[WPEntity::ENUM_TYPE_MEDIA]          = 'WPX\Entity\Media';
        $this->entity_types[WPEntity::ENUM_TYPE_TERM]           = 'WPX\Entity\Term';
        $this->entity_types[WPEntity::ENUM_TYPE_TERMRELATION]   = 'WPX\Entity\TermRelation';

        // Event emitter
        $this->emitter = new EventEmitter();

        return $this;
    }


    # +------------------------------------------------------------------------+
    # ESSENTIALS: POSTS, COMMENTS, CATEGORIES, TAGS
    # +------------------------------------------------------------------------+
    /**
     * Get a collection of Entities based on type, e.g. `post`, `page`, etc.
     * 
     * @param string $type Indicates the Entity type
     * @return Spot\Mapper
     */
    public function content($type) {
        if( !isset($this->locator) ) {
            throw new Exception("Datamapper unavailable.",1);
        }

        if( !array_key_exists($type, $this->entity_types) ) {
            throw new Exception("Entity type: '$type' does not exist");
        }

        try {
            $mapper = $this->getEntityByType( $type );
        }catch( \Exception $e ) {
            echo $e->getMessage();
        }

        return $mapper;
    }

    /**
     * Get the terms (tags or categories) associated with a given post.
     * 
     * @param   int     $cache_id     The ID of the post or page
     * @param   string  $type   The type of term to query. Defaults to null, accepts Term::ENUM_TERM_TAXONOMY_TAG or Term::ENUM_TERM_TAXONOMY_CATEGORY
     * @param   array   $fields An additional list of fields to include in the Terms query.
     * @return  array   Returns an array of results grouped by type
     */
    public function Terms(int $cache_id, string $type = null, array $fields = null) {
        $mapper     = $this->getEntityByType( 'term' );
        $fields     = (!empty($fields) && null != $fields) ? $fields : ["t.term_taxonomy_id", " t.taxonomy", "t.description", "t.parent", "t.count"];
        $fields     = array_merge(["wt.*"], $fields);

        // Table Prefix
        $prefix                     = rtrim(WPAPP::prefix(),'_');
        $field_terms_relationships  = sprintf("%s_term_relationships", $prefix);
        $field_term_taxonomy        = sprintf("%s_term_taxonomy", $prefix);
        $field_terms                = sprintf("%s_terms", $prefix);

        $sql    = [];
        $sql[]  = "SELECT " . implode(", ", $fields) . " FROM wp_posts p";
        $sql[]  = sprintf("INNER JOIN %s r ON r.object_id=p.ID", $field_terms_relationships);
        $sql[]  = sprintf("INNER JOIN %s t ON t.term_taxonomy_id = r.term_taxonomy_id", $field_term_taxonomy);
        $sql[]  = sprintf("INNER JOIN %s wt ON wt.term_id = t.term_id", $field_terms);

        if( $type == Term::ENUM_TERM_TAXONOMY_CATEGORY || $type == Term::ENUM_TERM_TAXONOMY_TAG ) {
            $sql[]  = "WHERE p.ID={$cache_id} AND t.taxonomy='{$type}'";
        }else{
            $sql[]  = "WHERE p.ID={$cache_id}";
            $sql[]  = sprintf("AND t.taxonomy LIKE '%s%s%s'", Term::ENUM_TERM_TAXONOMY_CATEGORY,  "%",  "%");
            $sql[]  = sprintf("OR t.taxonomy LIKE '%s%s%s'", Term::ENUM_TERM_TAXONOMY_TAG, "%",  "%");
        }

        $sql[] =  "ORDER BY t.taxonomy ASC";
        $sql[] =  ", wt.name ASC";

        try {

            $resultset = $mapper->query(implode(" ",$sql));  

            if( $resultset !== false ) {
                $results = [];
                foreach( $resultset as $result ) {
                    if( null != $result->taxonomy ) {
                        $term = $result->data();
                        unset($term['taxonomy']);
                        $results[$result->taxonomy][] = $term;
                    }
                }
            }

        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }

            Log::log( $e->getMessage(), Log::LOG_TYPE_ERROR);
        }

        return !empty($results) ? $results : $resultset->toArray();
    }

    # +------------------------------------------------------------------------+
    # LINKS
    # +------------------------------------------------------------------------+
    public function getPermalink( $id ) {
        return function_exists('get_the_permalink') ? get_the_permalink($id) : null;
    }

    # +------------------------------------------------------------------------+
    # ESSENTIALS: HELPERS (PUBLIC)
    # +------------------------------------------------------------------------+
    /**
     * Register a new content and Entity the list of valid types.
     * 
     * @param $type String Accepts any of the WPEntity::ENUM_TYPE_* constants
     * @param $entity String
     * @todo Check if class exists before adding the type.
     */
    public function addType( $type, $entity ) {
        if( (isset($type) && isset($entity)) && !array_key_exists($type, $this->entity_types) ) {
            $this->entity_types[ $type ] = $entity;
        }

        return $this;
    }

    /**
     * Extracts the Thumbnail Image metadata from a collction.
     * 
     * @return array|null Metadata entry if it exists, null if otherwise
     */
    public function getThumbnailMeta( array $metadata ) {
        if( empty($metadata) ) {
            return null;
        }

        foreach( $metadata as $meta ) {
            if( array_key_exists('meta_key', $meta) && $meta['meta_key'] == '_thumbnail_id' ) {
                return $meta;
            }
        }

        return null;
    }

    public function getPostPageTitle($id) {
        try {
            if( function_exists('get_the_title') ) {
                return get_the_title($id);
            }
        } catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return null;     
    }

    public function getPostPageThumbnail( $id, $as_image = false) {
        try {
            if( function_exists('get_the_post_thumbnail') ) {
                return $as_image ? get_the_post_thumbnail( $id, 'full' ) : get_the_post_thumbnail_url( $id, 'full' );
            }
        } catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return null;
    }

    /**
     * Remove a WPX\Entity\Media entry by ID, postmeta `meta_id` or both.
     * 
     * @param   int     $cache_id             The ID of the post.
     * @param   int     $wpmeta_post_id (Optional) The `meta_id` of the original wp_postmeta record.
     * @return  bool    Returns true if the operation succeeded, false if otherwise.
     */
    public function RemoveMediaAttachments( $cache_id, $wpmeta_post_id = null ) {
        $mapper = $this->getEntityByType( WPEntity::ENUM_TYPE_MEDIA );
        try {
            $data = [
                'OwnerID'       => $cache_id,
                'post_id'       => $wpmeta_post_id,        
            ];
            $result = $mapper->delete($data);
        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }
        return $result;        
    }

    /**
     * Adds a new WPX\Entity\Media attachment for use outside of the normal WP core features. 
     * Media references are synched by hooking WP events:
     *  
     * - `added_post_meta`
     * - `updated_post_meta`
     * - `deleted_post_meta`
     * 
     * @param   int     $cache_id                 The post ID to associate the media entry to
     * @param   int     $wpmeta_post_id     The postmeta `meta_id` to reference
     * @param   array   $wp_attachment_meta A datamap of the original post meta
     * @param   string  $type               The type of entry, WPEntity::ENUM_MEDIA_IMAGE or WPEntity::ENUM_MEDIA_THUMBNAIL
     * @param   string  $action             The action to perform, e.g. self::ENUM_MEDIA_CREATE or self::ENUM_MEDIA_UPDATE
     * @return  bool                        Returns true if the update/insert was successful, false if otherwise
     */
    public function MediaAttachment( $cache_id, $wpmeta_post_id, array $wp_attachment_meta, $type = WPEntity::ENUM_MEDIA_IMAGE, $action = self::ENUM_MEDIA_CREATE ) {
        if( !is_array($wp_attachment_meta) || empty( $wp_attachment_meta ) ) {
            return false;
        }

        $mapper = $this->getEntityByType( WPEntity::ENUM_TYPE_MEDIA );
        $result = false;
        
        try {

            if( array_key_exists('width', $wp_attachment_meta) && array_key_exists('height', $wp_attachment_meta) ) {
                $metadata = [
                    "width" => $wp_attachment_meta['width'],
                    "height" => $wp_attachment_meta['height']
                ];
            }

            // $mapper->migrate();

            $data = [ 
                'type'          => $type,
                'asset'         => array_key_exists('file', $wp_attachment_meta) ? $wp_attachment_meta['file'] : null,
                'metadata'      => !empty($metadata) ? serialize($metadata) : null,
                'othermetadata' => array_key_exists('image_meta', $wp_attachment_meta) ? serialize($wp_attachment_meta['image_meta']) : null
            ];

            switch( $action ) {
                case self::ENUM_MEDIA_UPDATE:
                    $data = array_merge(array_filter($data), [
                        'modified'      => new \DateTime()
                    ]);

                    // Reset all media references for this post
                    $mapper->exec(sprintf("UPDATE `%s` SET type = '%s' WHERE `%s` = %s", $mapper->table(), WPEntity::ENUM_MEDIA_IMAGE, 'OwnerID', $cache_id ));
                    
                    // Update the newly selected thumbnail
                    $entity = $mapper->first(['OwnerID' => $cache_id, 'post_id' => $wpmeta_post_id]);

                    if( $entity ) {
                        $entity->data($data, true);
                        $result = $mapper->update($entity);
                    }else{
                        $entity = $mapper->first(['post_id' => $wpmeta_post_id]);
                        if( $entity ) {
                            $data = $entity->data();
                            $data = array_merge($data, [
                                'ID'         => 0,
                                'OwnerID'    => $cache_id,        
                                'type'       => $type,
                            ]);
                            $result = $mapper->create($data);
                        }
                    }

                    break;
                case self::ENUM_MEDIA_CREATE:
                default :
                    if( !array_key_exists('file', $wp_attachment_meta) ) break;
                    
                    $asset      = WPAPP::DS(WP_CONTENT_DIR . '/uploads/' . $wp_attachment_meta['file']);
                    $fileinfo   = pathinfo( $asset );

                    $data = array_merge(array_filter($data), [
                        'ID'            => 0,
                        'OwnerID'       => $cache_id,
                        'post_id'       => $wpmeta_post_id,
                        'name'          => $fileinfo['filename'],
                        'title'         => $fileinfo['filename'],  
                        'hash'          => md5($wp_attachment_meta['file']),      
                    ]);

                    $result = $mapper->create($data);
                    break;
            }

            return $result;

        } catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(),Log::LOG_TYPE_ERROR);
        }
        
    }

    /**
     * Get the post thumbnail entry from a collection of `Media` entities.
     * 
     * @param array $attachments    A collection of `Media` entries.
     * @return  array|null  Returns an array containing details of the Thumbnail asset, null if no asset was found.
     */
    public function MediaThumbnail( array $attachments ) {

        $media_asset = [
            'filename'  => null,
            'uri'       => null,
            'size'      => null,
            'caption'   => null
        ];

        try {

            foreach($attachments as $attachment) {
                if( array_key_exists('type', $attachment) ) {
                    if( (isset($attachment['type']) && ($attachment['type'] == WPEntity::ENUM_MEDIA_THUMBNAIL ) && isset($attachment['asset']) ) ) {
                        $media_asset = [
                            'filename'  => WPAPP::DS(sprintf("%s/%s", WP_UPLOADS_DIR, $attachment['asset'])),
                            'uri'       => sprintf("%s/%s", WP_UPLOADS_URL_RELATIVE, $attachment['asset']),
                            'size'      => $attachment['metadata'],
                            'caption'   => $this->getMediaCaptionByAttachmentID( $attachment['post_id'] )
                        ];
                        break;
                    }
                }
            }

            if( !file_exists($media_asset['filename']) ) {
                $media_asset = null;
            }

        } catch( \Exception $e ) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return $media_asset;

    }

    public function getMediaCaption( $post_id ) {
        try {
            $mapper         = $this->Mapper(Post::class);
            $attachment     = $mapper->all()->where(['post_parent' => $post_id, 'post_type' => Post::ENUM_MEDIA_ATTACHMENT, 'post_excerpt !=' => ''])->first();
            $attachment     = $attachment ? $attachment->data() : null;
            $caption        = $attachment && is_array($attachment) ? $attachment['post_excerpt'] ?? null : null;
            return $caption;
        }catch( \Exception $e ) {
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return null;
    }

    public function getMediaCaptionByAttachmentID( $id ) {
        try {
            $mapper         = $this->Mapper(Post::class);
            $attachment     = $mapper->all()->where(['ID' => $id, 'post_type' => Post::ENUM_MEDIA_ATTACHMENT, 'post_excerpt !=' => ''])->first();
            $attachment     = $attachment ? $attachment->data() : null;
            $caption        = $attachment && is_array($attachment) ? $attachment['post_excerpt'] ?? null : null;
            return $caption;
        }catch( \Exception $e ) {
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return null;
    }

    /**
     * Extract the thumbnail id from the post meta to get the media's caption.
     */
    public function getMediaCaptionByThumbnailID( array $metadata ) {
        try {
            if( empty( $metadata ) ) return null;

            $id = null;
            $caption = null;

            foreach( $metadata as $meta ) {
                if( $meta['meta_key'] == '_thumbnail_id' ) {
                    $id = $meta['meta_value'];
                    break;
                }
            }

            if( !!$id ) {
                $caption = $this->getMediaCaptionByAttachmentID( $id );
            }
            return $caption;
        }catch( \Exception $e ) {
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return null;
    }

    public function NavigationTree( $id = 0 ) {
        $pages              = $this->getAllWPPages();
        $current_page_id    = $id;
        $max_depth          = 0;
        
        if( null == $pages ) return null;
        $tree   = new Tree($pages,['id' => 'ID', 'parent' => 'post_parent']);       
        $node   = $tree->getNodeById($id);
        if( $node->hasChildren() ) {
            $nodes  = $node->getChildren();
            $nodes  = array_merge( is_array($node) ? $node : [$node], $nodes ); 
        }else{
            $parent = $node->getParent();
            $nodes  = $parent->getChildren();
            $nodes  = array_merge( is_array($parent) ? $parent : [$parent], $nodes ); 
        }

        $slug   = $this->getPermalink($id);//sprintf("/%s", $this->buildURLFromNodes($node));
        $wp_nav = new WPNavMenu($nodes, false, $max_depth, $slug, $current_page_id, $this->trailing_slash);

        return $wp_nav->render();
    }
    
    /**
     * Generate a multidimensional array of Page entities, representing WP's page heirarchy, complete with all fields.
     * 
     * @param Bool  $root_only  Restrict the heirarch to only the root pages. Defaults to false
     * @param Bool  $refresh    Refreshed the cached results of the query used to build the heirarchy
     * @param Int   $cache_id   A unique ID to use for the cache.
     */
    public function SiteTree( bool $root_only = false, bool $refresh = false, int $cache_id = null) {

        $cache_key = $cache_id ? "tree-{$cache_id}" : "tree-" . __METHOD__ ;

        if( !$refresh ) {
            if( array_key_exists($cache_key, $this->site_tree) ) {
                return $this->site_tree[ $cache_key ];
            }
        }
        
        try {
    
            $mapper             = $this->content(WPEntity::ENUM_TYPE_PAGE);
            $pages              = $mapper->all()->where(['post_status' => WPEntity::ENUM_STATUS_PUBLISH, 'post_type' => WPEntity::ENUM_TYPE_PAGE])->with(['meta'])->order(['post_parent' => 'ASC','menu_order' => 'ASC']);
            $pages_collection   = $pages ? $pages->toArray() : [];

            // Allow pre-processing of resulting page collection
            if (class_exists('APP\\App')) {
                $emitter = App::inst()->emitter();
            }else{
                $emitter = WPAPP::inst()->Emitter();
            }
            $emitter->emit('BeforeSiteTreeRender', [ &$pages_collection ]);

            $tree   = new Tree($pages_collection,['rootId' => 0, 'id' => 'ID', 'parent' => 'post_parent']);
            $nodes = $tree->getRootNodes();            

            if( !empty($nodes) && null != $nodes) {
                $html = new WPNavMenu($nodes, $root_only, -1, null, null, true);
                $html = $html->render();

                $this->emitter->emit('AfterSiteTreeRender', [$html]);
            }
            
        } catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(),Log::LOG_TYPE_ERROR);
        }

        // Cache the results
        $this->site_tree[$cache_key] = $html;

        return $html;
        
    }

    /**
     * Generate a multidimensional array representing WP's page heirarchy.
     * 
     * @param Int   $max_depth  Restrict the depth of the heirarchy. Defaults to the root pages (post_parent = 0).
     * @param Bool  $refresh    Refreshed the cached results of the query used to build the heirarchy
     * @param Int   $cache_id   A unique ID to use for the cache.
     */
    public function Nav( $max_depth = 1, bool $refresh = false, int $cache_id = null) {

        $cache_key = $cache_id ? "tree-{$cache_id}" : "tree-" . __METHOD__ ;

        if( !$refresh ) {
            if( array_key_exists($cache_key, $this->site_tree) ) {
                return $this->site_tree[ $cache_key ];
            }
        }
        
        $pages  = $this->getAllWPPages();
        $tree   = new Tree($pages,['rootId' => 0, 'id' => 'ID', 'parent' => 'post_parent']);
   
        $nodes = $tree->getRootNodes();
        $pages = $this->buildNavTree($nodes, $max_depth);

        // Cache the results
        $this->site_tree[$cache_key] = $pages;

        return $pages;
    }   

    /**
     * Generate a multidimensional array representing a single WP page heirarchy.
     * 
     * @param Int   $max_depth  Restrict the depth of the heirarchy. Defaults to the root pages (post_parent = 0).
     * @param Bool  $refresh    Refreshed the cached results of the query used to build the heirarchy
     * @param Int   $cache_id   A unique ID to use for the cache.
     */
    public function SubNav( $parent_id = 0, $max_depth = 1, bool $as_html = true, int $current_page_id = null) {

        try {
            $pages  = $this->getAllWPPages();
            if( null == $pages ) return null;
    
            $tree       = new Tree($pages,['id' => 'ID', 'parent' => 'post_parent']);
       
            $node   = $tree->getNodeById($parent_id);
            $nodes  = $node->getChildren();
            $slug   = null;

            if( $parent_id != 0 && $as_html ) {
                $slug = sprintf("/%s", $this->buildURLFromNodes($node));
            }

            if( $as_html ) {
                $wp_nav = new WPNavMenu($nodes, false, $max_depth, $slug, $current_page_id, $this->trailing_slash);
            }

            return $as_html ? $wp_nav->render() : $this->buildSubNavTree($nodes, $max_depth);

        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        
        return null;
    }

    private function buildURLFromNodes( $node ) {
        try {
            $parent_nodes = $node->getAncestorsAndSelf();
            $url = [];
    
            if(  $parent_nodes == null ) return null;
    
            array_map(function($parent) use(&$url){
                array_unshift($url, $parent->post_name);
            },$parent_nodes);

        }catch( \Exception $e ){
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return !empty($url) ? implode('/', $url) : null;

    }

    /**
     * Retrieve all published pages/posts
     */
    private function getAllWPPages( $type = WPEntity::ENUM_TYPE_PAGE, $status = WPEntity::ENUM_STATUS_PUBLISH ):array {
        try {

            $mapper     = $this->content($type);
            $entities   = $mapper->all()->where(['post_status' => $status, 'post_type' => $type])->with(['meta'])->order(['menu_order' => 'ASC', 'post_parent' => 'ASC']);
            $collection = $entities->toArray();

        }catch( \Exception $e ){
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
        return $collection ?? null;
    }
    
    /**
     * Builds a multidimensional array of all pages
     */    
    private function buildNavTree( array $nodes, &$max_depth = 1, $slug = null, $children_show_in_menus = false) {
        $tree_nodes = [];

        try {
            foreach ($nodes as $node) {
                
                $show_in_menu   = $this->showInPrimaryNav($node->get('meta'));
                $hide_from_menu = $this->hideFromNavigation($node->get('meta'));
                if( !$hide_from_menu ) {
                    if( !!$show_in_menu || $children_show_in_menus ) {
                        $has_children   = ( $node->countChildren() > 0 );
                        $link           = (null != $slug) ? sprintf("%s/%s", $slug, $node->post_name) : sprintf("/%s", $node->post_name);
                        
                        $pages = [
                            'ID'        => $node->ID,
                            'title'     => $node->post_title,
                            'link'      => ( $this->trailing_slash ) ? sprintf("%s/", $link) : $link,
                        ];
        
                        if( $has_children && $max_depth > 0 ) {
                            $depth = $max_depth;
                            while( $depth > 0 ) {
                                --$depth;
                                $pages['children'] = $has_children ? $this->buildNavTree($node->getChildren(), $depth, $link, true) : null;
                            }
                        }
                        array_push($tree_nodes,$pages);                        
                    }
                }
            }        
        }catch(\Exception $e) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return $tree_nodes;
    }

    /**
     * Builds a multidimensional array of sub pages
     */    
    private function buildSubNavTree( array $nodes, $max_depth = 1, $slug = null) {
        $tree_nodes = [];

        try {
            foreach ($nodes as $node) {                
                $has_children   = ( $node->countChildren() > 0 );
                $link           = (null != $slug) ? $slug . '/' . $node->post_name : $node->post_name;
                $pages = [
                    'ID'        => $node->ID,
                    'title'     => $node->post_title,
                    'link'      => $link,
                ];

                if( !!$max_depth && $has_children ) {
                    $depth = $max_depth;
                    $pages['children'] = $has_children ? $this->buildSubNavTree($node->getChildren(), --$depth, $link, true) : null;
                }

                array_push($tree_nodes,$pages);
            }        
        }catch(\Exception $e) {
            if( defined('WP_DEBUG') && WP_DEBUG ) {
                echo $e->getMessage();
            }
            Log::log($e->getMessage(),Log::LOG_TYPE_ERROR);
        }

        return $tree_nodes;
    }

    # +------------------------------------------------------------------------+
    # WP FILTERS
    # +------------------------------------------------------------------------+
    /**
     * Filter internal content URLS to include trailing slash
     * 
     * @param String $content The content after wptexturize has run
     * @param Boolean $trailing_slash Include a trailing slash, defaults to false
     * @return String The content after the post-processing is complete.
     */
    public function filterWPContentLinks($content, $trailing_slash = false) {
        if( !$trailing_slash ) return $content;

        $url_base = parse_url( filter_var( strip_tags(WP_HOME), FILTER_SANITIZE_URL), PHP_URL_HOST);

        $dom = new DomQuery('<div class="fragment" />');
        $dom->append($content);
        $nodes = $dom->find('a');

        if( $nodes && $nodes->length ) {     
            foreach( $nodes as $node )   {
                $a = $node;

                if( $a && $a->length ) {
                    $href = $a->attr('href');

                    if( $href && strpos($href, $url_base) ) {

                        if( strpos($href, 'javascript:void(0)') ) break;
                        if( !!parse_url($href,PHP_URL_SCHEME) ) break;
                        if( !!pathinfo($href, PATHINFO_EXTENSION) ) break;
            
                        $normalize_href = $href;
                        $normalize_href = parse_url($normalize_href, PHP_URL_PATH);                        
                        $normalize_href = ltrim($normalize_href,"/");
                        $normalize_href = rtrim($normalize_href,"/");
                        $normalize_href = sprintf("/%s/", $normalize_href);
                        $a->attr('href',$normalize_href);
                        
                    }

                    // Replace https://www.landwithoutlimits.com/
                    if( $href ) {
                        $href = str_replace(sprintf("%s/",getenv('WP_HOME')), '/',$href);

                        if( !parse_url($href,PHP_URL_SCHEME) && !pathinfo($href, PATHINFO_EXTENSION) ) {
                            $href = ltrim($href,"/");
                            $href = rtrim($href,"/");
                            $href = sprintf("/%s/", $href);
                        }

                        $a->attr('href',$href);
                    } 

                    // Replace wp-content
                    if( $href && strpos($href, 'wp-content') ) {
                        $href = str_replace('wp-content', getenv('WP_RESOURCES') ?: 'resources',$href);
                        $a->attr('href',$href);
                    }                    
                }
            }
        }

        return (string) $dom->html() ;
    }

    /**
     * Filters WP HTML content after wptexturize has run, fixing `<p><iframe>...</iframe></p>` cases
     * 
     * @param string $content The content after wptexturize has run
     * @return string The content after the post-processing is complete.
     */
    public function filterWPContentIframes($content) {        
        $dom = new DomQuery('<div class="fragment" />');
        $dom->append($content);
        $nodes = $dom->find('p');
        
        if( $nodes && $nodes->length ) {     
            foreach( $nodes as $node )   {
                $p = $node;
                $i = $node->find('iframe');
                $i = $i->wrap('<div class="component wp-media" />')->parent('div.wp-media');

                if( $i && $i->length ) {
                    $p->addClass('ptarget');
                    $dom->find('p.ptarget')->replaceWith((string)$i);
                }
            }
        }

        return (string) $dom->html() ;
    }

    /**
     * Convert standard images into deferred images
     */
    public function filterWPContentImageDefer($content, $remove_srcset = false, $disable_defer = false) {        
        $dom = new DomQuery('<div class="fragment" />');
        $dom->append($content);
        $nodes  = $dom->find('img');

        if(!$disable_defer) {
            if( $nodes && $nodes->length ) {     
                foreach( $nodes as $node )   {
                    if( null == $node->attr('data-defer-src') ) {
                        $node->attr('data-defer-src', $node->attr('src') );
                        if( $remove_srcset ) {
                            $node->removeAttr('srcset');
                        }
                        $node->attr('src','resources/themes/default/assets/images/common/fpo_16_9.svg');
                    }
                }
            }
        }

        return $disable_defer ? $content : (string) $dom->html() ;
    }

    /**
     * Compile a comprehensive list of image assets used in this page.
     */
    public function filterGatherWPImages($content) {        
        $dom        = new DomQuery($content);
        $nodes      = $dom->find('img');
        $image_src  = [];

        if( $nodes && $nodes->length ) {     
            foreach( $nodes as $node )   {
                $src = $node->attr('data-defer-src') ?: $node->attr('src');
                if( (null != $src) && !ctype_space($src) ) {
                    array_push( $image_src, $src );
                }
            }
        }
        return $image_src;
    }

    /**
     * Filter the WP head to reformat scripts and remove cruft
     */
    public function filterWPHead() {
        $output = null;
        if( function_exists('do_action') ) {
            ob_start();
            if( function_exists('wpseo_head') ) {
                do_action('wpseo_head');
            }else{
                do_action('wp_head');
            }
            $output = ob_get_clean();
            $output = preg_replace('/<!--(.|\s)*?-->/', '', $output);            
        }
        return $output;
    }
    
    public function filterWPFooter() {
        $output = null;
        if( function_exists('do_action') ) {
            ob_start();
            do_action('wp_footer');
            $output = ob_get_clean();
            $output = preg_replace('/<!--(.|\s)*?-->/', '', $output);
        }
        return $output;
    }

    public function doShortCode( $shortcode ) {
        if( function_exists('do_shortcode') && $shortcode ) {
            $output = do_shortcode( $shortcode, true );
        }
        return $output;
    }
    
    /**
     * Allow WordPress to apply additional content filters and shortcodes;
     * additionally convert Latin-1 characters to utf8.
     * 
     * @param String $content   The content to be filtered
     * @return String           The filtered content
     */
    public function applyWPContentFilters( $content, $basic_filtering = false ) {
        $wp_content = $content;
        $wp_content = Encoding::toUTF8($wp_content);

        if( $basic_filtering ) {
            if( class_exists('SmartQuotesFactory') ) {
                $wp_content = SmartQuotesFactory::filter(new Utf8CharacterSet, $wp_content);
            }
            if( function_exists('do_shortcode') ) {
                $wp_content = do_shortcode( $wp_content );
            }       

        }else{
            if( function_exists('apply_filters') ) {
                $wp_content = apply_filters( 'the_content', $wp_content );
            }
    
            if( function_exists('do_shortcode') ) {
                $wp_content = do_shortcode( $wp_content );
            }       
    
            if( class_exists('SmartQuotesFactory') ) {
                $wp_content = SmartQuotesFactory::filter(new Utf8CharacterSet, $wp_content);
            }
        }
         
        return $wp_content;
    }

    /**
     * Replace special characters, e.g. curly quotes
     * @link https://plugins.svn.wordpress.org/smart-quote-fixer/trunk/smart-quote-fixer.php
     * 
     * @param String $content The content to filter
     * @return String The filtered content
     */
    public function filterCurlyQuotes( $content ) {
        // Replace the smart quotes that cause question marks to appear
        $str = str_replace(
            array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
            array("'", "'", '"', '"', '-', '--', '...'), $content);

        // Replace the smart quotes that cause question marks to appear
        $str = str_replace(
        array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
        array("'", "'", '"', '"', '-', '--', '...'), $str);

        // Replace special chars (tm) (c) (r)
        $str = str_replace(
        array('â„¢', 'Â©', 'Â®'),
        array('&trade;', '&copy;', '&reg;'), $str);

        // Return the fixed string
        return $str;
    }

    # +------------------------------------------------------------------------+
    # ESSENTIALS: HELPERS (PRIVATE)
    # +------------------------------------------------------------------------+
    /**
     * Get valid entity by Entity type
     */
    private function getEntityByType($type) {
        if( !array_key_exists($type, $this->entity_types) ) {
            throw new Exception("Requested type is not a valid Entity Type", 2);
        }

        $entity = $this->entity_types[$type];
        return $this->locator->mapper($entity);
    }

    /**
     * Is the current page a descendent of page identified by the path (not slug).
     * A page is not a descendent of itself!
     * @link http://wordpress.stackexchange.com/questions/101213/does-is-child-exist-in-wp-3-5-1
     * @link https://wordpress.stackexchange.com/a/101218
     * @link https://gist.github.com/stephenharris/5674643
     * 
     * @param string $page_path The path of the page, e.g. mypage/mysubpage 
     * @param WP_Post $wp_post An instance of the current post
     * @return int|boolean Returns the Root ID of the page ancestor, or false if current page is not a descednent of specified $page_path. False otherwise.
     */
    public function isDescendentOf( $page_path, array $wp_post = null ){
        if( !is_page() ) return false;
        
        $ancestors  = get_post_ancestors( get_queried_object_id() );
        $page       = null != $wp_post ? $wp_post : get_page_by_path( $page_path );

        if( $page ) return in_array( $page['post_parent'], $ancestors ) ? array_pop($ancestors) : false;
        
        return false;   
    }

    /**
     * Checks a given post or page's metadata to determine if it should be shown in menus.
     * @param Array The metadata containing the value for `show_in_primary_menu`
     * @return Boolean Returns true (default) if the page should be shown in a menu, false if otherwise.
     */
    public function showInPrimaryNav( $metadata ) {
        if( $show_in_menu = $this->getMetadataByKey($metadata, 'show_in_primary_menu') ) {
            return $show_in_menu['meta_value'] ?? false;
        }
        return false;
    }

    /**
     * Checks a given post or page's metadata to determine if it should be hidden—but still published—from menus.
     * @param Array The metadata containing the value for `hide_from_nav`
     * @return Boolean Returns true (default) if the page should be hidden from menus, false if otherwise.
     */
    public function hideFromNavigation( $metadata ) {
        if( $show_in_menu = $this->getMetadataByKey($metadata, 'hide_from_nav') ) {
            return $show_in_menu['meta_value'] ?? false;
        }
        return false;
    }

    /**
     * Checks a given post or page's metadata to determine if it should be shown in menus.
     * @param Array The metadata containing the value for `alternate_byline`
     * @return Boolean Returns true (default) if the page should be shown in a menu, false if otherwise.
     */
    public function getAlternateAuthor( $meta ) {
        $meta_result = $this->getMetadataByKey($meta, 'alternate_byline') ?: null;
        return $meta_result ? $meta_result['meta_value'] : null;
    }

    public function search() {
        
    }

    // ---------------------------------------------------------- //
    // METADATA
    // ---------------------------------------------------------- //
    /**
     * Get a collection of CTA (ACF) blocks for a given page.
     *
     * @param boolean $use_page_post_thumbnail
     * @return array
     */
    public function getCTABlocks( $use_page_post_thumbnail = false ) {
        try {
            $blocks_field = null;
            if( function_exists('get_field') ) {
                $blocks_field = get_field('calls_to_action');

                if( !!$use_page_post_thumbnail ) {
                    $blocks_field = $this->getCTABlockMedia($blocks_field ?: []);
                }
            }
            return $blocks_field;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
        }
    } 

    /**
     * Override the media selection in a collection of CTA blocks with the Page/Post thumbnail.
     *
     * @param array $collection
     * @return array
     */
    public function getCTABlockMedia( array $collection ) {
        $post_mapper = $this->Mapper(Post::class);

        $blocks = array_map(function($b) use($post_mapper){
            $block  = $b;
            $link   = !!array_key_exists('url',$block['link'] ?: []) ? $block['link']['url'] : null;
            if( null != $link ) {
                
                // Find a page/post by it's slug
                $link       = str_replace(WP_HOME, '', $link);
                $slug       = explode('/',trim($link,'/'));
                $slug       = null != $slug ? array_pop($slug) : null;
                $post       = $post_mapper->first(['post_type' => ['post','page'], 'post_status'=> EnumStatus::PUBLISH, 'post_name' => $slug ]);
                
                // Find it's featured image
                $thumbnail  = null;
                if( null != $post ) {
                    $thumbnail = $this->getPostPageThumbnail( $post->ID );
                }

                // Apply the featured image if found. Override the existing media selection.
                if( null != $thumbnail ) {
                    $current_media = $block['image'];
                    $new_media = str_replace(WP_HOME, '', $thumbnail);
                    $block['image'] = $current_media != $new_media ? $new_media : $current_media;
                }
            }
            return $block;
        }, $collection);

        return $blocks;
    }
   
   
    /**
     * Query ACF repeater fields.
     */
    public function getRepeaterContent() {
        try {
            $blocks_field = null;
            if( function_exists('get_field') && function_exists('have_rows') ) {
                if( have_rows('cta_content_group') ) {
                    $blocks_field = get_field('cta_content_group');
                }
            }
            return $blocks_field;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
        }
    }    

    /**
     * Gets the Crowdriff ID to generate the script tag
     * 
     * @param array $meta The post/page metadata
     * @return array An array containing the gallery ID. Extend this array as needed.
     */
    public function getCrowdriffContent($meta) {
        try {
            $field = [
                'id'    => $this->getMetadataValueByKey($meta, 'wpx_crf_script_id') ?: null
            ];

            return $field['id'] ? $field : null;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
        }        
    }

    /**
     * Get Post/Page metadata by key
     * @param Array $meta An associative array of metadata belonging to a post/page
     * @param String $key The key to search for in the metadata
     * @return Array Returns an array, populated or not, associated the given key
     */
    public function getMetadataByKey( $meta, $key, $include_index = false ) {
        $metadata = array_filter($meta,function($v, $k) use ($key){
            return array_key_exists('meta_key', $v) && ( $v['meta_key'] == $key );
        }, ARRAY_FILTER_USE_BOTH);
        return  $include_index ? $metadata : current($metadata);
    }

    /**
     * Get Post/Page metadata value by key.
     * @param Array $meta An associative array of metadata belonging to a post/page
     * @param String $key The key to search for in the metadata
     * @return mixed Returns the value of the metadata entry
     */
    public function getMetadataValueByKey( $meta, $key ) {
        $meta_result = $this->getMetadataByKey($meta, $key) ?: null;
        $meta_result = $meta_result ? $meta_result['meta_value'] : null;
        $meta_result = Text::serialized($meta_result) ? unserialize($meta_result) : $meta_result;
        return $meta_result;
    }

    // ---------------------------------------------------------- //
    // THIRDPARTY SUPPORT
    // ---------------------------------------------------------- //
    /**
     * @link https://docs.gravityforms.com/adding-a-form-to-the-theme-file/
     */
    public function getGravityForm($id_or_title, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, $tabindex = 0, $echo = true) {
        if( function_exists('gravity_form') ) {
            return call_user_func_array('gravity_form', func_get_args());
        }
        return null;
    }

    # +------------------------------------------------------------------------+
    # PROXY
    # +------------------------------------------------------------------------+
    /**
     * Generate a well-formatted name for cached items. Some items are generated from
     * an API call, such as the Google Maps api. These will require a unique name and
     * an appropriate extension.
     * 
     * @param string $asset_tag Optional. A unique tag that doubles as a filename for resources without a basename
     * @param string $asset_ext Optional. A known extension to be appended to filenames for resources without a basename or extension
     * @param array Returns an array containing the local filename, path, url and original url of a remote resource
     */
    private function getCachedAssetFilename( $asset_url, $asset_tag = null, $asset_ext = null ) {
        
        try {
            $allowed_extensions = ['js','css'];
            $query_string       = parse_url($asset_url,PHP_URL_QUERY);
            $cached_fileinfo    = pathinfo(parse_url($asset_url,PHP_URL_PATH));

            if( !array_key_exists('extension', $cached_fileinfo) ) {
                $replacement_name   = ( !!$asset_tag ) ? $asset_tag : $cached_fileinfo['basename'] ?: md5($asset_url);
                $cached_filename    = sprintf("%s.%s", str_replace('-','_',$replacement_name), $cached_fileinfo['extension'] ?? $asset_ext);
            }else{
                $cached_filename    = ($cached_fileinfo['extension'] ?: null) ? $cached_fileinfo['basename'] : sprintf( "%s.%s", $cached_fileinfo['basename'], $cached_fileinfo['extension']);
            }

            if( !in_array(pathinfo($cached_filename,PATHINFO_EXTENSION), $allowed_extensions) ) return null;

            $asset_path         = sprintf("%s/cache/%s", WP_CONTENT_DIR, $cached_filename);
            $asset_uri          = str_replace(WP_CONTENT_DIR, WP_CONTENT_URL, $asset_path);
            $result = [
                'filename'      => $cached_filename,
                'path'          => $asset_path,
                'uri'           => $query_string ? "${asset_uri}?${query_string}" : $asset_uri,
                'original_url'  => $asset_url
            ];

            return $result;
        }catch( \Exception $e ){
            Log::log($e->getMessage() . " \n " . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }   


        return null;
    }

    /**
     * Consume an external resource and cache it locally.
     * 
     * @param string $asset_url The originating source url
     * @param string $asset_tag Optional. A unique tag that doubles as a filename for resources without a basename
     * @param string $asset_ext Optional. A known extension to be appended to filenames for resources without a basename or extension
     * @param string Returns a URI for the locally cached resource
     */
    public function getProxiedAsset( $asset_url, $asset_tag = null, $asset_ext = null, $pack = true ) {

        $cached_fileinfo    = call_user_func_array([$this,'getCachedAssetFilename'], func_get_args());
        
        if( $cached_fileinfo ) {
            $cache_lifetime     = WPAPP::cachelifetime();
            $cache_namespace    = str_replace('\\','_', WPAPP::class);
            $cache_index        = sprintf("%s.cache", md5($asset_url));
            $cache_fsa          = new FilesystemAdapter($cache_namespace,$cache_lifetime,rtrim(WPAPP::cachelocation(), DIRECTORY_SEPARATOR));
            $cache              = new TagAwareAdapter($cache_fsa);

            $cached_asset = $cache->get($cache_index, function(ItemInterface $item) use($cached_fileinfo, $pack) {
                try {
                    $client     = HttpClient::create();
                    $r          = $client->request('GET',$cached_fileinfo['original_url']);
                    $payload    = ( $r->getStatusCode() == HttpStatus::HTTP_OK ) ? $r->getContent() : null;
                    $asset_uri  = $cached_fileinfo['original_url'];
    
                    if( $cached_fileinfo['path'] && $payload ) {
                        if( $pack ) {
                            $js_packer      = new JavascriptPacker($payload);
                            $payload_min    = $js_packer->pack();
                        }else{
                            $payload_min    = $payload;
                        }
                        if( file_put_contents($cached_fileinfo['path'], $payload_min)) {
                            $asset_uri = $cached_fileinfo['uri'];
                        }
                    }
    
                    return $asset_uri;
    
                }catch( \Exception $e ){
                    Log::log($e->getMessage() . "\n" . $e->getTraceAsString()(), Log::LOG_TYPE_ERROR);
                }
    
                return $cached_fileinfo['original_url'];
            });

            return  $cached_asset;

        }

        return $asset_url;
        
    }
}