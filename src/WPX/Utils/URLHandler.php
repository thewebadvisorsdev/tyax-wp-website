<?php

namespace WPX\Utils;

use AUX\Utils\Log;

class URLHandler {
    protected $uri;
    protected $url_method;
    protected $url_action;
    protected $url_param_key;
    protected $url_param_value;
    protected $url_param_extra;

    public function method() {
        return $this->url_method;
    }

    public function path() {
        return $this->uri;
    }

    public function action() {
        return $this->url_action;
    }

    public function key() {
        return $this->url_param_key;
    }

    public function value() {
        return $this->url_param_value;
    }
    
    public function params() {
        return $this->url_param_extra;
    }

    public function __construct($uri) {
        $this->uri = filter_var( strip_tags($uri), FILTER_SANITIZE_URL);

        $this->parse();
    }

    protected function parse() {
        try {
            $parts = explode( "/", trim($this->uri,"/") );
            $this->url_method       = array_shift($parts) ?: 'index';
            $this->url_action       = array_shift($parts) ?: null;
            $this->url_param_key    = array_shift($parts) ?: null;
            $this->url_param_value  = array_shift($parts) ?: null;

            if( count($parts) > 0 ) {
                while( count( $parts ) > 0 ) {
                    $key = array_shift($parts);
                    $value = array_shift($parts);
                    $this->url_param_extra[ $key ] = $value;
                }
            }
        }catch( \Exception $e ) {
            Log::log($e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
    }
}