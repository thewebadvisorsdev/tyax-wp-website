<?php

namespace WPX\Utils;

class ENUMUtils {

    public static function CanadianProvinces() {
        $enums = [
            ['value' => "BC", 'label' => "British Columbia"],
            ['value' => "ON", 'label' => "Ontario"],
            ['value' => "NL", 'label' => "Newfoundland and Labrador"],
            ['value' => "NS", 'label' => "Nova Scotia"],
            ['value' => "PE", 'label' => "Prince Edward Island"],
            ['value' => "NB", 'label' => "New Brunswick"],
            ['value' => "QC", 'label' => "Quebec"],
            ['value' => "MB", 'label' => "Manitoba"],
            ['value' => "SK", 'label' => "Saskatchewan"],
            ['value' => "AB", 'label' => "Alberta"],
            ['value' => "NT", 'label' => "Northwest Territories"],
            ['value' => "NU", 'label' => "Nunavut"],
            ['value' => "YT", 'label' => "Yukon Territor"]
        ];

        return $enums;
    }
}