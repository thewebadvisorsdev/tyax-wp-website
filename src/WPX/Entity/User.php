<?php

namespace WPX\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class User extends Entity {

    protected static $table = 'wp_users';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'ID' =>                     ['type' => 'bigint', 'required' => true, 'primary' => true, 'autoincrement' => true],
            'user_login' =>             ['type' => 'string', 'required' => true],
            'user_pass' =>              ['type' => 'string', 'required' => true],
            'user_nicename' =>          ['type' => 'string', 'required' => true],
            'user_email' =>             ['type' => 'string', 'required' => true],
            'user_url' =>               ['type' => 'string', 'required' => true],
            'user_registered' =>        ['type' => 'datetime', 'required' => true, 'default' => new \DateTime()],
            'user_activation_key' =>    ['type' => 'string', 'required' => true],
            'user_status' =>            ['type' => 'integer', 'required' => true, 'default' => 0],
            'display_name' =>           ['type' => 'string', 'required' => true]
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'posts' => $mapper->hasMany($entity, 'WPX\Entity\Post', 'post_author')->order(['post_date' => 'ASC']),
            'comments' => $mapper->hasMany($entity, 'WPX\Entity\Comment', 'comment_post_id')->order(['comment_date' => 'ASC']),
        ];
    }
}