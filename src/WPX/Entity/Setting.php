<?php

namespace WPX\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class Setting extends WPXEntity {

    const RELATION_HEADING              = "Manage Related Settings";
    const DISPLAY_FIELD                 = 'description';
    
    protected static $table             = 'Settings';
    protected static $display_fields    = ['description', 'key', 'modified', 'created'];
    protected static $entity_tag        = ['singluar' => 'setting', 'plural' => 'settings'];
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'id'            => ['type' => 'integer', 'primary' => true, 'autoincrement' => true, 'index' => true],
            'key'           => ['type' => 'string', 'required' => true, 'unique' => true],
            'value'         => ['type' => 'text', 'nullable' => true],
            'description'   => ['type' => 'string'],
            'created'       => ['type' => 'datetime', 'value' => new \DateTime('now')],
            'modified'      => ['type' => 'datetime', 'value' => new \DateTime('now')],
        ];

        return self::$dbfields;
    }
}