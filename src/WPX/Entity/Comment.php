<?php

namespace WPX\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

class Comment extends Entity {

    protected static $table = 'wp_comments';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'comment_ID'            => [ 'type' => "bigint", 'required' => true, 'primary' => true, 'autoincrement' => true],
            'comment_post_ID'       => [ 'type' => "bigint", 'required' => true, 'default' => 0],
            'comment_author'        => [ 'type' => "string", 'required' => true],
            'comment_author_email'  => [ 'type' => "string", 'required' => true],
            'comment_author_url'    => [ 'type' => "string", 'required' => true],
            'comment_author_IP'     => [ 'type' => "string", 'required' => true],
            'comment_date'          => [ 'type' => "datetime", 'required' => true, 'default' => new \DateTime()],
            'comment_date_gmt'      => [ 'type' => "datetime", 'required' => true, 'default' => new \DateTime()],
            'comment_content'       => [ 'type' => "text", 'required' => true],
            'comment_karma'         => [ 'type' => "integer", 'required' => true, 'default' => 0],
            'comment_approved'      => [ 'type' => "string", 'required' => true, 'default' =>"1"],
            'comment_agent'         => [ 'type' => "string", 'required' => true],
            'comment_type'          => [ 'type' => "string", 'required' => true],
            'comment_parent'        => [ 'type' => "bigint", 'required' => true, 'default' => 0],
            'user_id'               => [ 'type' => "bigint", 'required' => true , 'default' => 0]
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'post' => $mapper->belongsTo($entity, 'WPX\Entity\Post', 'ID'),
            'author' => $mapper->belongsTo($entity, 'WPX\Entity\User', 'post_author'),
        ];
    }
}