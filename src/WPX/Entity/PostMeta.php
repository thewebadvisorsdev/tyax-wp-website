<?php

namespace WPX\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class PostMeta extends Entity {

    // The heading that appears in relation views
    const RELATION_HEADING              = "Manage Related Metadata"; 
    // The field that's used for general purpose messages in the UI related to this entity
    const DISPLAY_FIELD                 = 'meta_key'; 

    // protected static $mapper            = null;
    // The fieds that are to appear in the Table View for each Model Admin
    protected static $display_fields    = ['meta_key','meta_value'];
    protected static $entity_tag        = ['singluar' => 'metadata', 'plural' => 'metadata'];
    
    // Spot ORM required properties
    protected static $table             = 'wp_postmeta';
    protected static $dbfields;    

    public static function fields() {
        self::$dbfields = [
            'meta_id' =>    ['type' => 'bigint', 'required' => true, 'primary' => true, 'autoincrement' => true],
            'post_id' =>    ['type' => 'integer', 'required' => true],
            'meta_key' =>   ['type' => 'string', 'required' => true],
            'meta_value' => ['type' => 'string', 'default' => 'default']
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'posts' => $mapper->hasMany($entity, 'WPX\Entity\Post', 'post_id'),
        ];
    }
}