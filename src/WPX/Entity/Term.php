<?php

namespace WPX\Entity;

use WPX\Entity\WPEntity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class Term extends WPEntity {

    const ENUM_TERM_TAXONOMY_TAG = 'post_tag';
    const ENUM_TERM_TAXONOMY_CATEGORY = 'category';

    protected static $table = 'wp_terms';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'term_id'       => ['type' => 'bigint', 'unsigned' => true, 'required' => true, 'primary' => true, 'autoincrement' => true],
            'name'          => ['type' => 'string', 'default' => ''],
            'slug'          => ['type' => 'string', 'default' => ''],
            'term_group'    => ['type' => 'bigint', 'default' => 0]
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'termrelations' => $mapper->belongsTo($entity, 'WPX\Entity\TermRelation', 'term_taxonomy_id')
        ];
    }
}