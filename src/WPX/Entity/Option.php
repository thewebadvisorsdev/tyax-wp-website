<?php

namespace WPX\Entity;

use Spot\Entity;

class Option extends Entity {
    protected static $table = 'wp_options';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'option_id' => [ 'type' => 'bigint', 'primary' => 'true', 'autoincrement' => true],
            'option_name' => [ 'type' => 'string', 'required' => true], 
            'option_value' => [ 'type' => 'text' ], 
            'autoload' => [ 'type' => 'string', 'default' => 'yes']
        ];

        return self::$dbfields;
    }
}