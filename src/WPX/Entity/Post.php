<?php

namespace WPX\Entity;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

class Post extends WPEntity {

    protected static $table = 'wp_posts';
    protected static $mapper = 'WPX\Mapper\PostMapper';
    protected static $dbfields;

    protected $type;

    public static function fields() {
        self::$dbfields = [
            'ID'                        => ['type'  => 'integer', 'primary' => true, 'autoincrement' => true, 'index' => true],
            'post_author'               => ['type' => 'bigint', 'default' => 0, 'index' => true],
            'post_date'                 => ['type' => 'datetime', 'value' => new \DateTime('now'), 'index' => true],
            'post_date_gmt'             => ['type' => 'datetime', 'value' => new \DateTime('now')],
            'post_content'              => ['type' => 'text'],
            'post_title'                => ['type' => 'text', 'required' => true],
            'post_excerpt'              => ['type' => 'text'],
            'post_status'               => ['type' => 'string', 'default' => self::ENUM_STATUS_DRAFT, 'index' => true],
            'comment_status'            => ['type' => 'string', 'default' => 'open'],
            'ping_status'               => ['type' => 'string', 'default' => 'open'],
            'post_password'             => ['type' => 'string'],
            'post_name'                 => ['type' => 'string', 'index' => true],
            'to_ping'                   => ['type' => 'text'],
            'pinged'                    => ['type' => 'text'],
            'post_modified'             => ['type' => 'datetime', 'value' => new \DateTime('now')],
            'post_modified_gmt'         => ['type' => 'datetime', 'value' => new \DateTime('now')],
            'post_content_filtered'     => ['type' => 'text'],
            'post_parent'               => ['type' => 'bigint', 'index' => true],
            'guid'                      => ['type' => 'string'],
            'menu_order'                => ['type' => 'integer'],
            'post_type'                 => ['type' => 'string', 'default' => 'post', 'index' => true],
            'post_mime_type'            => ['type' => 'string'],
            'comment_count'             => ['type' => 'bigint']
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'meta'      => $mapper->hasMany($entity, 'WPX\Entity\PostMeta', 'post_id')->whereSql("NOT (meta_key = '_edit_lock')"),
            'media'     => $mapper->hasMany($entity, 'WPX\Entity\Media', 'OwnerID')->order(['modified' => 'DESC']),
            'comments'  => $mapper->hasMany($entity, 'WPX\Entity\Comment', 'comment_post_id')->order(['comment_date' => 'ASC']),
            'terms'     => $mapper->hasManyThrough($entity, 'WPX\Entity\Term', 'WPX\Entity\TermRelation', 'term_taxonomy_id', 'object_id'),
            'author'    => $mapper->belongsTo($entity, 'WPX\Entity\User', 'post_author'),
        ];
    }

    # +------------------------------------------------------------------------+
    # BASIC API
    # +------------------------------------------------------------------------+
    public function type( $type = null ) {

        if( isset($type) ) {
            switch( $type ) {
                case WPEntity::ENUM_TYPE_POST:
                case WPEntity::ENUM_TYPE_PAGE:
                    $this->post_type = $type;
                    break;
                default : 
                    break;
            }
        }

        return $this->post_type;
    }

}