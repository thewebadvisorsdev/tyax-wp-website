<?php

namespace WPX\Entity;

use WPX\Entity\WPEntity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class TermRelation extends WPEntity {

    protected static $table = 'wp_term_relationships';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'object_id'         => ['type' => 'bigint', 'unsigned' => true, 'required' => true, 'primary' => true],
            'term_taxonomy_id'  => ['type' => 'bigint', 'unsigned' => true, 'required' => true],
            'term_order'        => ['type' => 'integer', 'default' => 0]
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'posts' => $mapper->belongsTo($entity, 'WPX\Entity\Post', 'post_id'),
            // 'terms' => $mapper->hasMany($entity, 'WPX\Entity\Term', 'term_id')
        ];
    }
}