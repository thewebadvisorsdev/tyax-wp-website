<?php 

namespace WPX\Entity;

use Spot\Entity;
use AUX\Utils\Log;
use Spot\Exception;
use AUX\Utils\Debug;

/**
 * @link http://phpdatamapper.com/docs/entities/
 * @link https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/basic-mapping.html#doctrine-mapping-types
 */
class WPEntity extends Entity {
    const ENUM_STATUS_DRAFT         = 'draft';
    const ENUM_STATUS_PUBLISH       = 'publish';
    const ENUM_STATUS_PENDING       = 'pending';
    const ENUM_STATUS_AUTO_DRAFT    = 'auto-draft';
    const ENUM_STATUS_FUTURE        = 'future';

    const ENUM_TYPE_ATTACHMENT      = 'attachment';
    const ENUM_TYPE_POST            = 'post';
    const ENUM_TYPE_PAGE            = 'page';
    const ENUM_TYPE_REVISION        = 'revision';
    const ENUM_TYPE_OPTION          = 'option';
    const ENUM_TYPE_META            = 'meta';
    const ENUM_TYPE_MEDIA           = 'media';
    const ENUM_TYPE_TERM            = 'term';
    const ENUM_TYPE_TERMRELATION    = 'termrelation';

    const ENUM_MEDIA_IMAGE          = 'image';
    const ENUM_MEDIA_ATTACHMENT     = 'attachment';
    const ENUM_MEDIA_THUMBNAIL      = 'thumbnail';

    
    public static function getENUMS($type) {
        static $class_constants = [];
        $enum_types = [];
        
        if( empty($class_constants) ) {
            $ref = new \ReflectionClass(__CLASS__);
            $class_constants = $ref->getConstants();
        }
        
        foreach( $class_constants as $k => $v ) {
            $_k = strtolower($k);
            if( strpos($_k, $type) !== false ) {
                preg_match('#_(\w+?)_#i', $_k, $matches);
                $enumkey = $matches[1];

                if( !empty($matches) && isset($enumkey) && !array_key_exists($enumkey,$class_constants) ) {
                    $class_constants[$enumkey] = [];
                }
                
                if( isset($class_constants[$enumkey]) && !in_array($v, $class_constants[$enumkey]) ) {
                    array_push($class_constants[$enumkey], $v);
                    array_push($enum_types, $v);
                }
            }
        }

        return $enum_types;
    }
}