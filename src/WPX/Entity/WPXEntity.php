<?php 

namespace WPX\Entity;

use Spot\Entity;
use AUX\Utils\Log;
use Spot\Exception;
use AUX\Utils\Debug;

/**
 * @link http://phpdatamapper.com/docs/entities/
 * @link https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/basic-mapping.html#doctrine-mapping-types
 */
class WPXEntity extends Entity {

    protected static $display_fields    = [];
    protected static $entity_tag        = ['singluar' => 'wpxentity', 'plural' => 'wpxentities'];

    /**
     * Get a collection of fields intended for displaying in a UI, e.g. Data Tables
     */
    public static function getDisplayFields( $primary_field = false ) {
        if( $primary_field ) {
            return !isset(static::$display_fields[0]) ? static::$display_fields[0] : [];
        }
        return static::$display_fields;
    }

    
    // ---------------------------------------------------------- //
    // HELPER FUNCTIONS
    // ---------------------------------------------------------- //
    /**
     * Get a list of all constants for this class
     */
    public function getENUMS() {
        static $class_constants = [];
        
        if( empty($class_constants) ) {
            $ref = new \ReflectionClass( $this );
            $class_constants = $ref->getConstants();
        }
 
        return $class_constants ;
    }

    /**
     * Additional business logic to call, typically used in conjunction with events
     */
    public function flush() {}    
}