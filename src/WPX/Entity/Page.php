<?php

namespace WPX\Entity;

use Spot\EntityInterface;
use Spot\MapperInterface;

class Page extends Post {
    protected static $table = 'wp_posts';
    protected static $mapper = 'WPX\Mapper\PageMapper';

    public static function fields() {
        $fields = parent::fields();

        $fields = array_replace($fields, [
            'post_type' => ['type' => 'string', 'default' => 'page', 'index' => true]
        ]);

        self::$dbfields = $fields;

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'meta' => $mapper->hasMany($entity, 'WPX\Entity\PostMeta', 'post_id')->whereSql("NOT (meta_key = '_edit_lock')"),
            'media' => $mapper->hasMany($entity, 'WPX\Entity\Media', 'OwnerID')->order(['modified' => 'DESC']),
            'author' => $mapper->belongsTo($entity, 'WPX\Entity\User', 'post_author'),
        ];
    }
}