<?php

namespace WPX\Entity;

use WPX\Entity\WPEntity;
use Spot\EntityInterface;
use Spot\MapperInterface;


class Media extends WPEntity {

    protected static $table = 'media';
    protected static $dbfields;

    public static function fields() {
        self::$dbfields = [
            'ID'        => ['type' => 'bigint', 'unsigned' => true, 'required' => true, 'primary' => true, 'autoincrement' => true],
            'OwnerID'   => ['type' => 'bigint', 'unsigned' => true, 'default' => 0],
            'post_id'   => ['type' => 'bigint', 'unsigned' => true, 'default' => 0],
            'created'   => ['type' => 'datetime', 'value' => new \DateTime()],
            'modified'  => ['type' => 'datetime', 'value' => new \DateTime()],
            'name'      => ['type' => 'string', 'required' => true],
            'title'     => ['type' => 'string', 'required' => true],
            'type'      => ['type' => 'string', 'default' => ''],
            'asset'     => ['type' => 'string', 'required' => true],
            'hash'      => ['type' => 'text', 'required' => true],
            'metadata'  => ['type' => 'text', 'default' => ''],
            'othermetadata'  => ['type' => 'text', 'default' => '']
        ];

        return self::$dbfields;
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'posts' => $mapper->hasMany($entity, 'WPX\Entity\Post', 'OwnerID')
        ];
    }
}