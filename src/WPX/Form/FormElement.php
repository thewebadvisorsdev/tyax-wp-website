<?php

namespace WPX\Form;

use AUX\Utils\Log;
use AUX\Utils\Debug;
use AUX\Utils\Text;

class FormElement {
    protected $element_attributes = [];
    protected $element_attributes_string;
    protected $element_classes = [];
    protected $element_prefix;
    protected $block_elements = ['textarea', 'select', 'radiogroup', 'checkboxgroup', 'fieldset','div'];
    protected $date_time_format = 'Y-m-d';

    public function __construct(array $attributes) {
        $this->element_attributes = array_merge( [], $this->element_attributes, $attributes );
        $this->init();
    }
    
    protected function init() {
        $this->__filterDateTime();
    }

    public function attributes( $attributes = [], $overwrite_all_attributes = true ) {
        if( !empty( $attributes ) ) {
            $this->element_attributes =  $overwrite_all_attributes ? $attributes : array_merge([], $this->element_attributes, $attributes);
            $this->init();
        }
        return $this->element_attributes;
    }

    public function attr( $key, $value = null ) {
        if( null != $value ) {
            $this->element_attributes[$key] = $value;
            return $this;
        }
        return array_key_exists($key, $this->element_attributes) ? $this->element_attributes[$key] : null;
    }

    public function getAttribute( $key ) {
        return array_key_exists($key, $this->element_attributes) ? $this->element_attributes[$key] : null;
    }

    public function setAttribute( $key, $value = null ) {
        $this->element_attributes[$key] = $value;
    }

    public function type( $value = null ) {
        if( $value ) {
            $this->element_attributes['type'] = $value;
        }

        return $this->element_attributes['type'];
    }

    public function prefix( $value = null ) {
        $this->element_prefix = $value;
        $id = $value ? sprintf("%s%s", rtrim($value, '_') . '_', $this->element_attributes['name']) : $this->element_attributes['name'];
        $this->attr('id', $id);
        return $this;
    }

    public function datetime($format) {
        $this->date_time_format = $format;
    }

    public function value($value = null) {
        if( $value ) {
            $this->element_attributes['value'] = $value;
            $this->__filterDateTime();
        }

        return $this->element_attributes['value'];
    }

    public function addClass($class) {
        array_push($this->element_classes, $class);
        $this->element_classes = array_unique($this->element_classes);
        
        return $this;
    }

    public function removeClass($class) {

        if( in_array($class,$this->element_classes) ) {
            unset( $this->element_classes[$class] );
            $this->element_classes = array_values($this->element_classes);
        }

        return $this;
    }

    public function data() {
        try {
            $valid_attributes = array_diff_key($this->attributes(), ['label' => null, 'instructions' => null]);
            $data = [
                'id'                => $this->attr('id'),
                'type'              => $this->type(),
                'name'              => $this->attr('name'),
                'label'             => Text::camelCaseToSentenceCase($this->attr('name')),
                'value'             => $this->attr('value'),
                'class'             => empty($this->element_classes) ? null : implode(' ', $this->element_classes),
                'attributes'        => $this->attributes(),
                'attributes_string' => $this->__attrToString($valid_attributes)
            ];
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }
        return $data;
    }

    // ---------------------------------------------------------- //
    // HELPER METHODS
    // ---------------------------------------------------------- //
    
    protected function __attrToString( $field_attributes ) {
        
        try {
            $type = $this->type();
            $attributes = [
                sprintf('%s="%s"', 'type', $type)
            ];
            
            foreach( $field_attributes as $attr_name => $value ) {
                if( FormAttributeValidator::isBooleanAttribute($attr_name) ) {
                    $val = (string) $value;
                    switch( $val ) {
                        case 'on':
                        case 'true':
                        case '1' :
                            array_push( $attributes, sprintf('%s', $attr_name) );
                            break;
                        case 'off':
                        case 'false':
                        case '0' :
                        case '' :
                            break;
                        default :
                            array_push( $attributes, sprintf('%s="%s"', $attr_name, $value) );
                            break;
                    }
                }else{
                    if( !in_array( $type, $this->block_elements ) ) {
                        array_push( $attributes, sprintf('%s="%s"', $attr_name, $value) );
                    }else{
                        if( $attr_name != 'value' && !is_array($value) ) {
                            array_push( $attributes, sprintf('%s="%s"', $attr_name, $value ) );
                        }
                    }
                }
            }
    
            // $attributes = array_map('stripslashes_deep', $attributes);
            // Log::trace(__METHOD__, array_unique($attributes));

            $this->element_attributes_string = implode(" ",  array_unique($attributes));
        }catch( \Exception $e ){
            Log::log($e->getMessage() . " on line: " . $e->getLine(), Log::LOG_TYPE_ERROR);
        }      


        return $this->element_attributes_string;
    }
    
    protected function __filterDateTime() {
        try {
            if( !array_key_exists('value', $this->element_attributes) ) return false;
            $value = $this->element_attributes['value'];
            if( $this->type() == 'date' && null != $value ) {
                
                if( $value instanceof \DateTime ) {
                    $value = (string)$value->format($this->date_time_format);
                }else{
                    $date = \DateTime::createFromFormat('Y-m-d', $value);
                    $value = (string)$date->format($this->date_time_format);
                }

                $this->element_attributes['value'] = $value;
            }
        } catch(\Exception $e) {
            Log::log( $e->getMessage() );
        }
    }

    public function __isDate($date_string){ 
        $date_string = str_replace('/', '-', $date_string);
        return is_numeric( strtotime($date_string) );
    }

    public function __isset($key) {
        return property_exists($this,$key) ? $this->{$key} : null;
    }
}