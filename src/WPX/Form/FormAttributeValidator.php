<?php

namespace WPX\Form;

use AUX\Utils\Log;
use AUX\Utils\Debug;

final class FormAttributeValidator {
    const ENUM_HTML = 'html';
    const ENUM_ARIA = 'aria';
    const ENUM_BOOLEAN = 'boolean';

    private static $instance;
    public static $html_attributes = [
        'autocomplete' => [
            'on',
            'off',
        ],
        'border' => [
            '1',
            '',
        ],
        'contenteditable' => [
            'true',
            'false',
            '',
        ],
        'crossorigin' => [
            'anonymous',
            'use-credentials',
        ],
        'dir' => [
            'ltr',
            'rtl',
            'auto',
        ],
        'draggable' => [
            'true',
            'false',
            'auto',
        ],
        'enctype' => [
            'application/x-www-form-urlencoded',
            'multipart/form-data',
            'text/plain',
        ],
        'formenctype' => [
            'application/x-www-form-urlencoded',
            'multipart/form-data',
            'text/plain',
        ],
        'formmethod' => [
            'get',
            'post',
            'dialog',
        ],
        'formtarget' => [
            '_blank',
            '_self',
            '_parent',
            '_top',
        ],
        'inputmode' => [
            'verbatim',
            'latin',
            'latin-name',
            'latin-prose',
            'full-width-latin',
            'kana',
            'kana-name',
            'katakana',
            'numeric',
            'tel',
            'email',
            'url',
        ],
        'kind' => [
            'subtitles',
            'captions',
            'descriptions',
            'chapters',
            'metadata',
        ],
        'link' => [
            'alternate',
            'author',
            'bookmark',
            'external',
            'help',
            'icon',
            'license',
            'next',
            'nofollow',
            'noopener',
            'noreferrer',
            'prefetch',
            'prev',
            'search',
            'stylesheet',
            'tag',
        ],
        'method' => [
            'get',
            'post',
            'dialog',
        ],
        'preload' => [
            'none',
            'metadata',
            'auto',
        ],
        'referrerpolicy' => [
            'no-referrer',
            'no-referrer-when-downgrade',
            'same-origin',
            'origin',
            'strict-origin',
            'origin-when-cross-origin',
            'strict-origin-when-cross-origin',
            'unsafe-url',
            '',
        ],
        'rel' => [
            'alternate',
            'author',
            'bookmark',
            'external',
            'help',
            'icon',
            'license',
            'next',
            'nofollow',
            'noopener',
            'noreferrer',
            'prefetch',
            'prev',
            'search',
            'stylesheet',
            'tag',
            'dns-prefetch',
            'icon',
            'next',
            'pingback',
            'preconnect',
            'prefetch',
            'preload',
            'prerender',
            'search',
            'serviceworker',
            'stylesheet',
        ],
        'rev' => [
            'alternate',
            'author',
            'bookmark',
            'external',
            'help',
            'icon',
            'license',
            'next',
            'nofollow',
            'noopener',
            'noreferrer',
            'prefetch',
            'prev',
            'search',
            'stylesheet',
            'tag',
        ],
        'role' => [
            'alert',
            'alertdialog',
            'application',
            'article',
            'banner',
            'button',
            'cell',
            'checkbox',
            'columnheader',
            'combobox',
            'complementary',
            'contentinfo',
            'definition',
            'dialog',
            'directory',
            'document',
            'feed',
            'figure',
            'form',
            'grid',
            'gridcell',
            'group',
            'heading',
            'img',
            'link',
            'list',
            'listbox',
            'listitem',
            'log',
            'main',
            'marquee',
            'math',
            'menu',
            'menubar',
            'menuitem',
            'menuitemcheckbox',
            'menuitemradio',
            'navigation',
            'none',
            'note',
            'option',
            'presentation',
            'progressbar',
            'radio',
            'radiogroup',
            'region',
            'row',
            'rowgroup',
            'rowheader',
            'scrollbar',
            'search',
            'searchbox',
            'separator',
            'slider',
            'spinbutton',
            'status',
            'switch',
            'tab',
            'table',
            'tabpanel',
            'term',
            'textbox',
            'timer',
            'toolbar',
            'tooltip',
            'tree',
            'treegrid',
            'treeitem',
        ],
        'sandbox' => [
            'allow-forms',
            'allow-pointer-lock',
            'allow-popups',
            'allow-presentation',
            'allow-same-origin',
            'allow-scripts',
            'allow-top-navigation',
        ],
        'shape' => [
            'circle',
            'circ',
            'default',
            'poly',
            'polygon',
            'rect',
            'rectangle',
        ],
        'spellcheck' => [
            'true',
            'false',
            '',
        ],
        'target' => [
            '_blank',
            '_self',
            '_parent',
            '_top',
        ],
        'translate' => [
            'yes',
            'no',
            '',
        ],
        'type' => [
            'hidden',
            'text',
            'search',
            'tel',
            'url',
            'email',
            'password',
            'date',
            'month',
            'week',
            'time',
            'datetime-local',
            'number',
            'range',
            'color',
            'checkbox',
            'radio',
            'file',
            'submit',
            'image',
            'reset',
            'button',
            'a',
            'A',
            'i',
            'I',
            '1'        
        ],
        'wrap' => [
            'soft',
            'hard',
        ],
    ];

    public static $aria_attributes = [
        'aria-autocomplete' => [
            'inline',
            'list',
            'both',
            'none',
        ],
        'aria-busy' => [
            'true',
            'false',
        ],
        'aria-checked' => [
            'true',
            'false',
            'mixed',
            'undefined',
        ],
        'aria-current' => [
            'page',
            'step',
            'location',
            'date',
            'time',
            'true',
            'false',
        ],
        'aria-disabled' => [
            'true',
            'false',
        ],
        'aria-expanded' => [
            'true',
            'false',
            'undefined',
        ],
        'aria-haspopup' => [
            'true',
            'false',
            'menu',
            'listbox',
            'tree',
            'grid',
            'dialog',
        ],
        'aria-hidden' => [
            'true',
            'false',
            'undefined',
        ],
        'aria-invalid' => [
            'true',
            'false',
            'grammar',
            'spelling',
        ],
        'aria-polite' => [
            'assertive',
            'off',
            'polite',
        ],
        'aria-modal' => [
            'true',
            'false',
        ],
        'aria-multiline' => [
            'true',
            'false',
        ],
        'aria-multiselectable' => [
            'true',
            'false',
        ],
        'aria-orientation' => [
            'horizontal',
            'vertical',
            'undefined',
        ],
        'aria-pressed' => [
            'true',
            'false',
            'mixed',
            'undefined',
        ],
        'aria-readonly' => [
            'true',
            'false',
        ],
        'aria-relevant' => [
            'additions',
            'additions text',
            'all',
            'removals',
            'text',
        ],
        'aria-required' => [
            'true',
            'false',
        ],
        'aria-selected' => [
            'true',
            'false',
            'undefined',
        ],
        'aria-sort' => [
            'ascending',
            'descending',
            'none',
            'other',
        ],
    ];

    public static $boolean_attributes = [
        "allowfullscreen" => [
			"allowfullscreen",
			'true',
			'false'
		],
        "allowpaymentrequest" => [
			"allowpaymentrequest",
			'true',
			'false'
		],
        "async" => [
			"async",
			'true',
			'false'
		],
        "autofocus" => [
			"autofocus",
			'true',
			'false'
		],
        "autoplay" => [
			"autoplay",
			'true',
			'false'
		],
        "checked" => [
			"checked",
			'true',
			'false'
		],
        "controls" => [
			"controls",
			'true',
			'false'
		],
        "default" => [
			"default",
			'true',
			'false'
		],
        "defer" => [
			"defer",
			'true',
			'false'
		],
        "disabled" => [
			"disabled",
			'true',
			'false'
		],
        "formnovalidate" => [
			"formnovalidate",
			'true',
			'false'
		],
        "hidden" => [
			"hidden",
			'true',
			'false'
		],
        "ismap" => [
			"ismap",
			'true',
			'false'
		],
        "itemscope" => [
			"itemscope",
			'true',
			'false'
		],
        "loop" => [
			"loop",
			'true',
			'false'
		],
        "multiple" => [
			"multiple",
			'true',
			'false'
		],
        "muted" => [
			"muted",
			'true',
			'false'
		],
        "nomodule" => [
			"nomodule",
			'true',
			'false'
		],
        "novalidate" => [
			"novalidate",
			'true',
			'false'
		],
        "open" => [
			"open",
			'true',
			'false'
		],
        "readonly" => [
			"readonly",
			'true',
			'false'
		],
        "required" => [
			"required",
			'true',
			'false'
		],
        "reversed" => [
			"reversed",
			'true',
			'false'
		],
        "selected" => [
			"selected",
			'true',
			'false'
		],
        "typemustmatch" => [
			"typemustmatch",
			'true',
			'false'
		]
    ];

    private function __construct(){ }

    public static function inst() {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function validate($attribute_name, $attribute_value = null) {
        // Ignore data-* attributes
        if( strpos(trim($attribute_name), 'data-') !== false ) return true;

        $clean_attribute_name = strtolower(trim($attribute_name));

        $combined_attributes = array_merge([], array_keys(self::$html_attributes), array_keys(self::$aria_attributes), array_keys(self::$boolean_attributes));
        $valid_property = in_array($clean_attribute_name, $combined_attributes);
        $valid_value    = false;

        if( null === $attribute_value ) {
            // Boolean values
            $valid_value = array_key_exists($clean_attribute_name, self::$boolean_attributes) ? self::$boolean_attributes[$clean_attribute_name] : [];
        }else{
            $attr_collection = array(
                self::$boolean_attributes,
                self::$html_attributes,
                self::$aria_attributes
            );
    
            // Non boolean
            foreach( $attr_collection as $collection_key => $collection ) {
                if( array_key_exists($clean_attribute_name, $collection) ) {
                    $match = $collection[ $clean_attribute_name ];
                    $valid_value = in_array($attribute_value, $match);
                    break;
                }
            }
        }

        return $valid_property && $valid_value;
    }

    public static function isBooleanAttribute( $attribute_name, $attribute_value = null ) {
        return array_key_exists($attribute_name,self::$boolean_attributes);
    }
}
