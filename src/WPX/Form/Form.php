<?php

namespace WPX\Form;

use AUX\Utils\Log;
use AUX\Utils\Debug;
use WPX\Form\FormElement;

/**
 * @author Chris Murphy
 * @version 0.0.1
 */
class Form {
    protected $form_elements = [];
    protected $form_html;
    protected $form_id;
    protected $render_engine;
    protected $field_id_prefix;
    protected $honeypot_field;

    protected $form_element_attributes = [
        'action'       => '',
        'method'       => 'post',
        'enctype'      => 'application/x-www-form-urlencoded',
        'classes'      => [],
        'id'           => '',
        'novalidate'   => false,
        'autocomplete' => false,
        'honeypot'     => false,
        'add_submit'   => true,
        'form_tags' => true
    ];

    protected $form_element_user_attributes = [];

    public function elements() {
        return $this->form_elements;
    }

    public function ID() {
        return $this->form_id;
    }

    // ---------------------------------------------------------- //
    // CONSTRUCTOR
    // ---------------------------------------------------------- //
    public function __construct( $render_engine, array $context ){
        $this->render_engine = $render_engine;
        $this->form_element_user_attributes = array_merge( [], $this->form_element_attributes, $context);
        $this->init();
    }
    
    /**
     * Initialize the form builder
     */
    protected function init() {
        $this->field_id_prefix = $this->__prefix( $this->form_element_user_attributes );
        $this->form_id = array_key_exists('id',$this->form_element_user_attributes) ? $this->form_element_user_attributes['id'] : null;

        if( array_key_exists('honeypot', $this->form_element_user_attributes) && 
            $this->form_element_user_attributes['honeypot'] == true ) {

            $honeypot_attributes = [
                'type' => 'text',
                'name' => 'wpx_instructions',
                "placeholder" => "Prove you're human, keep this field blank!",
                'value' => null,
                "required" => false
            ];
    
            $this->honeypot_field = new FormElement($honeypot_attributes);
        }
    }

    /**
     * Add a new form element.
     * 
     * Note: DateTime values intended for <date /> elements will have their values converted to strings
     * following the format: 'Y-m-d', e.g. 2020-01-01. Timezones are stripped. Field values may need to 
     * be converted back to a DateTime object, depending on the how you intended to persist this value.
     * 
     * @param Array $attributes An associative array of attributes and values for a given form element.
     * @return Form Returns the current Form instance for fluid method chaining.
     */
    public function add( $attributes = [] ) {
        $f = new FormElement($attributes);
        $f->prefix($this->field_id_prefix);
        $this->form_elements[ $f->attr('name') ] = $f;
        return $this;
    }
    
    /**
     * Get a form element by its field name.
     * 
     * @return FormElement|Boolean If found, returns the associated FormElement instance, false if otherwise
     */
    public function getFormElement( $key ) {
        if( array_key_exists($key, $this->form_elements) ) {
            return $this->form_elements[ $key ];
        }

        return false;
    }

    /**
     * Remove a form element by it's field name.
     * 
     * @param String    $field_name    The name of the field to remove
     * @return Form|Boolean If the element was removed, this method eturns the current Form instance 
     * for fluid method chaining, or false if otherwise
     */
    public function remove( $field_name ) {
        if( array_key_exists($field_name, $this->form_elements) ) {
            unset($this->form_elements[ $field_name ]);
            return $this;
        }
        return false;
    }

    /**
     * Replace the attributes of a FormElement.
     * 
     * @param String    $field_name The name of the field
     * @param Array     $attributes An associative array of valid field attributes.
     */
    public function replace( $field_name, array $attributes ) {
        if( array_key_exists($field_name, $this->form_elements) && !empty( $attributes ) ) {
            $f = $this->form_elements[ $field_name ];
            if( $f ) {
                $f->attributes( $attributes, false );
            }
            return null != $f ? $this : false;
        }
        return false;
    }

    /**
     * Maps a given entity to a form element type, using the following format:
     * 
     * [ 'type'      => 'text',
     *   'name'      => 'my_textfield',
     *   'required'  => true|false,
     *   'value'     => '' ]
     * 
     * For elements such as a <select> or radio group, selection values can be
     * supplied in a nested array named, 'options'.
     * 
     * [ 'type'      => 'radiogroup',
     *   'name'      => 'my_radiogroup',
     *   'required'  => true|false,
     *   'value'     => 'an numeric index, indicating your initial selection, 0 for default',
     *   'options'   => [ ['name' => 'fieldname[]','value' => 'somevalue'], ['name' => 'fieldname[]','value' => 'somevalue'] ]
     * ]
     * 
     * @param Array $entity_fields  An associative array of fields and their attributes
     * @param Array $filter_list    A blacklist of field names that should not be rendered in the form
     * @return Form Returns the current Form instance for fluid method chaining.
     */
    public function mapEntityToForm( array $entity_fields, array $filter_list = [] ) {
        try {
            foreach( $entity_fields as $field_name => $field_attr ) {
                if( array_key_exists('type', $field_attr) && !in_array($field_name, $filter_list) ) {
                    $type       = $this->__mapFieldToElementType($field_attr['type']);
                    $required   = array_key_exists( 'required', $field_attr ) ? $field_attr['required'] : null;
                    $value      = array_key_exists( 'value', $field_attr ) ? $field_attr['value'] : null;
                    $attributes = [
                        'type'      => $type,
                        'name'      => $field_name,
                        'required'  => $required,
                        'value'     => $value
                    ];
                    $this->add($attributes);
                }
            }

            return $this;
        } catch(\Exception $e) {
            Log::log( $e->getMessage() );
        }
    }

    /**
     * Build the form using Twig as the template engine. 
     * 
     * A form is built in two stages:
     * 1. Render the internal field elements of a form; then
     * 2. Render the form element itself, injecting the rendered field elements
     * 
     * This approach requires that two Twig templates need to be available:
     * 1. Form.html.twig — this contains the FORM element markup
     * 2. Form_Field.html.twig - this contains the FIELD element markup.
     * 
     * These templates may exist anywhere in your application so long as 
     * Twig is aware of where they are stored.
     * 
     * @return  String  Returns the compiled form markup.
     */
    public function build($field_order = []) {
        // Build Fields
        $fields_output = [];

        if( !empty( $field_order ) ) {
            // Render fields according to $field_order
            $fields_unsorted = [];
            foreach( $field_order as $key ) {
                if( array_key_exists( $key, $this->form_elements ) ) {
                    $el = $this->form_elements[ $key ];
                    array_push( $fields_output, $this->__renderFields($el) );
                }
            }

            foreach( $this->form_elements as $key => $el ) {
                if( $key != 'submit' ) {
                    if( !in_array($key, $field_order) ) {
                        array_push( $fields_output, $this->__renderFields($el) );
                    }
                }
            }

            if( null !== $this->honeypot_field ) {
                array_push($fields_output, $this->__renderFields($this->honeypot_field));
            }
            if( array_key_exists('submit', $this->form_elements) ) {
                array_push( $fields_unsorted, $this->__renderFields($this->form_elements['submit']) );
            }
            $fields_output = array_merge([], $fields_output, $fields_unsorted);
        }else{
            foreach( $this->form_elements as $el ) {
                array_push( $fields_output, $this->__renderFields($el) );
            }
            if( null !== $this->honeypot_field ) {
                array_push($fields_output, $this->__renderFields($this->honeypot_field));
            }    
        }


        // Build Form
        $form_output = $this->__renderForm(implode("\n",$fields_output));

        return $form_output;
    }

    // ---------------------------------------------------------- //
    // HELPER FUNCTIONS
    // ---------------------------------------------------------- //
    /**
     * Creates a field prefix
     * 
     * @param Array $context    An array containing the FORM attributes.
     * @return String Returns a new field prefix based on the FORM attributes.
     */
    protected function __prefix( $context ) {
        $prefix = '';

        if( array_key_exists('id', $context) ) {
            $prefix = sprintf("Field_%s_", $context['id']);
        }else{
            $hash = md5(json_encode($context));
            $prefix = sprintf("Field_%s_", $hash);
        }

        return $prefix;
    }

    /**
     * Maps database field types to html equivalents.
     * 
     * @param String $entity_field The database field type
     * @return String Returns value appropriate for use in an element `type` attribute. Defaults to `text` if no match found.
     */
    protected function __mapFieldToElementType( $entity_field ) {
        $type = null;
        switch( $entity_field ) {
            case 'text' :
                return 'textarea';
            case 'string' :
                return 'text';
            case 'date' :
            case 'datetime' :
                return 'date';
            case 'int' :
            case 'integer' :
            case 'bigint' :
                return 'number';
            default :
                return 'text';
        }
        return $type;
    }

    /**
     * Render the form element.
     * 
     * @param String $fields_html HTML markup containing the rendered form fields.
     * @return String   The rendered form markup, complete with internal fields.
     */
    protected function __renderForm($fields_html) {
        $attributes = (object)$this->form_element_user_attributes;
        $output = null;

        if( $attributes->form_tags ) {

            $context = [
                'attributes' => [
                    'id'            => $attributes->id,
                    'action'        => $attributes->action,
                    'method'        => $attributes->method,
                    'enctype'       => $attributes->enctype,
                    'novalidate'    => $attributes->novalidate,
                    'autocomplete'  => $attributes->autocomplete
                ],
                'fields'     => $fields_html
            ];
            
            // Attribute: class
            if( $attributes->classes ) {
                $context['attributes']['class'] = implode($attributes->classes);
            }

            $twig       = $this->render_engine;
            $template   = 'Form.html.twig';
            $field      = $twig->load($template);
            $output     = $field->render($context);
            
            
        }else{
            $output = $fields_html;
        }
        return $output;
    }

    /**
     * Render a single form field.
     * 
     * @param FormElement $element An instance of `FormElement`
     * @return String   Returns the rendered form element.
     */
    protected function __renderFields(FormElement $element) {

        $twig       = $this->render_engine;
        $attributes = [
            'field' => $element->data(),
            'form_id' => sprintf("Form%s_",$this->form_id)
        ];
        // Log::trace(__METHOD__, $attributes);
        $template   = 'Form_Field.html.twig';
        $field      = $twig->load($template);
        $html       = $field->render($attributes);
        
        return $html;
    }

    public function __isset($key) {
        return property_exists($this,$key) ? $this->{$key} : null;
    }
}