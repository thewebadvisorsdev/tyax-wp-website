/*! Copyright (c) 2014 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt)
 */

(function (factory) {
	if ( typeof define === 'function' && define.amd ) {
		// AMD. Register as an anonymous module.
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS style for Browserify
		module.exports = factory;
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	$.extend($.fn, {
		mutator: function(selector, matchedFn, unmatchedFn) {
			var q = $.mutator.findorcreate(this, selector, matchedFn, unmatchedFn);
			q.run();
			return this;
		},
		expire: function(selector, matchedFn, unmatchedFn) {
			var q = $.mutator.find(this, selector, matchedFn, unmatchedFn);
			if (q) { q.stop(); }
			return this;
		}
	});


	$.mutator = function(jq, selector, matchedFn, unmatchedFn) {
		this.selector    = selector;
		this.jq          = jq;
		this.context     = jq.context;
		this.matchedFn   = matchedFn;
		this.unmatchedFn = unmatchedFn;
		this.stopped     = false;
		this.id          = $.mutator.queries.push(this)-1;

		matchedFn.$lqguid = matchedFn.$lqguid || $.mutator.guid++;
		if (unmatchedFn) { unmatchedFn.$lqguid = unmatchedFn.$lqguid || $.mutator.guid++; }
	};
	$.mutator.prototype = {
		run: function() {
			this.stopped = false;
			this.jq.find(this.selector).each($.proxy(function(i, element) {
				this.added(element);
			}, this));
		},
		stop: function() {
			this.jq.find(this.selector).each($.proxy(function(i, element) {
				this.removed(element);
			}, this));
			this.stopped = true;
		},
		matches: function(element) {
			return !this.isStopped() && $(element, this.context).is(this.selector) && this.jq.has(element).length;
		},
		added: function(element) {
			if ( !this.isStopped() && !this.isMatched(element) ) {
				this.markAsMatched(element);
				this.matchedFn.call(element, element);
			}
		},
		removed: function(element) {
			if ( !this.isStopped() && this.isMatched(element) ) {
				this.removeMatchedMark(element);
				if (this.unmatchedFn) { this.unmatchedFn.call(element, element); }
			}
		},
		getLQArray: function(element) {
			var arr   = $.data(element, $.mutator.key) || [],
				index = $.inArray(this.id, arr);
			arr.index = index;
			return arr;
		},
		markAsMatched: function(element) {
			var arr  = this.getLQArray(element);
			if ( arr.index === -1 ) {
				arr.push(this.id);
				$.data(element, $.mutator.key, arr);
			}
		},
		removeMatchedMark: function(element) {
			var arr = this.getLQArray(element);
			if ( arr.index > -1 ) {
				arr.splice(arr.index, 1);
				$.data(element, $.mutator.key, arr);
			}
		},
		isMatched: function(element) {
			var arr = this.getLQArray(element);
			return arr.index !== -1;
		},
		isStopped: function() {
			return this.stopped === true;
		}
	};

	$.extend($.mutator, {
		version: '2.0.0-pre',
		guid: 0,
		queries: [],
		watchAttributes: true,
		attributeFilter: ['class', 'className'],
		setup: false,
		timeout: null,
		method: 'none',
		prepared: false,
		key: 'mutator',
		htcPath: false,
		prepare: {
			mutationobserver: function() {
				var observer = new MutationObserver($.mutator.handle.mutationobserver);
				observer.observe(document, { childList: true, attributes: $.mutator.watchAttributes, subtree: true, attributeFilter: $.mutator.attributeFilter });
				$.mutator.prepared = true;
			},
			mutationevent: function() {
				document.addEventListener('DOMNodeInserted', $.mutator.handle.mutationevent, false);
				document.addEventListener('DOMNodeRemoved', $.mutator.handle.mutationevent, false);
				if ( $.mutator.watchAttributes ) {
					document.addEventListener('DOMAttrModified', $.mutator.handle.mutationevent, false);
				}
				$.mutator.prepared = true;
			},
			iebehaviors: function() {
				if ( $.mutator.htcPath ) {
					$('head').append('<style>body *{behavior:url('+$.mutator.htcPath+')}</style>');
					$.mutator.prepared = true;
				}
			}
		},
		handle: {
			added: function(target) {
				$.each( $.mutator.queries, function(i, query) {
					if (query.matches(target)) {
						setTimeout(function() {
							query.added(target);
						}, 1);
					}
				});
			},
			removed: function(target) {
				$.each( $.mutator.queries, function(i, query) {
					if (query.isMatched(target)) {
						setTimeout(function() {
							query.removed(target);
						}, 1);
					}
				});
			},
			modified: function(target) {
				$.each( $.mutator.queries, function(i, query) {
					if ( query.isMatched(target) ) {
						if ( !query.matches(target) ) {
							query.removed(target);
						}
					} else {
						if (query.matches(target)) {
							query.added(target);
						}
					}
				});
			},
			mutationevent: function(event) {
				var map = {
						'DOMNodeInserted' : 'added',
						'DOMNodeRemoved'  : 'removed',
						'DOMAttrModified' : 'modified'
					},
					type = map[event.type];
				if ( type === 'modified' ) {
					if ( $.mutator.attributeFilter.indexOf(event.attrName) > -1 ) {
						$.mutator.handle.modified(event.target);
					}
				} else {
					$.mutator.handle[type](event.target);
				}
			},
			mutationobserver: function(mutations) {
				$.each(mutations, function(index, mutation) {
					if (mutation.type === 'attributes') {
						$.mutator.handle.modified(mutation.target);
					} else {
						$.each(['added', 'removed'], function(i, type) {
							$.each(mutation[type + 'Nodes'], function(i, element) {
								$.mutator.handle[type](element);
								$.mutator.findInSubtree(element, type);
							});
						});
					}
				});
			}
		},
		findInSubtree: function(element, type) {
			// Search subtree
			var queries = $.mutator.queries;
			var len = queries.length;
			var query;
			for( var i = 0; i < len; i++ ) {
				query = queries[ i ];
				var objSubtreeElements = $(query.selector, element);
				if( !!objSubtreeElements && objSubtreeElements.length ) {
					var el, matches = objSubtreeElements.toArray();
					while ((el = matches.shift()) !== undefined) {
						$.mutator.handle[type](el);
					}
				}
			}
		},
		find: function(jq, selector, matchedFn, unmatchedFn) {
			var q;
			$.each( $.mutator.queries, function(i, query) {
				if ( selector === query.selector && jq === query.jq &&
					(!matchedFn || matchedFn.$lqguid === query.matchedFn.$lqguid) &&
					(!unmatchedFn || unmatchedFn.$lqguid === query.unmatchedFn.$lqguid) ) {
					return (q = query) && false;
				}
			});
			return q;
		},
		findorcreate: function(jq, selector, matchedFn, unmatchedFn) {
			return $.mutator.find(jq, selector, matchedFn, unmatchedFn) ||
				new $.mutator(jq, selector, matchedFn, unmatchedFn);
		}
	});

	$(function() {
		if ('MutationObserver' in window) {
			$.mutator.method = 'mutationobserver';
		} else if ('MutationEvent' in window) {
			$.mutator.method = 'mutationevent';
		} else if ('behavior' in document.documentElement.currentStyle) {
			$.mutator.method = 'iebehaviors';
		}

		if ($.mutator.method) {
			$.mutator.prepare[$.mutator.method]();
		} else {
			throw new Error('Could not find a means to monitor the DOM');
		}
	});

}));