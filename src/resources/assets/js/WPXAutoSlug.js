/**
 * WPXAutoSlug
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXAutoSlug',{
    value: (function(_super){
        var $;

        // Extend this component to sub-class WPXAutoSlug
        WPX.___extends(WPXAutoSlug, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXAutoSlug
         */
        function WPXAutoSlug(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;

            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXAutoSlug.prototype.init = function() {
            if( $ === undefined ) return false;
            
            var self        = this;
            var el          = self.dom();
            var target      = $( el.data('autoslugtarget') );
            var container   = target.closest('.wpx-slugurlpreviewcontainer');
            var link        = $('.wpx-slugurlpreview__link', container);
            var basehref    = link.data('basehref') || null;

            // Add a clickable link after the slug field
            // target.after('<a href="https://www.google.ca">http://www.google.ca</a>');
            // target.addClass('wpx-slugurlpreview');

            if( !!target.length ) {
                var fnInput = function() {
                    var slug = self.slugify(el.val());
                    var href = basehref + slug;
                    el.data('dirty', true);
                    
                    if( !!link.length ) {
                        $('span', link).text( href );
                        link.attr('href', href);
                    }
                };

                el.on('input', fnInput);
                el.on('blur', function(){                    
                    var val = el.val().trim();
                    el.val(val);
                    if( el.data('dirty') && confirm("Update slug?") ) {
                        var slug = self.slugify(val);
                        target.val( slug );
                        el.data('dirty', false);
                    }
                });
                
                target.on('input', function(){
                    var slug = self.slugify( target.val());
                    var href = basehref + slug;
                    if( !!link.length ) {
                        $('span', link).text( href );
                        link.attr('href', href);
                    }
                });

                if( !!el.val() && !target.val() ) {
                    el.trigger('input');
                }

                if( !target.val() ) {
                    target.trigger('input');
                }
            }
        }

        // ---------------------------------------------------------- //
        // helpers
        // ---------------------------------------------------------- //
        /**
         * https://gist.githubusercontent.com/hagemann/382adfc57adbd5af078dc93feef01fe1/raw/c26ff1b9601327270162a6f1a4ca81726418b066/slugify.js
         */
        WPXAutoSlug.prototype.slugify = function(string) {
            var a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
            var b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
            var p = new RegExp(a.split('').join('|'), 'g')
            
            return string.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(p, function (c) {
                    return b.charAt(a.indexOf(c));
                }) // Replace special characters
                .replace(/&/g, '-and-') // Replace & with 'and'
                .replace(/[^\w\-]+/g, '') // Remove all non-word characters
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, '') // Trim - from end of text
        }

        return WPXAutoSlug;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});