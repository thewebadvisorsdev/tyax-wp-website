Object.defineProperty(WPX,'WPXTable',{
    value: (function(_super){
        var $;
        var tsorter;

		// Extend this component to sub-class UIComponent
		WPX.___extends(WPXTable, _super);
		
		/**
		 * Constructor
		 * 
		 * @param 	{object}	obj 	jQuery element instance
		 * @param 	{object}	jquery 	jQuery
		 * @returns {WPXTable}
		 */
		function WPXTable(obj, jquery, libtsorter) {
			var self = _super.call(this, obj) || this;
            
            $ = jquery;
            tsorter = libtsorter;
            self.relation_details;
            self.sortable;
            self.api_format;
            self.collection_name;
            self.search_datalist;
            self.reloadendpoint;
            self.search_fields = [];
			self.init();

			return self;
		};
		
		/**
		 * Initialize the component once it's bound to a DOM element
		 */
        WPXTable.prototype.init = function() {
			if( $ === undefined ) return false;
			
			var self                = this;
            var el                  = self.dom();
            var id                  = el.attr('id');
            
            self.api_format         = el.data('restinfo');
            self.collection_name    = el.data('collection');
            self.reloadendpoint     = el.data('reloadendpoint');
            self.relation_details   = el.data('relationdetails');

            // Event hooks
            WPX.EventDispatcher.subscribe('onRowContextMenuClick', self);
            WPX.EventDispatcher.subscribe('onUpdateTableRows', self);
            WPX.EventDispatcher.subscribe('onResetTableRows', self);
            // WPX.EventDispatcher.subscribe('onEntityRelationUpdated', self);

            // Sorting
            if( undefined !== tsorter ) {                
                if( !id ) {
                    id = el.attr('id', 'WPX_Table');
                }
                self.sortable = tsorter.create(id);
            }

            // Arranging
            if( undefined !== $.fn.arrangeable ) {
                if( el.is('.wpx-table_draggable')) {
                    $('.wpx_tablerow', el).arrangeable();

                    el.on('drag.end.arrangeable', function(table, row){
                        self.updateSortOrder(row);
                    });
                }
            }

            // Search
            if( $('.wpx-table__search',el ).length ) {
                var search_fields = $('.wpx-search',el );

                if( search_fields.length ) {
                    self.initTableFields( search_fields );
                }
            }

            // Full row click
            el.on('click', '.wpx_tablerow:not(.wpx_tablerow_container)', function(e){

                var obj     = $( this );
                var link    = $('.wpx_tablerow__link', obj);
                var href    = link.attr('href');
                var target  = $(e.target);

                // if( !!(target.is('.wpx-tableactions') || target.closest('.wpx-tableactions').length) ) {
                //     var action  = target.data('action');
                //     var pk      = self.relation_details.entity_primarykey;
                //     var json    = [];
                //     json[pk]    = obj.data('id');
                //     json['relation_details'] = self.relation_details ;
                //     json = $.extend({},json);

                //     var payload     = $.extend({}, self.api_format.request_format, {
                //         data : JSON.stringify(json),
                //         _action_ : action
                //     });

                //     WPX.EventDispatcher.dispatch('RowContextMenuClick',{ table: el, row: obj, el: obj, url: href, action: action, data: payload, relation_details: self.relation_details });
                //     return false;
                // }

                // // Menu Actions
                // if( !!(target.is('.wpx-tableactions') || target.closest('.wpx-tableactions').length) ) {
                //     var action = target.data('action');
                //     var payload     = $.extend({}, self.api_format.request_format, {
                //         data : JSON.stringify({ 'id': obj.data('id'), 'relation_details' : self.relation_details }),
                //         _action_ : action
                //     });
                //     WPX.EventDispatcher.dispatch('RowContextMenuClick',{ table: el, row: obj, el: obj, url: href, action: action, data: payload, relation_details: self.relation_details });
                //     return false;
                // }

                // Menu Actions
                if( !!(target.is('.wpx-tableactions') || !!target.closest('.wpx-tableactions').length) ) {
                    var action  = target.data('action');
                    console.log('target', obj.data());
                    
                    var pk      = ( !!self.relation_details ) ? self.relation_details.entity_primarykey : 'id';
                    var json    = [];
                    json[pk]    = obj.data('id');
                    json['relation_details'] = self.relation_details || null;
                    json = $.extend({},json);

                    var payload     = $.extend({}, self.api_format.request_format, {
                        data : JSON.stringify(json),
                        _action_ : action
                    });

                    WPX.EventDispatcher.dispatch('RowContextMenuClick',{ table: el, row: obj, el: obj, url: href, action: action, data: payload, relation_details: self.relation_details });
                    return false;
                }
                
                
                
                if( !(target.is('.wpx_tablerow__relationlink') && (!target.is('.wpx-tableactions') || !target.closest('.wpx-tableactions').length)) ) {
                    window.location.href = href;
                    return false;
                }
                
                return false;
            });

            // Card click
            el.on('click', '.wpx-paper', function(e){

                var obj     = $( this );
                var link    = $('.wpx_tablerow__link', obj);
                var href    = link.attr('href');
                var target  = $(e.target);

                // Menu Actions
                if( !!(target.closest('.wpx-cardheader-action').length) ) {
                    var action  = target.data('action');
                    var pk      = self.relation_details.entity_primarykey;
                    var json    = [];
                    json[pk]    = obj.data('id');
                    json['relation_details'] = self.relation_details ;
                    json = $.extend({},json);

                    var payload     = $.extend({}, self.api_format.request_format, {
                        data : JSON.stringify(json),
                        _action_ : action
                    });
                    
                    WPX.EventDispatcher.dispatch('RowContextMenuClick',{ table: el, row: obj, el: obj, url: href, action: action, data: payload, relation_details: self.relation_details });
                    return false;
                }
                
                if( target.is('.wpx_tablerow__link') ) {
                    window.location.href = href;
                }
                
                return false;
            });            

        }

        // ---------------------------------------------------------- //
        // SORT ORDER
        // ---------------------------------------------------------- //
        WPXTable.prototype.updateSortOrder = function( row ){
            var self = this;
            var el = self.dom();
            
            if( !self.api_format || !self.api_format.request_format ) {
                throw "Request format not set.";
            }

            var payload = $.extend({}, self.api_format.request_format);

            // Gather newly sorted
            var sorted = [];
            $('tr[data-id]',el).map(function(i,v){
                var r       = $( v );
                var id      = r.data('id');
                var order   = (i + 1);

                if( !!id ) {
                    sorted.push({
                        'id': id,
                        'order': order,
                    });
                    r.attr('data-order', order);
                }
            });

            // Update
            delete payload.id;
            payload.data    = JSON.stringify(sorted);
            payload._action_  = 'sort';
            self.doActionSort(payload);
        }

        // ---------------------------------------------------------- //
        // CONTEXT MENU
        // ---------------------------------------------------------- //
        WPXTable.prototype.onRowContextMenuClick = function(data) {
            var self = this;

            if( !data ) return false;

            switch( data.action ) {
                case 'edit' :
                    window.location.href = data.url;
                    break;
                case 'unlink' :
                    if (window.confirm("Unlink this item? Are you sure?")) {
                        self.doContextAction( data );
                    }
                    break;
                case 'delete' :
                    if (window.confirm("This is a permanent action. Do you wish to continue?")) {
                        self.doContextAction( data );
                    }
                    break;
            }
        }

        WPXTable.prototype.doContextAction = function( context ) {
            var self = this;
            var endpoint = self.getEndpoint([context.action]);
            if( !endpoint ) {
                throw "Endpoint not defined.";
            }

            console.log('context', context);
            

            var table   = context.table;
            var row     = context.row;
            var payload = context.data;
            var fnFail  = function(response){
                try {
                    console.error('Uh oh, looks like something went wrong.');
                    if( !!response && !!(errors in response) ) {
                        console.warn( response.errors );
                    }
                }catch( err ){
                    // Do nothing, but mak
                }
                
                WPX.EventDispatcher.dispatch('BlockUI', {active: false});
            };
            
            WPX.EventDispatcher.dispatch('BlockUI', {active: true});

            $.postJSON( endpoint, payload ).done( function(response){
                if( ('success' in response) && !!response.success ) {
                    row.remove();
                    self.updateCount(table);                    
                }else{
                    fnFail(response);
                }
            } ).fail( fnFail ).always(function(){
                WPX.EventDispatcher.dispatch('BlockUI', {state: false});
            });
        }
        
        WPXTable.prototype.doActionSort = function( payload ) {
            var self = this;
            var endpoint = self.getEndpoint();
            if( !endpoint ) throw "Endpoint not defined.";
            if( null == payload ) throw "Invalid payload.";
            
            var fnFail = function(response){
                console.error('Uh oh, looks like something went wrong.');
                console.log( response.errors );
                WPX.EventDispatcher.dispatch('BlockUI', {active: false});
            };
            
            WPX.EventDispatcher.dispatch('BlockUI', {active: true});

            $.postJSON( endpoint, payload ).done( function(response){
                if( !!response && ('success' in response) && !!response.success ) {
                    WPX.EventDispatcher.dispatch('WPXTableRowsSorted', {instance:self});
                }else{
                    fnFail(response);
                }
            } ).fail( fnFail ).always(function(){
                WPX.EventDispatcher.dispatch('BlockUI', {state: false});
            });
        }

        // ---------------------------------------------------------- //
        // HELPER METHODS
        // ---------------------------------------------------------- //
        WPXTable.prototype.updateCount = function(context) {
            // Update record counter
            var el          = context;
            var rows        = $('tbody tr:visible', el).length;
            var counters    = $('.wpx-tablecount', el);
            var lbl         = !!rows ? `1-${rows} of ${rows}` : '--';
            
            counters.text(lbl);
        }

        /**
         * Generates a REST endpoint
         */
        WPXTable.prototype.getEndpoint = function( params ) {
            var self = this;
            var extra_params = null;
            if( !self.api_format || !self.api_format.endpoint ) {
                throw "REST endpoint not defined.";
            }
            if( !self.collection_name ) {
                throw "Collection name not defined.";
            }
            
            var endpoint = self.api_format.endpoint + '/' + self.collection_name;
            if( !!params && Array.isArray(params) ) {
                extra_params = params.join('/');
                endpoint = endpoint +  '/' + extra_params;
            }

            return endpoint;
        }

        // ---------------------------------------------------------- //
        // SEARCH FIELD
        // ---------------------------------------------------------- //
        /**
         * Create a new search filter instance.
         * 
         * @param array fields A collection of search field elements
         */
        WPXTable.prototype.initTableFields = function( fields ) {
            var self = this;
            var el = self.dom();
            
            if( !fields.length ) return false;

            fields.map(function(i, val){
                var field   = $( val );
                var new_id;
                var id      = field.attr('id');
                var mode    = field.data('mode');

                if( !id ) {
                    new_id = [];
                    new_id.push(el.attr('id'));
                    new_id.push('Field');
                    new_id.push(mode);
                    new_id = new_id.join('_');
                    field.attr('id', new_id);
                }

                self.search_fields[ id ? id : new_id ] = new WPX.WPXSearchFilterField(field, el, mode, $);                
            });            
        }

        // ---------------------------------------------------------- //
        // EVENT HANDLERS
        // ---------------------------------------------------------- //
        WPXTable.prototype.onUpdateTableRows = function( event ) {
            var self    = this;
            var el      = event.table;
            var rows    = $('tbody tr', el);
            var row     = event.row;
            rows.addClass('hidden');
            row.removeClass('hidden');
            self.updateCount( el );
        }

        WPXTable.prototype.onResetTableRows = function( event ) {
            var self    = this;
            var el      = event.table
            var rows = $('tbody tr', el);
            rows.removeClass('hidden');
            self.updateCount( el );            
        }

        WPXTable.prototype.onEntityRelationUpdated = function( event ) {
			var self     = this;
            var el       = self.dom();
            var id       = el.attr('id');
            var table_id = event.id;
            var endpoint = self.reloadendpoint;
            if( id == table_id && !!endpoint ) {
                var payload = $.extend({}, self.api_format.request_format);
                payload.data = JSON.stringify({id:event.ownerid,relation:self.collection_name});
                payload._action_ = 'relations';
                // console.group('onEntityRelationUpdated');
                // console.log('event', event);
                // console.log('table_id', table_id);
                // console.log('id', id);
                // console.log('endpoint', endpoint);
                // console.log('payload', payload);
                // console.groupEnd();

                var fnFail = function(response){
                    console.error('Uh oh, looks like something went wrong.');
                    console.warn( 'Additional error information: ' + self.errors[ response.responseJSON.code ] );
                    WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                };

                WPX.EventDispatcher.dispatch('BlockUI', {active: true});

                $.postJSON(endpoint, payload).done(function(response){
                    if( ('success' in response) && !!response.success ) {
                        console.log('response', response);
                    }else{
                        fnFail(response);
                    }
                }).fail(fnFail).always(function(){
                    WPX.EventDispatcher.dispatch('BlockUI', {state: false});
                });                

            }
        }
        

		return WPXTable;

    })(WPX.UIComponent),
		enumerable: true,
		configurable: true,
		writable: true
	});