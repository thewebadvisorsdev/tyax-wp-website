Object.defineProperty(WPX,'App',{
	value: (function(WPX){
		'use strict';

		var instance;
        var $;
        var ___registryinstance___;
        var ___registry___ = {};

        // MCE PLUGINS + TOOLBARS
        var mce_plugins = [
            'charmap', 'colorpicker', 'compat3x', 'directionality', 'fullscreen', 'hr', 'image', 'lists', 'media', 'paste', 'tabfocus', 'textcolor', 'wordpress', 'wpautoresize', 'wpdialogs', 'wpeditimage', 'wpemoji', 'wpgallery', 'wplink', 'wptextpattern', 'wpview'
        ];

        var mce_toolbars = [
            'formatselect bold italic',
            'bullist numlist',
            'blockquote',
            'alignleft aligncenter alignright',
            'link unlink',
            'spellchecker'
        ];        

        /**
         * @Class Registry
         * 
         * A simple registry singleton to access a private store.
         */
        function Registry() {};

        Object.defineProperty(Registry.prototype, "___collection___", {
            get: function () {
                return ___registry___;
            },
            // set: function (val) {
            //     ___registry___ = val;
            // },
            enumerable: true,
            configurable: true
        });

        /**
         * Get the value of a property from the registry
         * 
         * @param {string} key  The property name
         */
        Registry.prototype.get = function(key) {
            return ___registry___[ key ];
        }

        /**
         * Set a property in the registry
         * 
         * @param {string}  key     The property name
         * @param {mixed}   value   The property value
         */
        Registry.prototype.set = function(key,value) {
            return ___registry___[ key ] = value;
        }
		
		function App(){
			
			this.dispatcher;
			$ = null;
			this.transitionEvent = this.whichTransitionEvent();
			this.scrollbarWidth = null;

			this.init();
		};

		/**
		 * Initialize this module.
		 * 
		 * Add additional Initialization logic to run **before** DOMReady is executed.
		 * Note: jQuery and other libraries may not be available at this point, and it's 
		 * possible that the DOM is not completely available yet.
		 */
		App.prototype.init = function() {
			if( !!WPX && 'EventDispatcher' in WPX ) {
				WPX.EventDispatcher.subscribe('onReady',this);
			}
        }
        
        /**
         * Get a reference to the App registry
         */
        Object.defineProperty(App.prototype, "registry", {
            get: function () {
                if( !___registryinstance___ ) {
                    ___registryinstance___ = new Registry();
                }
                return ___registryinstance___;
            },
            enumerable: true,
            configurable: true
        }); 

		/**
		 * Event handler for `Ready` event when jQuery or other `DOMReady` handlers are fired.
		 * 
		 * @param	{mixed}	Accepts arbitrary number of parameters.
		 */
		App.prototype.onReady = function(jquery) {
			var self = this;
            $ = jquery;

            // Call additional initialization here.
			// console.log( 'App::init()', $);
			self.scrollbarWidth = self.getScrollbarWidth();
		}

		/**
		 * Sets a local reference to the WPX EventDispatcher instance for convenience,
		 * and to avoid having to wrap dispatcher calls in additional safety checks.
		 * 
		 * @param	{EventDispatcher}	d	An instance of the WPX.EventDispatcher object.
		 */
		App.prototype.setDispatcher = function( d ) {
			this.dispatcher = d;
		}

		/**
		 * Determine the transition event that is supported by the current browser.
		 * 
		 * @returns	string	The name of the supported transition event.
		 */
		App.prototype.whichTransitionEvent = function () {
			var t;
			var el = document.createElement("fakeelement");

			var transitions = {
				"transition": "transitionend",
				"OTransition": "oTransitionEnd",
				"MozTransition": "transitionend",
				"WebkitTransition": "webkitTransitionEnd"
			}

			for (t in transitions) {
				if (el.style[t] !== undefined) {
					return transitions[t];
				}
			}
        }
        
        App.prototype.getMCEPlugins = function() {
            return mce_plugins;
        }

        App.prototype.getMCEToolbars = function() {
            return mce_toolbars;
        }

		/**
		 * Get the direction of the mouse while moving over an element.
		 * 
		 * @link https://tympanus.net/codrops/2012/04/09/direction-aware-hover-effect-with-css3-and-jquery/
		 * 
		 * @param {string}		ev	The mouse event
		 * @param {HTMLElement}	obj	the DOM Element.
		 */
		App.prototype.getMouseDirection = function(event, el) {
			
			var coordinates = {x: event.pageX, y: event.pageY};
			// the width and height of the current div
			var w = el.width(),
			h = el.height(),
			// calculate the x and y to get an angle to the center of the div from that x and y.
			// gets the x value relative to the center of the DIV and "normalize" it
			x = (coordinates.x - el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
			y = (coordinates.y - el.offset().top - (h / 2)) * (h > w ? (w / h) : 1),
			// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
			// first calculate the angle of the point,
			// add 180 deg to get rid of the negative values
			// divide by 90 to get the quadrant
			// add 3 and do a modulo by 4 to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
			direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

			return direction;
        }
        
        /**
         * Get the text of a given dom node.
         * 
         * @param Element An DOM element node to search for text content
         * @returns string Returns the plain-text contents of the supplied node.
         */
        App.prototype.text = function(html) {
            var tmp = document.implementation.createHTMLDocument("New").body;
            tmp.innerHTML = html.innerHTML;
            return (tmp.textContent || tmp.innerText || "").trim();
        }

		/**
		 * Memory-safe arguments to array.
		 * 
		 * @param {arguments}	The original arguments list
		 * @returns {array}	An array comprising all arguments in the original list
		 */
		App.prototype.func_get_args = function() {
			var args = new Array(arguments[0].length);
			for(var i = 0; i < args.length; ++i) {
				args[i] = arguments[0][i];
			}
			
			return args;
		}

		App.prototype.get_url_param = function(sParam) {
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			for (var i = 0; i < sURLVariables.length; i++) 
			{
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] == sParam) 
				{
					return decodeURIComponent(sParameterName[1]).replace(/\+/g, ' ');
				}
			}
			return null;			
		}

		// Singleton
		if( !instance ) {
			instance = new App();
		}

		return instance;

	})(window['WPX'] || function(){}),
	configurable: false
});

(function($){
    // Browser polyfill for IE11
    $.browser = {};
    $.browser.msie = false;
    $.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        $.browser.msie = true;
        $.browser.version = RegExp.$1;
    }

    // Verb: DELETE
    $.delete = function(url, data, callback, type){

        if ( $.isFunction(data) ){
          type = type || callback,
          callback = data,
          data = {}
        }
      
        return $.ajax({
          url: url,
          type: 'DELETE',
          success: callback,
          contentType: 'application/json',
          data: data,
          contentType: type
        });
        
    }

    // Verb: PUT
    $.put = function(url, data, callback, type){

        if ( $.isFunction(data) ){
          type = type || callback,
          callback = data,
          data = {}
        }
      
        return $.ajax({
          url: url,
          type: 'PUT',
          success: callback,
          contentType: 'application/json',
          data: data,
          contentType: type
        });
    }

    $.postJSON = function(url, data, callback, type){

        if ( $.isFunction(data) ){
          type = type || callback,
          callback = data,
          data = {}
        }
      
        return $.ajax({   
            url:        url,
            data:       data,
            method:     'post',
            dataType:   'json',  
            accepts:    {
                json: "application/json, text/javascript"
            }
        });
    }
})(jQuery);

/**
 * Module to encapsulate Entwine DOM hooks.
 */
(function($, WPX, Choices){
    var wp      = window['wp'];
    var moment  = window['moment'];
    var tsorter = window['tsorter'];

    WPX.App.registry.set('DATE_FORMAT', 'YYYY-MM-DD');
    WPX.App.registry.set('uploaders', []);
    WPX.App.registry.set('imgcollections', []);
    WPX.App.registry.set('wpxtables', []);
    

    // WORDPRESS RICH TEXT EDITOR
    $('.wpx-editor').entwine({
        onmatch: function() {
            var id = this.attr('id');
            var mce_plugins = WPX.App.getMCEPlugins();
            var mce_toolbars = WPX.App.getMCEToolbars();
            setTimeout(function(){
                wp.editor.initialize( id, {
                    tinymce: {
                        wpautop: true,
                        plugins: mce_plugins.join(' '),
                        toolbar1: mce_toolbars.join(' | '),
                        setup: function (editor) {
                            editor.on('change', function () {
                                editor.save();
                            });
                        }
                    },
                    quicktags: true
                });
            },1000);

        }
    });

    // WPX UPLOADER
    $('.wpx-mediaupload').each(function() {
        var uploaders = WPX.App.registry.get('uploaders');
        var el = $(this);
        uploaders.push( new WPX.WPXUploader(el,$) );
    });
    
    // WPX IMAGE COLLECTION
    $('.wpx-addmedia').entwine({
        onmatch: function() {
            var collection = WPX.App.registry.get('imgcollections');
            var el = this;
            collection.push( new WPX.WPXImageCollection(el,$) );
        }
    });

    // WPX TABLE
    $('.wpx-table.wpx-table_interactive').entwine({
        onmatch: function() {
            var wpxtables   = WPX.App.registry.get('wpxtables');
            var obj       = this;
            var objsorter = new WPX.WPXTable(obj,$,window['tsorter']);
            wpxtables.push( objsorter );            
        }
    });

    // WPX TAB CONTROLS
    $('.wpx-navtabs').entwine({
        onmatch: function() {
            new WPX.WPXTabControls(this,$);
        }
    });

    // WPX UI BLOCKER
    $('#wpx-uicomponents').entwine({
        onmatch: function() {
            var template    = $(this.html());
            var blocker     = $('.wpx-blocker', template);

            if( blocker.length ) {
                new WPX.WPXBlocker(blocker, $);
            }        
        }
    });

    // WPX TAB CONTROLS
    $('.wpx-datepicker').entwine({
        onmatch: function() {
            var el          = this;
            var date_format =  WPX.App.registry.get('DATE_FORMAT');

            el.daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: date_format
                },
                showDropdowns: true,
                minYear: parseInt( moment().subtract(3,'year').format('YYYY'),10),
                maxYear: parseInt( moment().add(2,'year').format('YYYY'),10)
            }, function(d) {
                el.val( d.format( date_format ) );
            });
        }
    });

    // WPX AUTOFILL
    $('.wpx-autofill:not(.wpx-autofill_trigger)').entwine({
        onmatch: function() {
            var el      = this;
            var hash    = el.data('autofill'); // A unique hash to use as an ID
            var target  = $('.wpx-autofill_trigger.wpx-autofill-' + hash);
            
            if( target.length ) {
                target.on('change', function(){
                    if( target.is("select") ) {
                        var selected = $('option:selected', target);
                        el.prop('value',selected.text() || null);
                    }else{
                        el.prop('value', target.val());
                    }
                });

                if( !el.val() ) {
                    target.trigger('change');
                }
            }
        }
    });
    
    // WPX FORMS
    $('form.wpx-form').entwine({
        onmatch: function() {
            var el = this;
            var form = new WPX.WPXForm(el, $);
        }
    });

    // Choices
    $('.wpx-choices').entwine({
        onmatch: function() {
            var el = this;
            el.attr('multiple', el.hasClass('wpx-multiple'));
            new Choices($(this)[0],{
                removeItemButton: true,
                paste: false,
                placeholder: el.data('placeholder') || 'Select'
            });
        }
    });

    // Slugs
    $('input[type="text"].wpx-autoslug').entwine({
        onmatch: function() {
            var el = this;
            var autoslug = new WPX.WPXAutoSlug( el, $ );
        }
    });

    // Slugs
    $('input[type="checkbox"].wpx-booleancheckbox').entwine({
        onmatch: function() {
            var el = this;
            el.data('stateval', el.val());
            el.on('change',function(e) {
                console.log('checked', el.is(':checked'));
                
                el.val( el.is(':checked') ? 1 : 0 );
            });
        }
    });

    // Import Utils
    $('.wpx-importutils__field').entwine({
        onmatch: function() {
            var el = this;
            var utils = new WPX.WPXImportUtils( el, $ );
        }
    });

    // Geo Complete
    $('input[type="text"].wpx-geocomplete').entwine({
        onmatch: function() {
            var el = this;
            var geocomplete = new WPX.WPXGeoComplete( el, $ );
        }
    });
    
    
})(jQuery, WPX);