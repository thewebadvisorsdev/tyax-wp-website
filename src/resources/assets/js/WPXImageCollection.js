Object.defineProperty(WPX,'WPXImageCollection',{
    value: (function(_super){
		var $;
		var wpwpmediauploader;

		// Extend this component to sub-class UIComponent
		WPX.___extends(WPXImageCollection, _super);
		
		/**
		 * Constructor
		 * 
		 * @param 	{object}	obj 	jQuery element instance
		 * @param 	{object}	jquery 	jQuery
		 * @returns {WPXImageCollection}
		 */
		function WPXImageCollection(obj, jquery) {
            var self = _super.call(this, obj) || this;
			
            $ = jquery;
            self.panelPreview;
            self.panelUpload;
            self.previewImage;

            self.fieldMetaData;
            self.fieldMediaID;

            self.mediaAsset;
            self.mediaAttachment;

			self.state('active', false);
			self.init();

			return self;
		};
		
		/**
		 * Initialize the component once it's bound to a DOM element
		 */
        WPXImageCollection.prototype.init = function() {
            
            if( $ === undefined ) return false;
			
			var self = this;
            var el = self.dom();

            // Initialize the WP Media Uploader
            wpmediauploader = wp.media.frames.file_frame = wp.media({
                title: 'Select Images',
                button: {
                    text: 'Select Image'
                }, multiple: true 
            });

            // Media Uploader event handler
            wpmediauploader.on('select', function() {
                var self = window.__current_uploader__;
                var selection =  wpmediauploader.state().get('selection');
                self.add( selection );
            });

            el.on('click', function(e){
                if( wpmediauploader ) {
                    window.__current_uploader__ = self;
                    wpmediauploader.open();
                }
                return false;
            });         

        }

        WPXImageCollection.prototype.add = function( collection ) {
            var self = this;
            var el = self.dom()
            var container = el.closest('.wpx-tableactions-container');
            var config      = container.data();
            var endpoint    = container.attr('action');
            var request;

            if( null == collection ) return false;
            
            var currentrelation = config.currentrelation;
            
            var fk              = currentrelation.foreign_key;
            var lk              = currentrelation.local_key;
            var pk              = currentrelation.entity_primarykey;
            var endpoint        = config.endpoint;
            var payload         = $.extend({}, config.format);
            var ownerid         = config.ownerid;
            
            var wpimages        = {
                'collection' : []
            };
            collection.map( function( attachment ) {
                attachment = attachment.toJSON();

                var objData             = {};
                objData['object_id']    = ownerid;
                objData[fk]             = parseInt(attachment.id, 10);
                wpimages.collection.push( objData );
            });                

            payload.data        = JSON.stringify(wpimages);
            payload._action_    = 'update';
            payload._relation_  = config.currentrelation;

            self.post(endpoint, payload);
            
        }

        WPXImageCollection.prototype.post = function(endpoint, payload) {

            // Create relationship
            var fnFail  = function(response, textStatus, jqXHR){
                try {
                    if( jqXHR == 'Unprocessable Entity' ) {
                        console.warn('No results found...');
                    }else{
                        console.error('Uh oh, looks like something went wrong.');
                    }

                    WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                }catch( err ){
                    // Do nothing
                }
            };
    
            WPX.EventDispatcher.dispatch('BlockUI', {active: true});
                    
            request = $.postJSON( endpoint, payload ).done( function(response, textStatus, jqXHR){
                
                if( jqXHR.status == 200 && ( !!response && 'success' in response) && !!response.success ) {
                    // console.success('Added new relations for these entities.');
                    WPX.EventDispatcher.dispatch('EntityRelationUpdated', {contxt:self});
                }else{
                    fnFail(response, textStatus, jqXHR);
                }

            } ).fail( fnFail ).always(function(){
                WPX.EventDispatcher.dispatch('BlockUI', {state: false});
            });       
        }
        
		return WPXImageCollection;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});