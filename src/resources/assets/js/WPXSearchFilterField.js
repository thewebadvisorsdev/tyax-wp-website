/**
 * WPXSearchFilterField
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXSearchFilterField',{
    value: (function(_super){
        let $;

        // Extend this component to sub-class WPXSearchFilterField
        WPX.___extends(WPXSearchFilterField, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXSearchFilterField
         */
        function WPXSearchFilterField(obj, table, mode, jquery) {
            let self = _super.call(this, obj) || this;
            
            $                           = jquery;
            self.mode                   = mode;
            self.table                  = table;
            self.api_format             = obj.data('restinfo');
            self.autocomplete_selected  = null;
            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXSearchFilterField.prototype.init = function() {
            if( $ === undefined ) return false;
            
            let self = this;
            let el = self.dom();

            switch( self.mode ) {
                case 'filter' :
                    self.initFilterField();
                    break;
                case 'search' :
                    self.initSearchField();
                    break;
            }

            el.data('WPXComponent', self);
        }

        /**
         * Initialize a filter field.
         * 
         * Filters rows in the associated table to quickly narrow down a single entry.
         */
        WPXSearchFilterField.prototype.initFilterField = function() {
            var self    = this;
            var el      = self.dom();
            var field   = $('input[type="text"]', el);

            var rows = $('tbody tr', self.table);
            var datalist = $('<datalist />',{
                id: el.attr('id') + '_datalist', 
            });

            // Iterate over the rows to build a keyword list
            rows.map(function(i,v){
                var row      = $( v );
                var td       = $('td:not(.wpx-tableactions)', row);
                var keywords = [];
                
                td.map(function(n,val){
                    var txt = WPX.App.text( val );
                    if(n == 0) {
                        var option = $('<option>' + txt + '</option>');
                        option.data('row', row);
                        datalist.append(option);
                    }
                    keywords.push(txt);
                    return txt;
                });
                
                row.attr('data-keywords', keywords.join(','));
                row.data('keywords', keywords);                
            });

            field.attr('list', datalist.attr('id') );
            datalist.insertAfter(field);

            // Bind selection
            field.on('input', function(e){
                var f           = $( this );                
                var selection   = f.val();
                var row;
                if( '' !== selection.trim() ) {
                    row = $('option',datalist).filter(function(){
                        return this.value.toLowerCase() == selection.toLowerCase();
                    });
    
                    if( !!row && row.length ) {
                        objRow = row.data('row');
                        WPX.EventDispatcher.dispatch('UpdateTableRows',{ row: objRow, context: self.table });
                    }
                }else{
                    WPX.EventDispatcher.dispatch('ResetTableRows',{ context: self.table });
                }
            });
        }

        WPXSearchFilterField.prototype.initSearchField = function() {
            var self        = this;
            var el          = self.dom();
            var field       = $('input[type="text"]', el);
            var btn         = $('.wpx-search__button',el);
            var config      = el.data();
            var endpoint    = el.attr('action');
            var request;

            var datalist = $('<datalist />',{
                id: el.attr('id') + '_datalist', 
            });

            field.attr('list', datalist.attr('id') );
            datalist.insertAfter(field);

            // Bind submit button
            btn.on('click', function(e){
                
                if( null === self.autocomplete_selected ) return false;
                var selection = self.autocomplete_selected.data('entity');

                if( null == selection ) return false;
                
                var currentrelation = el.data('currentrelation');
                
                var fk              = currentrelation.foreign_key;
                var lk              = currentrelation.local_key;
                var pk              = currentrelation.entity_primarykey;
                var endpoint        = el.data('endpoint');
                var payload         = $.extend({}, el.data('format'));
                var ownerid         = el.data('ownerid');
                
                var objData         = {};

                if( currentrelation.relation_type == 'HasManyThrough') {
                    // Swap foreign key with local key for HasManyThrough relations
                    // To save a HasManyThrough entity from either side of the relation.
                    if( fk == 'object_id' ) {
                        objData['object_id']    = parseInt(selection.entity[pk], 10);
                        objData[lk]             = ownerid;
                    }else{
                        objData['object_id']    = ownerid;
                        objData[fk]             = parseInt(selection.entity[pk], 10);
                    }
                }else{
                    switch( currentrelation.relation_type ) {
                        case 'HasMany' :
                        case 'HasOne' :
                            objData[pk]         = parseInt(selection.entity[pk], 10);
                            objData[fk]         = ownerid;
                            break;
                        case 'BelongsTo' :
                            objData[pk]         = ownerid;
                            objData[lk]         = parseInt(selection.entity[pk], 10);
                            break;

                    }
                }

                payload.data        = JSON.stringify(objData);
                payload._action_    = 'update';
                payload._relation_  = config.currentrelation;


                // Create relationship
                var fnFail  = function(response, textStatus, jqXHR){
                    try {
                        if( jqXHR == 'Unprocessable Entity' ) {
                            console.warn('No results found...');
                        }else{
                            console.error('Uh oh, looks like something went wrong.');
                        }

                        WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                    }catch( err ){
                        // Do nothing
                    }
                };
        
                WPX.EventDispatcher.dispatch('BlockUI', {active: true});
                        
                request = $.postJSON( endpoint, payload ).done( function(response, textStatus, jqXHR){
                    
                    // console.log('response', response);
                    
                    if( jqXHR.status == 200 && ( !!response && 'success' in response) && !!response.success ) {
                        // console.success('Added new relation for this entity.');
                        WPX.EventDispatcher.dispatch('EntityRelationUpdated', {id:self.table.attr('id'),ownerid:ownerid});
                        // window.location.reload();
                    }else{
                        fnFail(response, textStatus, jqXHR);
                    }

                } ).fail( fnFail ).always(function(){
                    WPX.EventDispatcher.dispatch('BlockUI', {state: false});
                });                   
                
                return false;
            });

            el.on('submit', function(e){
                e.stopPropagation();
                e.preventDefault();

                var terms = JSON.stringify({'terms':field.val()});
                var data = $.extend({}, config.format, {'data': terms});

                var fnFail  = function(response, textStatus, jqXHR){
                    try {
                        if( jqXHR == 'Unprocessable Entity' ) {
                            console.warn('No results found...');
                        }else{
                            console.error('Uh oh, looks like something went wrong.');
                        }
                        self.resetAutocomplete( datalist );
                    }catch( err ){
                        // Do nothing, but mak
                    }
                };
                
                request = $.postJSON( endpoint, data ).done( function(response, textStatus, jqXHR){
                    switch( jqXHR.status ) {
                        case 200 :
                            self.updateAutocomplete(datalist, response.data);
                            break;
                        case 204 :
                        default :
                            self.resetAutocomplete(datalist);
                            break;
                    }
                } ).fail( fnFail ).always(function(){
                    // Nothing...
                });                
                return false;
            });

            // Prevent specific keys from triggering
            field.on('keyup keydown', function(e){
                switch( e.which ) {
                    case 13: // Return/Enter key
                    case 16:
                    case 18:
                    case 20:
                    // case 37: // Arrow key
                    // case 38: // Arrow key
                    // case 39: // Arrow key
                    // case 40: // Arrow key
                    case 91:
                        // console.log('e.which', e.which);
                        return false;
                }
            });

            field.on('input', function(e){
                var min         = 3;
                var f           = $( this );
                var selection   = f.val();
                var len         = !!selection ? selection.length : 0;

                if( !selection || len < (min - 1) ) {
                    self.autocomplete_selected = null;
                    self.resetAutocomplete(datalist);

                    return;
                }else{
                    // Abort previous requests
                    if( !!request ) {
                        request.abort();
                    }

                    // Automatically determine a match
                    self.autocomplete_selected = $('option',datalist).filter(function(){
                        return this.value.toLowerCase() == selection.toLowerCase();
                    });

                    // Trigger a request only of no matches found in current datalist
                    
                    if( !self.autocomplete_selected.length ) {
                        el.trigger('submit');
                    }
                }
            });
        }

        WPXSearchFilterField.prototype.updateAutocomplete = function( datalist, data ) {
            if( !!datalist ) {                
                datalist.empty();
                data.map(function(v,i){
                    var txt = v.label;
                    var option = $('<option>' + txt + '</option>');
                    option.data('entity', v);
                    datalist.append(option);
                });
            }            
        }

        WPXSearchFilterField.prototype.resetAutocomplete = function( datalist ) {
            self.autocomplete_selected = null;
            if( !!datalist ) {
                datalist.empty();
            }
        }

        return WPXSearchFilterField;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});