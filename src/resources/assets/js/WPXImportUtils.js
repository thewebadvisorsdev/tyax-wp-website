/**
 * WPXImportUtils
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXImportUtils',{
    value: (function(_super){
        var $;
        var max = 100;

        // Extend this component to sub-class WPXImportUtils
        WPX.___extends(WPXImportUtils, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXImportUtils
         */
        function WPXImportUtils(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;

            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXImportUtils.prototype.init = function() {
            if( $ === undefined ) return false;
            
            var self = this;
            var el = self.dom();
            var target = $( el.data('output') );
            if( !!target.length ) {
                el.on('input', function(){
                    var json = JSON.stringify(self.parse(el.val()));
                    target.removeAttr('readonly').val( json );
                });

                if( !!el.val() && !target.val() ) {
                    el.trigger('input');
                }
            }
        }

        WPXImportUtils.prototype.parse = function(data) {
            var self = this;            
            var json = JSON.parse( data );
            var models = [];
            var location = {
                'owner_id' : null,
                'city_id' : null,
                'region' : null,
                'name' : null,
                'phone' : null,
                'toll_free' : null,
                'email' : null,
                'url' : null,
                'street' : null,
                'province' : null,
                'postal_code' : null,
                'map_coordinates' : null,
                'created' : null,
                'modified' : null,
                'stakeholder': null,
                'amenities' : null,
                'accommodations' : null,
                'experiences' : null,
                'status': null
            };
            max = json.length - 1;

            json.map(function(v,i){
                if( max > 0 ) {
                    var o = $.extend({}, location, {
                        'owner_id' : null,
                        'city_id' : self.getCityID(v.postmeta),
                        'region' : self.getRegion(v.category),
                        'name' : v.title,
                        'phone' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'stakeholder_phone_number' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'toll_free' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sh_toll_free' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'email' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sh_email' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'url' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sh_website' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'street' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sh_street_address' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'province' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sh_province' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'postal_code' : (function(d){
                            var match = d.find(function(v){
                                return v.meta_key == 'sf_postal_code' ? v.meta_value : null;
                            });
                            return null != match ? match.meta_value : null;
                        })(v.postmeta),
                        'map_coordinates' : null,
                        'created' : v.post_date,
                        'modified' : v.post_date_gmt,
                        'stakeholder': self.getStakeHolderData( v ),             
                        'amenities': self.getAmenities( v.category ),            
                        'accommodations': self.getAccommodations( v.category ),            
                        'experiences': self.getExperiences( v.category ),
                        'status': v.status ? v.status : 'pending'  
                    });

                    models.push(o);
                    console.log('model', o);
                }
                max--;
            });

            return models;
        }

        WPXImportUtils.prototype.getAmenities = function( categories ) {
            var amenities = (categories.filter(function(v,i){
                return (v['@domain'] == 'stacategory_tax');
            }).map(function(v,i){
                return v['#text'];
            }));
            
            return amenities;
        }

        WPXImportUtils.prototype.getAccommodations = function( categories ) {
            var amenities = (categories.filter(function(v,i){
                return (v['@domain'] == 'staaccommodation_tax');
            }).map(function(v,i){
                return v['#text'];
            }));
            
            return amenities;
        }

        WPXImportUtils.prototype.getExperiences = function( categories ) {
            var exp = (categories.filter(function(v,i){
                return (v['@domain'] == 'experience_tax');
            }).map(function(v,i){
                return v['#text'];
            }));
            
            return exp;
        }

        WPXImportUtils.prototype.getCityID = function( metadata ) {
            var city = (function(d){
                var match = d.find(function(v){
                    return v.meta_key == 'sh_city' ? v.meta_value : null;
                });
                return match? match.meta_value : null;
            })(metadata);

            var cities = {
                '100 Mile House' : 1,
                '108 Mile Ranch' : 2,
                '150 Mile House' : 3,
                '70 Mile House' : 4,
                '70 MILE HOUSE, BC' : 5,
                'Anahim Lake' : 6,
                'Ashcroft' : 7,
                'Barkerville' : 8,
                'Bella Coola' : 9,
                'Big Creek' : 10,
                'Boston Bar, BC' : 11,
                'Box 3400' : 12,
                'Bridge Lake' : 13,
                'Cache Creek' : 14,
                'Canim Lake, BC' : 15,
                'Charlotte Lake' : 16,
                'Chilanko Forks' : 17,
                'Chilcko Lake' : 18,
                'Clinton' : 19,
                'Denny Island' : 20,
                'Eagle Creek' : 21,
                'Firvale' : 22,
                'Forest Grove' : 23,
                'Gold Bridge' : 24,
                'Hagensborg' : 25,
                'Hixon' : 26,
                'Horsefly' : 27,
                'Kleena kleene' : 28,
                'Klemtu' : 29,
                'Lac La Hache' : 30,
                'Likely' : 31,
                'Lillooet' : 32,
                'Lone Butte' : 33,
                'McLeese Lake' : 34,
                'Nazko' : 35,
                'Nemaiah' : 36,
                'Nimpo Lake' : 37,
                'Quesnel' : 38,
                'Redstone' : 39,
                'Riske Creek' : 40,
                'Seton Portage' : 41,
                'Stuie' : 42,
                'Tatla Lake' : 43,
                'Tatlayoko Lake' : 44,
                'Thompson-Nicola E' : 45,
                'Valemont' : 46,
                'Wells' : 47,
                'Williams Lake' : 48,
            };
            return cities[ city ] || null;
        }

        WPXImportUtils.prototype.getRegion = function( metadata ) {
            var region = (function(d){
                var match = d.find(function(v){
                    return v['@domain'] == 'region_tax' ? v['#text'] : null;
                });
                return match['#text'];
            })(metadata);

            var regions = {
                'Default' : 0,
                'Coast' : 1,
                'Cariboo' : 2,
                'Chilcotin' : 3
            };

            return regions[ region ] || null;
        }

        WPXImportUtils.prototype.getStakeHolderData = function( metadata ) {
            var m = metadata;
            var stakeholder = {
                'image_id': null,
                'owner_id': null,
                'name': null,
                'slug': null,
                'blurb': null,
                'description': null,
                'created': null,
                'modified': null
            }
            var s = $.extend({}, stakeholder, {
                'image_id': (function(d){
                    var match = d.find(function(v){
                        return v.meta_key == 'sh_banner_image' ? v.meta_value : null;
                    });
                    return match? match.meta_value : null;
                })(m.postmeta),
                'owner_id': null,
                'name': m.title,
                'slug': m.post_name,
                'blurb': m.description[0] || null,
                'description': m.encoded[0] || null,
                'created': m.post_date,
                'modified': m.post_date_gmt
            });
            return s;
        }

        return WPXImportUtils;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});