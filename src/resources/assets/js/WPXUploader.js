Object.defineProperty(WPX,'WPXUploader',{
    value: (function(_super){
		var $;
		var wpwpmediauploader;

		// Extend this component to sub-class UIComponent
		WPX.___extends(WPXUploader, _super);
		
		/**
		 * Constructor
		 * 
		 * @param 	{object}	obj 	jQuery element instance
		 * @param 	{object}	jquery 	jQuery
		 * @returns {WPXUploader}
		 */
		function WPXUploader(obj, jquery) {
			var self = _super.call(this, obj) || this;
			
            $ = jquery;
            self.panelPreview;
            self.panelUpload;
            self.previewImage;

            self.fieldMetaData;
            self.fieldMediaID;

            self.mediaAsset;
            self.mediaAttachment;

			self.state('active', false);
			self.init();

			return self;
		};
		
		/**
		 * Initialize the component once it's bound to a DOM element
		 */
        WPXUploader.prototype.init = function() {
			if( $ === undefined ) return false;
			
			var self = this;
            var el = self.dom();

            self.panelPreview   = el.find('.wpx-mediaupload__preview');
            self.panelUpload    = el.find('.wpx-mediaupload__inner');
            self.previewImage   = $('img', self.panelPreview).data('preview');

            self.fieldMediaID   = el.find('input[type="hidden"]:not([name*="_media_metadata_"])');
            self.fieldMetaData  = el.find('input[type="hidden"][name*="_media_metadata_"]');
            
            // Initialize the WP Media Uploader
            wpmediauploader = wp.media.frames.file_frame = wp.media({
                title: 'Choose Image',
                button: {
                    text: 'Choose Image'
                }, multiple: false 
            });

            // Media Uploader event handler
            wpmediauploader.on('select', function() {
                var self = window.__current_uploader__;
                self.mediaAsset         = wpmediauploader.state().get('selection').first();
                self.mediaAttachment    = self.mediaAsset.toJSON();

                self.fieldMediaID.val(self.mediaAttachment.id);
                self.fieldMetaData.val(JSON.stringify(self.mediaAttachment));
                self.preview(self.mediaAttachment);
            });

            el.on('click', '.wpx-mediaupload__button', function(e){
                if( wpmediauploader ) {
                    window.__current_uploader__ = self;
                    wpmediauploader.open();
                }
                return false;
            });          

            el.on('click', '.wpx-mediaupload__clear', function(e){
                self.reset();
                return false;
            });          

        }

        WPXUploader.prototype.reset = function() {
            var self            = this;
            var dom             = self.dom();
            var img             = $('img', self.panelPreview);
            var metaTitle       = $('.wpx-mediaupload__meta_title', self.panelPreview);
            var metaFilesize    = $('.wpx-mediaupload__meta_filesize', self.panelPreview);

            img.one('load', function(){
                metaTitle.text('N/A');
                metaFilesize.text('--');
                // Toggle panels
                self.state('active', false);
                self.panelPreview.attr('data-state', 'hidden');
                self.panelUpload.attr('data-state','active');
            }).each(function() {
                if(this.complete) $(this).load();
            });

            img.replaceWith($('<img />',{
                src: self.previewImage
            }));

            self.fieldMediaID.val('');
            self.fieldMetaData.val('');

        }
        
        WPXUploader.prototype.preview = function(data) {
            var self            = this;
            var dom             = self.dom();
            var img             = $('img', self.panelPreview);
            var metaTitle       = $('.wpx-mediaupload__meta_title', self.panelPreview);
            var metaFilesize    = $('.wpx-mediaupload__meta_filesize', self.panelPreview);

            if( !!data ) {
                var url         = data.url;
                var title       = data.title;
                var filesize    = data.filesizeHumanReadable;
                var type        = data.type;
                var orientation = (data.width > data.height) ? 'landscape' : 'portrait';

                img.on('load', function(e){
                    // Set fields
                    metaTitle.text(title ? title : 'N/A');
                    metaFilesize.text(filesize ? filesize : '--');

                    // Toggle panels
                    self.state('active', true);
                    self.panelPreview.attr('data-state', 'active');
                    self.panelUpload.attr('data-state','hidden');
                }).each(function() {
                    if(this.complete) $(this).load();
                });

                switch( type ) {
                    case 'image' :
                        img.attr('src', url).addClass(orientation);
                        break;
                    case 'video' :
                        img.attr('src', data.icon).addClass(orientation);
                        break;
                }
                dom.addClass(type);
                window.__current_uploader__ = null;
            }
        }

		return WPXUploader;

    })(WPX.UIComponent),
		enumerable: true,
		configurable: true,
		writable: true
	});