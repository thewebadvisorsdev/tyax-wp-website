/**
 * WPXBlocker
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXBlocker',{
    value: (function(_super){
        var $;

        // Extend this component to sub-class WPXBlocker
        WPX.___extends(WPXBlocker, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXBlocker
         */
        function WPXBlocker(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;
            self.body = $('body');
            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXBlocker.prototype.init = function() {
            if( $ === undefined ) return false;
            
            var self = this;
            var el = self.dom();

            // Subscribe to events
            WPX.EventDispatcher.subscribe('onBlockUI', self);
        }

        WPXBlocker.prototype.block = function() {
            var self    = this;
            var dom     = self.dom();
            var blocker = dom.clone(true);
            blocker.attr('data-state','active');

            self.reset();
            self.body.addClass('wpx-noscroll');
            self.body.append(blocker);
        }

        WPXBlocker.prototype.unblock = function() {
            var self = this;
            self.reset();
        }

        WPXBlocker.prototype.reset = function() {
            var self = this;
            self.body.removeClass('wpx-noscroll');
            $('.wpx-blocker', self.body).remove();
        }

        // ---------------------------------------------------------- //
        // EVENT HANDLERS
        // ---------------------------------------------------------- //
        WPXBlocker.prototype.onBlockUI = function(state) {            
            if( !state ) return false;
            var self = this;
            if( state.active ) {
                self.block();
            }else{
                self.unblock();
            }
        }

        return WPXBlocker;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});