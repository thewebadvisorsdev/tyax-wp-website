/**
 * WPXForm
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXForm',{
    value: (function(_super){
        var $;

        // Extend this component to sub-class WPXForm
        WPX.___extends(WPXForm, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXForm
         */
        function WPXForm(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;
            self.errors = {
                0 : 'Unknown error.',
                1 : 'Unauthorized access.',
                2 : 'Invalid parameters.',
                3 : 'Could not add entity.',
                4 : 'Could not update entity.'
            };
            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXForm.prototype.init = function() {
            if( $ === undefined ) return false;
            
            var self = this;
            var el = self.dom();

            el.on('submit', function(e){
                e.preventDefault();
                var payload = self.serialize(el);
                var endpoint = el.attr('action');
                var fnFail = function(response){
                    console.log('response', response);
                    console.warn( 'Additional error information: ' + self.errors[ response.code ? response.code : 0 ] );
                    if( 'error' in console ) {
                        console.error('Uh oh, looks like something went wrong.');
                    }

                    if( !!response.responseJSON ) {
                        self.modal( response.responseJSON.error );
                    }

                    WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                };

                WPX.EventDispatcher.dispatch('BlockUI', {active: true});

                $.postJSON(endpoint, payload).done(function(response){
                    console.log('response', response);

                    if( ('success' in response) && !!response.success ) {
                        if( 'location' in response ) {
                            location.href = response.location;
                        }else{
                            location.reload(true);
                        }
                    }else{
                        fnFail(response);
                    }
                        
                }).fail(fnFail).always(function(){
                    WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                });

                return false;
            });


        }

        /**
         * Combines are Entity fields into a common JSON object,
         * leaving non-entity fields untouched.
         */
        WPXForm.prototype.serialize = function(form) {
            if( !form ) return null;

            var data            = [];
            var raw_data_array  = form.serializeArray();
            var raw_data_js     = form.serializeJSON();

            // Handle checkboxes and radio buttons
            form.find(':checkbox:not(:checked).wpx-booleancheckbox').map(function () { 
                raw_data_js[this.name] = this.checked ? this.value : 0;
            });

            raw_data_array.map(function(v,i){
                var key = v.name;
                key = key.replace(/_$/,'');
                key = key.replace(/^_/,'');

                if( key.length < v.name.length ) {
                    data.push(raw_data_array[i]);
                    delete raw_data_js[v.name];
                } 
            });

            data.push({name:'data', value:JSON.stringify(raw_data_js) });

            return data;
        }

        WPXForm.prototype.reset = function() {
            var self = this;
            var el = self.dom();
            return true;
        }
        
        WPXForm.prototype.modal = function(message) {
            if( typeof $.modal !== 'function' ) return false;

            var self = this;
            var el = self.dom();
            var html = [];

            html.push('<div id="WPXFormAlert" class="wpx-modalcontent wpx-alert">');
            html.push('    <div class="wpx-modalcontent__inner">');
            html.push(message);
            html.push('    </div>');
            html.push('</div>');     
            
            // Modal alert
            $(html.join("\n")).modal();
            return self;
        }

        // ---------------------------------------------------------- //
        // EVENT HANDLERS
        // ---------------------------------------------------------- //

        return WPXForm;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});