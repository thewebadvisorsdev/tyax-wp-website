/**
 * WPXTabControls
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXTabControls',{
    value: (function(_super){
        var $;

        // Extend this component to sub-class WPXTabControls
        WPX.___extends(WPXTabControls, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXTabControls
         */
        function WPXTabControls(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;
            self.body = $('body');
            self.errors = {
                0 : 'Fatal error.',
                1 : 'No relations found.',
                2 : 'ID not supplied.',
                3 : 'NULL is not a valid relation for this entity.',
                4 : 'No such relation exists for this entity.'
            };
            self.current = null;

            self.init();

            return self;
        };
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXTabControls.prototype.init = function() {
            if( $ === undefined ) return false;
            
            var self = this;
            var el = self.dom();

            if( el.is('.wpx-navtabs_panels') ) {

                // Event binding
                WPX.EventDispatcher.subscribe('onEntityRelationUpdated', self);

                el.on('click', '.wpx-navtabs__item', function(){
                    var obj         = $( this );
                    var uri         = obj.data('posturl');
                    var data        = obj.data('postdata');
                    var isDefault   = (obj.data('tabindex') == 'default');
                    var panel;

                    self.current = obj

                    if( !!isDefault ) {
                        if( self.reset() ) {
                            panel.addClass('wpx-navtabpanel_current');
                            obj.addClass('wpx-navtabs__item_current');
                        }
                    }                    

                    if( data ) {
                        var relation = obj.data('relation');
                        var tmp = JSON.parse(data.data);
                        tmp.relation = relation;
                        data.data = JSON.stringify(tmp);    
                        panel = $('.wpx-navtabpanel[data-panel="' + relation + '"]');
                    }else {
                        panel = $('.wpx-navtabpanel[data-panel="default"]');
                    }

                    if( !!data ) {
                        if( !uri ) return false;
    
                        var fnFail = function(response){
                            console.error('Uh oh, looks like something went wrong.');
                            console.warn( 'Additional error information: ' + self.errors[ response.responseJSON.code ] );
                            WPX.EventDispatcher.dispatch('BlockUI', {active: false});
                        };
        
                        WPX.EventDispatcher.dispatch('BlockUI', {active: true});
        
                        $.postJSON(uri, data).done(function(response){
                            if( ('success' in response) && !!response.success ) {
                                if( self.reset() ) {
                                    panel.html( response.html );
                                    panel.addClass('wpx-navtabpanel_current');
                                    obj.addClass('wpx-navtabs__item_current');
                                }
                            }else{
                                fnFail(response);
                            }
                        }).fail(fnFail).always(function(){
                            WPX.EventDispatcher.dispatch('BlockUI', {state: false});
                        });
                        
                    }
                    
                    return false;
                });    
            }

            // Default to the first tab
            self.current = $('.wpx-navtabs__item', el).eq(0);
            self.current.addClass('wpx-navtabs__item_current');

            // Store a reference
            el.data('WPXComponent', self);
        }

        WPXTabControls.prototype.reset = function() {
            var self = this;
            var el = self.dom();
            $('.wpx-navtabpanel').removeClass('wpx-navtabpanel_current');
            $('.wpx-navtabs__item', el).removeClass('wpx-navtabs__item_current');
            return true;
        }

        // ---------------------------------------------------------- //
        // EVENT HANDLERS
        // ---------------------------------------------------------- //
        /**
         * Reload the current tab
         */
        WPXTabControls.prototype.onEntityRelationUpdated = function(data) {
            var self = this;
            var current = self.current;
            if( null === current && !current.length ) return false;
            current.trigger('click');
        }        

        return WPXTabControls;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});