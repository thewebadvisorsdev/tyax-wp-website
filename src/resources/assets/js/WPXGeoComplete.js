/**
 * WPXGeoComplete
 * 
 * @author Chris Murphy
 * @version v0.0.1
 */
Object.defineProperty(WPX,'WPXGeoComplete',{
    value: (function(_super){
        var $;

        // Extend this component to sub-class WPXGeoComplete
        WPX.___extends(WPXGeoComplete, _super);
        
        /**
         * Constructor
         * 
         * @param     {object}    obj jQuery element instance
         * @param     {object}    jquery jQuery
         * @returns WPXGeoComplete
         */
        function WPXGeoComplete(obj, jquery) {
            var self = _super.call(this, obj) || this;
            
            $ = jquery;

            // self.initGMapsPrototypes();

            self.init();

            return self;
        };

        WPXGeoComplete.prototype.initGMapsPrototypes = function() {
            try {
                google.maps.Map.prototype.markers = new Array();
    
                google.maps.Map.prototype.getMarkers = function() {
                    return this.markers
                };
    
                google.maps.Map.prototype.clearMarkers = function() {
                    for(var i=0; i<this.markers.length; i++){
                        this.markers[i].setMap(null);
                    }
                    this.markers = new Array();
                };
    
                google.maps.Marker.prototype._setMap = google.maps.Marker.prototype.setMap;
    
                google.maps.Marker.prototype.setMap = function(map) {
                    if (map) {
                        map.markers[map.markers.length] = this;
                    }
                    this._setMap(map);
                }
            }catch( err ) {
                console.log('err', err.toString());
            }
        }
        
        
        /**
         * Initialize the component once it's bound to a DOM element
         */
        WPXGeoComplete.prototype.init = function() {
            
            if( $ === undefined || !('geocomplete' in $.fn) ) return false;
            
                var self            = this;
                var el              = self.dom();
                var field           = el.closest('.field.map');
                var fieldHidden     = field.find('input[type="hidden"]');
                var fieldMapData    = $('input[name*="map_data"], textarea[name*="map_data"]');
                var trigger         = field.find('.wpx-maptrigger');
                var coordstoggle    = field.find('.wpx-coordstoggle');
                var reset           = field.find('.wpx-mapreset');
                var preview         = $('.wpx-map',field).eq(0);
                var loc             = fieldHidden.val();
                var objLocDefault   = {lat:53.5681328,lng:-128.8088682};
                var objLoc          = objLocDefault;
                var address         = el.data('mapaddress') ? el.data('mapaddress') : 'British Columbia, Canada';
                var map;
                
            try {
                objLoc          = (loc && loc != 'wpcf-stakeholder-address') ? JSON.parse(loc) : null;
            }catch( err ) {
                el.prop('readonly',true).prop('disabled',true);
                fieldHidden.prop('type', 'text');
                self.modal('Invalid map coordinates: ' + loc + ".<br> <br> Coordinates must be formatted, ["+ loc +"] note the brackets '[]'.<br> <br> Update your coordinates, then save your changes to flush/refresh the map.");
                objLoc = [objLocDefault.lat, objLocDefault.lng];
            }

            if( !!preview.length ) {

                try {
                    
                    el.geocomplete({
                        map: preview,
                        mapOptions: {
                            zoom: objLoc ? 10 : 14,
                            center: objLoc ? {lat: objLoc[0], lng: objLoc[1]} : objLocDefault
                        },
                        markerOptions: {
                            draggable: true
                        }
                    }).on("geocode:error", function(event, result){
                        if( result == 'ZERO_RESULTS') {
                            // console.error("Uh oh! We couldn't find a matching location. '" + el.val() + "' does not appear to be a valid address.");                        
                            self.modal("<strong>Uh oh!</strong><br><br> We couldn't find a matching location. <em>'" + el.val() + "'</em> does not appear to be a valid address or lat/lng coordinates.");
                        }
                    }).on("geocode:result", function(event, result){
                        var coords = [result.geometry.location.lat(), result.geometry.location.lng()];
                        
                        fieldHidden.val(JSON.stringify(coords));
                        if( !!fieldMapData.length ) {
                            fieldMapData.val(JSON.stringify(result));
                            console.log('fieldMapData', fieldMapData.prop('value'));
                        }
                    }).on("geocode:dragged", function(event, latlng){
                        var coords = [latlng.lat(), latlng.lng()];                    
                        fieldHidden.val(JSON.stringify(coords));
                        
                        // Show the field
                        el.prop('readonly',true).prop('disabled',true);
                        fieldHidden.prop('type', 'text');
                        fieldHidden.trigger('input');
                    });

                    // Direct reference to the Google Map instance.
                    map = el.geocomplete('map');
                }catch( err ) {
                    console.log('err', err.toString());
                }

                // Draw a marker
                if( map ) {
                    var marker;
                    var markerOptions = {
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    };

                    var placeMarkerAndPanTo = function(latLng, map) {
                        var m = new google.maps.Marker({
                          position: latLng,
                          map: map
                        });
                        map.panTo(latLng);
                    };

                    var moveMarkerAndPanTo = function(latLng, map) {
                        marker.setPosition(latLng);
                        map.panTo(latLng);
                        fnSetNewLocation( latLng );
                    };

                    var fnSetNewLocation = function(latLng) {
                        el.prop('readonly',true).prop('disabled',true);
                        fieldHidden.prop('type', 'text');
                        fieldHidden.prop('value', JSON.stringify([latLng.lat(),latLng.lng()]));
                    }

                    try {
                        var mrkOptions = $.extend({},markerOptions,{
                            title: address,
                            position: new google.maps.LatLng( objLoc ? objLoc[0] : objLocDefault.lat, objLoc ? objLoc[1] : objLocDefault.lng)
                        });

                        marker = new google.maps.Marker(mrkOptions);
                        marker.addListener('dragend', function(e) {
                            fnSetNewLocation(e.latLng);
                        });

                        marker.setMap( map );
                        map.setZoom(objLoc ? 16:6);
                        map.addListener('click', function(e) {
                            moveMarkerAndPanTo(e.latLng, map);
                        });
                    }catch( err ) {
                        console.log('err',err, err.toString());
                    }
                    
                }
 

                trigger.on('click', function(e) {
                    el.trigger('geocode');
                    return false;
                });

                reset.on('click', function(e){
                    var input = $('input[type="text"], input[type="hidden"]', field);
                    if( input.length ) {
                        input.prop('value', null);
                        self.modal('Save this record to finalize resetting the map coordinates, and reset the mapping field.');
                    }                    
                    return false;
                });

                coordstoggle.on('click', function(){
                    var type = fieldHidden.attr('type');

                    switch( type ) {
                        case 'hidden' :
                            el.prop('readonly',true).prop('disabled',true);
                            fieldHidden.prop('type', 'text');
                            fieldHidden.prop('placeholder', 'e.g. [123,456]');
                            break;
                        case 'text' :
                            el.prop('readonly',false).prop('disabled',false);
                            fieldHidden.prop('type', 'hidden');
                            fieldHidden.prop('placeholder', null);
                            break;
                    }
                });

                fieldHidden.on('input',function(){
                    var coords = fieldHidden.val().substr(1).slice(0, -1) || null;
                    if( coords ) {
                        el.prop('value',coords);
                        el.trigger('geocode');
                    }
                });

                // Coordinates Validation
                fieldHidden.on('blur', function(){
                    self.validate( $( this ).prop('value') );
                });
                fieldHidden.triger('blur');                
            }
        }

        WPXGeoComplete.prototype.validate = function(subject){
            var self = this;
            var str = subject;
            if( !(str.match("^\\[") && str.match("\\]$")) ) {
                self.modal('Invalid map coordinates: ' + str + ".<br> <br> Coordinates must be formatted, [123,987] note the brackets '[]'.");
            }
        }

        WPXGeoComplete.prototype.modal = function(message) {
            if( typeof $.modal !== 'function' ) return false;

            var self = this;
            var el = self.dom();
            var html = [];

            html.push('<div id="WPXFormAlert" class="wpx-modalcontent wpx-alert">');
            html.push('    <div class="wpx-modalcontent__inner">');
            html.push(message);
            html.push('    </div>');
            html.push('</div>');     
            
            // Modal alert
            $(html.join("\n")).modal();
            return self;
        }

        return WPXGeoComplete;

    })(WPX.UIComponent),
    enumerable: true,
    configurable: true,
    writable: true
});