/**
 * Define a base object using the `WPX` namespace.
 * 
 * @description This method prevents polution of the global window object
 * 
 * This file contains all of the core WPX js modules in a simplified format, 
 * and excludes all unused depenendies. 
 */
if( !('WPX' in window) ) {
	Object.defineProperty(window, 'WPX',{
		value: (function(){
			var v = '1.5.0'; // Privately property

			function WPX() {
				this.v = v; // Public property
			}

			WPX.prototype.__version = function() {
				return this.v;
			}

			return WPX;
		})(),
		enumerable: true,
		configurable: true,
		writable: true
	});
}

/**
 * Extend a given object prototype (subclassing);
 * primarily use for extending WPX.UIComponent
 * 
 * @see WPX.UIComponent
 */
Object.defineProperty(WPX, '___extends',{
	value: (function(){
		return function (d, b) {
			for (var p in b)
				if (b.hasOwnProperty(p))
					d[p] = b[p];
			function __() {
				this.constructor = d;
			}
			__.prototype = b.prototype;
			d.prototype = new __();
		}
	})(),
	configurable: true,
	writable: false
});

/**
 * EventDispatcher singleton.
 */
Object.defineProperty(WPX,'EventDispatcher',{
	value: (function(){
		'use strict';
		var subscribers = {};
		var instance;

		function EventDispatcher() {}

		/**
		 * Allows objects to subscribe to events. Subscribers can optionally implement a `setDispatcher` method
		 * which this EventDispatcher will use to set reference to itself in the subscriber. 
		 * 
		 * Note: `setDispatcher` must resolve to type `function` to be considered a valid accessor.
		 * 
		 * @param	{string}	event		The name of the event for which an object is interested, e.g. `Ready`
		 * @param 	{object}	component	A reference to the subscribing object.
		 */
		EventDispatcher.prototype.subscribe = function(event, component) {
			var subscriber;
			
			// Add event if it doesn't exist
			if( !(event in subscribers) ) {
				subscribers[event] = [];
			}

			for( subscriber in subscribers[event] ) {
				if (subscribers[event][subscriber] === component) {
					break;
				}
			}
			
			if( !subscriber || subscriber === undefined ) {				
				if( 'setDispatcher' in component && typeof component['setDispatcher'] === 'function' ) {
					component.setDispatcher(instance);				
				}
				subscribers[event].push(component);
			}
		}

		/**
		 * Dispatch an event, notifying all subscribers.
		 * 
		 * @example Subscribers wanting to be notified of a particular event must implement a valid handler that follows this pattern:
		 * 
		 * Event: `Ready` the name of the event being broadcast
		 * Handler: `onReady` the name of the event prefixed with `on`, which must resolve to type `function` found in a subscriber
		 * 
		 * @param	{string}	event	The name of the event, e.g. `Ready`, `Foobar`, etc..
		 * @param	{args}		array	Any arguments that need to be passed to the event handler (e.g. `onReady`). By default if no arguments are supplied, an empty array is passed instead
		 * @param	{mixed}		source	An object subscribing to one or more events.
		 */
		EventDispatcher.prototype.dispatch = function(event, args, source) {
			var subscriber;
			if (!event) return;

			args = args || [];
			args = Array.isArray( args ) ? args : [args];

			var evt = "on" + event;
            for (subscriber in subscribers[evt]) {
				var sub = subscribers[evt][subscriber];

				if( (evt in sub) && (typeof sub[evt] === "function") ) {        
					source = source || sub;
                    sub[evt].apply(source, args);
                }
            }
		};
		
		if( !instance ) {
			instance = new EventDispatcher();
		}

		return instance;

	})(),
	enumerable: true,
	configurable: true
});

Object.freeze(WPX.EventDispatcher);

// ---------------------------------------------------------- //
// UI COMPONENT
// ---------------------------------------------------------- //
/**
 * Generic UIComponent class.
 * 
 * This version contains a default `state` variable that 
 * can be used for similar purposes as the React JS' `state` object.
 */
Object.defineProperty(WPX,'UIComponent',{
	value: (function(){
		function UIComponent(el) {
			if(this instanceof UIComponent === false) {
				return new UIComponent(el);
			}
			this._el = el;
			this._state = {};
			this.dispatcher;
		};

		/**
		 * Get the dom element to which this class is bound.
		 */
		UIComponent.prototype.dom = function() {
			return this._el;
		}

		/**
		 * Gets or sets state variables. If no key/value is supplied, the complete state object is returned.
		 * 
		 * @param	{string}	key		The property name for a state variable
		 * @param	{mixed}		value	The value of the state property
		 * @returns	{mixed}				Returns the value of the property entry
		 */
		UIComponent.prototype.state = function( key, value ) {

			switch( arguments.length ) {
				case 2 : // Set
					this._state[ key ] = value;
					break;
				case 1 : // Get
					return this._state[ key ];
				default : // Return the
					return this._state;
			}

		}

		/**
		 * Event handler for `ModuleReady` event when a new instance of this component is instantiated.
		 * 
		 * @param	{mixed}	Accepts arbitrary number of parameters.
		 */
		UIComponent.prototype.onModuleReady = function() {
			// Call additional initialization here.
			// console.log('UIComponent::onModuleReady', arguments);			
		}

		/**
		 * Sets a local reference to the WPX EventDispatcher instance for convenience,
		 * and to avoid having to wrap dispatcher calls in additional safety checks.
		 * 
		 * @param	{EventDispatcher}	d	An instance of the WPX.EventDispatcher object.
		 */
		UIComponent.prototype.setDispatcher = function( d ) {
			this.dispatcher = d;
		}

		UIComponent.prototype.raf = function(callback) {
			if( 'requestAnimationFrame' in window ) {
				if( typeof callback === 'function') {
					window.requestAnimationFrame( callback );
				}
			}else{
				if( typeof callback === 'function') {
					callback.apply(this);
				}
			}
		};

		return UIComponent;
	})(),
	enumerable: true,
	configurable: true,
	writable: true
});