<?php

namespace AUX\Utils;

/**
 * Lorem Ipsum Generator
 * 
 * e.g. http://loripsum.net/api/10/short/headers
 * (integer) - The number of paragraphs to generate.
 * short, medium, long, verylong - The average length of a paragraph.
 * decorate - Add bold, italic and marked text.
 * link - Add links.
 * ul - Add unordered lists.
 * ol - Add numbered lists.
 * dl - Add description lists.
 * bq - Add blockquotes.
 * code - Add code samples.
 * headers - Add headers.
 * allcaps - Use ALL CAPS.
 * prude - Prude version.
 * plaintext - Return plain text, no HTML.
 * 
 * @author Chris Murphy
 * @version 0.0.1
 */

class LoremIpsum {
    const API_ENDPOINT = 'http://loripsum.net/api';
    const API_DEFAULT = '/10/short/headers';

    protected $content;

    protected $api_properties = [];
    protected $allowed_length = ['short', 'medium', 'long', 'verylong'];

    public function __get($index) {
        return $this->api_properties[$index];
    }

    public function __set($index, $value) {        
        return $this->api_properties[$index] = (NULL == $value) ? null : $value;
    }

    public function __construct() {
        
    }

    /**
     * Generate content based on randomized params.
     */
    public function getRandomContent() {
        $this->api_properties = [];

        $this->paragraphs( rand(5, 25) )
            ->length( array_rand($this->allowed_length) )
            ->decorate()
            ->link()
            ->ul()
            ->ol()
            ->bq()
            ->headers();

        return $this->buildQuery();
    }

    /**
     * Generate content based on supplied params.
     * 
     * @return string
     */
    public function getContent() {
        return $this->buildQuery( empty($this->api_properties) );
    }

    /**
     * Build the query
     * 
     * @return string
     */
    private function buildQuery(bool $usedefault = true) {
        $api_properties = [];
        $query = self::API_ENDPOINT . self::API_DEFAULT;

        if( $usedefault == false ) {
            foreach( $this->api_properties as $key => $value ) {
                switch( $key ) {
                    case 'length' :
                    case 'paragraphs' :
                        array_push($api_properties, $value);
                        break;
                    default :
                        if( !!$value ) {
                            array_push($api_properties, $key);
                        }
                        break;
                }
            }
    
            $query = self::API_ENDPOINT . "/" . implode('/',$api_properties);
        }

        $loremipsum = file_get_contents($query);

        $this->content = $loremipsum;

        return $this->content;
    }

    # +------------------------------------------------------------------------+
    # PARAGRAPH & LENGTH
    # +------------------------------------------------------------------------+
    public function paragraphs(int $value = 5) {
        $this->paragraphs = $value;
        return $this;
    }

    public function length(string $value = 'random') {        

        if( $value == 'random' ) {
            $this->length = array_rand($this->allowed_length);
        } else {
            if( in_array($value, $this->allowed_length) ) {
                $this->length = $value;
            }
        }
        
        return $this;
    }

    # +------------------------------------------------------------------------+
    # FLAGS
    # +------------------------------------------------------------------------+
    public function decorate() {
        $this->decorate = true;

        return $this;
    }

    public function link() {
        $this->link = true;

        return $this;
    }

    public function ul() {
        $this->ul = true;

        return $this;
    }

    public function ol() {
        $this->ol = true;

        return $this;
    }

    public function dl() {
        $this->dl = true;

        return $this;
    }

    public function bq() {
        $this->bq = true;

        return $this;
    }

    public function code() {
        $this->code = true;

        return $this;
    }

    public function headers() {
        $this->headers = true;

        return $this;
    }

    public function allcaps() {
        $this->allcaps = true;

        return $this;
    }

    public function prude() {
        $this->prude = true;

        return $this;
    }

    public function plaintext() {
        $this->plaintext = true;

        return $this;
    }
}