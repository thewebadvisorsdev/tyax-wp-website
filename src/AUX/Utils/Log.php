<?php
namespace AUX\Utils;
/**
 * Log
 *
 * @author Chris Murphy
 * @version v0.0.1
 */
class Log {
    # +------------------------------------------------------------------------+
    # CONSTANTS
    # +------------------------------------------------------------------------+
    const LOG_TYPE_DEBUG    = 0; // Common debugging messages
    const LOG_TYPE_ERROR    = 1; // Errors
    const LOG_TYPE_NOTICE   = 2; // Notices
    const LOG_TYPE_DELETION = 4; // Database Deletion
    
    # +------------------------------------------------------------------------+
    # MEMBERS
    # +------------------------------------------------------------------------+
    private $properties = array( );
    private static $instance;
    private static $calls;
    private static $app_path;

    # +------------------------------------------------------------------------+
    # ACCESSORS
    # +------------------------------------------------------------------------+

    public function __get($index) {
        return ( array_key_exists($index, $this->properties) ) ? $this->properties[ $index ] : null;
    }

    public function __set($index, $value) {
        $this->properties[ $index ] = $value;
    }

    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __construct() {

        if( !defined('APP_PATH') ) {
            define('APP_PATH', dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR);
        }

        self::$app_path = APP_PATH;

        // Initialize
        $this->init();
    }

    private function init() {
        try {
            // Log type
            $this->log_type = self::LOG_TYPE_DEBUG;

            // The filehandler mode
            $this->mode = 'a+';
            
            // The log file
            $this->file = $this->mkLogFilename();
        } catch( \Exception $e ) {
            throw new \Exception(__METHOD__ . ' threw an exception: ' . $e->getMessage());
        }
    }

    # +------------------------------------------------------------------------+
    # METHODS (PUBLIC)
    # +------------------------------------------------------------------------+
    /**
     * Log a message to a log file
     * 
     * @param   string  $message    The message to log
     * @param   int     $type       The type of message
     * @return  bool
     * @throws  Exception 
     */
    public static function log( $message, $type = self::LOG_TYPE_DEBUG ) {
        try {
            $instance = self::getInstance();
            $backtrace = debug_backtrace();

            // Set the log type
            $instance->log_type = $type;
            $file   = ( $instance->file ) ? $instance->file : $instance->mkLogFilename();
            $data   = $instance->getMessageFormat( is_object($message) ? print_r($message, true) : $message, $type, $backtrace );
            $result = file_put_contents( $file, $data, FILE_APPEND );

            if( $type == self::LOG_TYPE_ERROR ) {
                error_log($data."\n", 0);
            }
            
            return $result;
        } catch( \Exception $e ) {
            throw new \Exception(__METHOD__ . ' threw an exception: ' . $e->getMessage());
        }
    }

    /**
     * Like Log::log, but uses the argument list instead of a dedicated message;
     * 
     * @see Log::log
     * @param mixed Supply any number of arguments to be logged
     * @return bool
     */
    public static function trace() {
        try {
            $instance   = self::getInstance();
            $backtrace  = debug_backtrace();
            $message    = func_get_args();
            $type       = self::LOG_TYPE_DEBUG;

            // Set the log type
            $instance->log_type = $type;
            $file   = ( $instance->file ) ? $instance->file : $instance->mkLogFilename();
            $data   = $instance->getMessageFormat( is_object($message) ? print_r($message, true) : $message, $type, $backtrace );
            $result = file_put_contents( $file, $data, FILE_APPEND );
            
            return $result;
        } catch( \Exception $e ) {
            throw new \Exception(__METHOD__ . ' threw an exception: ' . $e->getMessage());
        }
    }

    /**
     * Read 
     * @param type $file
     * @param type $raw 
     */
    public static function read( $file, $raw = false ) {
        $instance = self::getInstance();

        $file = ( !empty( $file ) ) ? $file : $instance->file;
        $log  = file_get_contents( $file );
        echo ( $raw ) ? $log : "<pre>{$log}</pre>";
    }

    # +------------------------------------------------------------------------+
    # METHODS (PRIVATE)
    # +------------------------------------------------------------------------+
    private function mkLogFilename($use_datetime=false) {
        $logfilename = sprintf("%slogs/%s.log",self::$app_path, $use_datetime ? date('Y_m_d') : 'log');
        return $logfilename;
    }

    private function getMessageFormat( $message, $type =  self::LOG_TYPE_DEBUG, $backtrace = null) {
        try {
            $tag = "ERROR";
            $timestamp = @date('d-M-Y H:i:s e O');
            
            // Convert all non-strings to strings
            if( !is_string( $message ) && !is_numeric( $message ) ) {
                $message = "\n" . print_r( $message, 1 );
            }
            
            switch( $type ) {
                case self::LOG_TYPE_ERROR :
                    $tag = "ERROR";
                    break;
                case self::LOG_TYPE_NOTICE :
                    $tag = "NOTICE";
                    break;
                case self::LOG_TYPE_DELETION :
                    $tag = "DATABASE DELETION";
                    break;
                case self::LOG_TYPE_DEBUG :
                default : 
                    $tag = "DEBUG";
                    break;
            }

            if( null != $backtrace ) {
                $info       = array_shift($backtrace);
                $line_trace = sprintf("Logged from file %s on line %s", $info['file'] ?? 'unknown', $info['line'] ?? 'unknown');
                $msg_format = sprintf("[%s] (%s) %s:\n %s\n", $timestamp, $tag, $line_trace, $message);
            }else{
                $msg_format = sprintf("[%s] %s %s\n", $timestamp, $tag, $message);
            }

            return $msg_format;

        } catch( \Exception $e ) {
            throw new \Exception(__METHOD__ . ' threw an exception: ' . $e->getMessage());
        }
    }
    
    public static function backtrace($message) {
        try {
            if(!is_array(self::$calls)) {
                self::$calls = array(); 
            }

            $call = debug_backtrace(false); 
            $call = (isset($call[1]))?$call[1]:$call[0]; 

            $call['message'] = $message;
            array_push(self::$calls, $call);  
        } catch( \Exception $e ) {
            throw new \Exception(__METHOD__ . ' threw an exception: ' . $e->getMessage());
        }
    }
    
    # +------------------------------------------------------------------------+
    # UTILITY METHODS (PRIVATE)
    # +------------------------------------------------------------------------+
    private function __clone(){}
}