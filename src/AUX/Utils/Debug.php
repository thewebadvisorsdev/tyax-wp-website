<?php
namespace AUX\Utils;

/**
 * A basic debugging class with methods that wrap var_dump and print_r in "<pre />" tags.
 *
 * @author Chris
 */
class Debug {
    # +------------------------------------------------------------------------+
    # CONSTANTS
    # +------------------------------------------------------------------------+
    const DEBUG_TYPE_DEBUG    = 0; // Common debugging messages
    const DEBUG_TYPE_ERROR    = 1; // Errors
    const DEBUG_TYPE_NOTICE   = 2; // Notices

    # +------------------------------------------------------------------------+
    # MEMBERS
    # +------------------------------------------------------------------------+
    // Singleton Instance
    private static $m_instance;
    private static $m_delimiter = "\r\n<!-- # +------------------------------------------------------------------------+ # -->\r\n";

    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    private function __construct(){}

    # +------------------------------------------------------------------------+
    # STATIC METHODS (PUBLIC)
    # +------------------------------------------------------------------------+

    public static function getInstance() {
        if( !self::$m_instance ) {
            self::$m_instance = new self;
        }
        return self::$m_instance;
    }
    
    public static function Log( $label, $vars, $halt = false ) {
        self::Dump($vars, $halt);
    }
    
    public static function Dump( $vars, $type = Debug::DEBUG_TYPE_ERROR, $halt = false, $return = false ) {
        $tag    = $type;
        $bt     = array_slice(debug_backtrace(), 1);
        $info   = array_shift($bt);

        $timestamp = @date('d-M-Y H:i:s e O');
        $line_trace = sprintf("Logged from file %s on line %s", $info['file'], $info['line']);
        $msg_format = sprintf("[%s] (%s) %s:\n %s\n", $timestamp, $tag, $line_trace,  print_r( $vars, 1 ));

        if( $return ) {
            return $msg_format;
        }else{
            echo self::$m_delimiter;
    
            self::PrintVars( $msg_format );
        }
        

        if( $halt ) die( self::$m_delimiter );
    }

    public static function PrintVars($vars, $halt = false) {
        echo '<pre class="wpx-debug">';
        print_r($vars);
        echo '</pre>';
        if( $halt ) die( self::$m_delimiter );
    }

    public static function VarDump($vars, $halt = false) {
        echo '<pre class="wpx-debug">';
        var_dump($vars);
        echo '</pre>';
        if( $halt ) die( self::$m_delimiter );
    }

    # +------------------------------------------------------------------------+
    # METHODS (PRIVATE)
    # +------------------------------------------------------------------------+
    private function __clone(){}

}