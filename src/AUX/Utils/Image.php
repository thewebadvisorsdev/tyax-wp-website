<?php
/**
 * Basic image manipulation library based on two open source libraries. 
 * This implementation has been modified to work as a static class.
 * 
 * @link https://gist.github.com/miguelxt/908143
 * @link https://github.com/claviska/SimpleImage/blob/master/src/claviska/SimpleImage.php
 */

namespace AUX\Utils;

use \Exception;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class Image {

    const IMAGE_QUALITY         = 85;

    const ORIENTATION_LANDSCAPE = 0; //landscape
    const ORIENTATION_PORTRAIT  = 1; //portrait
    const ORIENTATION_SQUARE    = 2; //square
   
    protected static $instance;
	protected $image;
	protected $image_copy;
    protected $image_type;
    protected $image_exif;
    protected $image_dimensions;
    protected $image_info;
    protected $image_pathinfo;
    protected $image_originalfilename;
    protected $image_newfilename;
    protected $cached_file;

    /**
     * Get the image resource.
     * 
     * @return resource The image.
     */
    public static function ImageResource() {
        $instance = self::getInstance();
        return $instance->image;
    }
    
    /**
     * Get the image type.
     * 
     * @return int An IMAGETYPE_* constant. Defaults to IMAGETYPE_JPEG. IMAGETYPE_GIF and IMAGETYPE_PNG are supported by this library
     */
    public static function ImageType() {
        $instance = self::getInstance();

        if( null == $instance->image_info ) {
            $instance->img_getImageInfo();
        }
        
        return $instance->image_type;
    }

    /**
     * Image info includes: Width, Height, an IMAGETYPE_* constant, width/height html <img> attributes, bit depth, and mime type.
     * 
     * @param   String  $key Lowercase, accepts: `dimensions`, `imagetype`, `attrsize`, `bits`, `mime`
     * @return  Mixed   Returns mixed types depending on what is requested.
     */
    public static function ImageInfo($key) {
        $instance = self::getInstance();

        if( null == $instance->image_info ) {
            $instance->img_getImageInfo();
        }

        switch( $key ) {
            case 'dimensions' :
                return $instance->image_dimensions;
            case 'imagetype' :
                return $instance->image_info[2];
            case 'attrsize' :
                return $instance->image_info[3];
            case 'bits' :
                return $instance->image_info['bits'];
            case 'mime' :
                return $instance->image_info['mime'];
            default :
                return null;
        }
    }

    /**
     * Get the EXIF data from an image resource if $key is supplied.
     * 
     * @param   String  (Optional) $key coresponding to and element in the EXIF data array.
     * @return  Mixed   The EXIF value corresponding to the $key, the EXIF data array if no key is supplied.
     */
    public static function EXIF(string $key = null) {
        $instance = self::getInstance();
        if( null != $key && null != $instance->image_exif ) {
            return array_key_exists($key, $instance->image_exif) ? $instance->image_exif[ $key ] : null;
        }
        return $instance->image_exif;
    }
    
    # +------------------------------------------------------------------------+
    # CONSTRUCTORS
    # +------------------------------------------------------------------------+
    public static function getInstance($filename = null) {
        if (self::$instance == null) {
            $class = __CLASS__;
            if(null != $filename) {
                if( !file_exists($filename) ) {
                    throw new Exception("Image does not exist.");
                }
            }
            self::$instance = new $class();
        }

        return self::$instance;
    }    

    /**
     * Explicitly uses the load method
     */
	private function __construct(){
	}

    # +------------------------------------------------------------------------+
    # CORE METHODS
    # +------------------------------------------------------------------------+
    /**
     * Load an image from file.
     * 
     * @param   String   $file  The filename
     * @return  Object   Returns the current AUX\Utils\Image instance
     */
    public static function Load( string $filename ) {
        $instance = self::getInstance();
        try {
            return $instance->img_load( $filename );
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

	private function img_load(string $filename) {
        $this->image        = $filename;
        $this->cached_file  = null;
        $this->img_getImageInfo($filename);

        switch( $this->image_type ) {
            case IMAGETYPE_JPEG :
                $this->image    = imagecreatefromjpeg($filename);
                
                $this->orientJPEGImageFromEXIF();
          
                break;
            case IMAGETYPE_GIF :
                $this->image = imagecreatefromgif($filename);
                break;
            case IMAGETYPE_PNG :
                $this->image = imagecreatefrompng($filename);
                break;
            default :
                throw new Exception("The file you're trying to open is not supported.");
                break;
        }

        return $this;
	}

    /**
     * Saves an image resource to disk.
     * Image formats supported by this library are:
     * 
     * - IMAGETYPE_JPEG
     * - IMAGETYPE_GIF
     * - IMAGETYPE_PNG
     * 
     * @link    https://secure.php.net/manual/en/image.constants.php
     * @link    https://secure.php.net/manual/en/function.chmod.php
     * 
     * @param   String  $filename       A new file name for the manipulated image. Will be saved alongside the original file.
     * @param   String  $image_type     An IMAGETYPE_* constant. By default, uses the original image type. 
     * @param   Int     $quality        The compression quality: 0-100
     * @param   Int     $permissions    File permissions, e.g. 0755
     * @return  String  Returns the full path to the newly manipulated image file.
     */    
    public static function Save(string $filename = null, int $image_type = null, int $quality = self::IMAGE_QUALITY, int $permissions = null) {
        $instance = self::getInstance();
        try {
            $filename = $filename ?: $instance->image_newfilename;
            $result = $instance->cached_file ?: $instance->img_save( $filename, $image_type, $quality, $permissions );
            // Log::trace(__METHOD__,  $instance->cached_file, $result);
            unset($instance);
            return $result;
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }       
    }

    public static function getAsset() {
        $instance = self::getInstance();
        try {
            $filename = $instance->cached_file ?: $instance->image_newfilename;
            $path_info = pathinfo($filename);

            // Ensure that a full path is supplied for output
            if( $path_info['dirname'] == '.' ) {
                $filename = sprintf("%s/%s", $instance->image_pathinfo['dirname'], $filename);
            }            
            unset($instance);
            return $filename;
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }  
    }

	private function img_save(string $filename, int $image_type = null, int $quality = self::IMAGE_QUALITY, int $permissions = null) {
        $type = null == $image_type ? $this->image_type : $image_type;
        $path_info = pathinfo($filename);

        // Ensure that a full path is supplied for output
        if( $path_info['dirname'] == '.' ) {
            $filename = sprintf("%s/%s", $this->image_pathinfo['dirname'], $filename);
        }

        switch( $type ) {
            case IMAGETYPE_JPEG :
                imagejpeg($this->image,$filename,$quality);
                break;
            case IMAGETYPE_GIF :
                imagegif($this->image,$filename);
                break;
            case IMAGETYPE_PNG :
                imagepng($this->image,$filename);
                break;
        }

		if ($permissions != null) {
			chmod($filename,$permissions);
        }

        // Optimize the image as much as possible
        // $this->optimize($filename);

        return $filename;
    }

    /**
     * Outputs an image resource to the browser.
     * 
     * @todo Testing actual out put, see: https://gist.github.com/miguelxt/908143#gistcomment-1948545
     * 
     * @param   String  $image_type     An IMAGETYPE_* constant. Defaults to IMAGETYPE_JPEG. IMAGETYPE_GIF and IMAGETYPE_PNG are supported by this library
     * @param   Int     $quality        The compression quality: 0-100
     * @return  Void
     */    
    public static function Output( int $image_type = IMAGETYPE_JPEG, int $quality = self::IMAGE_QUALITY ) {
        $instance = self::getInstance();
        try {
            $instance->img_output( $image_type, $quality );
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

	private function img_output( int $image_type = IMAGETYPE_JPEG, int $quality = self::IMAGE_QUALITY ) {

        switch( $image_type ) {
            case IMAGETYPE_JPEG :
                header("Content-type: image/jpeg");
                imagejpeg($this->image, null, $quality);
                break;
            case IMAGETYPE_GIF :
                header("Content-type: image/gif");
                imagegif($this->image);         
                break;
            case IMAGETYPE_PNG :
                header("Content-type: image/png");
                imagepng($this->image);
                break;
        }
        
        if( imagedestroy( $this->image ) ) {
            unset($this->image);
        }

        die();
	}

    # +------------------------------------------------------------------------+
    # HELPERS: DIMENSIONS
    # +------------------------------------------------------------------------+
    /**
     * Get the width of an image resource.
     * 
     * @return Int  The width of an image resource
     */
	public static function Width() {
        $instance = self::getInstance();
        try {
            return $instance->img_getWidth();
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

	private function img_getWidth() {
		return imagesx($this->image);
	}

    /**
     * Get the height of an image resource.
     * 
     * @return Int  The height of an image resource
     */
    public static function Height() {
        $instance = self::getInstance();
        try {
            return $instance->img_getHeight();
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

	private function img_getHeight() {
		return imagesy($this->image);
    }

    /**
     * Get the orientation of a loaded image.
     * 
     * @return  Int  Returns the orientation as a class constant, ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT or ORIENTATION_SQUARE
     */
    public static function Orientation(bool $as_string = false) {
        $instance = self::getInstance();
        try {
            return $instance->img_getOrientation($as_string);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

    private function img_getOrientation(bool $as_string = false) {
        $dimensions = $this->image_dimensions;
        $width      = $dimensions['width'];
        $height     = $dimensions['height'];
    
        if($width > $height) $result = self::ORIENTATION_LANDSCAPE;
        if($width < $height) $result = self::ORIENTATION_PORTRAIT;
        if($width == $height) $result = self::ORIENTATION_SQUARE;

        if( $as_string ) {
            switch( $result ) {
                case Image::ORIENTATION_LANDSCAPE :
                    $result = 'landscape';
                    break;
                case Image::ORIENTATION_PORTRAIT :
                    $result = 'portrait';
                    break;
                case Image::ORIENTATION_SQUARE :
                    $result = 'square';
                    break;
            }
        }        

        return $result;
    }    
    
    /**
     * Get the width and height of an image.
     * 
     * @return Array    An associative array containing the Width and Height of an image resource.
     */
    public static function Dimensions() {
        $instance = self::getInstance();
        try {
            if( (null == $this->image_dimensions) || (null == $this->image_info) ) {
                $this->img_getImageInfo();
            }
            return $instance->image_dimensions;
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

    private function img_getImageInfo($filename = null) {
        
        $filename           = null != $filename ? $filename : $this->image;
        $image_info         = getimagesize($filename);
        $this->image_info   = $image_info;

        if( null == $this->image_exif ) {
            $this->exif = @exif_read_data($this->image);
        }

        $this->image_pathinfo = pathinfo( $this->image );

        $this->image_originalfilename = $this->image_pathinfo['basename'];
        
        if( !empty($image_info) && isset($image_info[2]) ) {
            $this->image_type   = $image_info[2];
        }

        if( !empty($image_info) && (isset($image_info[0]) && isset($image_info[1])) ) {
            $this->image_dimensions = [
                'width' => $image_info[0],
                'height' => $image_info[1],
            ];
        }

        return $this->image_info;
    }

    # +------------------------------------------------------------------------+
    # METHODS: IMAGE OPERATIONS
    # +------------------------------------------------------------------------+
    
    /**
     * Proportionately resize an image to a specified height.
     * 
     * @param   Int     $height The final height of the image resource.
     * @return  Image   Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
    public static function ResizeHeight(int $height) {
        $instance = self::getInstance();
        try {
            return $instance->img_resizeHeight($height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

    private function img_resizeHeight(int $height, $filename = null) {
        $dimensions = $this->image_dimensions;
		$ratio = $height / $dimensions['height'];
		$width = intval($dimensions['width'] * $ratio);
        $this->image_newfilename = $filename ?: $this->filenameFromMethodAndArgs('ResizeHeight',[$width, $height]);

        $this->img_resize($width,$height, $this->image_newfilename);
        
        return $this;
	}

    /**
     * Proportionately resize an image to a specified width.
     * 
     * @param   Int    $width The final width of the image resource.
     * @return  Image  Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
	public static function ResizeWidth(int $width) {
        $instance = self::getInstance();
        try {
            return $instance->img_resizeWidth($width);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }        

	private function img_resizeWidth(int $width, $filename = null) {
		$ratio = $width / $this->img_getWidth();
		$height = intval($this->img_getHeight() * $ratio);
        $this->image_newfilename = $filename ?: $this->filenameFromMethodAndArgs('ResizeWidth',[$width, $height]);

        $this->img_resize($width,$height, $this->image_newfilename);
        
        return $this;
	}

    /**
     * Proportionately resize an image to a square.
     * 
     * @param   Int     $size   The final width and height of the image resource.
     * @return  Image   Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
    public static function Square(int $size) {
        $instance = self::getInstance();
        try {
            return $instance->img_square($size);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }   

	private function img_square(int $size) {
        $this->image_newfilename = $this->filenameFromMethodAndArgs('Square',[$size,$size]);

        $dimensions = $this->image_dimensions;
        $new_image = imagecreatetruecolor($size, $size);
        
		if ($dimensions['width'] > $dimensions['height']) {
			$this->img_resizeHeight($size, $this->image_newfilename);
			
			imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
			imagealphablending($new_image, false);
			imagesavealpha($new_image, true);
			imagecopy($new_image, $this->image, 0, 0, ($this->img_getWidth() - $size) / 2, 0, $size, $size);

		} else {
			$this->img_resizeWidth($size, $this->image_newfilename);
			
			imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
			imagealphablending($new_image, false);
			imagesavealpha($new_image, true);
			imagecopy($new_image, $this->image, 0, 0, 0, ($this->img_getHeight() - $size) / 2, $size, $size);
		}

        $this->image = $new_image;

        return $this;
	}
   
    /**
     * Proportionately scale an image by a given factor. Expects a value between 0-1.
     * 
     * @param   Float   $scale   The scaling factor. Expects a value between 0-1, e.g. 0.5
     * @return  Image   Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
	public static function Scale(float $scale) {
        $instance = self::getInstance();
        try {
            return $instance->img_scale($scale);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }  

	private function img_scale(float $scale) {
        if( !is_float($scale) ) {
            throw new Exception("$scale must be of type float");
            die();
        }

        $dimensions = $this->image_dimensions;
        $w = $dimensions['width'];
        $h = $dimensions['height'];

        $width = intval($w * $scale);
        $height = intval($h * $scale); 

        $this->image_newfilename = $this->filenameFromMethodAndArgs('Scale',[$width,$height]);

        try {
            $this->img_resize($width,$height, $this->image_newfilename);

        } catch( \Exception $e ) {
            Log::log($e->getMessage());
            echo $e->getMessage();
        }


        return $this;
	}
   
    /**
     * Proportionately resize an image to a specific width and height.
     * 
     * @param   Int     $width The final width of the image resource.
     * @param   Int     $height The final height of the image resource.
     * @return  Image   Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */    
	public static function Resize(int $width, int $height) {
        $instance = self::getInstance();
        try {
            return $instance->img_resize($width,$height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }  

	private function img_resize(int $width, int $height, $filename = null) {
        $this->image_newfilename = $filename ?: $this->filenameFromMethodAndArgs('Resize',func_get_args());
        
		$new_image = imagecreatetruecolor($width, $height);
		
		imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
		imagealphablending($new_image, false);
		imagesavealpha($new_image, true);
		
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->img_getWidth(), $this->img_getHeight());
        $this->image = $new_image;   

        return $this;
	}

    /**
     * Crop an image to the specified dimentions, at specified coordinates
     * 
     * @param   Int $width  The final width of the image resource.
     * @param   Int $height The final height of the image resource.
     * @param   Int $x      X-Axis crop coordinate, defaults to 0;
     * @param   Int $y      Y-Axis crop coordinate, defaults to 0;
     * @return  Image       Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
    public static function Crop(int $width, int $height, int $x = 0, int $y = 0 ) {
        $instance = self::getInstance();
        try {
            return $instance->img_crop($x,$y,$width,$height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }  

    private function img_crop(int $x, int $y, int $width, int $height) {
        $this->image_newfilename = $this->filenameFromMethodAndArgs('Crop',func_get_args());

        $new_image = imagecreatetruecolor($width, $height);	

		imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
		imagealphablending($new_image, false);
		imagesavealpha($new_image, true);
		imagecopy($new_image, $this->image, 0, 0, $x, $y, $width, $height);

        $this->image = $new_image;
        
        return $this;
    }
    
    /**
     * Crops an image starting from the cennter of the image, outwards the image boundaries.
     * 
     * @param   Int $width  The final width of the image resource.
     * @param   Int $height The final height of the image resource.
     * @return  Image       Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
    public static function CropFromCenter(int $width, int $height) {
        $instance = self::getInstance();
        try {
            return $instance->img_cropFromCenter($width,$height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

    private function img_cropFromCenter(int $width, int $height) {
        if( empty($this->image_dimensions) ) {
            $this->img_getImageInfo();
        }
        
        $dimensions = $this->image_dimensions;
        $w = $dimensions['width'];
        $h = $dimensions['height'];

        $ratio = $w/$h;
        if ($w < $h && $w > $width) {
            $after_resize_h = intval(($h / $w) * $width);
            if ($after_resize_h < $height) {
                $this->img_resizeHeight($height);
            }else{
                $this->img_resizeWidth($width);
            }
        } elseif ($w > $h && $h > $height) {

            $after_resize_w = intval(($w / $h) * $height);
            if($after_resize_w < $width) {
                $this->img_resizeWidth($width);
            } else {
                $this->img_resizeHeight($height);
            }
        }

        $x = ($this->img_getWidth() / 2) - ($width / 2);
        $y = ($this->img_getHeight() / 2) - ($height / 2);        

        $this->img_crop($x, $y, $width, $height);

        $this->image_newfilename = $this->filenameFromMethodAndArgs('CropFromCenter',func_get_args());

        return $this;
    }    

    /**
     * Resize an image, forcing the resulting image to fit within the maximum given width or height.
     * Downsamples the image's to fit within the desired width or height.
     */
	public static function FitImageMax(int $width, int $height = null) {
        $instance = self::getInstance();
        try {
            return $instance->img_maxarea($width,$height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }  
    
	private function img_maxarea(int $width, int $height = null) {
        $this->image_newfilename = $this->filenameFromMethodAndArgs('FitImageMax',func_get_args());
        // Log::trace(__METHOD__, $this->image_newfilename);

        $dimensions = $this->image_dimensions;
		$height = $height ? $height : $width;
		
		if ($dimensions['width'] > $width) {
			$this->img_resizeWidth($width, $this->image_newfilename);
		}
		if ($dimensions['height'] > $height) {
			$this->img_resizeHeight($height, $this->image_newfilename);
        }

        return $this;
	}
    
    /**
     * Resize an image, forcing the resulting image to fit within the maximum given width or height.
     * Upsamples the image's to fit within the desired width or height.
     */
	public static function FitImageMin(int $width, int $height = null) {
        $instance = self::getInstance();
        try {
            return $instance->img_minarea($width,$height);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }   

	private function img_minarea(int $width, int $height = null) {
        $this->image_newfilename = $this->filenameFromMethodAndArgs('FitImageMin',func_get_args());

        $dimensions = $this->image_dimensions;
		$height = $height ? $height : $width;
		
		if ($dimensions['width'] < $width) {
			$this->img_resizeWidth($width, $this->image_newfilename);
		}
		if ($dimensions['height'] < $height) {
			$this->img_resizeHeight($height, $this->image_newfilename);
        }

        return $this;
    }

    /**
     * Fills a given area with an image.
     * 
     * @param   Int     $width      The final width of the image resource.
     * @param   Int     $height     The final height of the image resource.
     * @param   String  $hex_color  A hexadecimal color string. Accepts either short or long hex strings, e.g. #fff or #ffffff
     * @return  oImage  Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */    
	public static function MaxAreaFill(int $width, int $height, string $hex_color = '#ffffff') {
        $instance = self::getInstance();
        try {
            return $instance->img_maxareafill($width, $height, $hex_color);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

	private function img_maxareafill(int $width, int $height, string $hex_color = '#ffffff') {
        $this->image_newfilename = $this->filenameFromMethodAndArgs('MaxAreaFill',str_replace('#','_',func_get_args()));

	    $this->img_maxarea($width, $height);
        $new_image = imagecreatetruecolor($width, $height); 
        $color_bg = $this->HEXToRGB($hex_color);
	    $color_fill = imagecolorallocate($new_image, $color_bg['r'], $color_bg['g'], $color_bg['b']);
	    imagefill($new_image, 0, 0, $color_fill);        
	    imagecopyresampled(	$new_image, 
	    					$this->image, 
	    					intval(($width - $this->img_getWidth())/2), 
	    					intval(($height-$this->img_getHeight())/2), 
	    					0, 0, 
	    					$this->img_getWidth(), 
	    					$this->img_getHeight(), 
	    					$this->img_getWidth(), 
	    					$this->img_getHeight()
	    				); 
        $this->image = $new_image;

        return $this;
    }

    /**
     * Flip and image Vertically, Horizontally, or both.
     * 
     * @param   Int     $direction  Determine the flip direction. Expects IMG_FLIP_HORIZONTAL, IMG_FLIP_VERTICAL, IMG_FLIP_BOTH
     * @return  Image   Returns the current AUX\Utils\Image instance. AUX\Utils\Image::Save() must be called to complete the operation.
     */
    public static function Flip(int $direction) {
        $instance = self::getInstance();
        try {
            return $instance->img_flip($direction);
        } catch( \Exception $e ) {
            Log::log( $e->getMessage() );
            echo $e->getMessage();
        }
    }

    private function img_flip(int $direction) {
        switch($direction) {
            case 'x':
                imageflip($this->image, IMG_FLIP_HORIZONTAL);
                break;
            case 'y':
                imageflip($this->image, IMG_FLIP_VERTICAL);
                break;
            case 'both':
                imageflip($this->image, IMG_FLIP_BOTH);
                break;
        }

        return $this;
    }

    # +------------------------------------------------------------------------+
    # HELPER METHODS
    # +------------------------------------------------------------------------+
    /**
     * Converts a hexadecimal string to an rgb string
     * 
     * @example
     * 
     * Returns #ffffff ->[ 'r' => 255, 'g' => 255, 'b' => 255 ] or #ffffff -> rgb(255,255,255)
     * 
     * @param   String  $hext       A valid hexadecimal color
     * @param   Bool    $as_array   Define whether to return an array or string.
     * @return  Array|String    Output determined by $as_array
     */
    private function HEXToRGB(string $hex, bool $as_array = true) {
        $color      = ltrim($hex, '#');
        $split_len  = (strlen( $color ) == 6) ? 2 : 1;
        $rgb_value  = str_split( $color, $split_len);

        if( $split_len  == 2 ) {
            $r = hexdec($rgb_value[0]);
            $g = hexdec($rgb_value[1]);
            $b = hexdec($rgb_value[2]);                
        }else{
            $r = hexdec($rgb_value[0].$rgb_value[0]);
            $g = hexdec($rgb_value[1].$rgb_value[1]);
            $b = hexdec($rgb_value[2].$rgb_value[2]);    
        }

        $result = $as_array ? [ 'r' => $r, 'g' => $g, 'b' => $b, ] : "rgb(" . $r . ", " . $g . ", " . $b . ")";
        return $result;
    }

    /**
     * Generates a safe file name from a given Image::{method} and arguments list.
     * 
     * @param   String  $method_name    Any string, typically a method name without the name-spacing
     * @param   Array   $size_params    The arguments list used to generate the resulting image.
     * @return  String  The resulting, new, filename with the method and arguments appended as a tag.
     */
    private function filenameFromMethodAndArgs( $method_name, array $size_params = null, $filename = null ) {
        if( null == $filename ) {
            $path_info          = $this->image_pathinfo;
        }else{
            $path_info = pathinfo( $filename );
            $this->image_pathinfo = $path_info;
        }

        $clean_filename     = $this->sanitize( $path_info['filename'] );
        $method_name_clean  = $this->sanitize( $method_name );
        $size_tag           = '';
        $sformat            = "%s_%s.%s";

        if( null != $size_params && !empty($size_params) ) {
            $sformat            = "%s_%s_%s.%s";
            $size_tag           = implode('x',$size_params);
            $new_filename       = sprintf($sformat, $clean_filename, $method_name_clean, $size_tag,$path_info['extension']);
            // Log::trace(__METHOD__, $sformat, $size_tag, $new_filename);
        }else{            
            $new_filename       = sprintf($sformat, $clean_filename, $method_name_clean, $path_info['extension']);
        }

        return $new_filename;
    }

    /**
     * Basic string santization with unicode support.
     * 
     * @param   String  The string to sanitize
     * @return  String  The sanitized string.
     */
    private function sanitize( $string ) {
        $clean_string = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $string);
        $clean_string = mb_ereg_replace("([\.]{2,})", '', $clean_string);
        return $clean_string;
    }

    private function optimize( $image_asset ) {
        try {
            $optimizer = OptimizerChainFactory::create();
            $optimizer->optimize($image_asset);
            return $image_asset;
        }catch( \Exception $e ) {
            Log::log($e->getMessage(), Log::LOG_TYPE_ERROR);
        }

        return $image_asset;
    }

    public static function readable_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    /**
     * Determine if this file has been previously processed with the given parameters of the calling method
     * 
     * @param   String  $filename The basename of the file, e.g. without directory
     * @return  Bool    Returns true if the file has been cached, false if otherwise
     */
    private function cache( string $filename ) {
        $tmp_file           = $this->image_pathinfo['dirname'] . DIRECTORY_SEPARATOR . $filename;
        $cached             = file_exists($tmp_file);
        $this->cached_file  = $cached ? $tmp_file : null;
        return $cached;
    }

    public function isCachedImage($method, $method_arguments, $filename = null) {
        $this->image_newfilename = $this->filenameFromMethodAndArgs( $method, $method_arguments, $filename );
        $cached = $this->cache( $this->image_newfilename );        
        return $cached ? $this->cached_file : false;
    }

    /**
     * Fix JPEG image orientation based on the EXIF data
     */
    private function orientJPEGImageFromEXIF() {

        // Handle orientation based on EXIF data
        if( !empty($this->exif['Orientation']) ) {
            switch($this->exif['Orientation']) {
                case 2: // Flip horizontally
                    imageflip($this->image, IMG_FLIP_HORIZONTAL);
                    break;
                case 3: // Rotate 180 degrees
                    $this->image = imagerotate($this->image,180 ,0); 
                    break;
                case 4: // Flip vertically
                    imageflip($this->image, IMG_FLIP_VERTICAL);
                    break;
                case 5: // Rotate 90 degrees clockwise and flip vertically
                    imageflip($this->image, IMG_FLIP_VERTICAL);
                    $this->image = imagerotate($this->image,90, 0); ;
                    break;
                case 6: // Rotate 90 clockwise
                    $this->image = imagerotate($this->image,90, 0); ;
                    break;
                case 7: // Rotate 90 clockwise and flip horizontally
                    imageflip($this->image, IMG_FLIP_HORIZONTAL);
                    $this->image = imagerotate($this->image,90, 0); ;
                    break;
                case 8: // Rotate 90 counterclockwise
                    $this->image = imagerotate($this->image,-90 ,0); 
                    break;
                case 1: // Do nothing!
                default :
                    break;

            }            
        }  

    }
    
    # +------------------------------------------------------------------------+
    # CLEANUP
    # +------------------------------------------------------------------------+
    public function __destruct() {
        if (is_resource($this->image) && 'gd' == get_resource_type($this->image)) {
            imagedestroy($this->image);
        }
    }

}