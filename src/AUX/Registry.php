<?php
namespace AUX;

use AUX\Structs\SplStorageObject;
use AUX\Utils\Log;

/**
 * Registry
 *
 * @author Chris Murphy
 * @version v0.0.1
 */
class Registry {
    # +------------------------------------------------------------------------+
    # MEMBERS
    # +------------------------------------------------------------------------+
    private $properties;

    # +------------------------------------------------------------------------+
    # ACCESSORS
    # +------------------------------------------------------------------------+
    /**
     * @set     mixed   Objects
     * @param   string  $index  A unique index
     * @param   mixed   $value  Objects to be stored in the registry
     * @return  void
     */
    public function set($index, $value) {
        $so = new SplStorageObject($value);
        $this->properties->attach($so, $index);
    }

    public function __set($index, $value) {
        call_user_func_array([$this,'set'], func_get_args());
    }

    /**
     * @get     mixed   Objects stored in the registry
     * @param   string  $index  A unique ID for the object
     * @return  object  Returns a object used by the core application.
     */
    public function get($index) {
        // Log::trace(__METHOD__, $index, $this->search($index));
        return $this->search($index);
    }

    public function __get($index) {
        return call_user_func_array([$this,'get'], func_get_args());
    }

    public function all( $as_json = false ) {
        $results = [];
        
        $this->properties->rewind();
        while( $this->properties->valid() ) {
            $so = $this->properties->current();
            Log::trace(__METHOD__, $so);

            $results[$this->properties->getInfo()] = $so->value();
            $this->properties->next();
        }

        return $as_json ? json_encode($results) : $results;
    }    
    
    public function replace( $index, $new_value ) {
        $value = new SplStorageObject($new_value);
        if( $this->properties->contains( $value ) ) {
            $current_value = $this->get( $index );
            $this->properties->detach($current_value);
        }
        $this->properties->attach( $value, $index );
    }

    protected function search( $needle ) {
        $this->properties->rewind();

        while( $this->properties->valid() ) {
            $obj_index   = $this->properties->getInfo();
            
            if( $obj_index === $needle ) {
                $so = $this->properties->current();
                return $so->value();
            }
            
            $this->properties->next();
        }

        return null;
    }
    
    # +------------------------------------------------------------------------+
    # CONSTRUCTOR
    # +------------------------------------------------------------------------+
    public function __construct() {
        $this->properties = new \SplObjectStorage();
    }

    public function __toString() {
        return $this->properties->serialize();
    }
    

}