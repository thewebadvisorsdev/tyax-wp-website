<?php

namespace AUX\HTTP;

class RestUtils {

    const ERROR_UNKNOWN             = 0;
    const ERROR_UNAUTHORIZED_ACCESS = 1;
    const ERROR_MISSING_PRIVATE_KEY = 2;
    const ERROR_INVALID_NONCE       = 3;
    const ERROR_INVALID_APITOKEN    = 4;
    const ERROR_WRONG_HTTP_METHOD   = 5;
    const ERROR_METHOD_NOT_ALLOWED  = 6;
    const ERROR_METHOD_NOT_FOUND    = 7;

    protected static $error_messages = [
        self::ERROR_UNKNOWN                 => "Unknown error.",
        self::ERROR_UNAUTHORIZED_ACCESS     => "Unauthorized Access.",
        self::ERROR_MISSING_PRIVATE_KEY     => "Missing private key.",
        self::ERROR_INVALID_NONCE           => "Invalid nonce.",
        self::ERROR_INVALID_APITOKEN        => "Invalid API token.",
        self::ERROR_WRONG_HTTP_METHOD       => "Invalid HTTP method.",
        self::ERROR_METHOD_NOT_ALLOWED      => "Method not allowed.",
        self::ERROR_METHOD_NOT_FOUND        => "Invalid method.",
    ];
    
    public static function getAPIEndpoint($endpoint, array  $params = null) {
        $endpoint_params = '';
        
        if( null != $params ) {
            $params = array_map('strtolower', $params);
            $endpoint_params = join('/', $params);
        }

        $endpoint   = '/' . ltrim($endpoint, '\/');
        $endpoint   = sprintf('%s/%s', $endpoint, $endpoint_params);
        $endpoint   = rtrim( $endpoint, '\/');

        return $endpoint;
    }

    public static function getResponseMessage( $error_code = self::ERROR_UNKNOWN ) {
        return new \Error( self::$error_messages[ $error_code ] ?? self::$error_messages[ 0 ] , $error_code);
    }
}