<?php

namespace AUX;

/**
 * Generic database settings
 * 
 * @author Chris Murphy
 * @version 1.0
 */
class DBSettings {

    public $db_name;
    public $db_user;
    public $db_pass;
    public $db_host;
    public $db_prefix;
    public $db_charset;
    public $db_collate;
    public $db_driver;

    public function __construct( $ENV ) {
        $this->init($ENV);
    }

    private function init($ENV) {
        $env_keys = array_keys($ENV);
        while( null !== ($key = array_shift($env_keys) ) ) {
            $k = strtolower($key);
            switch( $key ) {
                case 'DB_DRIVER':
                    $this->$k = getenv($key) ? getenv($key) : 'pdo_mysql';
                    break;
                case 'DB_PREFIX':
                    $this->$k = getenv($key) ? getenv($key) : 'wp_';
                    break;
                case 'DB_CHARSET':
                    $this->$k = getenv($key) ? getenv($key) : 'utf8';
                    break;
                default :
                    $this->$k = getenv($key);
                    break;
            }
        }
    }

    /**
     * Returns the settings as an array map
     * @link https://github.com/spotorm/spot2/issues/227#issuecomment-289147758     
     * @return Array
     */
    public function asConnectorObject() {
        return [
            'dbname'    => $this->db_name,
            'user'      => $this->db_user,
            'password'  => $this->db_password,
            'host'      => $this->db_host,
            'driver'    => $this->db_driver,
            'charset'   => 'utf8mb4'
        ];
    }

    public function asConnectionString() {
        return sprintf("mysql://%s:%s@%s/%s", $this->db_user, $this->db_password, $this->db_host, $this->db_name);
    }
}