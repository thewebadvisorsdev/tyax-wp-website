<?php
namespace AUX\Structs;

class Queue {
	/**
	 * Queue
	 * 
	 * @var array
	 */
	protected $__queue = array();
	
	/**
	 * Init queue (optional)
	 * 
	 * @param array $queue_items
	 * @return void
	 */
	public function __construct($queue_items = array())
	{
		if(is_array($queue_items)) {
			$this->__queue = $queue_items;
		}
	}
	
	/**
	 * Clears all items in queue
	 * 
	 * @return void
	 */
	public function clear() {
		$this->__queue = array();
	}
	
	/**
	 * Check if queue contains item
	 * 
	 * @param mixed $item
	 * @return boolean 
	 */
	public function contains($item) {
		foreach($this->__queue as $v)
		{
			if($item === $v)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets front item in queue and removes from queue
	 * 
	 * @return mixed
	 */
	public function dequeue() {
		return array_shift($this->__queue);
	}
	
	/**
	 * Add item to back of queue
	 * 
	 * @param mixed $item
	 * @return void
	 */
	public function enqueue($item) {
		$this->__queue[] = $item;
	}
	
	/**
	 * Gets front item in queue without removing it from queue
	 * 
	 * @return mixed
	 */
	public function peek() {
		return current($this->__queue);
	}
}