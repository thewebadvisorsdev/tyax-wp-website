<?php

namespace AUX\Structs;

class SplStorageObject {
    protected $item;
    protected $timestamp;

    public function value( $value = null ) {
        if( null != $value ) {
            $this->item = $value;
            return true;
        }
        return $this->item;
    }

    public function __construct($value) {
        $this->item = $value;
        $this->timestamp = microtime();
    }
}