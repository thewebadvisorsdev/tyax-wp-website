<?php

namespace APP;

use AUX\DBSettings;
use AUX\Utils\Log;
use Dotenv\Dotenv;
use Spot\Config;
use Spot\Locator;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Extension\ImageExtension;
use Twig\Extension\MarkupUtilsExtension;
use Twig\Extension\StringLoaderExtension;
use Twig\Loader\FilesystemLoader;
use Twig\SwitchTokenParser;
use WPX\Enums\EnumEnvironment;

class App {

    protected static $instance;
    protected static $autoload;
    protected $required_constants = [
        'WordPress' => [
            'WP_ENVIRONMENT',
            'WP_HOME',
            'WP_SITEURL',
            'WP_SUBDIRECTORY',
            'WP_RESOURCES',
        ],
        'App' => [
            'DB_NAME',
            'DB_USER',
            'DB_PASSWORD',
            'DB_HOST',
            'DB_PREFIX'
        ]
    ];
    protected $__config = [];
    protected $__db;
    protected $__twig;

    # +------------------------------------------------------------------------+
    # ACCESSORS
    # +------------------------------------------------------------------------+
    /**
     * Get the value of a configuration setting. Read-only.
     * 
     * @param string $key The name of a configuration property; if null, returns the complete collection of settings an associative array
     */
    public static function config( $key = null ) {
        $inst = self::inst();
        return (null == $key) ? $inst->__config : $inst->__config[ $key ] ?: null;
    }
    
    /**
     * Get the value of a database setting. Read-only.
     * 
     * @param string [$key] The name of a database property; If null, returns the complete collection of database settings an associative array
     */
    public static function db( $key = null ) {
        $inst = self::inst();
        if( null != $key ) {
            $result = property_exists(DBSettings::class, $key) ? $inst->__db->{$key} : null;
        }
        return (null == $key) ? $inst->__db : $result;
    }

    /**
     * Get the current autoloader instance
     */
    public static function autoloader( $autoloader = null ) {
        return ( null == $autoloader ) ? static::$autoload : static::$autoload = $autoloader;
    }

    # +------------------------------------------------------------------------+
    # SINGLETON CONSTRUCTOR
    # +------------------------------------------------------------------------+
    private function __construct() {}

    public static function inst() {
        return (null != static::$instance) ? static::$instance : static::$instance = new static();
    }

    # +------------------------------------------------------------------------+
    # INITIALIZE APP
    # +------------------------------------------------------------------------+
    /**
     * Initialize our application bootstrap to pre-define all of the necessary
     * WordPress constants along side our own application-specific constants.
     * Configuration values are read from out `.env` file, defined globally,
     * and encapsulated locally in our `$__config` to keep things tidy and
     * accessible in our application, e.g. plugins, etc.
     * 
     * @param string $application_root The root of our application, typically one directory above the DOCUMENT_ROOT
     * @return App Returns the current instance of our application singleton.
     */
    public function init( $application_root = null ) {

        // Register a shutdown handler
        register_shutdown_function([static::inst(), 'shutdown']);

        // Error logging
        $this->errorhandling();
        
        $app_root = $application_root ?: APP_PATH;
        $this->__config['application_root'] = $app_root;
        $this->__config['document_root'] =  sprintf("%s%s",$app_root, "public_html");


        $env = Dotenv::create(APP_PATH);
        $env->load();

        try {
            // Load WordPress settings
            $this->environment( $env );

            // Load database settings & establish a connection
            $this->database( $env );
            $this->connect();

            // Load all other config variables
            $this->load( $env );

            // Twig templating engine
            $this->twig();
        }catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }

        return static::$instance;
    }

    /**
     * Load all non-specific environement variables
     */
    protected function load( Dotenv $env_loader ) {
        // WordPress Environment
        $env_keys = $env_loader->getEnvironmentVariableNames();
        while( null !== ($key = array_shift($env_keys) ) ) {
            if( !in_array(substr($key,0,3), ['WP_', 'DB_']) ) {
                $this->__config[ $key ] = getenv($key) ?: null;
                define($key, getenv($key));
            }
        }

        return static::$instance;
    }

    /**
     * Load environment variables specific to WordPress
     */
    protected function environment( Dotenv $env_loader ) {        
        if( $env_loader->required( $this->required_constants['WordPress'] ) ) {
            $env_keys = $env_loader->getEnvironmentVariableNames();
            while( null !== ($key = array_shift($env_keys) ) ) {
                if (strpos($key, 'WP_') !== false) {
                    switch( $key ) {
                        case 'WP_FORCE_SSL_LOGIN':
                        case 'WP_FORCE_SSL_ADMIN':
                        case 'WP_DISALLOW_FILE_EDIT':
                            define( str_replace('WP_','', $key) , !!getenv($key) );
                            $this->__config[str_replace('WP_','', $key)] = !!getenv($key);
                            break;
                        case 'WP_POST_REVISIONS':
                        case 'WP_AUTOSAVE_INTERVAL':
                            define( str_replace('WP_','', $key) , getenv($key) );
                            $this->__config[str_replace('WP_','', $key)] = !!getenv($key);
                            break;        
                        case 'WP_MEMORY_LIMIT':
                            $memory = getenv($key);
                            define( 'WP_MEMORY_LIMIT', $memory );
                            @ini_set( 'upload_max_size' , $memory );                
                            $this->__config[$key] = getenv($key);
                            break;
                        case 'WP_DISALLOW_FILE_MODS':
                            $val = (getenv($key) == 1) || (self::config('environment') == EnumEnvironment::PRODUCTION);
                            define('DISALLOW_FILE_MODS', $val);
                            $this->__config['DISALLOW_FILE_MODS'] = $val;
                            break;
                        case 'WP_DISALLOW_FILE_EDIT':
                            define('WP_DISALLOW_FILE_EDIT', !!(getenv($key) == 1));
                            $this->__config['WP_DISALLOW_FILE_EDIT'] = !!(getenv($key) == 1);
                            break;
                        default:
                            define($key, getenv($key));
                            $this->__config[$key] = getenv($key);
                            break;
                    }
                }
            }

            // WordPress constants
            $this->__config['environment']        = getenv('WP_ENVIRONMENT');
            $this->__config['base_url']           = getenv('WP_HOME');
            $this->__config['site_url']           = getenv('WP_SITEURL');
            $this->__config['subdirectory']       = getenv('WP_SUBDIRECTORY');
            $this->__config['contentdirectory']   = getenv('WP_RESOURCES');
            $this->__config['site_title']         = getenv('WP_SITETITLE');     
            $this->__config['cache_location']     = APP_PATH . '/cache/';     
            
            // The new content path
            define('WP_CONTENT_DIR', self::config('document_root') . "/" . self::config('contentdirectory') );
            define('WP_CONTENT_URL', self::config('base_url') . "/" . self::config('contentdirectory') );

            // Uploads directory
            define('WP_UPLOADS_DIR', WP_CONTENT_DIR . '/uploads');
            define('WP_UPLOADS_URL_ABSOLUTE', WP_CONTENT_URL . '/uploads');
            define('WP_UPLOADS_URL_RELATIVE', self::config('contentdirectory') . '/uploads');

            // Common Plugin directories
            define('WP_PLUGIN_DIR', sprintf("%s/%s/plugins", self::config('document_root'), self::config('contentdirectory') ) );
            define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');

            // MU-Plugins
            define('WPMU_PLUGIN_DIR', sprintf("%s/%s/mu-plugins", self::config('document_root'), self::config('contentdirectory') ) );
            define('WPMU_PLUGIN_URL', WP_CONTENT_URL . '/mu-plugins');

            // Caching
            define('WP_CACHE_DIR', APP_PATH . '/cache/');

            // tmp directory
            $this->__config['tmp_dir'] = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();

        }else{
            throw new \Exception("Missing critical settings. Please check your configuration settings.");
        }

        return static::$instance;
    }
    
    /**
     * Load environment variables specific to the database
     */
    protected function database( Dotenv $env_loader ) {
        if( $env_loader->required( $this->required_constants['App'] ) ) {
            $env_keys = $env_loader->getEnvironmentVariableNames();
            $db_settings    = [];

            while( null !== ($key = array_shift($env_keys) ) ) {
                if (strpos($key, 'DB_') !== false) {
                    switch( $key ) {
                        case 'DB_CHARSET' :
                        case 'DB_COLLATE' :
                            define($key, getenv($key) ?: $key == 'DB_CHARSET' ? 'utf8' : 'utf8mb4_unicode_ci');
                            $db_settings[ $key ] = getenv($key);
                            break;
                        default: 
                            $db_settings[ $key ] = getenv($key);
                            define( $key, getenv($key) );
                            break;
                    }
                }
            }
    

            // Database constants
            $this->__db = new DBSettings($db_settings);

        }else{
            throw new \Exception("Missing critical database settings. Please check configuration settings.");
        }

        return static::$instance;
    }


    /**
     * Twig instance
     */
    public function twig() {

        if ( (null !== $this->__twig) && (null != self::config('template_path')) ) return $this->__twig;

        $this->__config['template_path'] = sprintf("%sbuild/themes/%s/templates", self::config('application_root'), self::config('WP_DEFAULT_THEME'));

        try {
            $template_root = self::config('template_path');
            $autoload_twig = new FilesystemLoader( $template_root );
            
            $theme_templates = [
                'Includes' => sprintf("%s/Includes", $template_root),
                'Components' => sprintf("%s/Components", $template_root),
                'Layout' => sprintf("%s/Layout", $template_root)
            ];

            $admin_templates = [
                'templates' => sprintf("%ssrc/resources/assets/templates", self::config('application_root')),
                'Includes' => sprintf("%ssrc/resources/assets/Includes", self::config('application_root')),
                'Layout' => sprintf("%ssrc/resources/assets/Layout", self::config('application_root')),
            ];

            // Theme Templates
            foreach( $theme_templates as $dir => $path ) {
                if( file_exists( $path ) && is_dir($path) ) {
                    $autoload_twig->addPath($path);
                }
            }
            
            // Model Admin Templates
            foreach( $admin_templates as $dir => $path ) {
                if( file_exists( $path ) && is_dir($path) ) {
                    $autoload_twig->addPath($path);
                }
            }

            // Twig Extensions
            $twig = 
            $twig = new Environment($autoload_twig);
            $twig->addTokenParser( new SwitchTokenParser() );
            $twig->addExtension( new StringLoaderExtension() );
            $twig->addExtension( new \Twig_Extensions_Extension_Text() );
            $twig->addExtension( new DebugExtension() );

            // Custom Extensions
            $twig->addExtension( new ImageExtension() );
            $twig->addExtension( new MarkupUtilsExtension() );

            $this->__twig = $twig;

        } catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
            throw $e;
        }

        return $this->__twig;
    }

    # +------------------------------------------------------------------------+
    # UTILITY
    # +------------------------------------------------------------------------+
    /**
     * Establish a database connection for use with our ORM
     */
    protected function connect() {
        try {
            $db_settings = $this->__db;
            if( null !== $db_settings ) {
                $cfg = new Config();
                $cfg->addConnection('mysql', $db_settings->asConnectorObject());
                $this->__config['database'] = $cfg;
                $this->__config['locator'] = new Locator( $this->__config['database'] );
            }
        }catch( \Exception $e ) {
            Log::log( $e->getMessage() . "\n" . $e->getTraceAsString(), Log::LOG_TYPE_ERROR);
        }
    }
    
    /**
     * A shutdown function with custom logging
     */
    public function shutdown() {
        $error = error_get_last();
        if ($error['type'] === E_ERROR) { 
            Log::trace(__METHOD__, $error, E_ERROR, Log::LOG_TYPE_ERROR);
        } 
    }    

    /**
     * Error handling and logging
     */
    protected function errorhandling() {
        try {
            // Errors visible on staging and development only
            $is_dev = (self::config('environment') == EnumEnvironment::DEVELOPMENT);
            define('WP_DEBUG', $is_dev); 
            define('WP_DEBUG_DISPLAY', $is_dev);
            @ini_set('display_errors', false);

            if( defined('WP_DEBUG') && WP_DEBUG ) {
                define('WP_DEBUG_LOG', $is_dev);
                error_reporting(E_ALL ^ E_WARNING);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}